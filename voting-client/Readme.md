# Voting Client

This module provides an implementation of the CHVote party *"Voting Client"*. In the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the voting client is described as follows:

> The voting client is a machine used by some voter to conduct the vote casting and confirmation process. Typically, this machine is either a desktop, notebook, or tablet computer with a network connection and enough computational power to perform cryptographic computations. The strict separation between voter and voting client is an important precondition for the protocol’s security concept.

The main purpose of this module is to provide an exemplary implementation of the voting client in Java, which can be used for simulating election events in a single JVM. In a real system implementation, the functionality provided by this Maven module would be implemented in languages suitable for common web browsers or mobile devices, for example in JavaScript.

## Dependencies

The following UML component diagram illustrates the dependencies  to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/VotingClient.svg)

## Messages

#### Incoming
| Protocol | Message | Sender             | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| 7.4      | MVC1    | Voter              | no     | no        | Voter index               | ![MVC1](img/MVC1.png) |
| 7.4      | MAC1    | Administrator      | yes    | no        | Voting parameters, public key share | ![MAC1](img/MAC1.png) |
| 7.4      | MVC2    | Voter              | no     | no        | Voting code and selection | ![MVC2](img/MVC2.png) |
| 7.5      | MEC1    | Election authority | yes    | no        | Public key share          | ![MEC1](img/MEC1.png) |
| 7.5      | MEC2    | Election authority | yes    | no        | Response                  | ![MEC2](img/MEC2.png) |
| 7.6      | MVC3    | Voter              | no     | no        | Confirmation code         | ![MVC3](img/MVC3.png) |
| 7.6      | MEC3    | Election authority | yes    | no        | Finalization              | ![MEC3](img/MEC3.png) |
| 7.10     | MVC5    | Voter              | no     | no        | Voter index               | ![MVC5](img/MVC5.png) |
| 7.10     | MEC4    | Election authority | yes    | no        | Inspection                | ![MEC4](img/MEC4.png) |
| --       | MVC4    | Voter              | no     | no        | Abstention                | – |

#### Outcoing
| Protocol | Message | Receiver           | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| 7.4      | MCA1    | Administrator      | no     | no        | Voter index        | ![MCA1](img/MCA1.png) |
| 7.4      | MCV1    | Voter              | no     | no        | Voting parameters  | ![MCV1](img/MCV1.png) |
| 7.5      | MCE1    | Election authority | no     | no        | Voter index        | ![MCE1](img/MCE1.png) |
| 7.5      | MCE2    | Election authority | no     | no        | Ballot             | ![MCE2](img/MCE2.png) |
| 7.6      | MCV2    | Voter              | no     | no        | Verification codes | ![MCV2](img/MCV2.png) |
| 7.6      | MCE3    | Election authority | no     | no        | Confirmation       | ![MCE3](img/MCE3.png) |
| 7.6      | MCV3    | Voter              | no     | no        | Finalization codes | ![MCV3](img/MCV3.png) |
| 7.10     | MCE4    | Election authority | no     | no        | Voter index        | ![MCE4](img/MCE4.png) |
| 7.10     | MCV4    | Voter              | no     | no        | Inspection code    | ![MCV4](img/MCV4.png) |

## Tasks

| Protocol | Task | State | Description  |
|:--------:|:----:|:-----:|:-------------|
| 7.4, 7.5 | T1   | S2,S4 | ![T1](img/T1.png) |
| 7.5      | T2   | S4    | ![T3](img/T2.png) |
| 7.5      | T3   | S5    | ![T3](img/T3.png) |
| 7.6      | T4   | S6    | ![T4](img/T4.png) |
| 7.6      | T5   | S7    | ![T5](img/T5.png) |
| 7.10     | T6   | S9    | ![T6](img/T6.png) |
