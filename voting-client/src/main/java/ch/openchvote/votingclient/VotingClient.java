/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient;

import ch.openchvote.framework.services.Logger;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;
import ch.openchvote.framework.Party;

/**
 * This class implements the 'Voting Client' party of the CHVote protocol. It is a direct sub-class of {@link Party}
 * with no particular extensions. The specific role of the voting client in the protocol is implemented in the classes
 * {@link ch.openchvote.votingclient.plain.EventData} (plain protocol) and {@link ch.openchvote.votingclient.writein.EventData}
 * (write-in protocol) and in corresponding state and task classes.
 */
public class VotingClient extends Party {

    /**
     * Constructs a new instance of this class.
     *
     * @param id   The voting client's party id
     * @param mode The logger's mode of operation
     */
    public VotingClient(String id, Logger.Mode mode) {
        super(id, PartyType.VOTING_CLIENT, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
    }

}
