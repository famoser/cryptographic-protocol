/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.tasks;

import ch.openchvote.algorithms.common.GetFinalizationCode;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.votingclient.writein.EventData;

public class T5 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var pk = eventData.pk.get();
        var VP_v = eventData.VP_v.get();
        var bold_s = eventData.bold_s.get();
        var bold_r = eventData.bold_r.get();
        var bold_beta = eventData.bold_beta.get();

        // perform task
        var bold_delta = eventData.bold_delta.get();
        var bold_n = VP_v.get_bold_n();
        var bold_k = VP_v.get_bold_k();
        var bold_e_v = VP_v.get_bold_e_v();
        var FC = GetFinalizationCode.run(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e_v, pk, params);

        // update event data
        eventData.FC.set(FC);
    }

}
