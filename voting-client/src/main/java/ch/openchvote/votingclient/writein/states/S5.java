/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCV2;
import ch.openchvote.protocol.message.writein.MEC2;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.writein.EventData;
import ch.openchvote.votingclient.writein.tasks.T3;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S5 extends State<VotingClient> {

    public S5(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEC2:
                this.handleMEC2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEC2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var VP_v = eventData.VP_v.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEC2.class, message, VP_v, eventSetup);
        var beta_j = messageContent.get_beta();

        // get sender's participant index
        var j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_beta.set(j, beta_j);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check if all MEC2 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MEC2)) {
            try {
                // perform task
                T3.run(eventData, params);

                // select event data
                var bold_rc = eventData.bold_rc.get();

                // get participant index of active party
                var i = eventSetup.getParticipantIndex(PartyType.VOTING_CLIENT, this.party.getId());

                // send MCV2 message to voter
                this.party.sendMessage(i, new MCV2(bold_rc), eventSetup);

                // update state
                context.setCurrentState(S6.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E4.class);
            }
        }
    }

}
