/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.tasks;

import ch.openchvote.algorithms.writein.GenBallot;
import ch.openchvote.algorithms.writein.GetPublicKeys;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.votingclient.writein.EventData;

public class T2 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var X_v = eventData.X_v.get();
        var bold_s = eventData.bold_s.get();
        var bold_s_prime = eventData.bold_s_prime.get();
        var VP_v = eventData.VP_v.get();

        // perform task
        var bold_pk = eventData.bold_pk.get();
        var bold_PK_prime = eventData.bold_PK_prime.get();
        var publicKeys = GetPublicKeys.run(bold_pk, bold_PK_prime, params);
        var pk = publicKeys.getFirst();
        var bold_pk_prime = publicKeys.getSecond();

        var bold_n = VP_v.get_bold_n();
        var bold_k = VP_v.get_bold_k();
        var bold_e_v = VP_v.get_bold_e_v();
        var w_v = VP_v.get_w_v();
        var bold_z = VP_v.get_bold_z();
        var bold_v = VP_v.get_bold_v();
        var z_max = VP_v.get_z_max();
        var pair = GenBallot.run(X_v, bold_s, bold_s_prime, pk, bold_pk_prime, bold_n, bold_k, bold_e_v, w_v, bold_z, bold_v, z_max, params);
        var alpha = pair.getFirst();
        var bold_r = pair.getSecond();

        // update event data
        eventData.pk.set(pk);
        eventData.bold_pk_prime.set(bold_pk_prime);
        eventData.alpha.set(alpha);
        eventData.bold_r.set(bold_r);

    }

}
