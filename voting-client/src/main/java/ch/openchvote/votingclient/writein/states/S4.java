/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCE2;
import ch.openchvote.protocol.message.writein.MEC1;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.writein.EventData;
import ch.openchvote.votingclient.writein.tasks.T1;
import ch.openchvote.votingclient.writein.tasks.T2;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S4 extends State<VotingClient> {

    public S4(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEC1:
                this.handleMEC1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEC1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var VP_v = eventData.VP_v.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEC1.class, message, VP_v, eventSetup);
        var pk_j = messageContent.get_pk();
        var bold_pk_prime_j = messageContent.get_bold_pk_prime();
        var pi_j = messageContent.get_pi();

        // get sender's participant index
        var j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_pk.set(j, pk_j);
        eventData.bold_PK_prime.set(j, bold_pk_prime_j);
        eventData.bold_pi.set(j, pi_j);

        try {
            // perform task
            T1.run(j, eventData, params);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E1.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check if all MEC1 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MEC1)) {
            try {
                // perform task
                T2.run(eventData, params);

                // select event data
                var v = eventData.v.get();
                var alpha = eventData.alpha.get();

                // send MCE2 message to all election authorities
                this.party.sendMessage(new MCE2(v, alpha), eventSetup);

                // update state
                context.setCurrentState(S5.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E3.class);
            }
        }
    }

}
