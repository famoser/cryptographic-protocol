/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCV4;
import ch.openchvote.protocol.message.writein.MEC4;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.writein.EventData;
import ch.openchvote.votingclient.writein.tasks.T6;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S9 extends State<VotingClient> {

    public S9(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEC4:
                this.handleMEC4(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEC4(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEC4.class, message, eventSetup);

        // select message content
        var I_j = messageContent.get_I();

        // get sender's participant index
        var j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_i.set(j, I_j);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check if all MEC4 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MEC4)) {
            try {
                // perform task
                T6.run(eventData, params);

                // select event data
                var IC = eventData.IC.get();

                // get participant index of active party
                var i = eventSetup.getParticipantIndex(PartyType.VOTING_CLIENT, this.party.getId());

                // send MCV4 message to voters
                this.party.sendMessage(i, new MCV4(IC), eventSetup);

                // update state
                context.setCurrentState(S10.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E7.class);
            }
        }
    }

}
