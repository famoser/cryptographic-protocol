/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MAC1;
import ch.openchvote.protocol.message.writein.MCV1;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.writein.EventData;
import ch.openchvote.votingclient.writein.tasks.T1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S2 extends State<VotingClient> {

    public S2(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MAC1:
                this.handleMAC1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMAC1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MAC1.class, message, eventSetup);
        var VP_v = messageContent.get_VP();
        var pk_0 = messageContent.get_pk();
        var bold_pk_prime_0 = messageContent.get_bold_pk_prime();
        var pi_0 = messageContent.get_pi();

        // update event data
        eventData.VP_v.set(VP_v);
        eventData.bold_pk.set(0, pk_0);
        eventData.bold_PK_prime.set(0, bold_pk_prime_0);
        eventData.bold_pi.set(0, pi_0);

        try {
            // perform task
            T1.run(0, eventData, params);

            // get participant index of active party
            var i = eventSetup.getParticipantIndex(PartyType.VOTING_CLIENT, this.party.getId());

            // send MCV1 message to voter
            this.party.sendMessage(i, new MCV1(VP_v), eventSetup);

            // update state
            context.setCurrentState(S3.class);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E1.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        }
    }

}
