/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCE3;
import ch.openchvote.protocol.message.writein.MVC3;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.Message;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.writein.EventData;
import ch.openchvote.votingclient.writein.tasks.T4;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S6 extends State<VotingClient> {

    public S6(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MVC3:
                this.handleMVC3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMVC3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MVC3.class, message, eventSetup);
        var Y_v = messageContent.get_Y();

        // update event data
        eventData.Y_v.set(Y_v);

        try {
            // perform task
            T4.run(eventData, params);

            // select event data
            var v = eventData.v.get();
            var gamma = eventData.gamma.get();

            // send MCE3 message to all election authorities
            this.party.sendMessage(new MCE3(v, gamma), eventSetup);

            // update state
            context.setCurrentState(S7.class);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E5.class);
        }
    }

}
