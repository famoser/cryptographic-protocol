/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.writein.tasks;

import ch.openchvote.algorithms.common.GetPointMatrix;
import ch.openchvote.algorithms.common.GetVerificationCodes;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.votingclient.writein.EventData;

public class T3 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var bold_s = eventData.bold_s.get();
        var bold_r = eventData.bold_r.get();

        // perform task
        var bold_beta = eventData.bold_beta.get();
        var bold_P = GetPointMatrix.run(bold_beta, bold_s, bold_r, params);
        var bold_rc = GetVerificationCodes.run(bold_s, bold_P, params);

        // update event data
        eventData.bold_P.set(bold_P);
        eventData.bold_rc.set(bold_rc);
    }

}
