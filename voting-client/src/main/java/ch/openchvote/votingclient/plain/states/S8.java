/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.plain.states;

import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MCE4;
import ch.openchvote.protocol.message.plain.MVC5;
import ch.openchvote.votingclient.VotingClient;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S8 extends State<VotingClient> {

    public S8(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MVC5:
                this.handleMVC5(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMVC5(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MVC5.class, message, eventSetup);
        int v = messageContent.get_v();

        // send MCE4 message to all election authorities
        this.party.sendMessage(new MCE4(v), eventSetup);

        // update state
        context.setCurrentState(S9.class);
    }

}
