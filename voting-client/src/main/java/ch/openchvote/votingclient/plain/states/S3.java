/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.plain.states;

import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MCE1;
import ch.openchvote.protocol.message.plain.MVC2;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.plain.EventData;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S3 extends State<VotingClient> {

    public S3(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MVC2:
                this.handleMVC2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMVC2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MVC2.class, message, eventSetup);
        var X_v = messageContent.get_X();
        var bold_s = messageContent.get_bold_s();

        // update event data
        eventData.X_v.set(X_v);
        eventData.bold_s.set(bold_s);

        // select event data
        var v = eventData.v.get();

        // send MCE1 message to all election authorities
        this.party.sendMessage(new MCE1(v), eventSetup);

        // update state
        context.setCurrentState(S4.class);
    }

}
