/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.plain.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MCV3;
import ch.openchvote.protocol.message.plain.MEC3;
import ch.openchvote.votingclient.VotingClient;
import ch.openchvote.votingclient.plain.EventData;
import ch.openchvote.votingclient.plain.tasks.T5;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S7 extends State<VotingClient> {

    public S7(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEC3:
                this.handleMEC3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEC3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var VP_v = eventData.VP_v.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEC3.class, message, VP_v, eventSetup);
        var delta_j = messageContent.get_delta();

        // get sender's participant index
        var j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_delta.set(j, delta_j);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check if all MEC3 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MEC3)) {
            try {
                // perform task
                T5.run(eventData, params);

                // select event data
                var FC = eventData.FC.get();

                // get participant index of active party
                var i = eventSetup.getParticipantIndex(PartyType.VOTING_CLIENT, this.party.getId());

                // send MCV3 message to voter
                this.party.sendMessage(i, new MCV3(FC), eventSetup);

                // update state
                context.setCurrentState(S8.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E6.class);
            }
        }
    }

}
