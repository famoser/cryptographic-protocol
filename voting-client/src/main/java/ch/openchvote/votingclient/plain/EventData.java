/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.plain;

import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.common.Response;
import ch.openchvote.model.plain.Ballot;
import ch.openchvote.model.plain.KeyPairProof;
import ch.openchvote.model.plain.VotingParameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.votingclient.plain.states.S1;

import java.math.BigInteger;

public class EventData extends ch.openchvote.framework.context.EventData {

    // voting parameters
    public final Value<Integer> s = new Value<>();
    public final Value<VotingParameters> VP_v = new Value<>();
    public final Value<Integer> v = new Value<>();

    // public keys
    public final ValueMap<QuadraticResidue, Vector<QuadraticResidue>> bold_pk = new ValueMap<>(Vector::fromSafeMap);
    public final Value<QuadraticResidue> pk = new Value<>();
    public final ValueMap<KeyPairProof, Vector<KeyPairProof>> bold_pi = new ValueMap<>(Vector::fromSafeMap);

    // ballot
    public final Value<String> X_v=new Value<>();
    public final Value<IntVector> bold_s = new Value<>();
    public final Value<Ballot> alpha = new Value<>();
    public final Value<Vector<BigInteger>> bold_r = new Value<>();

    // response
    public final ValueMap<Response, Vector<Response>> bold_beta = new ValueMap<>(Vector::fromSafeMap);
    public final Value<Matrix<Pair<BigInteger, BigInteger>>> bold_P = new Value<>();
    public final Value<Vector<String>> bold_rc = new Value<>();

    // confirmation
    public final Value<String> Y_v = new Value<>();
    public final Value<Confirmation> gamma = new Value<>();

    // finalization
    public final ValueMap<Finalization, Vector<Finalization>> bold_delta = new ValueMap<>(Vector::fromSafeMap);
    public final Value<String> FC = new Value<>();

    // inspection
    public final ValueMap<ByteArray, Vector<ByteArray>> bold_i = new ValueMap<>(Vector::fromSafeMap);
    public final Value<String> IC = new Value<>();

    @Override
    public Class<S1> getInitialState() {
        return S1.class;
    }

    @Override
    public void init(EventSetup eventSetup, String partyId) {
        int s = eventSetup.getMaxParticipantIndex(PartyType.ELECTION_AUTHORITY);
        this.s.set(s);
    }

}
