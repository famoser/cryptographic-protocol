/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.votingclient.plain.tasks;

import ch.openchvote.algorithms.plain.CheckKeyPairProof;
import ch.openchvote.votingclient.plain.EventData;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;

public class T1 {

    public static void run(int j, EventData eventData, Parameters params) {

        // select event data
        var pi_j = eventData.bold_pi.get(j);
        var pk_j = eventData.bold_pk.get(j);

        // perform task
        if (!CheckKeyPairProof.run(pi_j, pk_j, params)) {
            throw new TaskException(T1.class, TaskException.Type.INVALID_ZKP_PROOF);
        }
    }

}
