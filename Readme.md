# OpenCHVote Internet Voting System

This OpenCHVote system is a concise Java implementation of the CHVote voting protocol. The protocol specification is published on the *Cryptology ePrint Archive* since 2017. Current and earlier versions of this document are available at <https://eprint.iacr.org/2017/325>. The current Version 1.1 of this software matches with Version 3.2 of the specification document.

## Project Overview

The origin of this project is a collaboration between the State of Geneva and the Bern University of Applied Sciences, which lasted from 2016 until 2019. The aforementioned CHVote Protocol Specification document is the main output of this collaboration.

The software development for this project started in October 2019. At an early stage of the project, the code released by the State of Geneva in June 2019 at <https://chvote2.gitlab.io> served as a reference point for the software design. Otherwise, the two projects have nothing in common.

The general goal of this project is to pursue the achievements of CHVote project and to make them available to the general public under a free [license](Readme.md#License). By releasing the source code to the public, we invite the community to participate in this project.

#### Scope

In the design of the system, we introduced a strict separation between *cryptographically relevant* and *cryptographically irrelevant* components. As the scope of our software is limited to the cryptographically relevant components, OpenCHVote does not provide a complete Internet voting system that is ready to use in practice. However, the current software allows to simulate elections events by executing the CHVote protocol on a single machine. All cryptographically relevant aspects of the protocol are included in such a simulation.

Within this scope, the implementation is centered around the CHVote voting protocol. The code covers almost all relevant aspects of the specification document, including the extended protocol that allows write-in candidates and the proposed performance optimizations. In order to achieve the highest possible degree of correspondence, we looked at the specification and the code as two facets of *the same thing*. In such a mindset, deviations are only acceptable in exceptional cases. 

#### Key Features

The current OpenCHVote implementation provides the following key features:
* Complete parameterizable protocol run.
* Simultaneous execution of multiple protocol runs.
* Two protocol variants: *plain* (basic protocol without write-ins) and *writein* (extended protocol with write-ins).
* Performance optimizations: fixed-based exponentiations, product exponentiations, group membership tests, caching of recursive hash values, etc.
* Infrastructure interfaces to cryptographically irrelevant components (messaging service, persistence service, logging, etc.).
* No compulsory dependencies to third-party libraries or frameworks (dependencies to [VMGJ](https://github.com/verificatum/verificatum-vmgj) and [Spock](http://spockframework.org/) are optional).
* No dependencies to non-JavaSE technologies.

#### Missing Features

* A software for performing the universal verification (the *Verifier*).
* A JavaScript implementation of the voting client. 
* Reliable and secure infrastructure services.
* Other infrastructure software components.
* Strategy to recover from error states.
* Rigorous testing of algorithms and framework.
* Update of formal proofs.

## Modules


The following UML component diagram shows all Maven modules of  this project and illustrates their dependencies:

![Maven dependency](img/maven-dependency.svg)


## Software and Installation

OpenCHVote is a multi-module [Maven](http://www.maven.org) project for Java SE. Currently, no other platforms or languages (e.g. JavaScript) are supported by any of the included components.

#### Version History

| Branch Name  | Tag Name     | Comment              | Java  | Release Date | [CHVote Protocol Specification](https://eprint.iacr.org/2017/325) | 
|:-----------:|:-----------:|:---------------------|:----:|:------------------|:----:|
|     1.1     |    r1.1     | Release 1.1          | 11   | December 22, 2020 | 3.2	 |
|     1.0     |    r1.0     | Release 1.0          | 11   | October 7, 2020   | 3.1  |

See [release notes](ReleaseNotes.md)

#### Dependencies

Except for the natural dependency to Java SE, there are only two optional dependencies to other non-proprietary software components:

* For improving the performance of modular exponentiations, the [Verificatum Multiplicative Groups Library for Java](https://github.com/verificatum/verificatum-vmgj) (VMGJ) can be used. For convenience purposes, a wrapper is part of this distribution, located in the `./project-maven-repo` repository. At runtime, the wrapper tries to resolve this dependency. No harm occurs if the wrapper cannot resolve the dependency (a warning is displayed). Douglas Wikstrom's [free license](LICENSE_VMGJ.md) applies. VMGJ implies two transitive dependencies to [GMP](https://gmplib.org) and [GMPMEE](https://github.com/verificatum/verificatum-gmpmee). Corresponding licenses apply.

* The [Spock](http://spockframework.org/) testing framework is used for implementing data-driven unit tests. The [Apache License, Version 2.0,](LICENSE_APACHE.md) applies.


#### Compilation, Packaging, and Installation

To build every artifact in each module of this project, simply type the following in the project's root directory:

````shell script
$ mvn clean install
-- a lot of output
````

This will generate a JAR file in each of the `target` sub-directories of each module and install them in the local Maven repository.

> **Note:** At the time being, you can safely ignore warnings of this type:
> ```
> ...
> [ERROR] WARNING: An illegal reflective access operation has occurred
> [INFO] Running ch.openchvote.algorithms.general.RecHashSpec
> ```

#### Executing the Election Runner

Election events can be simulated by using the class <code>ElectionRunner</code> from the <code>runner</code> module. Further instructions for launching a test run can be taken from [there](runner/Readme.md).


#### Generating Local Project Documentation

You can use Maven to build the local project documentation. The files will reside in your file system under `target/staging`. To create this documentation, type the following:

````shell script
$ mvn site site:stage
-- a lot of output
...
````

> **Note:** You can safely ignore warnings of this type:
> ```
> [WARNING] Could not transfer metadata org.codehaus.groovy:...
> ```


## Publications

* R. Haenni, E. Dubuis, R. E. Koenig, P. Locher, [*CHVote: Sixteen Best Practices and Lessons Learned*](https://e-voting.bfh.ch/app/download/8038993661/HDKL20.pdf), E-Vote-ID 2020}, 5th International Joint Conference on Electronic Voting, LNCS 12455, 2020

* R. Haenni, R. E. Koenig and P. Locher and E. Dubuis, [*CHVote Protocol Specification - Version 3.2*](https://eprint.iacr.org/2017/325), IACR Cryptology ePrint Archive, 2017/325, 2020

* D. Bernhard, V. Cortier, P. Gaudry, M. Turuani, B. Warinschi, [*Verifiability Analysis of CHVote*](https://eprint.iacr.org/2018/1052), IACR Cryptology ePrint Archive, 2018/1052, 2018

* R. Haenni, E. Dubuis, R. E. Koenig, [*Cast-as-Intended Verification in Electronic Elections Based on Oblivious Transfer*](https://e-voting.bfh.ch/app/download/7233132561/HKD16.pdf), E-Vote-ID 2016, 1st International Joint Conference on Electronic Voting, LNCS 10141, 2016

* R. Haenni, P. Locher, N. Gailly, [*Improving the Performance of Cryptographic Voting Protocols*](https://e-voting.bfh.ch/app/download/7833228661/HLG19.pdf), Voting'19, FC 2019 International Workshops, LNCS 11599, 272–288, 2019

* R. Haenni, P. Locher, [*Performance of Shuffling: Taking it to the Limits*](https://e-voting.bfh.ch/app/download/8038993261/HL20.pdf), Voting'20, FC 2020 International Workshops, LNCS 12063, 369–385, 2020

## License

For this software, the [GNU AFFERO GENERAL PUBLIC LICENSE](LICENSE.md) Version 3, 19 November 2007, applies.

## Feedback 

We'd be happy to receive feedback of any kind from the community. Please use the GitLab issue tracking service at [https://gitlab.com/groups/openchvote](https://gitlab.com/groups/openchvote/-/issues) or contact the contributors list [here](Credits.md).

## Credits

Click [here](./Credits.md).
