# Credits

## Contributors

In alphabetical order:

| Name                 | E-Mail       | Affiliation |
| ---------------------| -------------|---|
| Timo Lennart Bürk | [timo.buerk@bfh.ch](MAILTO:timo.buerk@bfh.ch) | Bern University of Applied Sciences |
| Eric Dubuis | [eric.dubuis@bfh.ch](MAILTO:eric.dubuis@bfh.ch) | Bern University of Applied Sciences |
| Stephan Fischli | [stephan.fischli@bfh.ch](MAILTO:stephan.fischli@bfh.ch) | Bern University of Applied Sciences |
| Rolf Haenni (lead) | [rolf.haenni@bfh.ch](MAILTO:rolf.haenni@bfh.ch) | Bern University of Applied Sciences |
| Reto Koenig | [reto.koenig@bfh.ch](MAILTO:reto.koenig@bfh.ch) | Bern University of Applied Sciences |
| Philipp Locher | [philipp.locher@bfh.ch](MAILTO:philipp.locher@bfh.ch) | Bern University of Applied Sciences |

## Special Thanks

Special thanks goes to Thomas Hofer (Objectif Sécurité, Switzerland), who provided valuable recommendations about the software architecture at an early stage of the project.

## Support

This research has been supported by the Swiss Federal Chancellery.

