# Algorithms

This module contains implementations of all pseudo-code algorithms contained in the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325). Both protocol versions (*plain* and *writein*) are fully covered. A class with a single static <code>run</code> method exists for every pseudo-code algorithm. The input parameters of all top-level algorithms are tested systematically according to the specification. In sub-algorithms, these tests are omitted.

The primary design goal in this module was to reach the highest possible correspondence between pseudo-code algorithms and Java code. Here is an example (Alg. 8.32 GetVerificationCodes) that demonstrated the level of correspondence that has been reached:

| Pseudo-Code Algorithm |
| ----------------------|
| ![Pseudo-Code](img/pseudocode.png) |

| Java Code |
| ----------------------|
| ![Java Code](img/javacode.png) |  

The Java method shown in the above example contains an argument of type <code>Parameter</code>. This is an important utility class of this module, which provides the security and systems parameters as defined in Section 6.3 and Chapter 10 of the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325).

## Dependencies

Within the OpenCHVote project, this module only depends on the module <code>utilities</code>. However, there is an optional dependency to the [Verificatum Multiplicative Groups Library for Java (VMGJ)](https://github.com/verificatum/verificatum-vmgj), for which [this license](LICENSE_VMGLJ.md) applies. At runtime, a wrapper tries to resolve the dependency to VMGJ. No harm occurs if the dependency can not be resolved. A warning is displayed in that case. Without VMGJ, some of the computations are less efficient. 

![Maven dependency](img/maven-dependency.svg)


## Installation of Maven Artefact

This module can be built as follows from its root:

````shell script
$ mvn clean install
-- a lot of output
````

