/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general

import ch.openchvote.algorithms.AlgorithmException
import ch.openchvote.util.Alphabet
import ch.openchvote.util.ByteArray
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import java.util.stream.Stream

class ToIntegerSpec extends Specification {

    @Shared
    def default_S
    @Shared
    def default_A

    def setupSpec() {
        default_S = "123"
        default_A = Alphabet.BASE32
    }

    @Unroll
    def "run(ByteArray B) test preconditions case: #caseDesc"() {
        when:
        ToInteger.run(B)
        then:
        thrown(AlgorithmException)
        where:
        caseDesc    | B
        "B is null" | null
    }

    def "tests run(ByteArray B) by using ByteArray.FromArray(r.toByteArray())"(BigInteger r) {
        given:
        ByteArray x = new ByteArray.FromSafeArray(r.toByteArray())
        expect:
        ToInteger.run(x) == r
        where:
        r << Stream.iterate(BigInteger.ZERO, { i -> i.add(BigInteger.ONE) }).limit(1000)

    }

    @Unroll
    def "run(String S, Alphabet A) test preconditions case: #caseDesc"() {

        when:
        ToInteger.run(S, A)
        then:
        thrown(AlgorithmException)
        where:

        caseDesc                | S         | A
        "S is null"             | null      | default_A
        "A is null"             | default_S | null
        "S is not element of A" | "123"     | Alphabet.BASE2
        "S is not element of A" | "9AB"     | Alphabet.BASE8
        "S is not element of A" | "ABC"     | Alphabet.BASE10
        "S is not element of A" | "GHI"     | Alphabet.BASE16
        "S is not element of A" | "789"     | Alphabet.BASE32
        "S is not element of A" | "XYZ"     | Alphabet.BASE32HEX
        "S is not element of A" | "!?"      | Alphabet.BASE64
        "S is not element of A" | "123"     | Alphabet.LOWER_CASE
        "S is not element of A" | "123"     | Alphabet.UPPER_CASE
        "S is not element of A" | "123"     | Alphabet.LETTERS
        "S is not element of A" | "!?"      | Alphabet.ALPHANUMERIC
    }

    @Unroll
    def "run(String S, Alphabet A) converts #S to #expected with alphabet #A.getCharacters()"() {
        expect:
        ToInteger.run(S, A) == expected
        where:
        S       | A                     | expected
        "11010" | Alphabet.BASE2        | BigInteger.valueOf(26)
        "32"    | Alphabet.BASE8        | BigInteger.valueOf(26)
        "26"    | Alphabet.BASE10       | BigInteger.valueOf(26)
        "1A"    | Alphabet.BASE16       | BigInteger.valueOf(26)
        "2"     | Alphabet.BASE32       | BigInteger.valueOf(26)
        "Q"     | Alphabet.BASE32HEX    | BigInteger.valueOf(26)
        "a"     | Alphabet.BASE64       | BigInteger.valueOf(26)
        "ba"    | Alphabet.LOWER_CASE   | BigInteger.valueOf(26)
        "BA"    | Alphabet.UPPER_CASE   | BigInteger.valueOf(26)
        "a"     | Alphabet.LETTERS      | BigInteger.valueOf(26)
        "a"     | Alphabet.ALPHANUMERIC | BigInteger.valueOf(26)

    }

}
