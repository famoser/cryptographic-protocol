/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general

import ch.openchvote.algorithms.AlgorithmException
import ch.openchvote.algorithms.Precondition
import spock.lang.Specification

class PreconditionSpec extends Specification {

    def "check(boolean b): should throw an AlgorithmException if b is false"() {
        given:
        def b = false
        when:
        Precondition.check(b)
        then:
        AlgorithmException ex = thrown()
        ex.getType() == AlgorithmException.Type.PRECONDITION_FAILED
    }

    def "check(boolean b): should not throw an AlgorithmException if b is true"() {
        given:
        def b = true
        when:
        Precondition.check(b)
        then:
        notThrown(AlgorithmException)
    }

    def "checkNotNull(Object... objects) should throw an IllegalArgumentException if atleast one of the objects is null"() {
        when:
        Precondition.checkNotNull(objects.toArray())
        then:
        thrown(AlgorithmException)
        where:
        objects                           | _
        [1, 2, null, 4]                   | _
        ["hello", "world", "lorem", null] | _
        [null, 1, "two"]                  | _
    }

    def "checkNotNull(Object... objects) should not throw an IllegalArgumentException if none of the objects are null"() {
        when:
        Precondition.checkNotNull(objects.toArray())
        then:
        notThrown(AlgorithmException)
        where:
        objects                             | _
        [1, 2, 3, 4]                        | _
        ["hello", "world", "lorem", "null"] | _
        [0, 1, "two"]                       | _
    }

}
