/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general


import ch.openchvote.util.crypto.Entropy
import spock.lang.Specification


class RandomBytesSpec extends Specification {


    def "The bits in the output of the RandomBytes method should be uniform distributed "() {

        def l = 5000
        def randByte = RandomBytes.run(l)
        def samples = new ArrayList<Integer>()
        for (int i = 0; i < randByte.size(); i++) {
            samples.add(randByte.getByte(i).toInteger())
        }
        expect:
        Entropy.stuckTest(samples) == true
        Entropy.repetitionCountTest(samples) == true
        Entropy.adaptiveProportionTest(samples) == true

    }


}
