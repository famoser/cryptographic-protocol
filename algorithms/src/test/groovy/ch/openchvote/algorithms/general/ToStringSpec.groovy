/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general

import ch.openchvote.algorithms.AlgorithmException
import ch.openchvote.util.Alphabet
import ch.openchvote.util.ByteArray
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

class ToStringSpec extends Specification {

    @Shared
    def default_x
    @Shared
    def default_n
    @Shared
    def default_A
    @Shared
    def default_B

    def setupSpec() {
        default_x = BigInteger.valueOf(1)
        default_n = 1
        default_A = Alphabet.BASE16
        default_B = new ByteArray.FromSafeArray(([0x00] as byte[]))
    }

    @Unroll
    def "ToString.run(x, n, A) test preconditions case: #caseDesc"() {
        when:
        ToString.run(x, n, A)
        then:
        thrown(AlgorithmException)
        where:
        caseDesc                    | x                       | n         | A
        "x is null"                 | null                    | default_n | default_A
        "x is negativ "             | BigInteger.valueOf(-1)  | default_n | default_A
        "n is negativ"              | default_x               | -1        | default_A
        "x does not fit in n bytes" | BigInteger.valueOf(256) | 1         | default_A
        "A is null"                 | default_x               | default_n | null
    }

    //Tests adopted from https://gitlab.com/chvote2/protocol-core/chvote-protocol/blob/master/protocol-algorithms/src/test/groovy/ch/ge/ve/protocol/core/support/ConversionTest.groovy

    def "run(BigInteger x, int n, Alphabet A)"() {
        expect:
        ToString.run(x, n, A) == expected

        where:
        x                          | n | A                   || expected
        BigInteger.valueOf(0)      | 4 | Alphabet.BASE2      || "0000"
        BigInteger.valueOf(0)      | 0 | Alphabet.BASE2      || ""
        BigInteger.valueOf(1)      | 4 | Alphabet.BASE2      || "0001"
        BigInteger.valueOf(1)      | 1 | Alphabet.BASE2      || "1"
        BigInteger.valueOf(2)      | 4 | Alphabet.BASE2      || "0010"
        BigInteger.valueOf(2)      | 2 | Alphabet.BASE2      || "10"
        BigInteger.valueOf(4)      | 4 | Alphabet.BASE2      || "0100"
        BigInteger.valueOf(4)      | 3 | Alphabet.BASE2      || "100"
        BigInteger.valueOf(8)      | 4 | Alphabet.BASE2      || "1000"
        BigInteger.valueOf(15)     | 4 | Alphabet.BASE2      || "1111"
        BigInteger.valueOf(731)    | 4 | Alphabet.UPPER_CASE || "ABCD"
        BigInteger.valueOf(25)     | 1 | Alphabet.UPPER_CASE || "Z"
        BigInteger.valueOf(650)    | 2 | Alphabet.UPPER_CASE || "ZA"
        BigInteger.valueOf(675)    | 2 | Alphabet.UPPER_CASE || "ZZ"
        BigInteger.valueOf(16900)  | 3 | Alphabet.UPPER_CASE || "ZAA"
        BigInteger.valueOf(17575)  | 3 | Alphabet.UPPER_CASE || "ZZZ"
        BigInteger.valueOf(439400) | 4 | Alphabet.UPPER_CASE || "ZAAA"
        BigInteger.valueOf(456975) | 4 | Alphabet.UPPER_CASE || "ZZZZ"
    }

    @Unroll
    def "ToString.run(ByteArray B, Alphabet A) test preconditions case: #caseDesc"() {
        when:
        ToString.run(B, A)
        then:
        thrown(AlgorithmException)
        where:
        caseDesc    | B         | A
        "B is null" | null      | default_A
        "A is null" | default_B | null
    }

    @Unroll
    def "run(ByteArray B, Alphabet A)"() {
        given:
        def B = new ByteArray.FromSafeArray((bytes as byte[]))
        expect:
        ToString.run(B, A) == expected

        where:
        bytes              | A                   || expected
        [0x00]             | Alphabet.BASE2      || "00000000"
        [0x01]             | Alphabet.BASE2      || "00000001"
        [0x02]             | Alphabet.BASE2      || "00000010"
        [0x04]             | Alphabet.BASE2      || "00000100"
        [0x08]             | Alphabet.BASE2      || "00001000"
        [0x0F]             | Alphabet.BASE2      || "00001111"
        [0x1A]             | Alphabet.BASE2      || "00011010"
        [0x02, 0xDB]       | Alphabet.BASE16     || "02DB"
        [0x19]             | Alphabet.BASE16     || "19"
        [0x02, 0x8A]       | Alphabet.BASE16     || "028A"
        [0x02, 0xA3]       | Alphabet.BASE16     || "02A3"
        [0x42, 0x04]       | Alphabet.BASE16     || "4204"
        [0x44, 0xA7]       | Alphabet.BASE16     || "44A7"
        [0x06, 0xB4, 0x68] | Alphabet.BASE16     || "06B468"
        [0x06, 0xF9, 0x0F] | Alphabet.BASE16     || "06F90F"
        [0x02, 0xDB]       | Alphabet.UPPER_CASE || "ABCD"
        [0x19]             | Alphabet.UPPER_CASE || "AZ"
        [0x02, 0x8A]       | Alphabet.UPPER_CASE || "AAZA"
        [0x02, 0xA3]       | Alphabet.UPPER_CASE || "AAZZ"
        [0x42, 0x04]       | Alphabet.UPPER_CASE || "AZAA"
        [0x44, 0xA7]       | Alphabet.UPPER_CASE || "AZZZ"
        [0x06, 0xB4, 0x68] | Alphabet.UPPER_CASE || "AAZAAA"
        [0x06, 0xF9, 0x0F] | Alphabet.UPPER_CASE || "AAZZZZ"
    }

    @Unroll
    def "ToString.run(ByteArray B) test preconditions case: #caseDesc"() {
        when:
        ToString.run(B)
        then:
        thrown(AlgorithmException)
        where:
        caseDesc        | B
        "B is null"     | null
        "B not in UTF8" | new ByteArray.FromSafeArray(([0xFF] as byte[]))
    }

    def "run(ByteArray B)"() {
        given:
        def B = new ByteArray.FromSafeArray((bytes as byte[]))
        expect:
        ToString.run(B) == expected

        where:
        bytes                                                              || expected
        [0x61, 0x62, 0x63]                                                 || "abc"
        [0x68, 0x65, 0x6C, 0x6C, 0x6F]                                     || "hello"
        [0x77, 0x6F, 0x72, 0x6C, 0x64, 0x21]                               || "world!"
        [0x6C, 0x6F, 0x72, 0x65, 0x6D, 0x20, 0x69, 0x70, 0x73, 0x75, 0x6D] || "lorem ipsum"

    }
}
