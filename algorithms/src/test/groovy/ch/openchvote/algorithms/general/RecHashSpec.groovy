/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general

import ch.openchvote.util.Alphabet
import ch.openchvote.util.Matrix
import ch.openchvote.util.Vector
import ch.openchvote.util.crypto.SHA3
import ch.openchvote.util.math.QuadraticResidue
import ch.openchvote.util.tuples.*
import spock.lang.Specification
import spock.lang.Unroll

class RecHashSpec extends Specification {


    @Unroll
    def "run(int L, Object... values)"() {
        given:
        def expected = ToByteArray.run(new BigInteger(expectedHexString, 16))

        expect:
        RecHash.run(new SHA3(), objects) == expected

        where:
        objects                                             | expectedHexString
        null                                                | "A7FFC6F8BF1ED76651C14756A061D662F580FF4DE43B49FA82D80A4B80F8434A"
        "test"                                              | "36F028580BB02CC8272A9A020F4200E346E276AE664E45EE80745574E2F5AB80"
        42                                                  | "B3291957374E0A836351D5129CF45A5E0F73A92EDFF7B2C85EF159062301829E"
        BigInteger.valueOf(42739L)                          | "DA1E3F4CFFB75C7F07B978471EB122ABFE7B0EB0A592FD99084A8A4EC24CEEAE"
        new QuadraticResidue(121, 167)                      | "9D0F3DB671F9FB22104B984763616732D383154A7A0DCDBB9EC17AB647B64961"
        ["test", BigInteger.valueOf(42L)] as Object[]       | "962CFADC42541AAE5022FF67A027AD96F7A2778F594EC0679D545CFA27648EB0"
        [null, "test", BigInteger.valueOf(42L)] as Object[] | "75FB9C4AFFD515FC55DE24EFB9AFF9B6D71118E3AE234655AB60D37EE140F1CF"

    }

    def "run(int L, Object... values) for Matrix"() {
        given:
        def height = inputMatrix.size()
        def width = inputMatrix.get(0).size()
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))

        and: "build Matrix"

        def builder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                builder.setValue(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def matrix = builder.build()

        expect:
        RecHash.run(new SHA3(), matrix) == expected

        where:
        inputMatrix               | L  | eString
        [[1, 2, 3],
         [2, 3, 4],
         [3, 4, 5]]               | 32 | "38E2B980E72F7B3F4BD30B506460C075D87C02443E6EE55F5B7E1C9EDD52DE2C"
        [[null, "2", "3", "4"],
         ["5", "6", "7", "8"],
         ["9", "10", "11", "12"]] | 32 | "26A476E09CF0ABC8F186A3C82EB1C970624F32DA17EFA5766FAA040D0997390F"

    }

    @Unroll
    def "run(int L, Object... values) for Vector"() {
        def vector = Vector.fromSafeArray(input.toArray())
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))

        expect:
        RecHash.run(new SHA3(), vector) == expected

        where:

        input                                                                                        | L  | eString
        [1, 2, 3, 4, 5]                                                                              | 32 | "3D8ABC0C37271C87C6E2A94C92991C90D36E72EB1C3EF8F9EDEDEE5715A9E6ED"
        [BigInteger.valueOf(1), BigInteger.valueOf(3), BigInteger.valueOf(5), BigInteger.valueOf(7)] | 32 | "18BCC279845DB2595F8D6A6AE8401A65D5A571757234D3C539CA2E92D2F724EE"
        ["test", null, "null", "hello", "lorem"]                                                     | 32 | "BEEFC97C15C593C9B26767BFEE8C3B10F7F85CD261444E645ACBBFB07E629568"
        [1, 1, 1, 1]                                                                                 | 32 | "460D999D15098DFA4175C3B5A9B276659B3CCA609A42DFA6BBD20592E042CBD8"
        [null, null, null]                                                                           | 32 | "AC92201C841C47E7E56F0D9CEBBDBE7AA5F21D471112CCBD2D7741CB0C32CB7D"
    }

    def "run(int L, Object... values) for Pair"() {
        given:
        def pair = new Pair(elems1.get(0), elems1.get(1))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), pair) == expected

        where:
        elems1              | l  | eString
        ["first", "second"] | 32 | "2E7AC497993B95DDB27BF3E6AEC9AFA6B8ECD228B77519F850631227A5903B27"
        [1, 2]              | 32 | "1630A150C37B9107B27ECBA1D374F398667BD59E9F34F0822792DCFA8BDC5A7E"

    }

    def "run(int L, Object... values) for Triple"() {
        given:
        def triple = new Triple(elems1.get(0), elems1.get(1), elems1.get(2))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), triple) == expected

        where:
        elems1                       | l  | eString
        ["first", "second", "third"] | 32 | "666AB9814B2CD95B7319BAAFC074BB28A0C2CC6C2B1DD141AF32F540645ABAD7"
        [1, 2, 3]                    | 32 | "76DC0F7FEA9928D985CFAB79A1D86574C69D9EC0C4EF34DEDC5D6774897A0CE2"

    }

    def "run(int L, Object... values) for Quadruple"() {
        given:
        def quadruple = new Quadruple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), quadruple) == expected

        where:
        elems1                                 | l  | eString
        ["first", "second", "third", "fourth"] | 32 | "876A2DEAE2B71C8F91B3595ED1119BF88285122A7D0DF3375CDE9970F1EB6B54"
        [1, 2, 3, 4]                           | 32 | "B7F73CDC37BB6F158515CAC4B41957EDEB2B29F786433A5A0E5D98E250104B7F"

    }

    def "run(int L, Object... values) for Quintuple"() {
        given:
        def quintuple = new Quintuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), quintuple) == expected

        where:
        elems1                                          | l  | eString
        ["first", "second", "third", "fourth", "fifth"] | 32 | "8D0B0DC38D057BEEA53F53398655317B40A3E3A1715EB46B07A1CF987CA2C530"
        [1, 2, 3, 4, 5]                                 | 32 | "3D8ABC0C37271C87C6E2A94C92991C90D36E72EB1C3EF8F9EDEDEE5715A9E6ED"

    }

    def "run(int L, Object... values) for Sextuple"() {
        given:
        def sextuple = new Sextuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), sextuple) == expected

        where:
        elems1                                                   | l  | eString
        ["first", "second", "third", "fourth", "fifth", "sixth"] | 32 | "BBE532F74E7C91A545CF1C9E9C9F5E6213E203511C7422D0F284EFA2616F93D5"
        [1, 2, 3, 4, 5, 6]                                       | 32 | "074D448D38636AACA6D6B79F2F85CB5A1C0BFB92B40890042C666F7015B2D5B2"

    }

    def "run(int L, Object... values) for Septuple"() {
        given:
        def septuple = new Septuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), septuple) == expected

        where:
        elems1                                                              | l  | eString
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh"] | 32 | "8A13C663D6FCDA464C72689B85B01D108305AF68A93CA795B3CD90CF7AD1D8D7"
        [1, 2, 3, 4, 5, 6, 7]                                               | 32 | "EE4BAAE83C869D0DF33223F1858090DBD3D68C491837B03FE08BBC208B9CEA98"

    }

    def "run(int L, Object... values) for Octuple"() {
        given:
        def octuple = new Octuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), octuple) == expected

        where:
        elems1                                                                        | l  | eString
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth"] | 32 | "B58545710C90DB360D6ACC6AF618509DF41EE1F0654302C73B11101137DE89F8"
        [1, 2, 3, 4, 5, 6, 7, 8]                                                      | 32 | "CBD55346A794B8B8378F1FE0FD4FA4353B87CE56909B790C99D73E9C4C204950"

    }

    def "run(int L, Object... values) for Nonuple"() {
        given:
        def nonuple = new Nonuple(elems1.get(0), elems1.get(1), elems1.get(2), elems1.get(3), elems1.get(4), elems1.get(5), elems1.get(6), elems1.get(7), elems1.get(8))
        def expected = ToByteArray.run(ToInteger.run(eString, Alphabet.BASE16))
        expect:
        RecHash.run(new SHA3(), nonuple) == expected

        where:
        elems1                                                                                 | l  | eString
        ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eighth", "ninth"] | 32 | "BF4F0C97B607CC79FB5B353067249AE69A77F43AB138103D975EE26521DAE7F7"
        [1, 2, 3, 4, 5, 6, 7, 8, 9]                                                            | 32 | "A791D00B769D1BC847F4AAD21EB5B567F7C22CD1804C8E5B20DECE3AEFA395B2"

    }

}
