/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.CredentialProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.16
 */
public class CheckCredentialProof {

    public static boolean run(CredentialProof pi, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(pi, bold_x_hat, bold_y_hat, bold_z_hat, params);

        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var N_E = bold_s.getLength();

        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Vector(Set.Triple(params.ZZ_q_hat, params.ZZ_q_hat, params.ZZ_q_hat), N_E)).contains(pi));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_x_hat));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_y_hat));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_z_hat));

        var builder_bold_t = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);

        // ALGORITHM
        Parallel.forLoop(1, N_E, i -> {
            var s_i = bold_s.getValue(i);
            var s_i_1 = s_i.getFirst();
            var s_i_2 = s_i.getSecond();
            var s_i_3 = s_i.getThird();
            var x_hat_i = bold_x_hat.getValue(i);
            var y_hat_i = bold_y_hat.getValue(i);
            var z_hat_i = bold_z_hat.getValue(i);
            var t_i_1 = Mod.multiply(Mod.pow(x_hat_i, c, params.p_hat), Mod.pow(params.g_hat, s_i_1, params.p_hat), params.p_hat);
            var t_i_2 = Mod.multiply(Mod.pow(y_hat_i, c, params.p_hat), Mod.pow(params.g_hat, s_i_2, params.p_hat), params.p_hat);
            var t_i_3 = Mod.multiply(Mod.pow(z_hat_i, c, params.p_hat), Mod.pow(params.g_hat, s_i_3, params.p_hat), params.p_hat);
            var t_i = new Triple<>(t_i_1, t_i_2, t_i_3);
            builder_bold_t.setValue(i, t_i);
        });
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(bold_x_hat, bold_y_hat, bold_z_hat);
        var c_prime = GetChallenge.run(y, bold_t, params);
        return c.equals(c_prime);
    }
}
