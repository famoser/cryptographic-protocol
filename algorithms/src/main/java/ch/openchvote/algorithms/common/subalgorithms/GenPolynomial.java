/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;

import java.math.BigInteger;

/**
 * ALGORITHM 8.12
 */
public class GenPolynomial {

    public static final java.util.Set<BigInteger> ZERO = java.util.Set.of(BigInteger.ZERO);

    public static Vector<BigInteger> run(int d, Parameters params) {

        // PREPARATION
        var builder_bold_a = new Vector.Builder<BigInteger>(0, (d == -1 ? 0 : d));

        // ALGORITHM
        if (d == -1) {
            builder_bold_a.setValue(0, BigInteger.ZERO);
        } else {
            for (int i = 0; i <= d - 1; i++) {
                var a_i = GenRandomInteger.run(params.q_hat);
                builder_bold_a.setValue(i, a_i);
            }
            var a_d = GenRandomInteger.run(params.q_hat, ZERO);
            builder_bold_a.setValue(d, a_d);
        }
        var bold_a = builder_bold_a.build();
        return bold_a;
    }

}
