/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.ConfirmationProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.36
 */
public class CheckConfirmationProof {

    public static boolean run(ConfirmationProof pi, BigInteger y_hat, BigInteger z_hat, Parameters params) {

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();

        // ALGORITHM
        var y = new Pair<>(y_hat, z_hat);
        var t1 = Mod.multiply(Mod.pow(y_hat, c, params.p_hat), Mod.pow(params.g_hat, s_1, params.p_hat), params.p_hat);
        var t2 = Mod.multiply(Mod.pow(z_hat, c, params.p_hat), Mod.pow(params.g_hat, s_2, params.p_hat), params.p_hat);
        var t = new Pair<>(t1, t2);
        var c_prime = GetChallenge.run(y, t, params);
        return c.equals(c_prime);
    }
}
