/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.algorithms.general.SetWatermark;
import ch.openchvote.algorithms.general.ToString;
import ch.openchvote.model.common.VotingCard;
import ch.openchvote.model.common.VotingCardData;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.tuples.Quadruple;

/**
 * ALGORITHM 8.19
 */
public class GetVotingCard {

    public static VotingCard run(int v, Vector<VotingCardData> bold_d, Parameters params) {

        // PREPARATION
        int s = bold_d.getLength();
        var n = bold_d.getValue(1).getFourth().getLength();

        var builder_bold_f = new Vector.Builder<ByteArray>(s);
        var builder_bold_rc = new Vector.Builder<String>(n);

        // ALGORITHM
        var X = ToString.run(Math.sum(bold_d.map(Quadruple::getFirst)), params.ell_X, params.A_X);
        var Y = ToString.run(Math.sum(bold_d.map(Quadruple::getSecond)), params.ell_Y, params.A_Y);
        for (int i = 1; i <= n; i++) {
            var builder_bold_r_i = new Vector.Builder<ByteArray>(s);
            for (int j = 1; j <= s; j++) {
                var R_ij = RecHash.run(params.hashAlgorithm, bold_d.getValue(j).getFourth().getValue(i)).truncate(params.L_R);
                builder_bold_r_i.setValue(j, R_ij);
            }
            var bold_r_i = builder_bold_r_i.build();
            var R_i = ByteArray.xor(bold_r_i);
            R_i = SetWatermark.run(R_i, i - 1, params.n_max);
            var RC_i = ToString.run(R_i, params.A_R);
            builder_bold_rc.setValue(i, RC_i);
        }
        var bold_rc = builder_bold_rc.build();
        for (int j = 1; j <= s; j++) {
            var F_j = RecHash.run(params.hashAlgorithm, bold_d.getValue(j).getFourth()).truncate(params.L_FA);
            builder_bold_f.setValue(j, F_j);
        }
        var bold_f = builder_bold_f.build();
        var FC = ToString.run(ByteArray.xor(bold_f), params.A_FA);
        var bold_a = bold_d.map(Quadruple::getThird);
        var AC = ToString.run(ByteArray.xor(bold_a), params.A_FA);
        return new VotingCard(v, X, Y, bold_rc, FC, AC);
    }

}
