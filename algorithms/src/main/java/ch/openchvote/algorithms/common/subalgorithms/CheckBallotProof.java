/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.BallotProof;
import ch.openchvote.model.common.Query;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.26
 */
public class CheckBallotProof {

    public static boolean run(BallotProof pi, BigInteger x_hat, Vector<Query> bold_a, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();
        var s_3 = s.getThird();

        // ALGORITHM
        var a_1 = Mod.prod(bold_a.map(Query::get_a_1));
        var a_2 = Mod.prod(bold_a.map(Query::get_a_2));
        var t_1 = Mod.multiply(Mod.pow(x_hat, c, params.p_hat), Mod.pow(params.g_hat, s_1, params.p_hat), params.p_hat);
        var t_2 = Mod.multiply(Mod.pow(a_1, c), s_2, Mod.pow(pk, s_3));
        var t_3 = Mod.multiply(Mod.pow(a_2, c), Mod.pow(params.g, s_3));
        var t = new Triple<>(t_1, t_2, t_3);
        var y = new Triple<>(x_hat, bold_a, pk);
        var c_prime = GetChallenge.run(y, t, params);
        return c.equals(c_prime);
    }

}
