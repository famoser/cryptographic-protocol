/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomElement;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.BallotProof;
import ch.openchvote.model.common.Query;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.24
 */
public class GenBallotProof {

    public static BallotProof run(BigInteger x, QuadraticResidue m, BigInteger r, BigInteger x_hat, Vector<Query> bold_a, QuadraticResidue pk, Parameters params) {

        // ALGORITHM
        var omega_1 = GenRandomInteger.run(params.q_hat);
        var omega_2 = GenRandomElement.run(params);
        var omega_3 = GenRandomInteger.run(params.q);
        var t_1 = Mod.pow(params.g_hat, omega_1, params.p_hat);
        var t_2 = Mod.multiply(omega_2, Mod.pow(pk, omega_3));
        var t_3 = Mod.pow(params.g, omega_3);
        var t = new Triple<>(t_1, t_2, t_3);
        var y = new Triple<>(x_hat, bold_a, pk);
        var c = GetChallenge.run(y, t, params);
        var s_1 = Mod.subtract(omega_1, Mod.multiply(c, x, params.q_hat), params.q_hat);
        var s_2 = Mod.multiply(omega_2, Mod.pow(m, Mod.minus(c, params.q)));
        var s_3 = Mod.subtract(omega_3, Mod.multiply(c, r, params.q), params.q);
        var s = new Triple<>(s_1, s_2, s_3);
        var pi = new BallotProof(c, s);
        return pi;
    }

}
