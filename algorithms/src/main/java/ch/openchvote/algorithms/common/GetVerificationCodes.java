/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.algorithms.general.SetWatermark;
import ch.openchvote.algorithms.general.ToString;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.30
 */
public class GetVerificationCodes {

    public static Vector<String> run(IntVector bold_s, Matrix<Pair<BigInteger, BigInteger>> bold_P, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_s, bold_P, params);

        int k = bold_P.getHeight();
        int s = bold_P.getWidth();

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.IntVector(IntSet.NN_plus(params.n_max), k).contains(bold_s));
        Precondition.check(Set.Matrix(Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat), k, s).contains(bold_P));

        Precondition.check(bold_s.isSorted());

        var builder_bold_rc = new Vector.Builder<String>(k);

        // ALGORITHM
        for (int i = 1; i <= k; i++) {
            var builder_bold_r_i = new Vector.Builder<ByteArray>(s);
            for (int j = 1; j <= s; j++) {
                var p_ij = bold_P.getValue(i, j);
                var R_ij = RecHash.run(params.hashAlgorithm, p_ij).truncate(params.L_R);
                builder_bold_r_i.setValue(j, R_ij);
            }
            var bold_r_i = builder_bold_r_i.build();
            var R_i = ByteArray.xor(bold_r_i);
            R_i = SetWatermark.run(R_i, bold_s.getValue(i) - 1, params.n_max);
            var RC_i = ToString.run(R_i, params.A_R);
            builder_bold_rc.setValue(i, RC_i);
        }
        var bold_rc = builder_bold_rc.build();
        return bold_rc;
    }

}
