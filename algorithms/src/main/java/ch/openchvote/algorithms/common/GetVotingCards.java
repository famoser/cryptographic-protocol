/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GetVotingCard;
import ch.openchvote.model.common.VotingCard;
import ch.openchvote.model.common.VotingCardData;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;

/**
 * ALGORITHM 8.18
 */
public class GetVotingCards {

    public static Vector<VotingCard> run(Matrix<VotingCardData> bold_D, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_D, params);

        int N_E = bold_D.getHeight();
        int s = bold_D.getWidth();
        int n = (N_E > 0 && s > 0) ? bold_D.getValue(1, 1).get_bold_p().getLength() : 0;

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.Matrix(Set.Quadruple(params.ZZ_q_hat_x, params.ZZ_q_hat_y, Set.B(params.L_FA), Set.Vector(Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat), n)), N_E, s).contains(bold_D));

        var builder_bold_vc = new Vector.Builder<VotingCard>(N_E);

        // ALGORITHM
        for (int i = 1; i <= N_E; i++) {
            var bold_d_i = bold_D.getRow(i);
            var VC_i = GetVotingCard.run(i, bold_d_i, params);
            builder_bold_vc.setValue(i, VC_i);
        }
        var bold_vc = builder_bold_vc.build();
        return bold_vc;
    }

}
