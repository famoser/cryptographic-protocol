/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Quintuple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.14
 */
public class GenCredentials {

    public static Quintuple<BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> run(BigInteger z, int s, Parameters params) {

        // ALGORITHM
        var x = GenRandomInteger.run(params.q_hat_x.divide(BigInteger.valueOf(s)));
        var y = GenRandomInteger.run(params.q_hat_y.divide(BigInteger.valueOf(s)));
        var x_hat = Mod.pow(params.g_hat, x, params.p_hat);
        var y_hat = Mod.pow(params.g_hat, y, params.p_hat);
        var z_hat = Mod.pow(params.g_hat, z, params.p_hat);
        return new Quintuple<>(x, y, x_hat, y_hat, z_hat);
    }

}
