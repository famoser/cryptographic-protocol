/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.*;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.common.Query;
import ch.openchvote.model.common.Response;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.27
 */
public class GenResponse {

    public static Pair<Response, Finalization> run(Integer v, Vector<Query> bold_a, QuadraticResidue pk, IntVector bold_n, IntVector bold_k, IntMatrix bold_E, Matrix<Pair<BigInteger, BigInteger>> bold_P, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(v, bold_a, pk, bold_n, bold_k, bold_E, bold_P, params);

        var N_E = bold_E.getHeight();
        var t = bold_E.getWidth();
        var n = bold_P.getWidth();
        var k = bold_a.getLength();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), k).contains(bold_a));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(Set.Matrix(Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat), N_E, n).contains(bold_P));

        Precondition.check(n == Math.intSum(bold_n));
        Precondition.check(bold_k.isLess(bold_n));
        Precondition.check(k == Math.intSumProd(bold_E.getRow(v), bold_k));

        var builder_bold_beta = new Vector.Builder<QuadraticResidue>(k);
        var builder_bold_b = new Vector.Builder<QuadraticResidue>(k);
        var builder_C = new Matrix.Builder<ByteArray>(n, k);

        // ALGORITHM
        var bold_e = bold_E.getRow(v);
        var z_1 = GenRandomInteger.run(params.q);
        var z_2 = GenRandomInteger.run(params.q);
        Parallel.forLoop(1, k, j -> {
            var beta_j = GenRandomElement.run(params);
            var a_j = bold_a.getValue(j);
            var b_j = Mod.multiply(Mod.pow(a_j.get_a_1(), z_1), Mod.multiply(Mod.pow(a_j.get_a_2(), z_2), beta_j));
            builder_bold_beta.setValue(j, beta_j);
            builder_bold_b.setValue(j, b_j);
        });
        var bold_beta = builder_bold_beta.build();
        var ell_M = Math.ceilDiv(params.L_M, params.L);
        var bold_p = GetPrimes.run(n, params);
        var n_prime = 0;
        var k_prime = 0;
        for (int l = 1; l <= t; l++) {
            var n_l = bold_n.getValue(l);
            var builder_bold_p_prime = new Vector.Builder<QuadraticResidue>(n_prime + 1, n_prime + n_l);
            if (bold_e.getValue(l) == 1) {
                Parallel.forLoop(n_prime + 1, n_prime + n_l, i -> {
                    var p_i = bold_p.getValue(i);
                    var p_prime_i = Mod.pow(p_i, z_1);
                    builder_bold_p_prime.setValue(i, p_prime_i);
                });
            }
            var bold_p_prime = builder_bold_p_prime.build();

            for (int i = n_prime + 1; i <= n_prime + n_l; i++) {
                for (int j = 1; j <= k; j++) {
                    builder_C.setValue(i, j, null);
                }
                if (bold_e.getValue(l) == 1) {
                    //var p_i = bold_p.getValue(i);
                    //var p_prime_i = Mod.pow(p_i, z_1);
                    var p_prime_i = bold_p_prime.getValue(i);
                    var x_vi = bold_P.getValue(v, i).getFirst();
                    var y_vi = bold_P.getValue(v, i).getSecond();
                    var M_i = ToByteArray.run(x_vi, params.L_M / 2).concatenate(ToByteArray.run(y_vi, params.L_M / 2));
                    var k_l = bold_k.getValue(l);
                    for (int j = k_prime + 1; j <= k_prime + k_l; j++) {
                        var beta_j = bold_beta.getValue(j);
                        var k_ij = Mod.multiply(p_prime_i, beta_j);
                        var K_ij = ByteArray.EMPTY;
                        for (int c = 1; c <= ell_M; c++) {
                            K_ij = K_ij.concatenate(RecHash.run(params.hashAlgorithm, k_ij, c));
                        }
                        K_ij = K_ij.truncate(params.L_M);
                        var C_ij = M_i.xor(K_ij);
                        builder_C.setValue(i, j, C_ij);
                    }
                }
            }
            n_prime = n_prime + n_l;
            k_prime = k_prime + bold_e.getValue(l) * bold_k.getValue(l);
        }
        var bold_b = builder_bold_b.build();
        var bold_C = builder_C.build();
        var d = Mod.multiply(Mod.pow(pk, z_1), Mod.pow(params.g, z_2));
        var beta = new Response(bold_b, bold_C, d);
        var delta = new Finalization(z_1, z_2);
        return new Pair<>(beta, delta);
    }

}
