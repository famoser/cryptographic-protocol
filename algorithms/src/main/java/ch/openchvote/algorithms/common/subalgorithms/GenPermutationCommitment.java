/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.46
 */
public class GenPermutationCommitment {

    public static Pair<Vector<QuadraticResidue>, Vector<BigInteger>> run(IntVector psi, Vector<QuadraticResidue> bold_h, Parameters params) {

        // PREPARATION
        var N = psi.getLength();
        var builder_bold_c = new Vector.Builder<QuadraticResidue>(N);
        var builder_bold_r = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        Parallel.forLoop(1, N, i -> {
            var j_i = psi.getValue(i);
            var r_j_i = GenRandomInteger.run(params.q);
            var c_j_i = Mod.multiply(Mod.pow(params.g, r_j_i), bold_h.getValue(i));
            builder_bold_r.setValue(j_i, r_j_i);
            builder_bold_c.setValue(j_i, c_j_i);
        });
        var bold_c = builder_bold_c.build();
        var bold_r = builder_bold_r.build();
        return new Pair<>(bold_c, bold_r);
    }

}
