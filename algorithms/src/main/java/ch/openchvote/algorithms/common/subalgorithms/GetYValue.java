/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;

import java.math.BigInteger;

/**
 * ALGORITHM 8.13
 */
public class GetYValue {

    public static BigInteger run(BigInteger x, Vector<BigInteger> bold_a, Parameters params) {

        // PREPARATION
        var d = bold_a.getLength() - 1;
        BigInteger y;

        // ALGORITHM
        if (x.equals(BigInteger.ZERO)) {
            y = bold_a.getValue(0);
        } else {
            y = BigInteger.ZERO;
            for (int i = d; i >= 0; i--) {
                y = Mod.add(bold_a.getValue(i), Mod.multiply(x, y, params.q_hat), params.q_hat);
            }
        }
        return y;
    }

}
