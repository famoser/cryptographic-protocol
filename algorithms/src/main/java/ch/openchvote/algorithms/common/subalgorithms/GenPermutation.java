/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.util.IntVector;

import java.util.stream.IntStream;

/**
 * ALGORITHM 8.43
 */
public class GenPermutation {

    public static IntVector run(int N) {

        // PREPARATION
        var builder_phi = new IntVector.Builder(N);

        // ALGORITHM
        int[] I = IntStream.rangeClosed(1, N).toArray();
        for (int i = 0; i <= N - 1; i++) {
            int k = GenRandomInteger.run(i, N - 1);
            builder_phi.setValue(i + 1, I[k]);
            I[k] = I[i];
        }
        var phi = builder_phi.build();
        return phi;
    }

}
