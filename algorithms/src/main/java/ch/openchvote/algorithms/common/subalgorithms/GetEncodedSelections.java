/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.22
 */
public class GetEncodedSelections {

    public static Vector<QuadraticResidue> run(IntVector bold_s, Vector<QuadraticResidue> bold_p) {

        // PREPARATION
        var k = bold_s.getLength();
        var builder_bold_m = new Vector.Builder<QuadraticResidue>(k);

        // ALGORITHM
        for (int j = 1; j <= k; j++) {
            var s_j = bold_s.getValue(j);
            var m_j = bold_p.getValue(s_j);
            builder_bold_m.setValue(j, m_j);
        }
        var bold_m = builder_bold_m.build();
        return bold_m;
    }

}
