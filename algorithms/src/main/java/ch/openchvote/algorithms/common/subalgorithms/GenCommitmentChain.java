/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.47
 */
public class GenCommitmentChain {

    public static Pair<Vector<QuadraticResidue>, Vector<BigInteger>> run(Vector<BigInteger> bold_u_tilde, Parameters params) {

        // PREPARATION
        int N = bold_u_tilde.getLength();
        var builder_bold_c_hat = new Vector.Builder<QuadraticResidue>(1, N);
        var builder_bold_r_hat = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var R_i_minus_1 = BigInteger.ZERO;
        var U_i_minus_1 = BigInteger.ONE;
        var builder_bold_R = new Vector.Builder<BigInteger>(N);
        var builder_bold_U = new Vector.Builder<BigInteger>(N);
        for (int i = 1; i <= N; i++) {
            var r_hat_i = GenRandomInteger.run(params.q);
            var R_i = Mod.add(r_hat_i, Mod.multiply(bold_u_tilde.getValue(i), R_i_minus_1, params.q), params.q);
            var U_i = Mod.multiply(bold_u_tilde.getValue(i), U_i_minus_1, params.q);
            builder_bold_r_hat.setValue(i, r_hat_i);
            builder_bold_R.setValue(i, R_i);
            builder_bold_U.setValue(i, U_i);
            R_i_minus_1 = R_i; // preparation for next loop cycle
            U_i_minus_1 = U_i; // preparation for next loop cycle
        }
        var bold_R = builder_bold_R.build();
        var bold_U = builder_bold_U.build();
        for (int i = 1; i <= N; i++) {
            var R_i = bold_R.getValue(i);
            var U_i = bold_U.getValue(i);
            var c_hat_i = Mod.multiply(Mod.pow(params.g, R_i), Mod.pow(params.h, U_i));
            builder_bold_c_hat.setValue(i, c_hat_i);
        }
        var bold_c_hat = builder_bold_c_hat.build();
        var bold_r_hat = builder_bold_r_hat.build();
        return new Pair<>(bold_c_hat, bold_r_hat);
    }

}
