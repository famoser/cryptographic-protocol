/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.55
 */
public class GetInspection {

    public static ByteArray run(int v, Matrix<Pair<BigInteger, BigInteger>> bold_P, Vector<ByteArray> bold_a, SearchableList<Confirmation> C, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_P, bold_a, C, params);

        var N_E = bold_P.getHeight();
        var n = bold_P.getWidth();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(Set.Matrix(Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat), N_E, n).contains(bold_P));
        Precondition.check(Set.Vector(Set.B(params.L_FA), N_E).contains(bold_a));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(C.getKeys()));

        ByteArray I_v;

        // ALGORITHM
        if (C.contains(v)) {
            var bold_p_v = bold_P.getRow(v);
            I_v = RecHash.run(params.hashAlgorithm, bold_p_v).truncate(params.L_FA);
        } else {
            I_v = bold_a.getValue(v);
        }
        return I_v;
    }

}
