/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.CheckConfirmationProof;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.SearchableList;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;

import java.math.BigInteger;

/**
 * ALGORITHM 8.35
 */
public class CheckConfirmation {

    public static boolean run(int v, Confirmation gamma, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, SearchableList<?> B, SearchableList<Confirmation> C, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(gamma, bold_y_hat, bold_z_hat, B, params);

        var N_E = bold_y_hat.getLength();
        var y_hat = gamma.get_y_hat();
        var z_hat = gamma.get_z_hat();
        var pi = gamma.get_pi();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(params.GG_q_hat.contains(y_hat));
        Precondition.check(params.GG_q_hat.contains(z_hat));
        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat)).contains(pi));
        // for improved efficiency, we do not perform exact group membership tests for all values of bold_y_hat and bold_z_hat
        Precondition.check(Set.Vector(params.ZZ_star_p_hat, N_E).contains(bold_y_hat));
        Precondition.check(Set.Vector(params.ZZ_star_p_hat, N_E).contains(bold_z_hat));
        Precondition.check(params.GG_q_hat.contains(bold_y_hat.getValue(v)));
        Precondition.check(params.GG_q_hat.contains(bold_z_hat.getValue(v)));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(B.getKeys()));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(C.getKeys()));

        // ALGORITHM
        if (B.contains(v) && !C.contains(v)) {
            var y_hat_v = bold_y_hat.getValue(v);
            var z_hat_v = bold_z_hat.getValue(v);
            return y_hat.equals(y_hat_v) && z_hat.equals(z_hat_v) && CheckConfirmationProof.run(pi, y_hat, z_hat, params);
        }
        return false;
    }

}
