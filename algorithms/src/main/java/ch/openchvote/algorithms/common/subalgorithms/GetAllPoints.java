/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.common.Response;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.38
 */
public class GetAllPoints {

    public static final Pair<BigInteger, BigInteger> ZERO_ZERO = new Pair<>(BigInteger.ZERO, BigInteger.ZERO);

    public static Vector<Pair<BigInteger, BigInteger>> run(Response beta, Finalization delta, IntVector bold_s, Vector<BigInteger> bold_r, IntVector bold_n, IntVector bold_k, IntVector bold_e, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        int n = Math.intSum(bold_n);
        int t = bold_n.getLength();

        var bold_b = beta.get_bold_b();
        var bold_C = beta.get_bold_C();
        var d = beta.get_d();

        var z_1 = delta.get_z_1();
        var z_2 = delta.get_z_2();

        var builder_bold_p = new Vector.Builder<Pair<BigInteger, BigInteger>>(n);

        // ALGORITHM
        var d_prime = Mod.multiply(Mod.pow(pk, z_1), Mod.pow(params.g, z_2));
        if (!d_prime.equals(d)) {
            throw new AlgorithmException(GetAllPoints.class, AlgorithmException.Type.INCOMPATIBLE_VALUES);
        }
        var bold_primes = GetPrimes.run(n, params);
        var ell_M = Math.ceilDiv(params.L_M, params.L);
        for (int i = 1; i <= n; i++) {
            builder_bold_p.setValue(i, ZERO_ZERO);
        }
        int n_prime = 0;
        int k_prime = 0;
        for (int l = 1; l <= t; l++) {
            var n_l = bold_n.getValue(l);
            var k_l = bold_k.getValue(l);
            var e_l = bold_e.getValue(l);
            if (e_l == 1) {
                var builder_bold_p_prime_l = new Vector.Builder<QuadraticResidue>(n_prime, n_prime + n_l);
                Parallel.forLoop(n_prime + 1, n_prime + n_l, i -> {
                    var p_i = bold_primes.getValue(i);
                    var p_prime_i = Mod.pow(p_i, z_1);
                    builder_bold_p_prime_l.setValue(i, p_prime_i);
                });
                var bold_p_prime_l = builder_bold_p_prime_l.build();
                var builder_bold_beta_l = new Vector.Builder<QuadraticResidue>(k_prime, k_prime + k_l);
                Parallel.forLoop(k_prime + 1, k_prime + k_l, j -> {
                    var b_j = bold_b.getValue(j);
                    var p_prime_s_j = bold_p_prime_l.getValue(bold_s.getValue(j));
                    var r_j = bold_r.getValue(j);
                    var beta_j = Mod.multiply(b_j, Mod.pow(d, Mod.minus(r_j, params.q)), Mod.invert(p_prime_s_j));
                    builder_bold_beta_l.setValue(j, beta_j);
                });
                var bold_beta_l = builder_bold_beta_l.build();
                for (int i = n_prime + 1; i <= n_prime + n_l; i++) {
                    var builder_bold_m_i = new Vector.Builder<ByteArray>(k_prime + 1, k_prime + bold_k.getValue(l));
                    for (int j = k_prime + 1; j <= k_prime + k_l; j++) {
                        var p_prime_i = bold_p_prime_l.getValue(i);
                        var beta_j = bold_beta_l.getValue(j);
                        var k_ij = Mod.multiply(p_prime_i, beta_j);
                        var K_ij = ByteArray.EMPTY;
                        for (int c = 1; c <= ell_M; c++) {
                            K_ij = K_ij.concatenate(RecHash.run(params.hashAlgorithm, k_ij, c));
                        }
                        K_ij = K_ij.truncate(params.L_M);
                        var C_ij = bold_C.getValue(i, j);
                        var M_ij = C_ij.xor(K_ij);
                        builder_bold_m_i.setValue(j, M_ij);
                    }
                    var bold_m_i = builder_bold_m_i.build();
                    if (!bold_m_i.isUniform()) {
                        throw new AlgorithmException(GetAllPoints.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
                    }
                    var x_i = ToInteger.run(bold_m_i.getValue(k_prime + 1).truncate(params.L_M / 2));
                    var y_i = ToInteger.run(bold_m_i.getValue(k_prime + 1).skip(params.L_M / 2));
                    var p_i = new Pair<>(x_i, y_i);
                    builder_bold_p.setValue(i, p_i);
                }
            }
            n_prime = n_prime + n_l;
            k_prime = k_prime + e_l * k_l;
        }
        var bold_p = builder_bold_p.build();
        return bold_p;
    }
}
