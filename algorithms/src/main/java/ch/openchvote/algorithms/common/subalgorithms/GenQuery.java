/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.model.common.Query;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.23
 */
public class GenQuery {

    public static Pair<Vector<Query>, Vector<BigInteger>> run(Vector<QuadraticResidue> bold_m, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        var k = bold_m.getLength();
        var builder_bold_a = new Vector.Builder<Query>(k);
        var builder_bold_r = new Vector.Builder<BigInteger>(k);

        // ALGORITHM
        Parallel.forLoop(1, k, j -> {
            var m_j = bold_m.getValue(j);
            var r_j = GenRandomInteger.run(params.q);
            var a_j = new Query(Mod.multiply(m_j, Mod.pow(pk, r_j)), Mod.pow(params.g, r_j));
            builder_bold_a.setValue(j, a_j);
            builder_bold_r.setValue(j, r_j);
        });
        var bold_a = builder_bold_a.build();
        var bold_r = builder_bold_r.build();
        return new Pair<>(bold_a, bold_r);
    }

}
