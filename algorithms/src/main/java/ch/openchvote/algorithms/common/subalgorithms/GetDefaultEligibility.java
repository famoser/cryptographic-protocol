/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.util.IntMatrix;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.math.Math;

/**
 * ALGORITHM 8.41
 */
public class GetDefaultEligibility {

    public static IntMatrix run(IntVector bold_w, IntMatrix bold_E) {

        // PREPARATION
        int N_E = bold_w.getLength();
        int t = bold_E.getWidth();
        int w = Math.intMax(bold_w);
        var builder_bold_E_star = new IntMatrix.Builder(w, t);

        // ALGORITHM
        for (int j = 1; j <= t; j++) {
            for (int c = 1; c <= w; c++) {
                builder_bold_E_star.setValue(c, j, 0);
            }
            for (int i = 1; i <= N_E; i++) {
                if (bold_E.getValue(i, j) == 1) {
                    builder_bold_E_star.setValue(bold_w.getValue(i), j, 1);
                }
            }
        }
        var bold_E_star = builder_bold_E_star.build();
        return bold_E_star;
    }

}
