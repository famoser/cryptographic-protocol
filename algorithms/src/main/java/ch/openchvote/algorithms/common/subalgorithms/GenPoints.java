/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;
import java.util.HashSet;

/**
 * ALGORITHM 8.11
 */
public class GenPoints {

    public static final Pair<BigInteger, BigInteger> ZERO_ZERO = new Pair<>(BigInteger.ZERO, BigInteger.ZERO);

    public static Pair<Vector<Pair<BigInteger, BigInteger>>, BigInteger> run(IntVector bold_n, IntVector bold_k, IntVector bold_e, Parameters params) {

        // PREPARATION
        var t = bold_n.getLength();
        var n = Math.intSum(bold_n);

        var builder_bold_p = new Vector.Builder<Pair<BigInteger, BigInteger>>(n);

        // ALGORITHM
        var k = Math.intSumProd(bold_e, bold_k);
        var bold_a = GenPolynomial.run(k - 1, params);
        var X = new HashSet<>(java.util.Set.of(BigInteger.ZERO));
        var n_prime = 0;
        for (int j = 1; j <= t; j++) {
            for (int i = n_prime + 1; i <= n_prime + bold_n.getValue(j); i++) {
                Pair<BigInteger, BigInteger> p_i;
                if (bold_e.getValue(j) == 1) {
                    var x = GenRandomInteger.run(params.q_hat, X);
                    X.add(x);
                    var y = GetYValue.run(x, bold_a, params);
                    p_i = new Pair<>(x, y);
                } else {
                    p_i = ZERO_ZERO;
                }
                builder_bold_p.setValue(i, p_i);
            }
            n_prime = n_prime + bold_n.getValue(j);
        }
        var y_0 = GetYValue.run(BigInteger.ZERO, bold_a, params);
        var bold_p = builder_bold_p.build();
        return new Pair<>(bold_p, y_0);
    }

}
