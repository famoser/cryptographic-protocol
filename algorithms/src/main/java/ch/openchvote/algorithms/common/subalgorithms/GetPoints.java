/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.model.common.Response;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.29
 */
public class GetPoints {

    public static Vector<Pair<BigInteger, BigInteger>> run(Response beta, IntVector bold_s, Vector<BigInteger> bold_r, Parameters params) {

        // PREPARATION
        var bold_b = beta.get_bold_b();
        var bold_C = beta.get_bold_C();
        var d = beta.get_d();
        var k = bold_b.getLength();

        var builder_bold_p = new Vector.Builder<Pair<BigInteger, BigInteger>>(k);

        // ALGORITHM
        var ell_M = Math.ceilDiv(params.L_M, params.L);
        for (int j = 1; j <= k; j++) {
            var b_j = bold_b.getValue(j);
            var r_j = bold_r.getValue(j);
            var k_j = Mod.multiply(b_j, Mod.pow(d, Mod.minus(r_j, params.q)));
            var K_j = ByteArray.EMPTY;
            for (int c = 1; c <= ell_M; c++) {
                K_j = K_j.concatenate(RecHash.run(params.hashAlgorithm, k_j, c));
            }
            K_j = K_j.truncate(params.L_M);
            var C_sj_j = bold_C.getValue(bold_s.getValue(j), j);
            if (C_sj_j == null) {
                throw new AlgorithmException(GetPoints.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
            }
            var M_j = C_sj_j.xor(K_j);
            var x_j = ToInteger.run(M_j.truncate(params.L_M / 2));
            var y_j = ToInteger.run(M_j.skip(params.L_M / 2));
            if (x_j.compareTo(params.q_hat) >= 0 || y_j.compareTo(params.q_hat) >= 0) {
                throw new AlgorithmException(GetPoints.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
            }
            var p_j = new Pair<>(x_j, y_j);
            builder_bold_p.setValue(j, p_j);
        }
        var bold_p = builder_bold_p.build();
        return bold_p;
    }

}
