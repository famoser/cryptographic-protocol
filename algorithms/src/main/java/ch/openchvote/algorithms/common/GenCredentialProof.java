/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.CredentialProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.15
 */
public class GenCredentialProof {

    public static CredentialProof run(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, Vector<BigInteger> bold_z, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat, params);

        var N_E = bold_x.getLength();

        Precondition.check(Set.Vector(params.ZZ_q_hat, N_E).contains(bold_x));
        Precondition.check(Set.Vector(params.ZZ_q_hat, N_E).contains(bold_y));
        Precondition.check(Set.Vector(params.ZZ_q_hat, N_E).contains(bold_z));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_x_hat));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_y_hat));
        Precondition.check(Set.Vector(params.GG_q_hat, N_E).contains(bold_z_hat));

        var builder_bold_omega = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);
        var builder_bold_t = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);
        var builder_bold_s = new Vector.Builder<Triple<BigInteger, BigInteger, BigInteger>>(N_E);

        // ALGORITHM
        Parallel.forLoop(1, N_E, i -> {
            var omega_i_1 = GenRandomInteger.run(params.q_hat);
            var omega_i_2 = GenRandomInteger.run(params.q_hat);
            var omega_i_3 = GenRandomInteger.run(params.q_hat);
            var omega_i = new Triple<>(omega_i_1, omega_i_2, omega_i_3);
            builder_bold_omega.setValue(i, omega_i);
            var t_i_1 = Mod.pow(params.g_hat, omega_i_1, params.p_hat);
            var t_i_2 = Mod.pow(params.g_hat, omega_i_2, params.p_hat);
            var t_i_3 = Mod.pow(params.g_hat, omega_i_3, params.p_hat);
            var t_i = new Triple<>(t_i_1, t_i_2, t_i_3);
            builder_bold_t.setValue(i, t_i);
        });

        var bold_omega = builder_bold_omega.build();
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(bold_x_hat, bold_y_hat, bold_z_hat);
        var c = GetChallenge.run(y, bold_t, params);
        for (int i = 1; i <= N_E; i++) {
            var s_i_1 = Mod.subtract(bold_omega.getValue(i).getFirst(), Mod.multiply(c, bold_x.getValue(i), params.q_hat), params.q_hat);
            var s_i_2 = Mod.subtract(bold_omega.getValue(i).getSecond(), Mod.multiply(c, bold_y.getValue(i), params.q_hat), params.q_hat);
            var s_i_3 = Mod.subtract(bold_omega.getValue(i).getThird(), Mod.multiply(c, bold_z.getValue(i), params.q_hat), params.q_hat);
            var s_i = new Triple<>(s_i_1, s_i_2, s_i_3);
            builder_bold_s.setValue(i, s_i);
        }
        var bold_s = builder_bold_s.build();
        var pi = new CredentialProof(c, bold_s);
        return pi;
    }

}
