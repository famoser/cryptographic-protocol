/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.ConfirmationProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.34
 */
public class GenConfirmationProof {

    public static ConfirmationProof run(BigInteger y, BigInteger z, BigInteger y_hat, BigInteger z_hat, Parameters params) {

        // ALGORITHM
        var omega_1 = GenRandomInteger.run(params.q_hat);
        var omega_2 = GenRandomInteger.run(params.q_hat);
        var t_1 = Mod.pow(params.g_hat, omega_1, params.p_hat);
        var t_2 = Mod.pow(params.g_hat, omega_2, params.p_hat);
        var t = new Pair<>(t_1, t_2);
        var c = GetChallenge.run(new Pair<>(y_hat, z_hat), t, params);
        var s_1 = Mod.subtract(omega_1, Mod.multiply(c, y, params.q_hat), params.q_hat);
        var s_2 = Mod.subtract(omega_2, Mod.multiply(c, z, params.q_hat), params.q_hat);
        var s = new Pair<>(s_1, s_2);
        var pi = new ConfirmationProof(c, s);
        return pi;
    }

}
