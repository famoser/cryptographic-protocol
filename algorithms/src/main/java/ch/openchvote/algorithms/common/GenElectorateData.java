/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenCredentials;
import ch.openchvote.algorithms.common.subalgorithms.GenPoints;
import ch.openchvote.algorithms.general.RandomBytes;
import ch.openchvote.model.common.ElectorateData;
import ch.openchvote.model.common.VotingCardData;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.10
 */
public class GenElectorateData {

    public static ElectorateData run(IntVector bold_n, IntVector bold_k, IntMatrix bold_E, int s, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_n, bold_k, bold_E, params);

        var N_E = bold_E.getHeight();
        int t = bold_E.getWidth();
        var n = Math.intSum(bold_n);

        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(IntSet.NN_plus.contains(s));

        Precondition.check(bold_k.isLess(bold_n));

        var builder_bold_a = new Vector.Builder<ByteArray>(N_E);
        var builder_bold_d = new Vector.Builder<VotingCardData>(N_E);
        var builder_bold_x = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_x_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_P = new Matrix.RowBuilder<Pair<BigInteger, BigInteger>>(N_E, n);

        // ALGORITHM
        Parallel.forLoop(1, N_E, i -> {
            var bold_e_i = bold_E.getRow(i);
            var pair = GenPoints.run(bold_n, bold_k, bold_e_i, params);
            var bold_p_i = pair.getFirst();
            var z_i = pair.getSecond();
            var quintuple = GenCredentials.run(z_i, s, params);
            var x_i = quintuple.getFirst();
            var y_i = quintuple.getSecond();
            var x_hat_i = quintuple.getThird();
            var y_hat_i = quintuple.getFourth();
            var z_hat_i = quintuple.getFifth();
            var A_i = RandomBytes.run(params.L_FA);
            var d_i = new VotingCardData(x_i, y_i, A_i, bold_p_i);
            builder_bold_P.setRow(i, bold_p_i);
            builder_bold_x.setValue(i, x_i);
            builder_bold_y.setValue(i, y_i);
            builder_bold_z.setValue(i, z_i);
            builder_bold_x_hat.setValue(i, x_hat_i);
            builder_bold_y_hat.setValue(i, y_hat_i);
            builder_bold_z_hat.setValue(i, z_hat_i);
            builder_bold_a.setValue(i, A_i);
            builder_bold_d.setValue(i, d_i);
        });

        var bold_a = builder_bold_a.build();
        var bold_d = builder_bold_d.build();
        var bold_x = builder_bold_x.build();
        var bold_y = builder_bold_y.build();
        var bold_z = builder_bold_z.build();
        var bold_x_hat = builder_bold_x_hat.build();
        var bold_y_hat = builder_bold_y_hat.build();
        var bold_z_hat = builder_bold_z_hat.build();
        var bold_P = builder_bold_P.build();
        return new ElectorateData(bold_a, bold_d, bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat, bold_P);
    }

}
