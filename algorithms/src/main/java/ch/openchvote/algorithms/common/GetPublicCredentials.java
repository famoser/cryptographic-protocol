/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.common.PublicCredentials;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;

import java.math.BigInteger;

/**
 * ALGORITHM 8.17
 */
public class GetPublicCredentials {

    public static PublicCredentials run(Matrix<BigInteger> bold_X_hat, Matrix<BigInteger> bold_Y_hat, Matrix<BigInteger> bold_Z_hat, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_X_hat, bold_Y_hat, bold_Z_hat, params);

        int N_E = bold_X_hat.getHeight();
        int s = bold_X_hat.getWidth();

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.Matrix(params.GG_q_hat, N_E, s).contains(bold_X_hat));
        Precondition.check(Set.Matrix(params.GG_q_hat, N_E, s).contains(bold_Y_hat));
        Precondition.check(Set.Matrix(params.GG_q_hat, N_E, s).contains(bold_Z_hat));

        var builder_bold_x_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_y_hat = new Vector.Builder<BigInteger>(N_E);
        var builder_bold_z_hat = new Vector.Builder<BigInteger>(N_E);

        // ALGORITHM
        for (int i = 1; i <= N_E; i++) {
            var x_hat_i = Mod.prod(bold_X_hat.getRow(i), params.p_hat);
            var y_hat_i = Mod.prod(bold_Y_hat.getRow(i), params.p_hat);
            var z_hat_i = Mod.prod(bold_Z_hat.getRow(i), params.p_hat);
            builder_bold_x_hat.setValue(i, x_hat_i);
            builder_bold_y_hat.setValue(i, y_hat_i);
            builder_bold_z_hat.setValue(i, z_hat_i);
        }
        var bold_x_hat = builder_bold_x_hat.build();
        var bold_y_hat = builder_bold_y_hat.build();
        var bold_z_hat = builder_bold_z_hat.build();
        return new PublicCredentials(bold_x_hat, bold_y_hat, bold_z_hat);
    }

}
