/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenConfirmationProof;
import ch.openchvote.algorithms.common.subalgorithms.GetValidityCredential;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.32
 */
public class GenConfirmation {

    public static Confirmation run(String Y, Matrix<Pair<BigInteger, BigInteger>> bold_P, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(Y, bold_P, params);

        int k = bold_P.getHeight();
        int s = bold_P.getWidth();

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.String(params.A_Y, params.ell_Y).contains(Y));
        Precondition.check(Set.Matrix(Set.Pair(params.ZZ_q_hat, params.ZZ_q_hat), k, s).contains(bold_P));

        var builder_bold_z = new Vector.Builder<BigInteger>(s);

        // ALGORITHM
        var y = ToInteger.run(Y, params.A_Y);
        var y_hat = Mod.pow(params.g_hat, y, params.p_hat);
        for (int j = 1; j <= s; j++) {
            var bold_p_j = bold_P.getCol(j);
            var z_j = GetValidityCredential.run(bold_p_j, params);
            builder_bold_z.setValue(j, z_j);
        }
        var bold_z = builder_bold_z.build();
        var z = Mod.sum(bold_z, params.q_hat);
        var z_hat = Mod.pow(params.g_hat, z, params.p_hat);
        var pi = GenConfirmationProof.run(y, z, y_hat, z_hat, params);
        var gamma = new Confirmation(y_hat, z_hat, pi);
        return gamma;
    }

}
