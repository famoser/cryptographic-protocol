/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common.subalgorithms;

import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.33
 */
public class GetValidityCredential {

    public static BigInteger run(Vector<Pair<BigInteger, BigInteger>> bold_p, Parameters params) {

        // PREPARATION
        var k = bold_p.getLength();

        // ALGORITHM
        var z = BigInteger.ZERO;
        for (int i = 1; i <= k; i++) {
            var n = BigInteger.ONE;
            var d = BigInteger.ONE;
            var x_i = bold_p.getValue(i).getFirst();
            var y_i = bold_p.getValue(i).getSecond();
            for (int j = 1; j <= k; j++) {
                if (i != j) {
                    var x_j = bold_p.getValue(j).getFirst();
                    n = Mod.multiply(n, x_j, params.q_hat);
                    d = Mod.multiply(d, Mod.subtract(x_j, x_i, params.q_hat), params.q_hat);
                }
            }
            z = Mod.add(z, Mod.multiply(y_i, Mod.divide(n, d, params.q_hat), params.q_hat), params.q_hat);
        }
        return z;
    }

}
