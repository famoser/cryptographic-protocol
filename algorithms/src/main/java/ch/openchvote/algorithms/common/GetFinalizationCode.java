/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GetAllPoints;
import ch.openchvote.algorithms.general.RecHash;
import ch.openchvote.algorithms.general.ToString;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.common.Response;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 8.37
 */
public class GetFinalizationCode {

    public static String run(Vector<Response> bold_beta, Vector<Finalization> bold_delta, IntVector bold_s, Vector<BigInteger> bold_r, IntVector bold_n, IntVector bold_k, IntVector bold_e, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_beta, bold_delta, bold_s, bold_r, bold_n, bold_k, bold_e, pk, params);

        int s = bold_beta.getLength();
        Precondition.check(IntSet.NN_plus.contains(s));

        int t = bold_k.getLength();
        int n = bold_beta.getValue(1).get_bold_C().getHeight();
        int k = bold_beta.getValue(1).get_bold_C().getWidth();

        Precondition.check(Set.Vector(Set.Triple(Set.Vector(params.GG_q, k), Set.Matrix(Set.B(params.L_M).orNull(), n, k), params.GG_q), s).contains(bold_beta));
        Precondition.check(Set.Vector(Set.Pair(params.ZZ_q, params.ZZ_q), s).contains(bold_delta));
        Precondition.check(Set.IntVector(IntSet.NN_plus(n), k).contains(bold_s));
        Precondition.check(Set.Vector(params.ZZ_q, k).contains(bold_r));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_e));
        Precondition.check(params.GG_q.contains(pk));

        Precondition.check(bold_s.isSorted());
        Precondition.check(n == Math.intSum(bold_n));
        Precondition.check(bold_k.isLess(bold_n));
        Precondition.check(k == Math.intSumProd(bold_e, bold_k));

        var bold_f_builder = new Vector.Builder<ByteArray>(s);

        // ALGORITHM
        for (int j = 1; j <= s; j++) {
            var bold_p_j = GetAllPoints.run(bold_beta.getValue(j), bold_delta.getValue(j), bold_s, bold_r, bold_n, bold_k, bold_e, pk, params);
            var F_j = RecHash.run(params.hashAlgorithm, bold_p_j).truncate(params.L_FA);
            bold_f_builder.setValue(j, F_j);
        }
        var bold_f = bold_f_builder.build();
        var FC = ToString.run(ByteArray.xor(bold_f), params.A_FA);
        return FC;
    }

}
