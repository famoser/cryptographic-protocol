/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;

/**
 * ALGORITHM 8.31
 */
public class CheckVerificationCodes {

    public static boolean run(Vector<String> bold_rc, Vector<String> bold_rc_prime, IntVector bold_s, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_rc, bold_rc_prime, bold_s, params);

        var n = bold_rc.getLength();
        var k = bold_rc_prime.getLength();

        Precondition.check(Set.Vector(Set.String(params.A_R, params.ell_R), n).contains(bold_rc));
        Precondition.check(Set.Vector(Set.String(params.A_R, params.ell_R), k).contains(bold_rc_prime));
        Precondition.check(Set.IntVector(IntSet.NN_plus(n), k).contains(bold_s));

        Precondition.check(bold_s.isSorted());

        // ALGORITHM
        for (int j = 1; j < k; j++) {
            var RC_s_j = bold_rc.getValue(bold_s.getValue(j));
            var RC_prime_j = bold_rc_prime.getValue(j);
            if (!RC_s_j.equals(RC_prime_j)) {
                return false;
            }
        }
        return true;
    }

}
