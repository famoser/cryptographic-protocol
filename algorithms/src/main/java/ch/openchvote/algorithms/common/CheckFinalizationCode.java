/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Set;

/**
 * ALGORITHM 8.39
 */
public class CheckFinalizationCode {

    public static boolean run(String FC, String FC_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(FC, FC_prime, params);

        Precondition.check(Set.String(params.A_FA, params.ell_FA).contains(FC));
        Precondition.check(Set.String(params.A_FA, params.ell_FA).contains(FC_prime));

        // ALGORITHM
        return FC.equals(FC_prime);
    }

}
