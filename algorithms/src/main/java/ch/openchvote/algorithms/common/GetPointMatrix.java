/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.common;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GetPoints;
import ch.openchvote.model.common.Response;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.28
 */
public class GetPointMatrix {

    public static Matrix<Pair<BigInteger, BigInteger>> run(Vector<Response> bold_beta, IntVector bold_s, Vector<BigInteger> bold_r, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_beta, bold_s, bold_r, params);

        int s = bold_beta.getLength();
        Precondition.check(IntSet.NN_plus.contains(s));

        int n = bold_beta.getValue(1).get_bold_C().getHeight();
        int k = bold_beta.getValue(1).get_bold_C().getWidth();

        Precondition.check(Set.Vector(Set.Triple(Set.Vector(params.GG_q, k), Set.Matrix(Set.B(params.L_M).orNull(), n, k), params.GG_q), s).contains(bold_beta));
        Precondition.check(Set.IntVector(IntSet.NN_plus(n), k).contains(bold_s));
        Precondition.check(Set.Vector(params.ZZ_q, k).contains(bold_r));

        Precondition.check(bold_s.isSorted());

        var builder_bold_P = new Matrix.ColBuilder<Pair<BigInteger, BigInteger>>(k, s);

        // ALGORITHM
        for (int j = 1; j <= s; j++) {
            var bold_p_j = GetPoints.run(bold_beta.getValue(j), bold_s, bold_r, params);
            builder_bold_P.addCol(bold_p_j);
        }
        var bold_P = builder_bold_P.build();
        return bold_P;
    }

}
