/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.44
 */
public class GenReEncryption {

    public static Pair<Encryption, BigInteger> run(Encryption e, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        var a = e.get_a();
        var b = e.get_b();

        // ALGORITHM
        var r_tilde = GenRandomInteger.run(params.q);
        var a_tilde = Mod.multiply(a, Mod.pow(pk, r_tilde));
        var b_tilde = Mod.multiply(b, Mod.pow(params.g, r_tilde));
        var e_tilde = new Encryption(a_tilde, b_tilde);
        return new Pair<>(e_tilde, r_tilde);
    }

}
