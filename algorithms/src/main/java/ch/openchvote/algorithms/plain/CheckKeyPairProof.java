/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.plain.KeyPairProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.8
 */
public class CheckKeyPairProof {

    public static boolean run(KeyPairProof pi, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(pi, pk, params);

        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, params.ZZ_q).contains(pi));
        Precondition.check(params.GG_q.contains(pk));

        var c = pi.get_c();
        var s = pi.get_s();

        // ALGORITHM
        var t = Mod.multiply(Mod.pow(pk, c), Mod.pow(params.g, s));
        var c_prime = GetChallenge.run(pk, t, params);
        return c.equals(c_prime);
    }

}
