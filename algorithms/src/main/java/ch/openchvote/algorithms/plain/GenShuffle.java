/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenPermutation;
import ch.openchvote.algorithms.plain.subalgorithms.GenReEncryption;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.42
 */
public class GenShuffle {

    public static Triple<Vector<Encryption>, Vector<BigInteger>, IntVector> run(Vector<Encryption> bold_e, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_e, pk, params);

        int N = bold_e.getLength();

        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), N).contains(bold_e));
        Precondition.check(params.GG_q.contains(pk));

        var builder_bold_e_tilde = new Vector.Builder<Encryption>(N);
        var builder_bold_r_tilde = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var psi = GenPermutation.run(N);
        Parallel.forLoop(1, N, i -> {
            int j_i = psi.getValue(i);
            var pair = GenReEncryption.run(bold_e.getValue(j_i), pk, params);
            builder_bold_e_tilde.setValue(i, pair.getFirst());
            builder_bold_r_tilde.setValue(j_i, pair.getSecond());
        });
        var bold_e_tilde = builder_bold_e_tilde.build();
        var bold_r_tilde = builder_bold_r_tilde.build();
        return new Triple<>(bold_e_tilde, bold_r_tilde, psi);
    }

}
