/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.9
 */
public class GetPublicKey {

    public static QuadraticResidue run(Vector<QuadraticResidue> bold_pk, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_pk, params);

        int s = bold_pk.getLength();

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.Vector(params.GG_q, s).contains(bold_pk));

        // ALGORITHM
        var pk = Mod.prod(bold_pk);
        return pk;
    }

}
