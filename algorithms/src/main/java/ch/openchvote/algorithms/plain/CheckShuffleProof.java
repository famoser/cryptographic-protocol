/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.algorithms.general.GetChallenges;
import ch.openchvote.algorithms.general.GetGenerators;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.plain.ShuffleProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Quadruple;
import ch.openchvote.util.tuples.Quintuple;

/**
 * ALGORITHM 8.48
 */
public class CheckShuffleProof {

    public static boolean run(String U, ShuffleProof pi, Vector<Encryption> bold_e, Vector<Encryption> bold_e_tilde, QuadraticResidue pk, Parameters params) {

        Precondition.checkNotNull(U, pi, bold_e, bold_e_tilde, pk);

        int N = bold_e.getLength();

        Precondition.check(Set.UCS_star.contains(U));
        Precondition.check(Set.Quadruple(params.ZZ_twoToTheTau, Set.Sextuple(params.ZZ_q, params.ZZ_q, params.ZZ_q, params.ZZ_q, Set.Vector(params.ZZ_q, N), Set.Vector(params.ZZ_q, N)), Set.Vector(params.GG_q, N), Set.Vector(params.GG_q, N)).contains(pi));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), N).contains(bold_e_tilde));
        Precondition.check(params.GG_q.contains(pk));

        var c = pi.get_c();
        var s = pi.get_s();
        var s_1 = s.getFirst();
        var s_2 = s.getSecond();
        var s_3 = s.getThird();
        var s_4 = s.getFourth();
        var bold_s_hat = s.getFifth();
        var bold_s_tilde = s.getSixth();
        var bold_c = pi.get_bold_c();
        var bold_c_hat = pi.get_bold_c_hat();

        var builder_bold_t_hat = new Vector.Builder<QuadraticResidue>(N);

        // ALGORITHM
        var bold_h = GetGenerators.run(N, U, params);
        var bold_u = GetChallenges.run(N, new Quadruple<>(bold_e, bold_e_tilde, bold_c, pk), params);
        var c_hat_0 = params.h;
        var c_bar = Mod.divide(Mod.prod(bold_c), Mod.prod(bold_h));
        var u = Mod.prod(bold_u, params.q);
        var c_hat = Mod.divide(N == 0 ? c_hat_0 : bold_c_hat.getValue(N), Mod.pow(params.h, u));
        var c_tilde = Mod.prodPow(bold_c, bold_u);
        var a_tilde = Mod.prodPow(bold_e.map(Encryption::get_a), bold_u);
        var b_tilde = Mod.prodPow(bold_e.map(Encryption::get_b), bold_u);
        Parallel.forLoop(1, N, i -> {
            var c_hat_i_minus_1 = i == 1 ? c_hat_0 : bold_c_hat.getValue(i - 1);
            var t_hat_i = Mod.multiply(Mod.pow(bold_c_hat.getValue(i), c), Mod.multiply(Mod.pow(params.g, bold_s_hat.getValue(i)), Mod.pow(c_hat_i_minus_1, bold_s_tilde.getValue(i))));
            builder_bold_t_hat.setValue(i, t_hat_i);
        });
        var t_1 = Mod.multiply(Mod.pow(c_bar, c), Mod.pow(params.g, s_1));
        var t_2 = Mod.multiply(Mod.pow(c_hat, c), Mod.pow(params.g, s_2));
        var t_3 = Mod.multiply(Mod.pow(c_tilde, c), Mod.multiply(Mod.pow(params.g, s_3), Mod.prodPow(bold_h, bold_s_tilde)));
        var t_41 = Mod.multiply(Mod.pow(a_tilde, c), Mod.multiply(Mod.invert(Mod.pow(pk, s_4)), Mod.prodPow(bold_e_tilde.map(Encryption::get_a), bold_s_tilde)));
        var t_42 = Mod.multiply(Mod.pow(b_tilde, c), Mod.multiply(Mod.invert(Mod.pow(params.g, s_4)), Mod.prodPow(bold_e_tilde.map(Encryption::get_b), bold_s_tilde)));
        var t = new Quintuple<>(t_1, t_2, t_3, new Pair<>(t_41, t_42), builder_bold_t_hat.build());
        var y = new Quintuple<>(bold_e, bold_e_tilde, bold_c, bold_c_hat, pk);
        var c_prime = GetChallenge.run(y, t, params);
        return c.equals(c_prime);
    }
}
