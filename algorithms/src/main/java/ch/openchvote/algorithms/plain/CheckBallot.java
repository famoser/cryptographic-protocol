/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.CheckBallotProof;
import ch.openchvote.model.plain.Ballot;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 8.25
 */
public class CheckBallot {

    public static boolean run(int v, Ballot alpha, QuadraticResidue pk, IntVector bold_k, IntMatrix bold_E, Vector<BigInteger> bold_x_hat, SearchableList<?> B, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(v, alpha, pk, bold_k, bold_E, bold_x_hat, B, params);

        var x_hat = alpha.get_x_hat();
        var bold_a = alpha.get_bold_a();
        var pi = alpha.get_pi();

        int N_E = bold_E.getHeight();
        int t = bold_E.getWidth();
        int k = bold_a.getLength();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(params.GG_q_hat.contains(x_hat));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), k).contains(bold_a));
        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Triple(params.ZZ_q_hat, params.GG_q, params.ZZ_q)).contains(pi));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        // for improved efficiency, we do not perform exact group membership tests for all values of bold_x_hat
        Precondition.check(Set.Vector(params.ZZ_star_p_hat, N_E).contains(bold_x_hat) && params.GG_q_hat.contains(bold_x_hat.getValue(v)));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(B.getKeys()));

        // ALGORITHM
        int k_prime = Math.intSumProd(bold_E.getRow(v), bold_k);
        var x_hat_v = bold_x_hat.getValue(v);
        if (!B.contains(v) && x_hat.equals(x_hat_v) && k == k_prime) {
            return CheckBallotProof.run(pi, x_hat, bold_a, pk, params);
        }
        return false;
    }

}
