/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.model.plain.ElectionResult;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.54
 */
public class GetElectionResult {

    public static ElectionResult run(Vector<QuadraticResidue> bold_m, IntVector bold_n, IntVector bold_w, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_m, bold_n, bold_w, params);

        int N = bold_m.getLength();
        int t = bold_n.getLength();
        int N_E = bold_w.getLength();

        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_m));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));

        // ALGORITHM
        int n = Math.intSum(bold_n);
        int w = Math.intMax(bold_w);
        var bold_p = GetPrimes.run(n + w, params);
        var builder_bold_u = new IntVector.Builder(N);
        var builder_bold_V = new IntMatrix.Builder(N, n);
        var builder_bold_W = new IntMatrix.Builder(N, w);
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= n; k++) {
                if (Math.divides(bold_p.getValue(k).getValue(), bold_m.getValue(i).getValue())) {
                    builder_bold_V.setValue(i, k, 1);
                } else {
                    builder_bold_V.setValue(i, k, 0);
                }
            }
            for (int j = 1; j <= w; j++) {
                if (Math.divides(bold_p.getValue(n + j).getValue(), bold_m.getValue(i).getValue())) {
                    builder_bold_W.setValue(i, j, 1);
                } else {
                    builder_bold_W.setValue(i, j, 0);
                }
            }
            if (Math.divides(bold_p.getValue(0).getValue(), bold_m.getValue(i).getValue())) {
                builder_bold_u.setValue(i, -1);
            } else {
                builder_bold_u.setValue(i, 1);
            }
        }
        var bold_u = builder_bold_u.build();
        var bold_V = builder_bold_V.build();
        var bold_W = builder_bold_W.build();
        return new ElectionResult(bold_u, bold_V, bold_W);
    }

}
