/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.plain.ElectionParameters;
import ch.openchvote.model.plain.VotingParameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Math;

/**
 * ALGORITHM 8.20
 */
public class GetVotingParameters {

    public static VotingParameters run(int v, ElectionParameters EP) {

        // PREPARATION
        Precondition.checkNotNull(EP);

        var U = EP.get_U();
        var bold_c = EP.get_bold_c();
        var bold_d = EP.get_bold_d();
        var bold_e = EP.get_bold_e();
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();
        var bold_w = EP.get_bold_w();

        var n = bold_c.getLength();
        var N_E = bold_E.getHeight();
        var t = bold_E.getWidth();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(Set.UCS_star.contains(U));
        Precondition.check(Set.Vector(Set.UCS_star, n).contains(bold_c));
        Precondition.check(Set.Vector(Set.UCS_star, N_E).contains(bold_d));
        Precondition.check(Set.Vector(Set.UCS_star, t).contains(bold_e));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));

        Precondition.check(bold_k.isLess(bold_n));
        Precondition.check(n == Math.intSum(bold_n));

        var D_v = bold_d.getValue(v);
        var w_v = bold_w.getValue(v);

        // ALGORITHM
        var bold_e_v = bold_E.getRow(v);
        var VP_v = new VotingParameters(U, bold_c, D_v, bold_e, bold_n, bold_k, bold_e_v, w_v);
        return VP_v;
    }

}
