/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.plain.KeyPairProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 8.7
 */
public class GenKeyPairProof {

    public static KeyPairProof run(BigInteger sk, QuadraticResidue pk, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(sk, pk, params);

        Precondition.check(params.ZZ_q.contains(sk));
        Precondition.check(params.GG_q.contains(pk));

        // ALGORITHM
        var omega = GenRandomInteger.run(params.q);
        var t = Mod.pow(params.g, omega);
        var c = GetChallenge.run(pk, t, params);
        var s = Mod.subtract(omega, Mod.multiply(c, sk, params.q), params.q);
        var pi = new KeyPairProof(c, s);
        return pi;
    }

}
