/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.52
 */
public class GetCombinedDecryptions {

    public static Vector<QuadraticResidue> run(Matrix<QuadraticResidue> bold_C, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_C, params);

        var N = bold_C.getHeight();
        var s = bold_C.getWidth();

        Precondition.check(Set.Matrix(params.GG_q, N, s).contains(bold_C));

        var builder_bold_c = new Vector.Builder<QuadraticResidue>(N);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var bold_c_i = bold_C.getRow(i);
            var c_i = Mod.prod(bold_c_i);
            builder_bold_c.setValue(i, c_i);
        }
        var bold_c = builder_bold_c.build();
        return bold_c;
    }

}
