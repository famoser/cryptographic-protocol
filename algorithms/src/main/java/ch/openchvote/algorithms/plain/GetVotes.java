/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 8.53
 */
public class GetVotes {

    public static Vector<QuadraticResidue> run(Vector<Encryption> bold_e, Vector<QuadraticResidue> bold_c, Vector<QuadraticResidue> bold_c_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_e, bold_c, bold_c_prime, params);

        var N = bold_e.getLength();

        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c_prime));

        var builder_bold_m = new Vector.Builder<QuadraticResidue>(N);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var a_i = bold_e.getValue(i).get_a();
            var c_i = bold_c.getValue(i);
            var c_prime_i = bold_c_prime.getValue(i);
            var m_i = Mod.multiply(a_i, Mod.invert(Mod.multiply(c_i, c_prime_i)));
            builder_bold_m.setValue(i, m_i);
        }
        var bold_m = builder_bold_m.build();
        return bold_m;
    }

}
