/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.plain.DecryptionProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.50
 */
public class GenDecryptionProof {

    public static DecryptionProof run(BigInteger sk, QuadraticResidue pk, Vector<Encryption> bold_e, Vector<QuadraticResidue> bold_c, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(sk, pk, bold_e, bold_c, params);

        int N = bold_e.getLength();

        Precondition.check(params.ZZ_q.contains(sk));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c));

        var builder_bold_t = new Vector.Builder<QuadraticResidue>(0, N);

        // ALGORITHM
        var omega = GenRandomInteger.run(params.q);
        var t_0 = Mod.pow(params.g, omega);
        builder_bold_t.setValue(0, t_0);
        Parallel.forLoop(1, N, i -> {
            var b_i = bold_e.getValue(i).get_b();
            var t_i = Mod.pow(b_i, omega);
            builder_bold_t.setValue(i, t_i);
        });
        var bold_t = builder_bold_t.build();
        var y = new Triple<>(pk, bold_e, bold_c);
        var c = GetChallenge.run(y, bold_t, params);
        var s = Mod.subtract(omega, Mod.multiply(c, sk, params.q), params.q);
        var pi = new DecryptionProof(c, s);
        return pi;
    }

}
