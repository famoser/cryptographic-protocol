/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.plain;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenBallotProof;
import ch.openchvote.algorithms.common.subalgorithms.GenQuery;
import ch.openchvote.algorithms.common.subalgorithms.GetEncodedSelections;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.model.plain.Ballot;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 8.21
 */
public class GenBallot {

    public static Pair<Ballot, Vector<BigInteger>> run(String X, IntVector bold_s, QuadraticResidue pk, IntVector bold_n, int w, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(X, bold_s, pk, bold_n, params);

        int k = bold_s.getLength();
        int t = bold_n.getLength();
        int n = Math.intSum(bold_n);

        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(IntSet.NN_plus.contains(w));
        Precondition.check(Set.String(params.A_X, params.ell_X).contains(X));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus(n), k).contains(bold_s));

        Precondition.check(bold_s.isSorted());

        // ALGORITHM
        var x = ToInteger.run(X, params.A_X);
        var x_hat = Mod.pow(params.g_hat, x, params.p_hat);
        var bold_p = GetPrimes.run(n + w, params);
        var bold_m = GetEncodedSelections.run(bold_s, bold_p);
        var m = Math.prod(bold_m.map(QuadraticResidue::getValue));
        if (bold_p.getValue(n + w).getValue().multiply(m).compareTo(params.p) >= 0) {
            throw new AlgorithmException(GenBallot.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
        }
        var pair = GenQuery.run(bold_m, pk, params);
        var bold_a = pair.getFirst();
        var bold_r = pair.getSecond();
        var r = Mod.sum(bold_r, params.q);
        var pi = GenBallotProof.run(x, Mod.prod(bold_m), r, x_hat, bold_a, pk, params);
        var alpha = new Ballot(x_hat, bold_a, pi);
        return new Pair<>(alpha, bold_r);
    }

}

