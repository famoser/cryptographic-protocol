/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms;

/**
 * This class is used for throwing specific exceptions while executing protocol algorithms. The different types of
 * algorithm exceptions are represented as an internal enum class.
 */
public class AlgorithmException extends RuntimeException {

    public enum Type {
        PRECONDITION_FAILED,
        INCOMPATIBLE_MATRIX,
        UNSUPPORTED_HASH_TYPE,
        INCOMPATIBLE_VALUES,
        NULL_POINTER
    }

    private final String algorithmName;
    private final Type type;

    public AlgorithmException(Class<?> algorithmClass, Type type) {
        this(algorithmClass.getName(), type);
    }

    public AlgorithmException(String algorithmName, Type type) {
        super(algorithmName + ": " + type);
        this.algorithmName = algorithmName;
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

    public String getAlgorithmName() {
        return this.algorithmName;
    }

}
