/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 9.26
 */
public class GetPartialDecryption {

    public static Pair<QuadraticResidue, Vector<QuadraticResidue>> run(AugmentedEncryption e, BigInteger sk, Vector<BigInteger> bold_sk_prime, Parameters params) {

        // PREPARATION
        var z = bold_sk_prime.getLength();
        var b = e.get_b();
        var b_prime = e.get_b_prime();
        var builder_bold_c_prime = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        var c = Mod.pow(b, sk);
        for (int j = 1; j <= z; j++) {
            var sk_prime_j = bold_sk_prime.getValue(j);
            var c_prime_j = Mod.pow(b_prime, sk_prime_j);
            builder_bold_c_prime.setValue(j, c_prime_j);
        }
        var bold_c_prime = builder_bold_c_prime.build();
        return new Pair<>(c, bold_c_prime);
    }

}
