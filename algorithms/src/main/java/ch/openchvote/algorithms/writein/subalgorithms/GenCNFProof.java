/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.writein.WriteInProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.13
 */
public class GenCNFProof {

    public static WriteInProof run(Matrix<Triple<QuadraticResidue, QuadraticResidue, Encryption>> bold_Y, Vector<BigInteger> bold_r, IntVector bold_j, Parameters params) {

        // PREPARATION
        int m = bold_Y.getHeight();
        int n = bold_Y.getWidth();
        var builder_bold_c = new Vector.Builder<BigInteger>(m);
        var builder_bold_omega = new Vector.Builder<BigInteger>(m);
        var builder_bold_T = new Matrix.Builder<Pair<QuadraticResidue, QuadraticResidue>>(m, n);
        var builder_bold_C = new Matrix.Builder<BigInteger>(m, n);
        var builder_bold_S = new Matrix.Builder<BigInteger>(m, n);

        // ALGORITHM
        for (int i = 1; i <= m; i++) {
            var c_i = BigInteger.ZERO;
            BigInteger omega_i;
            for (int j = 1; j <= n; j++) {
                Pair<QuadraticResidue, QuadraticResidue> t_ij;
                var y_ij = bold_Y.getValue(i, j);
                var pk_ij = y_ij.getFirst();
                var m_ij = y_ij.getSecond();
                var e_ij = y_ij.getThird();
                var a_ij = e_ij.get_a();
                var b_ij = e_ij.get_b();
                BigInteger c_ij;
                BigInteger s_ij;
                if (j == bold_j.getValue(i)) {
                    omega_i = GenRandomInteger.run(params.q);
                    t_ij = new Pair<>(Mod.pow(pk_ij, omega_i), Mod.pow(params.g, omega_i));
                    builder_bold_omega.setValue(i, omega_i);
                } else {
                    c_ij = GenRandomInteger.run(BigInteger.TWO.pow(params.tau));
                    s_ij = GenRandomInteger.run(params.q);
                    t_ij = new Pair<>(
                            Mod.multiply(Mod.pow(pk_ij, s_ij), Mod.pow(Mod.divide(a_ij, m_ij), c_ij)),
                            Mod.multiply(Mod.pow(params.g, s_ij), Mod.pow(b_ij, c_ij)));
                    c_i = Mod.add(c_i, c_ij, BigInteger.TWO.pow(params.tau));
                    builder_bold_C.setValue(i, j, c_ij);
                    builder_bold_S.setValue(i, j, s_ij);
                }
                builder_bold_T.setValue(i, j, t_ij);
            }
            builder_bold_c.setValue(i, c_i);
        }
        var bold_T = builder_bold_T.build();
        var c = GetChallenge.run(bold_Y, bold_T, params);
        var bold_c = builder_bold_c.build();
        var bold_omega = builder_bold_omega.build();
        for (int i = 1; i <= m; i++) {
            var j = bold_j.getValue(i);
            var c_ij = Mod.subtract(c, bold_c.getValue(i), BigInteger.TWO.pow(params.tau));
            var s_ij = Mod.subtract(bold_omega.getValue(i), Mod.multiply(c_ij, bold_r.getValue(i), params.q), params.q);
            builder_bold_C.setValue(i, j, c_ij);
            builder_bold_S.setValue(i, j, s_ij);
        }
        var bold_C = builder_bold_C.build();
        var bold_S = builder_bold_S.build();
        var pi = new WriteInProof(bold_C, bold_S);
        return pi;
    }

}
