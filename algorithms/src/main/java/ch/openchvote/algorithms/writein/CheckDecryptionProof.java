/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.model.writein.DecryptionProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Quintuple;

/**
 * ALGORITHM 9.24
 */
public class CheckDecryptionProof {

    public static boolean run(DecryptionProof pi, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, Vector<AugmentedEncryption> bold_e, Vector<QuadraticResidue> bold_c, Matrix<QuadraticResidue> bold_D, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(pi, pk, bold_pk_prime, bold_e, bold_c, bold_D, params);

        int z = bold_pk_prime.getLength();
        int N = bold_e.getLength();

        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Vector(params.ZZ_q, z + 1)).contains(pi));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));
        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c));
        Precondition.check(Set.Matrix(params.GG_q, N, z).contains(bold_D));

        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var builder_bold_T = new Matrix.Builder<QuadraticResidue>(0, N, 0, z);

        // PERFORMANCE OPTIMIZATIONS (precompute some modexps)
        var builder_bold_c_power_c = new Vector.Builder<QuadraticResidue>(N);
        bold_c.forEach(c_i -> builder_bold_c_power_c.addValue(Mod.pow(c_i, c)));
        var bold_c_power_c = builder_bold_c_power_c.build();

        var builder_bold_pk_prime_power_c = new Vector.Builder<QuadraticResidue>(z);
        bold_pk_prime.forEach(pk_prime_j -> builder_bold_pk_prime_power_c.addValue(Mod.pow(pk_prime_j, c)));
        var bold_pk_prime_power_c = builder_bold_pk_prime_power_c.build();

        // ALGORITHM
        for (int j = 0; j <= z; j++) {
            var s_j = bold_s.getValue(j);
            var g_power_s_j = Mod.pow(params.g, s_j);
            for (int i = 0; i <= N; i++) {
                QuadraticResidue t_ij;
                if (i == 0 && j == 0) {
                    t_ij = Mod.multiply(Mod.pow(pk, c), g_power_s_j);
                } else if (i == 0) {
                    var pk_prime_j_power_c = bold_pk_prime_power_c.getValue(j);
                    t_ij = Mod.multiply(pk_prime_j_power_c, g_power_s_j);
                } else {
                    var e_i = bold_e.getValue(i);
                    if (j == 0) {
                        var b_i = e_i.get_b();
                        var c_i_power_c = bold_c_power_c.getValue(i);
                        t_ij = Mod.multiply(c_i_power_c, Mod.pow(b_i, s_j));
                    } else {
                        var b_prime_i = e_i.get_b_prime();
                        var d_ij = bold_D.getValue(i, j);
                        t_ij = Mod.multiply(Mod.pow(d_ij, c), Mod.pow(b_prime_i, s_j));
                    }
                }
                builder_bold_T.setValue(i, j, t_ij);
            }
        }
        var bold_T = builder_bold_T.build();
        var y = new Quintuple<>(pk, bold_pk_prime, bold_e, bold_c, bold_D);
        var c_prime = GetChallenge.run(y, bold_T, params);
        return c.equals(c_prime);
    }

}
