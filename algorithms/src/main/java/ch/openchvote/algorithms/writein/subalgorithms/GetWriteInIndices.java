/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.util.IntSet;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.tuples.Pair;

import java.util.HashSet;

/**
 * ALGORITHM 9.11
 */
public class GetWriteInIndices {

    public static Pair<IntSet, IntSet> run(IntVector bold_n, IntVector bold_k, IntVector bold_e, IntVector bold_z, IntVector bold_v) {

        // PREPARATION
        int t = bold_n.getLength();

        // ALGORITHM
        var I = new HashSet<Integer>();
        var J = new HashSet<Integer>();
        int k_prime = 0;
        int n_prime = 0;
        for (int l = 1; l <= t; l++) {
            if (bold_e.getValue(l) == 1) {
                var z_l = bold_z.getValue(l);
                if (z_l == 1) {
                    for (int i = k_prime + 1; i <= k_prime + bold_k.getValue(l); i++) {
                        I.add(i);
                    }
                    for (int j = n_prime + 1; j <= n_prime + bold_n.getValue(l); j++) {
                        if (bold_v.getValue(j) == 1) {
                            J.add(j);
                        }
                    }
                }
                k_prime = k_prime + bold_k.getValue(l);
            }
            n_prime = n_prime + bold_n.getValue(l);
        }
        return new Pair<>(IntSet.of(I), IntSet.of(J));
    }

}
