/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.writein.GetEncodedStrings;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;

/**
 * ALGORITHM 9.9
 */
public class GetEncodedWriteIns {

    public static Vector<QuadraticResidue> run(Vector<WriteIn> bold_s, Parameters params) {

        // PREPARATION
        int z = bold_s.getLength();
        var builder_bold_m = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        for (int i = 1; i <= z; i++) {
            var m_i = GetEncodedStrings.run(bold_s.getValue(i), params.A_W, params.ell_W, params.c_W, params);
            builder_bold_m.setValue(i, m_i);
        }
        var bold_m = builder_bold_m.build();
        return bold_m;
    }

}
