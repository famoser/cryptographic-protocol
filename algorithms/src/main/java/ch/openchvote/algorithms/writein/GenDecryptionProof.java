/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.model.writein.DecryptionProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Quintuple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.23
 */
public class GenDecryptionProof {

    public static DecryptionProof run(BigInteger sk, QuadraticResidue pk, Vector<BigInteger> bold_sk_prime, Vector<QuadraticResidue> bold_pk_prime, Vector<AugmentedEncryption> bold_e, Vector<QuadraticResidue> bold_c, Matrix<QuadraticResidue> bold_D, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(sk, pk, bold_sk_prime, bold_pk_prime, bold_e, bold_c, bold_D, params);

        int z = bold_sk_prime.getLength();
        int N = bold_e.getLength();

        Precondition.check(params.ZZ_q.contains(sk));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.ZZ_q, z).contains(bold_sk_prime));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));
        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c));
        Precondition.check(Set.Matrix(params.GG_q, N, z).contains(bold_D));

        var builder_bold_omega = new Vector.Builder<BigInteger>(0, z);
        var builder_bold_T = new Matrix.Builder<QuadraticResidue>(0, N, 0, z);
        var builder_bold_s = new Vector.Builder<BigInteger>(0, z);

        // ALGORITHM
        for (int j = 0; j <= z; j++) {
            var omega_j = GenRandomInteger.run(params.q);
            for (int i = 0; i <= N; i++) {
                QuadraticResidue t_ij;
                if (i == 0) {
                    t_ij = Mod.pow(params.g, omega_j);
                } else {
                    var e_i = bold_e.getValue(i);
                    if (j == 0) {
                        var b_i = e_i.get_b();
                        t_ij = Mod.pow(b_i, omega_j);
                    } else {
                        var b_prime_i = e_i.get_b_prime();
                        t_ij = Mod.pow(b_prime_i, omega_j);
                    }
                }
                builder_bold_T.setValue(i, j, t_ij);
            }
            builder_bold_omega.setValue(j, omega_j);
        }
        var bold_Omega = builder_bold_omega.build();
        var bold_T = builder_bold_T.build();
        var y = new Quintuple<>(pk, bold_pk_prime, bold_e, bold_c, bold_D);
        var c = GetChallenge.run(y, bold_T, params);
        for (int j = 0; j <= z; j++) {
            var omega_j = bold_Omega.getValue(j);
            BigInteger s_j;
            if (j == 0) {
                s_j = Mod.subtract(omega_j, Mod.multiply(c, sk, params.q), params.q);
            } else {
                var sk_prime_j = bold_sk_prime.getValue(j);
                s_j = Mod.subtract(omega_j, Mod.multiply(c, sk_prime_j, params.q), params.q);
            }
            builder_bold_s.setValue(j, s_j);
        }
        var bold_s = builder_bold_s.build();
        var pi = new DecryptionProof(c, bold_s);
        return pi;
    }

}
