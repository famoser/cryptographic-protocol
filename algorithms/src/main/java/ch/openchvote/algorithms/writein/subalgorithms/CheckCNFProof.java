/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.writein.WriteInProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.16
 */
public class CheckCNFProof {

    public static boolean run(WriteInProof pi, Matrix<Triple<QuadraticResidue, QuadraticResidue, Encryption>> bold_Y, Parameters params) {

        // PREPARATION
        var m = bold_Y.getHeight();
        var n = bold_Y.getWidth();
        var bold_C = pi.get_bold_C();
        var bold_S = pi.get_bold_S();
        var builder_bold_c = new Vector.Builder<BigInteger>(m);
        var builder_bold_T = new Matrix.Builder<Pair<QuadraticResidue, QuadraticResidue>>(m, n);

        // ALGORITHM
        for (int i = 1; i <= m; i++) {
            var c_i = Mod.sum(bold_C.getRow(i), BigInteger.TWO.pow(params.tau));
            builder_bold_c.setValue(i, c_i);
            for (int j = 1; j <= n; j++) {
                var y_ij = bold_Y.getValue(i, j);
                var pk_ij = y_ij.getFirst();
                var m_ij = y_ij.getSecond();
                var e_ij = y_ij.getThird();
                var a_ij = e_ij.get_a();
                var b_ij = e_ij.get_b();
                var c_ij = bold_C.getValue(i, j);
                var s_ij = bold_S.getValue(i, j);
                var t_ij = new Pair<>(Mod.multiply(Mod.pow(pk_ij, s_ij), Mod.pow(Mod.divide(a_ij, m_ij), c_ij)), Mod.multiply(Mod.pow(params.g, s_ij), Mod.pow(b_ij, c_ij)));
                builder_bold_T.setValue(i, j, t_ij);
            }
        }
        var bold_c = builder_bold_c.build();
        var bold_T = builder_bold_T.build();
        var c_prime = GetChallenge.run(bold_Y, bold_T, params);
        return bold_c.isUniform() && (m == 0 || bold_c.getValue(1).equals(c_prime));
    }

}
