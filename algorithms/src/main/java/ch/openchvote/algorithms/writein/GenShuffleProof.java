/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenCommitmentChain;
import ch.openchvote.algorithms.common.subalgorithms.GenPermutationCommitment;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.algorithms.general.GetChallenges;
import ch.openchvote.algorithms.general.GetGenerators;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.model.writein.ShuffleProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Quadruple;
import ch.openchvote.util.tuples.Quintuple;
import ch.openchvote.util.tuples.Sextuple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.20
 */
public class GenShuffleProof {

    public static ShuffleProof run(String U, Vector<AugmentedEncryption> bold_e, Vector<AugmentedEncryption> bold_e_tilde, Vector<BigInteger> bold_r_tilde, Vector<BigInteger> bold_r_tilde_prime, IntVector psi, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(U, bold_e, bold_e_tilde, bold_r_tilde, bold_r_tilde_prime, psi, pk, bold_pk_prime, params);

        int N = bold_e.getLength();
        int z = bold_pk_prime.getLength();

        Precondition.check(Set.UCS_star.contains(U));
        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e_tilde));
        Precondition.check(Set.Vector(params.ZZ_q, N).contains(bold_r_tilde));
        Precondition.check(Set.Vector(params.ZZ_q, N).contains(bold_r_tilde_prime));
        Precondition.check(Set.Phi(N).contains(psi));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));

        var builder_bold_u_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_omega_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_omega_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_t_hat = new Vector.Builder<QuadraticResidue>(N);
        var builder_bold_v = new Vector.Builder<BigInteger>(N);
        var builder_bold_s_hat = new Vector.Builder<BigInteger>(N);
        var builder_bold_s_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_t_43 = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        var bold_h = GetGenerators.run(N, U, params);
        var pair1 = GenPermutationCommitment.run(psi, bold_h, params);
        var bold_c = pair1.getFirst();
        var bold_r = pair1.getSecond();
        var bold_u = GetChallenges.run(N, new Quadruple<>(bold_e, bold_e_tilde, bold_c, pk), params);
        for (int i = 1; i <= N; i++) {
            builder_bold_u_tilde.setValue(i, bold_u.getValue(psi.getValue(i)));
        }
        var bold_u_tilde = builder_bold_u_tilde.build();
        var pair2 = GenCommitmentChain.run(bold_u_tilde, params);
        var bold_c_hat = pair2.getFirst();
        var bold_r_hat = pair2.getSecond();
        var R_i_minus_1 = BigInteger.ZERO;
        var U_i_minus_1 = BigInteger.ONE;
        for (int i = 1; i <= N; i++) {
            var omega_hat_i = GenRandomInteger.run(params.q);
            var omega_tilde_i = GenRandomInteger.run(params.q);
            var R_i = Mod.add(bold_r_hat.getValue(i), Mod.multiply(bold_u_tilde.getValue(i), R_i_minus_1, params.q), params.q);
            var U_i = Mod.multiply(bold_u_tilde.getValue(i), U_i_minus_1, params.q);
            var R_prime_i = Mod.add(omega_hat_i, Mod.multiply(omega_tilde_i, R_i_minus_1, params.q), params.q);
            var U_prime_i = Mod.multiply(omega_tilde_i, U_i_minus_1, params.q);
            var t_hat_i = Mod.multiply(Mod.pow(params.g, R_prime_i), Mod.pow(params.h, U_prime_i));
            builder_bold_omega_hat.setValue(i, omega_hat_i);
            builder_bold_omega_tilde.setValue(i, omega_tilde_i);
            builder_bold_t_hat.setValue(i, t_hat_i);
            R_i_minus_1 = R_i; // preparation for next loop cycle
            U_i_minus_1 = U_i; // preparation for next loop cycle
        }
        var bold_omega_hat = builder_bold_omega_hat.build();
        var bold_omega_tilde = builder_bold_omega_tilde.build();
        var omega_1 = GenRandomInteger.run(params.q);
        var omega_2 = GenRandomInteger.run(params.q);
        var omega_3 = GenRandomInteger.run(params.q);
        var omega_4 = GenRandomInteger.run(params.q);
        var omega_prime_4 = GenRandomInteger.run(params.q);
        var t_1 = Mod.pow(params.g, omega_1);
        var t_2 = Mod.pow(params.g, omega_2);
        var t_3 = Mod.multiply(Mod.pow(params.g, omega_3), Mod.prodPow(bold_h, bold_omega_tilde));
        var t_41 = Mod.multiply(Mod.invert(Mod.pow(pk, omega_4)), Mod.prodPow(bold_e_tilde.map(AugmentedEncryption::get_a), bold_omega_tilde));
        var t_42 = Mod.multiply(Mod.invert(Mod.pow(params.g, omega_4)), Mod.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b), bold_omega_tilde));
        for (int j = 1; j <= z; j++) {
            int j_final = j; // necessary for using j in lambda expression
            var pk_prime_j = bold_pk_prime.getValue(j);
            var t_43_j = Mod.multiply(Mod.invert(Mod.pow(pk_prime_j, omega_prime_4)), Mod.prodPow(bold_e_tilde.map(AugmentedEncryption::get_bold_a_prime).map(a -> a.getValue(j_final)), bold_omega_tilde));
            builder_bold_t_43.setValue(j, t_43_j);
        }
        var bold_t_43 = builder_bold_t_43.build();
        var t_44 = Mod.multiply(Mod.invert(Mod.pow(params.g, omega_prime_4)), Mod.prodPow(bold_e_tilde.map(AugmentedEncryption::get_b_prime), bold_omega_tilde));
        var t = new Quintuple<>(t_1, t_2, t_3, new Quadruple<>(t_41, t_42, bold_t_43, t_44), builder_bold_t_hat.build());
        var y = new Sextuple<>(bold_e, bold_e_tilde, bold_c, bold_c_hat, pk, bold_pk_prime);
        var c = GetChallenge.run(y, t, params);
        var r_bar = Mod.sum(bold_r, params.q);
        var s_1 = Mod.subtract(omega_1, Mod.multiply(c, r_bar, params.q), params.q);
        var v_i = BigInteger.ONE;
        for (int i = N; i >= 1; i--) {
            builder_bold_v.setValue(i, v_i);
            v_i = Mod.multiply(bold_u_tilde.getValue(i), v_i, params.q);
        }
        var bold_v = builder_bold_v.build();
        var r_hat = Mod.sumProd(bold_r_hat, bold_v, params.q);
        var s_2 = Mod.subtract(omega_2, Mod.multiply(c, r_hat, params.q), params.q);
        var r = Mod.sumProd(bold_r, bold_u, params.q);
        var s_3 = Mod.subtract(omega_3, Mod.multiply(c, r, params.q), params.q);
        var r_tilde = Mod.sumProd(bold_r_tilde, bold_u, params.q);
        var s_4 = Mod.subtract(omega_4, Mod.multiply(c, r_tilde, params.q), params.q);
        for (int i = 1; i <= N; i++) {
            var s_hat_i = Mod.subtract(bold_omega_hat.getValue(i), Mod.multiply(c, bold_r_hat.getValue(i), params.q), params.q);
            builder_bold_s_hat.setValue(i, s_hat_i);
            var s_tilde_i = Mod.subtract(bold_omega_tilde.getValue(i), Mod.multiply(c, bold_u_tilde.getValue(i), params.q), params.q);
            builder_bold_s_tilde.setValue(i, s_tilde_i);
        }
        var r_tilde_prime = Mod.sumProd(bold_r_tilde_prime, bold_u, params.q);
        var s_prime_4 = Mod.subtract(omega_prime_4, Mod.multiply(c, r_tilde_prime, params.q), params.q);
        var s = new Sextuple<>(s_1, s_2, s_3, new Pair<>(s_4, s_prime_4), builder_bold_s_hat.build(), builder_bold_s_tilde.build());
        var pi = new ShuffleProof(c, s, bold_c, bold_c_hat);
        return pi;
    }

}
