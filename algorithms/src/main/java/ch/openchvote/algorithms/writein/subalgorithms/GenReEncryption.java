/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.19
 */
public class GenReEncryption {

    public static Triple<AugmentedEncryption, BigInteger, BigInteger> run(AugmentedEncryption e, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, Parameters params) {

        // PREPARATION
        var z = bold_pk_prime.getLength();
        var a = e.get_a();
        var b = e.get_b();
        var bold_a_prime = e.get_bold_a_prime();
        var b_prime = e.get_b_prime();
        var builder_bold_a_tilde_prime = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        var r_tilde = GenRandomInteger.run(params.q);
        var r_tilde_prime = GenRandomInteger.run(params.q);
        var a_tilde = Mod.multiply(a, Mod.pow(pk, r_tilde));
        var b_tilde = Mod.multiply(b, Mod.pow(params.g, r_tilde));
        for (int i = 1; i <= z; i++) {
            var a_prime_i = bold_a_prime.getValue(i);
            var pk_prime_i = bold_pk_prime.getValue(i);
            var a_tilde_prime_i = Mod.multiply(a_prime_i, Mod.pow(pk_prime_i, r_tilde_prime));
            builder_bold_a_tilde_prime.setValue(i, a_tilde_prime_i);
        }
        var b_tilde_prime = Mod.multiply(b_prime, Mod.pow(params.g, r_tilde_prime));
        var bold_a_tilde_prime = builder_bold_a_tilde_prime.build();
        var e_tilde = new AugmentedEncryption(a_tilde, b_tilde, bold_a_tilde_prime, b_tilde_prime);
        return new Triple<>(e_tilde, r_tilde, r_tilde_prime);
    }

}
