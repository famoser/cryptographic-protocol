/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.6
 */
public class GetPublicKeys {

    public static Pair<QuadraticResidue, Vector<QuadraticResidue>> run(Vector<QuadraticResidue> bold_pk, Matrix<QuadraticResidue> bold_PK_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_pk, bold_PK_prime, params);

        int s = bold_pk.getLength();
        int z = bold_PK_prime.getHeight();

        Precondition.check(IntSet.NN_plus.contains(s));
        Precondition.check(Set.Vector(params.GG_q, s).contains(bold_pk));
        Precondition.check(Set.Matrix(params.GG_q, z, s).contains(bold_PK_prime));

        var builder_bold_pk_prime = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        var pk = Mod.prod(bold_pk);
        for (int i = 1; i <= z; i++) {
            var bold_pk_prime_i = bold_PK_prime.getRow(i);
            var pk_prime_i = Mod.prod(bold_pk_prime_i);
            builder_bold_pk_prime.setValue(i, pk_prime_i);
        }
        var bold_pk_prime = builder_bold_pk_prime.build();
        return new Pair<>(pk, bold_pk_prime);
    }

}
