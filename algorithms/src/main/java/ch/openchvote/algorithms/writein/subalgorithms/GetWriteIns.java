/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.writein.GetDecodedStrings;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.28
 */
public class GetWriteIns {

    public static Pair<Matrix<WriteIn>, IntMatrix> run(Matrix<QuadraticResidue> bold_M, IntMatrix bold_V, IntVector bold_n, IntVector bold_k, IntVector bold_z, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_M, bold_V, bold_n, bold_k, bold_z);

        int N = bold_M.getHeight();
        int z = bold_M.getWidth();
        int t = bold_n.getLength();

        var builder_bold_S = new Matrix.Builder<WriteIn>(N, z);
        var builder_bold_T = new IntMatrix.Builder(N, z);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var bold_v = bold_V.getRow(i);
            var bold_e = GetEligibility.run(bold_v, bold_n);
            int k = 1;
            for (int j = 1; j <= t; j++) {
                int e_j = bold_e.getValue(j);
                int z_j = bold_z.getValue(j);
                if (e_j == 1 && z_j == 1) {
                    int k_j = bold_k.getValue(j);
                    for (int l = 1; l <= k_j; l++) {
                        if (k > z) {
                            throw new RuntimeException();
                        }
                        var m_ik = bold_M.getValue(i, k);
                        var S_ik = GetDecodedStrings.run(m_ik, params.A_W, params.ell_W, params.c_W, params);
                        builder_bold_S.setValue(i, k, S_ik);
                        builder_bold_T.setValue(i, k, j);
                        k = k + 1;
                    }
                }
            }
            while (k <= z) {
                builder_bold_S.setValue(i, k, null);
                builder_bold_T.setValue(i, k, 0); // due to int data type of t_ik, we use 0 instead of null to indicate empty entries
                k = k + 1;
            }
        }
        var bold_S = builder_bold_S.build();
        var bold_T = builder_bold_T.build();
        return new Pair<>(bold_S, bold_T);
    }

}
