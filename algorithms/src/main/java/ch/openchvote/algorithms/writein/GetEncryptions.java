/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GetDefaultEligibility;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.model.common.Query;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.model.writein.Ballot;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.17
 */
public class GetEncryptions {

    public static Vector<AugmentedEncryption> run(SearchableList<Ballot> B, SearchableList<Confirmation> C, IntVector bold_n, IntVector bold_k, IntMatrix bold_E, IntVector bold_w, IntVector bold_z, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(B, C, bold_n, bold_k, bold_E, bold_w, bold_z, params);

        int N_E = bold_E.getHeight();
        int t = bold_E.getWidth();

        Precondition.check(IntSet.NN_plus(N_E).containsAll(B.getKeys()));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q)).containsAll(B.getValues().map(Ballot::get_bold_a)));
        Precondition.check(Set.Pair(Set.Vector(params.GG_q), params.GG_q).containsAll(B.getValues().map(Ballot::get_e_prime)));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(C.getKeys()));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.NN_plus, N_E).contains(bold_w));

        Precondition.check(bold_k.isLess(bold_n));

        var builder_bold_e = new Vector.Builder<AugmentedEncryption>();

        // ALGORITHM
        int n = Math.intSum(bold_n);
        int w = Math.intMax(bold_w);
        int z_max = Math.intMax(bold_E.getRows().mapToInt(bold_e_i -> Math.intSumProd(bold_e_i, bold_k, bold_z)));
        var bold_E_star = GetDefaultEligibility.run(bold_w, bold_E);
        var bold_p = GetPrimes.run(n + w, params);
        int i = 0;
        for (Pair<Integer, Ballot> b : B) {
            int v = b.getFirst();
            var alpha = b.getSecond();
            if (C.contains(v)) {
                int c = bold_w.getValue(v);
                var a = bold_p.getValue(n + c);
                int n_prime = 0;
                for (int l = 1; l <= t; l++) {
                    int z_l = bold_z.getValue(l);
                    if (z_l == 0 && bold_E.getValue(v, l) < bold_E_star.getValue(c, l)) {
                        for (int j = n_prime + 1; j <= n_prime + bold_k.getValue(l); j++) {
                            a = Mod.multiply(a, bold_p.getValue(j));
                        }
                    }
                    n_prime = n_prime + bold_n.getValue(l);
                }
                var bold_a1 = alpha.get_bold_a().map(Query::get_a_1);
                var bold_a2 = alpha.get_bold_a().map(Query::get_a_2);
                var e_prime = alpha.get_e_prime();
                var bold_a_prime = e_prime.get_bold_a();
                int z_prime = bold_a_prime.getLength();
                var builder_bold_a_prime = new Vector.Builder<QuadraticResidue>(z_max);
                for (int l = 1; l <= z_max; l++) {
                    if (l <= z_prime) {
                        builder_bold_a_prime.setValue(l, bold_a_prime.getValue(l));
                    } else {
                        builder_bold_a_prime.setValue(l, QuadraticResidue.ONE);
                    }
                }
                bold_a_prime = builder_bold_a_prime.build();
                var b_prime = e_prime.get_b();
                i = i + 1;
                var e_i = new AugmentedEncryption(Mod.multiply(a, Mod.prod(bold_a1)), Mod.prod(bold_a2), bold_a_prime, b_prime);
                builder_bold_e.setValue(i, e_i);
                if (!a.equals(bold_p.getValue(n + c))) {
                    i = i + 1;
                    var bold_one = new Vector.Builder<QuadraticResidue>(z_max).fill(QuadraticResidue.ONE).build();
                    e_i = new AugmentedEncryption(Mod.multiply(bold_p.getValue(0), a), QuadraticResidue.ONE, bold_one, QuadraticResidue.ONE);
                    builder_bold_e.setValue(i, e_i);
                }
            }
        }
        var bold_e = builder_bold_e.build();
        var bold_e_prime = Vector.sort(bold_e);
        return bold_e_prime;
    }

}
