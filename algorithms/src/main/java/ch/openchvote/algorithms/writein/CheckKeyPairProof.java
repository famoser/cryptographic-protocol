/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.writein.KeyPairProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.5
 */
public class CheckKeyPairProof {

    public static boolean run(KeyPairProof pi, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(pi, pk, bold_pk_prime, params);

        int z = bold_pk_prime.getLength();

        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Vector(params.ZZ_q, z + 1)).contains(pi));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));

        var c = pi.get_c();
        var bold_s = pi.get_bold_s();
        var builder_bold_t = new Vector.Builder<QuadraticResidue>(0, z);

        // ALGORITHM
        var s_0 = bold_s.getValue(0);
        var t_0 = Mod.multiply(Mod.pow(pk, c), Mod.pow(params.g, s_0));
        builder_bold_t.setValue(0, t_0);
        for (int i = 1; i <= z; i++) {
            var s_i = bold_s.getValue(i);
            var pk_prime_i = bold_pk_prime.getValue(i);
            var t_i = Mod.multiply(Mod.pow(pk_prime_i, c), Mod.pow(params.g, s_i));
            builder_bold_t.setValue(i, t_i);
        }
        var bold_t = builder_bold_t.build();
        var y = new Pair<>(pk, bold_pk_prime);
        var c_prime = GetChallenge.run(y, bold_t, params);
        return c.equals(c_prime);
    }

}
