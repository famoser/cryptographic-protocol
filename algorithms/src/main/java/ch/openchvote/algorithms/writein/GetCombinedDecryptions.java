/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.25
 */
public class GetCombinedDecryptions {

    public static Pair<Vector<QuadraticResidue>, Matrix<QuadraticResidue>> run(Matrix<QuadraticResidue> bold_C, Vector<Matrix<QuadraticResidue>> bold_d_arrow, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_C, bold_d_arrow, params);

        var N = bold_C.getHeight();
        var s = bold_d_arrow.getLength();

        Precondition.check(IntSet.NN_plus.contains(s));

        var bold_D_1 = bold_d_arrow.getValue(1);
        var z = bold_D_1.getWidth();

        Precondition.check(Set.Matrix(params.GG_q, N, z).contains(bold_D_1));
        Precondition.check(Set.Matrix(params.GG_q, N, s).contains(bold_C));
        Precondition.check(Set.Vector(Set.Matrix(params.GG_q, N, z), s).contains(bold_d_arrow));

        var builder_bold_c = new Vector.Builder<QuadraticResidue>(N);
        var builder_bold_D = new Matrix.Builder<QuadraticResidue>(N, z);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var bold_c_i = bold_C.getRow(i);
            var c_i = Mod.prod(bold_c_i);
            builder_bold_c.setValue(i, c_i);
            for (int j = 1; j <= z; j++) {
                int i_final = i; // effectively final variables needed
                int j_final = j; // ... in subsequent lambda expression
                var bold_d_ij = bold_d_arrow.map(matrix -> matrix.getValue(i_final, j_final));
                var d_ij = Mod.prod(bold_d_ij);
                builder_bold_D.setValue(i, j, d_ij);
            }
        }
        var bold_c = builder_bold_c.build();
        var bold_D = builder_bold_D.build();
        return new Pair<>(bold_c, bold_D);
    }

}
