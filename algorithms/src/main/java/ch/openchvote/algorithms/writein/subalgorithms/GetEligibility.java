/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.util.IntVector;

/**
 * ALGORITHM 9.29
 */
public class GetEligibility {

    public static IntVector run(IntVector bold_v, IntVector bold_n) {

        // PREPARATION
        int t = bold_n.getLength();
        var builder_bold_e = new IntVector.Builder(t);

        // ALGORITHM
        int n_prime = 0;
        for (int j = 1; j <= t; j++) {
            int e_j = 0;
            int n_j = bold_n.getValue(j);
            for (int i = n_prime + 1; i <= n_prime + n_j; i++) {
                int v_i = bold_v.getValue(i);
                if (v_i == 1) {
                    e_j = 1;
                }
            }
            n_prime = n_prime + n_j;
            builder_bold_e.setValue(j, e_j);
        }
        var bold_e = builder_bold_e.build();
        return bold_e;
    }

}
