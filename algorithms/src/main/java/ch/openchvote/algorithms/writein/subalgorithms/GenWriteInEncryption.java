/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.model.writein.MultiEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 9.10
 */
public class GenWriteInEncryption {

    public static Pair<MultiEncryption, BigInteger> run(Vector<QuadraticResidue> bold_pk, Vector<QuadraticResidue> bold_m, Parameters params) {

        // PREPARATION
        int z = bold_m.getLength();
        var builder_bold_a = new Vector.Builder<QuadraticResidue>(z);

        // ALGORITHM
        var r = GenRandomInteger.run(params.q);
        for (int i = 1; i <= z; i++) {
            var a_i = Mod.multiply(bold_m.getValue(i), Mod.pow(bold_pk.getValue(i), r));
            builder_bold_a.setValue(i, a_i);
        }
        var bold_a = builder_bold_a.build();
        var b = Mod.pow(params.g, r);
        var e = new MultiEncryption(bold_a, b);
        return new Pair<>(e, r);
    }

}
