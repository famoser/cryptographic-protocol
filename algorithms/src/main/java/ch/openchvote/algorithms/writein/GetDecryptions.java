/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.writein.subalgorithms.GetPartialDecryption;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 9.22
 */
public class GetDecryptions {

    public static Pair<Vector<QuadraticResidue>, Matrix<QuadraticResidue>> run(Vector<AugmentedEncryption> bold_e, BigInteger sk, Vector<BigInteger> bold_sk_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_e, sk, bold_sk_prime, params);

        var N = bold_e.getLength();
        var z = bold_sk_prime.getLength();

        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(params.ZZ_q.contains(sk));
        Precondition.check(Set.Vector(params.ZZ_q, z).contains(bold_sk_prime));

        var builder_bold_c = new Vector.Builder<QuadraticResidue>(N);
        var builder_bold_C_prime = new Matrix.RowBuilder<QuadraticResidue>(N, z);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var e_i = bold_e.getValue(i);
            var pair = GetPartialDecryption.run(e_i, sk, bold_sk_prime, params);
            var c_i = pair.getFirst();
            var bold_c_prime_i = pair.getSecond();
            builder_bold_c.setValue(i, c_i);
            builder_bold_C_prime.setRow(i, bold_c_prime_i);
        }
        var bold_c = builder_bold_c.build();
        var bold_C_prime = builder_bold_C_prime.build();
        return new Pair<>(bold_c, bold_C_prime);
    }

}
