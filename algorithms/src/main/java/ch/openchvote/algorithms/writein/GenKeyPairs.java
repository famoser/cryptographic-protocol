/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Quadruple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.3
 */
public class GenKeyPairs {

    public static Quadruple<BigInteger, QuadraticResidue, Vector<BigInteger>, Vector<QuadraticResidue>> run(IntVector bold_k, IntMatrix bold_E, IntVector bold_z, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_k, bold_E, bold_z, params);

        int t = bold_k.getLength();
        int N_E = bold_E.getHeight();

        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));

        // ALGORITHM
        var sk = GenRandomInteger.run(params.q);
        var pk = Mod.pow(params.g, sk);
        var z_max = Math.intMax(bold_E.getRows().mapToInt(bold_e_i -> Math.intSumProd(bold_e_i, bold_k, bold_z)));
        var builder_bold_sk_prime = new Vector.Builder<BigInteger>(z_max);
        var builder_bold_pk_prime = new Vector.Builder<QuadraticResidue>(z_max);
        for (int i = 1; i <= z_max; i++) {
            var sk_prime_i = GenRandomInteger.run(params.q);
            var pk_prime_i = Mod.pow(params.g, sk_prime_i);
            builder_bold_sk_prime.setValue(i, sk_prime_i);
            builder_bold_pk_prime.setValue(i, pk_prime_i);
        }
        var bold_sk = builder_bold_sk_prime.build();
        var bold_pk = builder_bold_pk_prime.build();
        return new Quadruple<>(sk, pk, bold_sk, bold_pk);
    }

}
