/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

/**
 * ALGORITHM 9.26
 */
public class GetVotes {

    public static Pair<Vector<QuadraticResidue>, Matrix<QuadraticResidue>> run(Vector<AugmentedEncryption> bold_e, Vector<QuadraticResidue> bold_c, Vector<QuadraticResidue> bold_c_prime, Matrix<QuadraticResidue> bold_D, Matrix<QuadraticResidue> bold_D_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_e, bold_c, bold_c_prime, bold_D, bold_D_prime, params);

        var N = bold_e.getLength();
        var z = bold_D.getWidth();

        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c));
        Precondition.check(Set.Vector(params.GG_q, N).contains(bold_c_prime));
        Precondition.check(Set.Matrix(params.GG_q, N, z).contains(bold_D));
        Precondition.check(Set.Matrix(params.GG_q, N, z).contains(bold_D_prime));

        var builder_bold_m = new Vector.Builder<QuadraticResidue>(N);
        var builder_bold_M = new Matrix.Builder<QuadraticResidue>(N, z);

        // ALGORITHM
        for (int i = 1; i <= N; i++) {
            var a_i = bold_e.getValue(i).get_a();
            var c_i = bold_c.getValue(i);
            var c_prime_i = bold_c_prime.getValue(i);
            var m_i = Mod.multiply(a_i, Mod.invert(Mod.multiply(c_i, c_prime_i)));
            builder_bold_m.setValue(i, m_i);
            var bold_a_prime = bold_e.getValue(i).get_bold_a_prime();
            for (int j = 1; j <= z; j++) {
                var a_prime_ij = bold_a_prime.getValue(j);
                var d_ij = bold_D.getValue(i, j);
                var d_prime_ij = bold_D_prime.getValue(i, j);
                var m_ij = Mod.multiply(a_prime_ij, Mod.invert(Mod.multiply(d_ij, d_prime_ij)));
                builder_bold_M.setValue(i, j, m_ij);
            }
        }
        var bold_m = builder_bold_m.build();
        var bold_M = builder_bold_M.build();
        return new Pair<>(bold_m, bold_M);
    }

}
