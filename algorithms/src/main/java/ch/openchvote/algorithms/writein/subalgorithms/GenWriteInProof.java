/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.writein.GetEncodedStrings;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.common.Query;
import ch.openchvote.model.writein.MultiEncryption;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.model.writein.WriteInProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.12
 */
public class GenWriteInProof {

    public static WriteInProof run(QuadraticResidue pk, Vector<QuadraticResidue> bold_m, Vector<Query> bold_a, Vector<BigInteger> bold_r, Vector<QuadraticResidue> bold_pk_prime, Vector<QuadraticResidue> bold_m_prime, MultiEncryption e_prime, BigInteger r_prime, Vector<QuadraticResidue> bold_p, Parameters params) {

        // PREPARATION
        int z = bold_m.getLength();
        var bold_a_prime = e_prime.get_bold_a();
        var b_prime = e_prime.get_b();
        var builder_bold_Y_star = new Matrix.Builder<Triple<QuadraticResidue, QuadraticResidue, Encryption>>(z, 2);
        var builder_bold_r_star = new Vector.Builder<BigInteger>(z);
        var builder_bold_j = new IntVector.Builder(z);

        // ALGORITHM
        var epsilon = GetEncodedStrings.run(WriteIn.EMPTY, params.A_W, params.ell_W, params.c_W, params);
        for (int i = 1; i <= z; i++) {
            var p_i = bold_p.getValue(i);
            builder_bold_Y_star.setValue(i, 1, new Triple<>(pk, p_i, new Encryption(bold_a.getValue(i).get_a_1(), bold_a.getValue(i).get_a_2())));
            builder_bold_Y_star.setValue(i, 2, new Triple<>(bold_pk_prime.getValue(i), epsilon, new Encryption(bold_a_prime.getValue(i), b_prime)));
            if (bold_m.getValue(i).equals(p_i)) {
                builder_bold_r_star.setValue(i, bold_r.getValue(i));
                builder_bold_j.setValue(i, 1);
            } else if (bold_m_prime.getValue(i).equals(epsilon)) {
                builder_bold_r_star.setValue(i, r_prime);
                builder_bold_j.setValue(i, 2);
            } else {
                throw new AlgorithmException(WriteInProof.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
            }
        }
        var bold_y_star = builder_bold_Y_star.build();
        var bold_r_star = builder_bold_r_star.build();
        var bold_j = builder_bold_j.build();
        var pi = GenCNFProof.run(bold_y_star, bold_r_star, bold_j, params);
        return pi;
    }

}
