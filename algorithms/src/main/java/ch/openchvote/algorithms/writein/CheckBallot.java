/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.CheckBallotProof;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.algorithms.writein.subalgorithms.CheckWriteInProof;
import ch.openchvote.algorithms.writein.subalgorithms.GetWriteInIndices;
import ch.openchvote.model.writein.Ballot;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 9.14
 */
public class CheckBallot {

    public static boolean run(int v, Ballot alpha, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, IntVector bold_n, IntVector bold_k, IntMatrix bold_E, IntVector bold_z, IntVector bold_v, Vector<BigInteger> bold_x_hat, SearchableList<Ballot> B, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(v, alpha, pk, bold_pk_prime, bold_n, bold_k, bold_E, bold_z, bold_v, bold_x_hat, B, params);

        var x_hat = alpha.get_x_hat();
        var bold_a = alpha.get_bold_a();
        var pi = alpha.get_pi();
        var e_prime = alpha.get_e_prime();
        var pi_prime = alpha.get_pi_prime();

        var N_E = bold_E.getHeight();
        var t = bold_E.getWidth();
        int k = bold_a.getLength();
        var z = e_prime.get_bold_a().getLength();
        int z_max = bold_pk_prime.getLength();
        int n = bold_v.getLength();

        Precondition.check(IntSet.NN_plus(N_E).contains(v));
        Precondition.check(params.GG_q_hat.contains(x_hat));
        Precondition.check(Set.Vector(Set.Pair(params.GG_q, params.GG_q), k).contains(bold_a));
        Precondition.check(Set.Pair(params.ZZ_twoToTheTau, Set.Triple(params.ZZ_q_hat, params.GG_q, params.ZZ_q)).contains(pi));
        Precondition.check(Set.Pair(Set.Vector(params.GG_q, z), params.GG_q).contains(e_prime));
        Precondition.check(Set.Pair(Set.Matrix(params.ZZ_twoToTheTau, z, 2), Set.Matrix(params.ZZ_q, z, 2)).contains(pi_prime));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z_max).contains(bold_pk_prime));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntMatrix(IntSet.BB, N_E, t).contains(bold_E));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));
        Precondition.check(Set.IntVector(IntSet.BB, n).contains(bold_v));
        // for improved efficiency, we do not perform exact group membership tests for all values of bold_x_hat
        Precondition.check(Set.Vector(params.ZZ_star_p_hat, N_E).contains(bold_x_hat) && params.GG_q_hat.contains(bold_x_hat.getValue(v)));
        Precondition.check(IntSet.NN_plus(N_E).containsAll(B.getKeys()));

        Precondition.check(n == Math.intSum(bold_n));
        Precondition.check(bold_k.isLess(bold_n));
        Precondition.check(z_max == Math.intMax(bold_E.getRows().mapToInt(bold_e_i -> Math.intSumProd(bold_e_i, bold_k, bold_z))));

        // ALGORITHM
        int k_prime = Math.intSumProd(bold_E.getRow(v), bold_k);
        int z_prime = Math.intSumProd(bold_E.getRow(v), bold_k, bold_z);
        var x_hat_v = bold_x_hat.getValue(v);
        if (!B.contains(v) && x_hat.equals(x_hat_v) && k == k_prime && z == z_prime) {
            if (CheckBallotProof.run(pi, x_hat, bold_a, pk, params)) {
                var bold_p = GetPrimes.run(n, params);
                bold_pk_prime = bold_pk_prime.select(IntSet.range(1, z));
                var bold_e_v = bold_E.getRow(v);
                var pair = GetWriteInIndices.run(bold_n, bold_k, bold_e_v, bold_z, bold_v);
                var I = pair.getFirst();
                var bold_a_I = bold_a.select(I);
                var J = pair.getSecond();
                var bold_p_J = bold_p.select(J);
                return CheckWriteInProof.run(pi_prime, pk, bold_a_I, bold_pk_prime, e_prime, bold_p_J, params);
            }
        }
        return false;
    }

}
