/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Alphabet;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 9.1
 */
public class GetEncodedStrings {

    public static QuadraticResidue run(WriteIn S, Alphabet A, int ell, char c, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(S, A, params);

        Precondition.check(Set.Pair(Set.String(A, 0, ell), Set.String(A, 0, ell)).contains(S));
        Precondition.check(A.getSize() >= 2);
        Precondition.check(ell >= 0);
        Precondition.check(params.ZZ_q.contains(BigInteger.valueOf(A.getSize()).add(BigInteger.ONE).pow(2 * ell)));
        Precondition.check(!A.contains(c));

        // ALGORITHM
        var S_1 = S.get_S_1();
        while (S_1.length() < ell) {
            S_1 = c + S_1;
        }
        var S_2 = S.get_S_2();
        while (S_2.length() < ell) {
            S_2 = c + S_2;
        }
        var S12 = S_1 + S_2;
        var x = ToInteger.run(S12, A.addCharacter(c)).add(BigInteger.ONE);
        var y = Mod.multiply(x, x, params.p);
        return new QuadraticResidue(y, x, params.p);
    }

}
