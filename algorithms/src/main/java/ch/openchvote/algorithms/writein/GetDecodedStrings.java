/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.ToString;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Alphabet;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 9.2
 */
public class GetDecodedStrings {

    public static WriteIn run(QuadraticResidue y, Alphabet A, int ell, char c, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(y, A, params);

        Precondition.check(A.getSize() >= 2);
        Precondition.check(params.GG_q.contains(y));
        Precondition.check(IntSet.NN.contains(ell));
        Precondition.check(params.ZZ_q.contains(BigInteger.valueOf(A.getSize()).add(BigInteger.ONE).pow(2 * ell)));
        Precondition.check(!A.contains(c));

        // ALGORITHM
        var x = Mod.pow(y.getValue(), params.q.add(BigInteger.ONE).shiftRight(1), params.p);
        x = x.min(params.p.subtract(x));
        var S12 = ToString.run(x.subtract(BigInteger.ONE), 2 * ell, A.addCharacter(c));
        var S1 = S12.substring(0, ell);
        while (S1.length() > 0 && S1.charAt(0) == c) {
            S1 = S1.substring(1);
        }
        var S2 = S12.substring(ell, 2 * ell);
        while (S2.length() > 0 && S2.charAt(0) == c) {
            S2 = S2.substring(1);
        }
        var S = new WriteIn(S1, S2);
        return S;
    }

}
