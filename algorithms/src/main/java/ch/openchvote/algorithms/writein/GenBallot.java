/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenBallotProof;
import ch.openchvote.algorithms.common.subalgorithms.GenQuery;
import ch.openchvote.algorithms.common.subalgorithms.GetEncodedSelections;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.algorithms.general.ToInteger;
import ch.openchvote.algorithms.writein.subalgorithms.GenWriteInEncryption;
import ch.openchvote.algorithms.writein.subalgorithms.GenWriteInProof;
import ch.openchvote.algorithms.writein.subalgorithms.GetEncodedWriteIns;
import ch.openchvote.algorithms.writein.subalgorithms.GetWriteInIndices;
import ch.openchvote.model.writein.Ballot;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.*;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 9.8
 */
public class GenBallot {

    public static Pair<Ballot, Vector<BigInteger>> run(String X, IntVector bold_s, Vector<WriteIn> bold_s_prime, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, IntVector bold_n, IntVector bold_k, IntVector bold_e, int w, IntVector bold_z, IntVector bold_v, int z_max, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(X, bold_s, bold_s_prime, pk, bold_pk_prime, bold_n, bold_k, bold_e, bold_z, bold_v, params);

        int k = bold_s.getLength();
        int z_prime = bold_s_prime.getLength();
        int t = bold_n.getLength();
        int n = bold_v.getLength();

        Precondition.check(Set.String(params.A_X, params.ell_X).contains(X));
        Precondition.check(Set.IntVector(IntSet.NN_plus(n), k).contains(bold_s));
        Precondition.check(Set.Vector(Set.Pair(Set.String(params.A_W, 0, params.ell_W), Set.String(params.A_W, 0, params.ell_W)), z_prime).contains(bold_s_prime));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z_max).contains(bold_pk_prime));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_n));
        Precondition.check(Set.IntVector(IntSet.NN_plus, t).contains(bold_k));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_e));
        Precondition.check(IntSet.NN_plus.contains(w));
        Precondition.check(Set.IntVector(IntSet.BB, t).contains(bold_z));
        Precondition.check(Set.IntVector(IntSet.BB, n).contains(bold_v));
        Precondition.check(IntSet.NN.contains(z_max));

        int z = Math.intSumProd(bold_k, bold_z);

        Precondition.check(bold_s.isSorted());
        Precondition.check(n == Math.intSum(bold_n));
        Precondition.check(bold_k.isLess(bold_n));
        Precondition.check(k == Math.intSumProd(bold_e, bold_k));
        Precondition.check(z_prime == Math.intSumProd(bold_e, bold_k, bold_z));
        Precondition.check(z_prime <= z_max && z_max <= z);

        // ALGORITHM
        var x = ToInteger.run(X, params.A_X);
        var x_hat = Mod.pow(params.g_hat, x, params.p_hat);
        var bold_p = GetPrimes.run(n + w, params);
        var bold_m = GetEncodedSelections.run(bold_s, bold_p);
        var m = Math.prod(bold_m.map(QuadraticResidue::getValue));
        if (bold_p.getValue(n + w).getValue().multiply(m).compareTo(params.p) >= 0) {
            throw new AlgorithmException(GenBallot.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
        }
        var pair1 = GenQuery.run(bold_m, pk, params);
        var bold_a = pair1.getFirst();
        var bold_r = pair1.getSecond();
        var r = Mod.sum(bold_r, params.q);
        var pi = GenBallotProof.run(x, Mod.prod(bold_m), r, x_hat, bold_a, pk, params);
        var bold_m_prime = GetEncodedWriteIns.run(bold_s_prime, params);
        var pair2 = GenWriteInEncryption.run(bold_pk_prime, bold_m_prime, params);
        var e_prime = pair2.getFirst();
        var r_prime = pair2.getSecond();
        var pair3 = GetWriteInIndices.run(bold_n, bold_k, bold_e, bold_z, bold_v);
        var I = pair3.getFirst();
        var bold_m_I = bold_m.select(I);
        var bold_a_I = bold_a.select(I);
        var bold_r_I = bold_r.select(I);
        var J = pair3.getSecond();
        var bold_p_J = bold_p.select(J);
        var pi_prime = GenWriteInProof.run(pk, bold_m_I, bold_a_I, bold_r_I, bold_pk_prime, bold_m_prime, e_prime, r_prime, bold_p_J, params);
        var alpha = new Ballot(x_hat, bold_a, pi, e_prime, pi_prime);
        return new Pair<>(alpha, bold_r);
    }

}
