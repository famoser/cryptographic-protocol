/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.GenRandomInteger;
import ch.openchvote.algorithms.general.GetChallenge;
import ch.openchvote.model.writein.KeyPairProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * ALGORITHM 9.4
 */
public class GenKeyPairProof {

    public static KeyPairProof run(BigInteger sk, QuadraticResidue pk, Vector<BigInteger> bold_sk_prime, Vector<QuadraticResidue> bold_pk_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(sk, pk, bold_sk_prime, bold_pk_prime, params);

        var z = bold_sk_prime.getLength();

        Precondition.check(params.ZZ_q.contains(sk));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.ZZ_q, z).contains(bold_sk_prime));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));

        var builder_bold_omega = new Vector.Builder<BigInteger>(0, z);
        var builder_bold_t = new Vector.Builder<QuadraticResidue>(0, z);
        var builder_bold_s = new Vector.Builder<BigInteger>(0, z);

        // ALGORITHM
        for (int i = 0; i <= z; i++) {
            var omega_i = GenRandomInteger.run(params.q);
            var t_i = Mod.pow(params.g, omega_i);
            builder_bold_omega.setValue(i, omega_i);
            builder_bold_t.setValue(i, t_i);
        }
        var bold_omega = builder_bold_omega.build();
        var bold_t = builder_bold_t.build();
        var y = new Pair<>(pk, bold_pk_prime);
        var c = GetChallenge.run(y, bold_t, params);
        var omega_0 = bold_omega.getValue(0);
        var s_0 = Mod.subtract(omega_0, Mod.multiply(c, sk, params.q), params.q);
        builder_bold_s.setValue(0, s_0);
        for (int i = 1; i <= z; i++) {
            var omega_i = bold_omega.getValue(i);
            var sk_i = bold_sk_prime.getValue(i);
            var s_i = Mod.subtract(omega_i, Mod.multiply(c, sk_i, params.q), params.q);
            builder_bold_s.setValue(i, s_i);
        }
        var bold_s = builder_bold_s.build();
        var pi = new KeyPairProof(c, bold_s);
        return pi;
    }

}
