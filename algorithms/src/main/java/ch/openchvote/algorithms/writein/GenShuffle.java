/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.common.subalgorithms.GenPermutation;
import ch.openchvote.algorithms.writein.subalgorithms.GenReEncryption;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Quadruple;

import java.math.BigInteger;

/**
 * ALGORITHM 9.18
 */
public class GenShuffle {

    public static Quadruple<Vector<AugmentedEncryption>, Vector<BigInteger>, Vector<BigInteger>, IntVector> run(Vector<AugmentedEncryption> bold_e, QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(bold_e, pk, bold_pk_prime, params);

        int N = bold_e.getLength();
        int z = bold_pk_prime.getLength();

        Precondition.check(Set.Vector(Set.Quadruple(params.GG_q, params.GG_q, Set.Vector(params.GG_q, z), params.GG_q), N).contains(bold_e));
        Precondition.check(params.GG_q.contains(pk));
        Precondition.check(Set.Vector(params.GG_q, z).contains(bold_pk_prime));

        var builder_bold_e_tilde = new Vector.Builder<AugmentedEncryption>(N);
        var builder_bold_r_tilde = new Vector.Builder<BigInteger>(N);
        var builder_bold_r_tilde_prime = new Vector.Builder<BigInteger>(N);

        // ALGORITHM
        var psi = GenPermutation.run(N);
        for (int i = 1; i <= N; i++) {
            int j_i = psi.getValue(i);
            var triple = GenReEncryption.run(bold_e.getValue(j_i), pk, bold_pk_prime, params);
            builder_bold_e_tilde.setValue(i, triple.getFirst());
            builder_bold_r_tilde.setValue(j_i, triple.getSecond());
            builder_bold_r_tilde_prime.setValue(j_i, triple.getThird());
        }
        var bold_e_tilde = builder_bold_e_tilde.build();
        var bold_r_tilde = builder_bold_r_tilde.build();
        var bold_r_tilde_prime = builder_bold_r_tilde_prime.build();
        return new Quadruple<>(bold_e_tilde, bold_r_tilde, bold_r_tilde_prime, psi);
    }

}
