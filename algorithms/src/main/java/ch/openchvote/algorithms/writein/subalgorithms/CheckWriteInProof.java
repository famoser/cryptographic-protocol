/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.writein.subalgorithms;

import ch.openchvote.algorithms.writein.GetEncodedStrings;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.model.common.Query;
import ch.openchvote.model.writein.MultiEncryption;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.model.writein.WriteInProof;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

/**
 * ALGORITHM 9.15
 */
public class CheckWriteInProof {

    public static boolean run(WriteInProof pi, QuadraticResidue pk, Vector<Query> bold_a, Vector<QuadraticResidue> bold_pk_prime, MultiEncryption e_prime, Vector<QuadraticResidue> bold_p, Parameters params) {

        // PREPARATION
        var z = bold_a.getLength();
        var builder_bold_Y_star = new Matrix.Builder<Triple<QuadraticResidue, QuadraticResidue, Encryption>>(z, 2);
        var bold_a_prime = e_prime.get_bold_a();
        var b_prime = e_prime.get_b();

        // ALGORITHM
        var epsilon = GetEncodedStrings.run(WriteIn.EMPTY, params.A_W, params.ell_W, params.c_W, params);
        for (int i = 1; i <= z; i++) {
            var a_i = bold_a.getValue(i);
            var y_star_i_1 = new Triple<>(pk, bold_p.getValue(i), new Encryption(a_i.get_a_1(), a_i.get_a_2())); // type conversion from query to encryption
            var y_star_i_2 = new Triple<>(bold_pk_prime.getValue(i), epsilon, new Encryption(bold_a_prime.getValue(i), b_prime));
            builder_bold_Y_star.setValue(i, 1, y_star_i_1);
            builder_bold_Y_star.setValue(i, 2, y_star_i_2);
        }
        var bold_Y_star = builder_bold_Y_star.build();
        var b = CheckCNFProof.run(pi, bold_Y_star, params);
        return b;
    }

}
