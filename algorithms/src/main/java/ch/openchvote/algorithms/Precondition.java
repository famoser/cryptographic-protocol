/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms;

/**
 * This class provides two static methods for performing precondition tests with respect to algorithm parameters. The
 * actual precondition tests are performed independently of this class, only the resulting boolean values are passed to
 * the method {@link Precondition#check(boolean)}. The goal of this method is to collect the results of all possible
 * precondition tests in one single place and to throw the same type of {@link AlgorithmException}.
 *
 */
public class Precondition {

    /**
     * Throws an {@link AlgorithmException} if the given boolean {@code b} is false.
     * @param b A boolean value representing the result of a precondition test
     */
    public static void check(boolean b) {
        Precondition.check(b, AlgorithmException.Type.PRECONDITION_FAILED);
    }

    /**
     * Throws an {@link AlgorithmException} if the given array of objects or one of the objects in the given array is
     * {@code null}.
     */
    public static void checkNotNull(Object... objects) {
        Precondition.check(objects != null, AlgorithmException.Type.NULL_POINTER);
        for (Object object : objects) {
            Precondition.check(object != null, AlgorithmException.Type.NULL_POINTER);
        }
    }

    // private helper method for throwing an exception in case of a failed precondition
    private static void check(boolean b, AlgorithmException.Type exceptionType) {
        if (!b) {
            var algorithmName = new Exception().getStackTrace()[2].getClassName(); // gets the name of the calling class
            throw new AlgorithmException(algorithmName, exceptionType);
        }
    }

}
