/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.math.Math;

/**
 * ALGORITHM 4.1
 */
public class SetWatermark {

    public static ByteArray run(ByteArray B, int m, int n) {

        // PREPARATION
        Precondition.checkNotNull(B);

        int b = 8 * B.getLength();
        int l = Math.bitLength(n - 1);

        Precondition.check(0 <= m && m < n);
        Precondition.check(l <= b);

        // ALGORITHM
        for (int j = 0; j <= l - 1; j++) {
            var i = (j * b) / l;
            B = SetBit.run(B, i, m % 2);
            m = m / 2;
        }
        return B;
    }

}
