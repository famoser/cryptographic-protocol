/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.util.math.Math;
import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.tuples.Sextuple;

import java.math.BigInteger;

/**
 * ALGORITHM 10.1
 */
public class GetGroupParameters {

    public static final String EULER = "B7E151628AED2A6ABF7158809CF4F3C762E7160F38B4DA56A784D9045190CFEF324E7738926CFBE5F4BF8D8D8C31D763DA06C80ABB1185EB4F7C7B5757F5958490CFD47D7C19BB42158D9554F7B46BCED55C4D79FD5F24D6613C31C3839A2DDF8A9A276BCFBFA1C877C56284DAB79CD4C2B3293D20E9E5EAF02AC60ACC93ED874422A52ECB238FEEE5AB6ADD835FD1A0753D0A8F78E537D2B95BB79D8DCAEC642C1E9F23B829B5C2780BF38737DF8BB300D01334A0D0BD8645CBFA73A6160FFE393C48CBBBCA060F0FF8EC6D31BEB5CCEED7F2F0BB088017163BC60DF45A0ECB1BCD289B06CBBFEA21AD08E1847F3F7378D56CED94640D6EF0D3D37BE67008E186D1BF275B9B241DEB64749A47DFDFB96632C3EB061B6472BBF84C26144E49C2D04C324EF10DE513D3F5114B8B5D374D93CB8879C7D52FFD72BA0AAE7277DA7BA1B4AF1488D8E836AF14865E6C37AB6876FE690B571121382AF341AFE94F77BCF06C83B8FF5675F0979074AD9A787BC5B9BD4B0C5937D3EDE4C3A79396215EDA";

    public static Sextuple<BigInteger, BigInteger, BigInteger, BigInteger, BigInteger, BigInteger> run(int s, int t, int kappa) {

        // PREPARATION
        Precondition.check(0 < t && t < s && s <= 4 * EULER.length());

        // ALGORITHM
        var p = new BigInteger(EULER.substring(0, s / 4 + 1), 16).shiftRight(4 - s % 4).setBit(0);
        var p_hat = p;
        while (!p.isProbablePrime(128) || !Math.div2(p).isProbablePrime(kappa)) {
            p = p.add(BigInteger.TWO);
        }
        var q = Math.div2(p);

        var q_hat = new BigInteger(EULER.substring(0, t / 4 + 1), 16).shiftRight(4 - t % 4).setBit(0);
        while (!q_hat.isProbablePrime(kappa)) {
            q_hat = q_hat.add(BigInteger.TWO);
        }
        var k = p_hat.clearBit(0).divide(q_hat);
        p_hat = k.multiply(q_hat).add(k.testBit(0) ? q_hat : BigInteger.ZERO).add(BigInteger.ONE);
        while (!p_hat.isProbablePrime(kappa)) {
            p_hat = p_hat.add(Math.mult2(q_hat));
        }
        var k_hat = p_hat.clearBit(0).divide(q_hat);
        var g_hat = BigInteger.TWO.modPow(k_hat, p_hat);
        return new Sextuple<>(p, q, p_hat, q_hat, k_hat, g_hat);
    }

    public static void main(String[] args) {
        var params = run(2048, 224, 112);
        System.out.println("p = " + params.getFirst().toString(16).toUpperCase());
        System.out.println("q = " + params.getSecond().toString(16).toUpperCase());
        System.out.println("p_hat = " + params.getThird().toString(16).toUpperCase());
        System.out.println("q_hat = " + params.getFourth().toString(16).toUpperCase());
        System.out.println("k_hat = " + params.getFifth().toString(16).toUpperCase());
        System.out.println("g_hat = " + params.getSixth().toString(16).toUpperCase());
    }

}
