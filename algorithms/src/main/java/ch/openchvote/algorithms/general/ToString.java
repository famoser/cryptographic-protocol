/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.Alphabet;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Math;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

/**
 * ALGORITHMS 4.7, 4.9, 4.10
 */
public class ToString {

    /**
     * ALGORITHM 4.7
     */
    public static String run(BigInteger x, int n, Alphabet A) {

        // PREPARATION
        Precondition.checkNotNull(x, A);

        Precondition.check(Set.NN.contains(x));
        Precondition.check(IntSet.NN.contains(n));

        var N = BigInteger.valueOf(A.getSize());
        Precondition.check(x.compareTo(N.pow(n)) < 0);

        // PREPARATION
        var builder_S = new StringBuilder(n);
        builder_S.setLength(n); // ensures that the StringBuilder can be filled up backwards

        // ALGORITHM
        for (int i = 1; i <= n; i++) {
            builder_S.setCharAt(n - i, A.getChar(x.mod(N).intValue()));
            x = x.divide(N);
        }
        var S = builder_S.toString();
        return S;
    }

    /**
     * ALGORITHM 4.9
     */
    public static String run(ByteArray B, Alphabet A) {

        // PREPARATION
        Precondition.checkNotNull(B, A);

        int n = B.getLength();

        Precondition.check(Set.B(n).contains(B));

        // ALGORITHM
        int N = A.getSize();
        var x_B = ToInteger.run(B);
        var m = Math.ceilLog(BigInteger.TWO.pow(8 * n), N);
        var S = ToString.run(x_B, m, A);
        return S;
    }

    /**
     * ALGORITHM 4.10
     */
    public static String run(ByteArray B) {

        // PREPARATION
        Precondition.checkNotNull(B);
        Precondition.check(Set.UTF8.contains(B));

        // ALGORITHM
        var S = new String(B.toByteArray(), StandardCharsets.UTF_8);
        return S;
    }

}
