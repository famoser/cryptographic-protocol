/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Set;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 8.3
 */
public class GetGenerators {

    public static Vector<QuadraticResidue> run(int n, String U, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(U, params);

        Precondition.check(IntSet.NN.contains(n));
        Precondition.check(Set.UCS_star.contains(U));

        var builder_bold_h = new Vector.Builder<QuadraticResidue>(n);
        BigInteger h_i, h_i_square;

        // ALGORITHM
        for (int i = 1; i <= n; i++) {
            int x = 0;
            do {
                x = x + 1;
                h_i = ToInteger.run(RecHash.run(params.hashAlgorithm, U, "ggen", i, x)).mod(params.p);
                h_i_square = Mod.multiply(h_i, h_i, params.p);
            } while (h_i.equals(BigInteger.ZERO) || h_i.equals(BigInteger.ONE));
            builder_bold_h.setValue(i, new QuadraticResidue(h_i_square, h_i, params.p));
        }
        var bold_h = builder_bold_h.build();
        return bold_h;
    }

}
