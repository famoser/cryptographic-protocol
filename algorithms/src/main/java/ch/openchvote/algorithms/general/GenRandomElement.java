/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;
import java.util.Set;

/**
 * ALGORITHM 4.14
 */
public class GenRandomElement {

    public static final Set<BigInteger> ZERO = Set.of(BigInteger.ZERO);

    public static QuadraticResidue run(Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(params);

        // ALGORITHM
        var r = GenRandomInteger.run(params.p, ZERO);
        var r_prime = Mod.multiply(r, r, params.p);
        return new QuadraticResidue(r_prime, r, params.p);
    }

}
