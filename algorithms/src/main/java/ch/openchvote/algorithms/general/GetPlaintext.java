/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.general.Ciphertext;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Mod;

import java.math.BigInteger;

/**
 * ALGORITHM 8.61
 */
public class GetPlaintext {

    public static ByteArray run(BigInteger sk, Ciphertext e, Parameters params) {

        var AES = params.symmetricEncryption; // AES-GCM-128
        int K_length = AES.getKeyLength(); // 16 for AES-GCM-128
        int IV_length = AES.getIVLength(); // 12 for AES-GCM-128

        // PREPARATION
        Precondition.checkNotNull(sk, e, params);

        Precondition.check(params.ZZ_q_hat.contains(sk));
        Precondition.check(Set.Triple(params.GG_q_hat, Set.B(IV_length), Set.B_star).contains(e));

        var ek = e.get_ek();
        var IV = e.get_IV();
        var C = e.get_C();

        // ALGORITHM
        var K = RecHash.run(params.hashAlgorithm, Mod.pow(ek, sk, params.p_hat)).truncate(K_length);
        var M = AES.decrypt(K, IV, C);
        return M;
    }

}
