/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.general.Signature;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.59
 */
public class CheckSignature {

    public static boolean run(Signature sigma, BigInteger pk, Object m, Object aux, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(pk, m, sigma, params);

        Precondition.check(params.ZZ_twoToTheTau.contains(sigma.get_c()));
        Precondition.check(params.ZZ_q_hat.contains(sigma.get_s()));
        Precondition.check(params.GG_q_hat.contains(pk));

        var c = sigma.get_c();
        var s = sigma.get_s();

        // ALGORITHM
        var t = Mod.multiply(Mod.pow(pk, c, params.p_hat), Mod.pow(params.g_hat, s, params.p_hat), params.p_hat);
        var y = (aux == null) ? new Pair<>(pk, m) : new Triple<>(pk, m, aux);
        var c_prime = GetChallenge.run(y, t, params);
        return c.equals(c_prime);
    }

}
