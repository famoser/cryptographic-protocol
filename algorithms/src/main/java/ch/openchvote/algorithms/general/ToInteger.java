/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.Alphabet;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Math;

import java.math.BigInteger;

/**
 * ALGORITHMS 4.5, 4.8
 */
public class ToInteger {

    /**
     * ALGORITHM 4.5
     */
    public static BigInteger run(ByteArray B) {

        // PREPARATION
        Precondition.checkNotNull(B);
        Precondition.check(Set.B_star.contains(B));

        // ALGORITHM
        var x = BigInteger.ZERO;
        for (int i = 0; i <= B.getLength() - 1; i++) {
            x = Math.mult256(x).add(BigInteger.valueOf(B.getInteger(i)));
        }
        return x;
    }

    /**
     * ALGORITHM 4.8
     */
    public static BigInteger run(String S, Alphabet A) {

        // PREPARATION
        Precondition.checkNotNull(S, A);
        Precondition.check(Set.String(A).contains(S));

        // ALGORITHM
        BigInteger N = BigInteger.valueOf(A.getSize());
        var x = BigInteger.ZERO;
        for (int i = 0; i <= S.length() - 1; i++) {
            x = x.multiply(N).add(BigInteger.valueOf(A.getRank(S.charAt(i))));
        }
        return x;
    }

}
