/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.Precondition;
import ch.openchvote.algorithms.general.subalgorithms.IsPrime;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * ALGORITHM 8.1 & 8.2
 */
public class GetPrimes {

    /**
     * ALGORITHM 8.1
     */
    public static Vector<QuadraticResidue> run(int n, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(params);

        Precondition.check(IntSet.NN.contains(n));

        var builder_p_hat = new Vector.Builder<QuadraticResidue>(0, n);

        // ALGORITHM
        var x = BigInteger.ONE;
        for (int i = 0; i <= n; i++) {
            do {
                if (x.equals(BigInteger.ONE) || x.equals(BigInteger.TWO)) {
                    x = x.add(BigInteger.ONE);
                } else {
                    x = x.add(BigInteger.TWO);
                }
                if (x.compareTo(params.p) >= 0) {
                    throw new AlgorithmException(GetPrimes.class, AlgorithmException.Type.INCOMPATIBLE_MATRIX);
                }
            } while (!(IsPrime.run(x.intValueExact()) && Math.jacobiSymbol(x, params.p) == 1));
            builder_p_hat.setValue(i, new QuadraticResidue(x, params.p));
        }
        var p_hat = builder_p_hat.build();
        return p_hat;
    }

}

