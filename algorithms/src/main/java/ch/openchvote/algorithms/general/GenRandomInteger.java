/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Math;

import java.math.BigInteger;

/**
 * ALGORITHMS 4.11, 4.12, 4.13
 */
public class GenRandomInteger {

    /**
     * ALGORITHM 4.11
     */
    public static BigInteger run(BigInteger q) {

        // PREPARATION
        Precondition.check(Set.NN_plus.contains(q));
        BigInteger r;

        // ALGORITHM
        int ell = q.subtract(BigInteger.ONE).bitLength();
        int L = Math.ceilDiv(ell, 8);
        do {
            var R = RandomBytes.run(L);
            for (int i = ell; i <= 8 * L - 1; i++) {
                R = SetBit.run(R, i, 0);
            }
            r = ToInteger.run(R);
        } while (r.compareTo(q) >= 0);
        return r;
    }

    /**
     * ALGORITHM 4.11 (version for int values)
     */
    public static int run(int q) {
        return run(BigInteger.valueOf(q)).intValue();
    }

    /**
     * ALGORITHM 4.12
     */
    public static BigInteger run(BigInteger q, java.util.Set<BigInteger> X) {

        // PREPARATION
        Precondition.checkNotNull(q, X);

        Precondition.check(Set.NN_plus.contains(q));
        Precondition.check(Set.ZZ(q).containsAll(X));

        BigInteger r;

        // ALGORITHM
        do {
            r = GenRandomInteger.run(q);
        } while (X.contains(r));
        return r;
    }

    /**
     * ALGORITHM 4.13 (version for int values)
     */
    public static int run(int a, int b) {

        // PREPARATION
        Precondition.check(a <= b);

        // ALGORITHM
        int r_prime = run(b - a + 1);
        int r = a + r_prime;
        return r;
    }

}
