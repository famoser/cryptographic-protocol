/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.util.ByteArray;
import ch.openchvote.util.crypto.Entropy;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;

/**
 * This class implements a static method {@link RandomBytes#run(int)}} for generating random byte arrays of a given
 * length. Internally, the class uses an instance of the standard Java class {@link SecureRandom}, which is instantiated
 * with the {@code SHA1PRNG} pseudorandom number generator (PRNG). Initially, the PRNG is seeded using the predefined
 * entropy gathering mechanism of the available {@code SHA1PRNG} implementation. Additional entropy (so-called CPU jitter
 * entropy) is then gathered to complement the initial seed and for reseeding the PRNG. The reseeding is invoked at random
 * intervals.
 */
public class RandomBytes {

    private static final SecureRandom RANDOM_GENERATOR;

    private static final int SEEDING_BITS_OF_ENTROPY = 256; // see https://eprint.iacr.org/2018/349.pdf, page 5
    private static final int RESEEDING_BITS_OF_ENTROPY = 256;
    private static final int RESEEDING_FREQUENCY = 100000;

    static {
        try {
            RANDOM_GENERATOR = SecureRandom.getInstance("SHA1PRNG", "SUN");
            RANDOM_GENERATOR.nextBytes(new byte[1]); // provoke initial seeding
            RANDOM_GENERATOR.setSeed(Entropy.getEntropy(SEEDING_BITS_OF_ENTROPY).toByteArray());
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Exception during SecureRandom initialization", e);
        }
    }

    /**
     * Generates a random byte array of length {@code L}.
     *
     * @param L The desired length of the random byte array
     * @return The random byte array
     */
    public static ByteArray run(int L) {
        if (L < 0)
            throw new IllegalArgumentException("Length must be positive");
        // perform reseeding if necessary on a probabilistic base
        if (RANDOM_GENERATOR.nextInt(RESEEDING_FREQUENCY) == 0) {
            RANDOM_GENERATOR.setSeed(Entropy.getEntropy(RESEEDING_BITS_OF_ENTROPY).toByteArray());
        }
        // generate and return random bytes
        byte[] bytes = new byte[L];
        RANDOM_GENERATOR.nextBytes(bytes);
        return new ByteArray.FromSafeArray(bytes);
    }

}
