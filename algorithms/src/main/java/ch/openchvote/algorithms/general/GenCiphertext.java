/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.general.Ciphertext;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Mod;

import java.math.BigInteger;

/**
 * ALGORITHM 8.60
 */
public class GenCiphertext {

    public static Ciphertext run(BigInteger pk, ByteArray M, Parameters params) {

        var AES = params.symmetricEncryption; // AES-GCM-128
        int K_length = AES.getKeyLength(); // 16 for AES-GCM-128
        int IV_length = AES.getIVLength(); // 12 for AES-GCM-128

        // PREPARATION
        Precondition.checkNotNull(pk, M, params);

        Precondition.check(params.GG_q_hat.contains(pk));
        Precondition.check(Set.B_star.contains(M));

        // ALGORITHM
        var r = GenRandomInteger.run(params.q_hat);
        var ek = Mod.pow(params.g_hat, r, params.p_hat);
        var K = RecHash.run(params.hashAlgorithm, Mod.pow(pk, r, params.p_hat)).truncate(K_length);
        var IV = RandomBytes.run(IV_length);
        var C = AES.encrypt(K, IV, M);
        var c = new Ciphertext(ek, IV, C);
        return c;
    }

}
