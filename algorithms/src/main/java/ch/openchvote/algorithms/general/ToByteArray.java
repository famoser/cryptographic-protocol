/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.Set;
import ch.openchvote.util.math.Math;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;

/**
 * ALGORITHMS 4.3, 4.4, 4.6
 */
public class ToByteArray {

    /**
     * ALGORITHM 4.3
     */
    public static ByteArray run(BigInteger x) {

        // PREPARATION
        Precondition.checkNotNull(x);
        Precondition.check(Set.NN.contains(x));

        // ALGORITHM
        int n_min = Math.ceilDiv(x.bitLength(), 8);
        var B = ToByteArray.run(x, n_min);
        return B;
    }

    /**
     * ALGORITHM 4.4
     */
    public static ByteArray run(BigInteger x, int n) {

        // PREPARATION
        Precondition.checkNotNull(x);

        Precondition.check(Set.NN.contains(x));
        Precondition.check(IntSet.NN.contains(n));
        Precondition.check(Math.ceilDiv(x.bitLength(), 8) <= n);

        var builder_B = new ByteArray.Builder(n);

        // ALGORITHM
        for (int i = 1; i <= n; i++) {
            builder_B.setByte(n - i, Math.mod256(x));
            x = Math.div256(x);
        }
        var B = builder_B.build();
        return B;
    }

    /**
     * ALGORITHM 4.6
     */
    public static ByteArray run(String S) {

        // PREPARATION
        Precondition.checkNotNull(S);
        Precondition.check(Set.UCS_star.contains(S));

        // ALGORITHM
        var bytes = S.getBytes(StandardCharsets.UTF_8);
        return new ByteArray.FromSafeArray(bytes);
    }

}
