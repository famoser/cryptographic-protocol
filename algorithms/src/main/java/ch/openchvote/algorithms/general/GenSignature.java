/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.model.general.Signature;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Triple;

import java.math.BigInteger;

/**
 * ALGORITHM 8.58
 */
public class GenSignature {

    public static Signature run(BigInteger sk, Object m, Object aux, Parameters params) {

        // PREPARATION
        Precondition.checkNotNull(sk, m, params);

        Precondition.check(params.ZZ_q_hat.contains(sk));

        // ALGORITHM
        var pk = Mod.pow(params.g_hat, sk, params.p_hat); // cache pk over multiple calls for improving efficiency
        var omega = GenRandomInteger.run(params.q_hat);
        var t = Mod.pow(params.g_hat, omega, params.p_hat);
        var y = (aux == null) ? new Pair<>(pk, m) : new Triple<>(pk, m, aux);
        var c = GetChallenge.run(y, t, params);
        var s = Mod.subtract(omega, Mod.multiply(c, sk, params.q_hat), params.q_hat);
        var sigma = new Signature(c, s);
        return sigma;
    }

}
