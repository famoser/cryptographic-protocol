/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.util.*;
import ch.openchvote.util.crypto.HashAlgorithm;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Tuple;

import java.math.BigInteger;

/**
 * ALGORITHM 4.15
 */
public class RecHash {

    public static ByteArray run(HashAlgorithm algorithm, Object... values) {

        // ALGORITHM
        if (values == null) {
            return algorithm.hash(ByteArray.EMPTY);
        }
        int k = values.length;
        if (k > 1) {
            var bold_v = Vector.fromSafeArray(values);
            return RecHash.run(algorithm, bold_v);
        }
        var w = values[0];
        if (w == null) {
            return algorithm.hash(ByteArray.EMPTY);
        }
        if (w instanceof ByteArray) {
            var w_cast = (ByteArray) w;
            return algorithm.hash(w_cast);
        }
        if (w instanceof Integer) {
            int w_cast = (int) w;
            if (w_cast >= 0) {
                return algorithm.hash(ToByteArray.run(BigInteger.valueOf(2 * w_cast)));
            } else {
                return algorithm.hash(ToByteArray.run(BigInteger.valueOf(-2 * w_cast - 1)));
            }
        }
        if (w instanceof BigInteger) {
            var w_cast = (BigInteger) w;
            if (w_cast.signum() >= 0) {
                return algorithm.hash(ToByteArray.run(Math.mult2(w_cast)));
            } else {
                return algorithm.hash(ToByteArray.run(Math.mult2(w_cast.abs()).subtract(BigInteger.ONE)));
            }
        }
        if (w instanceof QuadraticResidue) {
            var w_cast = (QuadraticResidue) w;
            return algorithm.hash(ToByteArray.run(w_cast.getValue()));
        }
        if (w instanceof String) {
            var w_cast = (String) w;
            return algorithm.hash(ToByteArray.run(w_cast));
        }
        if (w instanceof Tuple) {
            var w_cast = (Tuple) w;
            ByteArray H = w_cast.getHashValue(algorithm);
            if (H == null) {
                H = RecHash.run(algorithm, w_cast.toVector());
                w_cast.putHashValue(algorithm, H);
            }
            return H;
        }
        if (w instanceof Vector) {
            var w_cast = (Vector<?>) w;
            ByteArray H = w_cast.getHashValue(algorithm);
            if (H == null) {
                H = algorithm.hash(ByteArray.concatenate(w_cast.map(x -> RecHash.run(algorithm, x), RecHash.run(algorithm, ByteArray.EMPTY))));
                w_cast.putHashValue(algorithm, H);
            }
            return H;
        }
        if (w instanceof IntVector) {
            var w_cast = (IntVector) w;
            ByteArray H = w_cast.getHashValue(algorithm);
            if (H == null) {
                H = algorithm.hash(ByteArray.concatenate(w_cast.mapToObj(x -> RecHash.run(algorithm, x))));
                w_cast.putHashValue(algorithm, H);
            }
            return H;
        }
        if (w instanceof Matrix) {
            var w_cast = (Matrix<?>) w;
            ByteArray H = w_cast.getHashValue(algorithm);
            if (H == null) {
                H = RecHash.run(algorithm, w_cast.getRows());
                w_cast.putHashValue(algorithm, H);
            }
            return H;
        }
        if (w instanceof IntMatrix) {
            var w_cast = (IntMatrix) w;
            ByteArray H = w_cast.getHashValue(algorithm);
            if (H == null) {
                H = RecHash.run(algorithm, w_cast.getRows());
                w_cast.putHashValue(algorithm, H);
            }
            return H;
        }
        throw new AlgorithmException(RecHash.class, AlgorithmException.Type.UNSUPPORTED_HASH_TYPE);
    }

}
