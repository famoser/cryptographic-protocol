/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.algorithms.general;

import ch.openchvote.algorithms.Precondition;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.IntSet;
import ch.openchvote.util.math.Math;

/**
 * ALGORITHM 4.2
 */
public class SetBit {

    public static ByteArray run(ByteArray B, int i, int b) {

        // PREPARATION
        Precondition.checkNotNull(B);

        Precondition.check(IntSet.ZZ(8 * B.getLength()).contains(i));
        Precondition.check(IntSet.BB.contains(b));

        byte z;

        // ALGORITHM
        int j = i / 8;
        int x = Math.powerOfTwo(i % 8);
        if (b == 0) {
            z = Math.and(B.getByte(j), Math.intToByte(255 - x));
        } else {
            z = Math.or(B.getByte(j), Math.intToByte(x));
        }
        B = B.setByte(j, z);
        return B;
    }

}
