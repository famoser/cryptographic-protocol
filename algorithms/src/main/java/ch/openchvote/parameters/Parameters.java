/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.parameters;

import ch.openchvote.util.Alphabet;
import ch.openchvote.util.Set;
import ch.openchvote.util.crypto.HashAlgorithm;
import ch.openchvote.util.crypto.SymmetricEncryption;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

/**
 * Objects of this class represent a full set of system and security parameters from Section 6.3. The goal of providing
 * all parameters in a single instance of this class is to maximize the convenience in accessing the parameters in
 * the algorithm and protocol implementations. For enabling the shortest possible access, all instance variables are
 * declared as {@code public} and {@code final}. In this way, no getter method are required to access the parameters.
 */
public class Parameters {

    public static final java.util.Set<Integer> SECURITY_LEVELS = java.util.Set.of(0, 1, 2, 3);

    // security parameters
    public final int sigma;
    public final int tau;
    public final double epsilon;
    // group parameters
    public final BigInteger p, q;
    public final BigInteger p_hat, q_hat;
    // generators
    public final QuadraticResidue g, h;
    public final BigInteger g_hat;
    // hash algorithm and length of hash values
    public final HashAlgorithm hashAlgorithm;
    public final int L;
    public final int ell;
    // length of OT messages
    public final int L_M;
    // voting and confirmation credential upper bounds
    public final BigInteger q_hat_x, q_hat_y;
    // maximal number of candidates;
    public final int n_max;
    // alphabets
    public final Alphabet A_X, A_Y, A_R, A_FA;
    // lengths of codes (characters)
    public final int ell_X, ell_Y, ell_R, ell_FA;
    // lengths of codes (bytes)
    public final int L_R, L_FA;
    // symmetric encryption scheme
    public final SymmetricEncryption symmetricEncryption;
    // groups
    public final Set<BigInteger> ZZ_q;
    public final Set<BigInteger> ZZ_q_hat;
    public final Set<BigInteger> ZZ_q_hat_x;
    public final Set<BigInteger> ZZ_q_hat_y;
    public final Set<QuadraticResidue> GG_q;
    public final Set<BigInteger> GG_q_hat;
    public final Set<BigInteger> ZZ_star_p_hat;
    public final Set<BigInteger> ZZ_twoToTheTau;
    // write-in alphabet
    public final Alphabet A_W;
    // write-in padding character
    public final char c_W;
    // length of write-ins
    public final int ell_W;

    // general constructor for default alphabet
    public Parameters(int lambda) {
        this(lambda, Alphabet.BASE16);
    }

    // general constructor for a single given alphabet
    public Parameters(int lambda, Alphabet A) {
        this(lambda, A, A, A, A);
    }

    // general constructor for different alphabets
    public Parameters(int lambda, Alphabet A_X, Alphabet A_Y, Alphabet A_R, Alphabet A_FA) {
        this(lambda, A_X, A_Y, A_R, A_FA, Alphabet.ALPHANUMERIC, '|', 80);
    }

    // general constructor for different alphabets (including write-in alphabets)
    public Parameters(int lambda, Alphabet A_X, Alphabet A_Y, Alphabet A_R, Alphabet A_FA, Alphabet A_W, char c_W, int ell_W) {
        if (lambda >= SecurityLevel.values().length) {
            throw new IllegalArgumentException();
        }
        var securityLevel = SecurityLevel.values()[lambda];
        this.sigma = securityLevel.sigma;
        this.tau = securityLevel.tau;
        this.epsilon = securityLevel.epsilon;
        this.p = securityLevel.p;
        this.q = securityLevel.q;
        this.g = securityLevel.g;
        this.h = securityLevel.h;
        this.p_hat = securityLevel.p_hat;
        this.q_hat = securityLevel.q_hat;
        this.g_hat = securityLevel.g_hat;
        this.hashAlgorithm = securityLevel.hashAlgorithm;
        this.L = this.hashAlgorithm.getLength();
        this.ell = 8 * this.L;
        this.q_hat_x = BigInteger.ONE.shiftLeft(2 * this.tau - 1);
        this.q_hat_y = BigInteger.ONE.shiftLeft(2 * this.tau - 1);
        this.n_max = 1678; // see remark in Subsection 11.1.2
        this.A_X = A_X;
        this.A_Y = A_Y;
        this.A_R = A_R;
        this.A_FA = A_FA;
        this.L_M = 2 * Math.ceilDiv(this.q_hat.bitLength(), 8);
        this.L_R = Math.ceilLog(BigInteger.valueOf((int) ((this.n_max - 1.0) / (1.0 - this.epsilon))), 256);
        this.L_FA = Math.ceilLog(BigInteger.valueOf((int) (1.0 / (1.0 - this.epsilon))), 256);
        this.ell_X = Math.floorLog(this.q_hat_x, A_X.getSize()) + 1;
        this.ell_Y = Math.floorLog(this.q_hat_y, A_Y.getSize()) + 1;
        this.ell_R = Math.ceilLog(BigInteger.TWO.pow(8 * this.L_R), A_R.getSize());
        this.ell_FA = Math.ceilLog(BigInteger.TWO.pow(8 * this.L_FA), A_FA.getSize());
        this.symmetricEncryption = securityLevel.symmetricEncryption;
        this.ZZ_q = Set.ZZ(this.q);
        this.ZZ_q_hat = Set.ZZ(this.q_hat);
        this.ZZ_q_hat_x = Set.ZZ(this.q_hat_x);
        this.ZZ_q_hat_y = Set.ZZ(this.q_hat_y);
        this.GG_q = Set.GG(this.p);
        this.GG_q_hat = Set.GG(this.p_hat, this.q_hat);
        this.ZZ_star_p_hat = Set.ZZ_star(this.p_hat);
        this.ZZ_twoToTheTau = Set.ZZ_powerOfTwo(this.tau);
        this.A_W = A_W;
        this.c_W = c_W;
        this.ell_W = ell_W;
    }

}

