/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Quadruple;

import java.math.BigInteger;

public class VotingCardData extends Quadruple<BigInteger, BigInteger, ByteArray, Vector<Pair<BigInteger, BigInteger>>> {

    public VotingCardData(BigInteger x, BigInteger y, ByteArray A, Vector<Pair<BigInteger, BigInteger>> bold_p) {
        super(x, y, A, bold_p);
    }

    public BigInteger get_x() {
        return getFirst();
    }

    public BigInteger get_y() {
        return getSecond();
    }

    public ByteArray get_A() {
        return getThird();
    }

    public Vector<Pair<BigInteger, BigInteger>> get_bold_p() {
        return getFourth();
    }

}
