/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Sextuple;

public class VotingCard extends Sextuple<Integer, String, String, Vector<String>, String, String> {

    public VotingCard(Integer v, String X, String Y, Vector<String> bold_rc, String FC, String AC) {
        super(v, X, Y, bold_rc, FC, AC);
    }

    public Integer get_v() {
        return getFirst();
    }

    public String get_X() {
        return getSecond();
    }

    public String get_Y() {
        return getThird();
    }

    public Vector<String> get_bold_rc() {
        return getFourth();
    }

    public String get_FC() {
        return getFifth();
    }

    public String get_AC() {
        return getSixth();
    }
}
