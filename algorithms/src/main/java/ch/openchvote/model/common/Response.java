/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

public class Response extends Triple<Vector<QuadraticResidue>, Matrix<ByteArray>, QuadraticResidue> {

    public Response(Vector<QuadraticResidue> bold_b, Matrix<ByteArray> bold_C, QuadraticResidue d) {
        super(bold_b, bold_C, d);
    }

    public Vector<QuadraticResidue> get_bold_b() {
        return getFirst();
    }

    public Matrix<ByteArray> get_bold_C() {
        return getSecond();
    }

    public QuadraticResidue get_d() {
        return getThird();
    }

}
