/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Nonuple;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

public class ElectorateData extends Nonuple<Vector<ByteArray>, Vector<VotingCardData>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, Matrix<Pair<BigInteger, BigInteger>>> {

    public ElectorateData(Vector<ByteArray> bold_a, Vector<VotingCardData> bold_d, Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, Vector<BigInteger> bold_z, Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, Matrix<Pair<BigInteger, BigInteger>> bold_P) {
        super(bold_a, bold_d, bold_x, bold_y, bold_z, bold_x_hat, bold_y_hat, bold_z_hat, bold_P);
    }

    public Vector<ByteArray> get_bold_a() {
        return getFirst();
    }

    public Vector<VotingCardData> get_bold_d() {
        return getSecond();
    }

    public Vector<BigInteger> get_bold_x() {
        return getThird();
    }

    public Vector<BigInteger> get_bold_y() {
        return getFourth();
    }

    public Vector<BigInteger> get_bold_z() {
        return getFifth();
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return getSixth();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return getSeventh();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return getEighth();
    }

    public Matrix<Pair<BigInteger, BigInteger>> get_bold_P() {
        return getNinth();
    }
}
