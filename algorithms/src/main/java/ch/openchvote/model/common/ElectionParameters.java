/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.IntMatrix;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;

/**
 * The purpose of this interface is to provide a common API for both types of election parameters (plain, write-ins).
 * It contains the union of all methods for making it as widely applicable as possible. This interface is mainly needed
 * for testing purposes.
 */
public interface ElectionParameters {

    String get_U();

    Vector<String> get_bold_c();

    Vector<String> get_bold_d();

    Vector<String> get_bold_e();

    IntVector get_bold_n();

    IntVector get_bold_k();

    IntMatrix get_bold_E();

    IntVector get_bold_w();

    IntVector get_bold_z(); // only needed for write-in elections

    IntVector get_bold_v(); // only needed for write-in elections

}
