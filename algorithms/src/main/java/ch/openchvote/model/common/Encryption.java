/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

public class Encryption extends Pair<QuadraticResidue, QuadraticResidue> implements Comparable<Encryption> {

    public Encryption(QuadraticResidue a, QuadraticResidue b) {
        super(a, b);
    }

    public QuadraticResidue get_a() {
        return getFirst();
    }

    public QuadraticResidue get_b() {
        return getSecond();
    }

    @Override
    public int compareTo(Encryption other) {
        int c = this.get_a().compareTo(other.get_a());
        return c == 0 ? this.get_b().compareTo(other.get_b()) : c;
    }

}
