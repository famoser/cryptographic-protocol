/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.util.IntMatrix;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Matrix;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static ch.openchvote.util.math.Math.*;

/**
 * The purpose of this interface is to provide a common API for both types of election results (plain, write-ins). It
 * contains the union of all methods for making it as widely applicable as possible. This interface is mainly needed
 * for testing purposes.
 */
public interface ElectionResult {

    IntVector get_bold_u();

    IntMatrix get_bold_V();

    IntMatrix get_bold_W();

    Matrix<WriteIn> get_bold_S(); // only needed for write-in elections

    IntMatrix get_bold_T(); // only needed for write-in elections

    /**
     * Returns the number of votes for candidate k in counting circle c.
     *
     * @param k Candidate index
     * @param c Counting circle
     * @return Number of votes for candidate k in counting circle c
     */
    default int getNumberOfVotes(int k, int c) {
        var bold_v_k = this.get_bold_V().getCol(k);
        var bold_w_c = this.get_bold_W().getCol(c);
        return intSumProd(this.get_bold_u(), bold_v_k, bold_w_c);
    }

    /**
     * Returns the number of votes for candidate k.
     *
     * @param k Candidate index
     * @return Number of votes for candidate k
     */
    default int getNumberOfVotes(int k) {
        var bold_v_k = this.get_bold_V().getCol(k);
        return intSumProd(this.get_bold_u(), bold_v_k);
    }

    /**
     * Returns the total number of votes in counting circle c.
     *
     * @param c Counting circle
     * @return Total number of votes in counting circle c
     */
    default int getTotalNumberOfVotes(int c) {
        var bold_w_c = this.get_bold_W().getCol(c);
        return intSumProd(this.get_bold_u().map(u_i -> u_i + 1), bold_w_c) / 2;
    }

    /**
     * Returns the total number of votes.
     *
     * @return Total number of votes
     */
    default int getTotalNumberOfVotes() {
        return intSum(this.get_bold_u().map(u_i -> u_i + 1)) / 2;
    }

    default Set<Integer> getWriteInElections() {
        return this.get_bold_T().toStream().filter(j -> j > 0).boxed().collect(Collectors.toSet());
    }

    default Set<WriteIn> getWriteIns() {
        var writeIns = new TreeSet<WriteIn>();
        int N = this.get_bold_S().getHeight();
        int z = this.get_bold_S().getWidth();
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= z; k++) {
                var S_ik = this.get_bold_S().getValue(i, k);
                if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                    writeIns.add(S_ik);
                }
            }
        }
        return writeIns;
    }

    /**
     * Returns the set of write-ins submitted to election j.
     *
     * @param j Election index
     * @return The set of write-ins submitted to election j
     */
    default Set<WriteIn> getWriteIns(int j) {
        var writeIns = new TreeSet<WriteIn>();
        int N = this.get_bold_S().getHeight();
        int z = this.get_bold_S().getWidth();
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= z; k++) {
                var t_ik = this.get_bold_T().getValue(i, k);
                if (t_ik == j) {
                    var S_ik = this.get_bold_S().getValue(i, k);
                    if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                        writeIns.add(S_ik);
                    }
                }
            }
        }
        return writeIns;
    }

    default int getNumberOfWriteIns(int j) {
        int result = 0;
        int N = this.get_bold_S().getHeight();
        int z = this.get_bold_S().getWidth();
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= z; k++) {
                var t_ik = this.get_bold_T().getValue(i, k);
                if (t_ik == j) {
                    var S_ik = this.get_bold_S().getValue(i, k);
                    if (S_ik != null && !S_ik.equals(WriteIn.EMPTY)) {
                        result++;
                    }
                }
            }
        }
        return result;
    }

    default int getNumberOfWriteIns(WriteIn S, int c, int j) {
        int result = 0;
        int N = this.get_bold_S().getHeight();
        int z = this.get_bold_S().getWidth();
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= z; k++) {
                var S_ik = this.get_bold_S().getValue(i, k);
                var t_ik = this.get_bold_T().getValue(i, k);
                var w_ic = this.get_bold_W().getValue(i, c);
                if (S.equals(S_ik) && t_ik == j && w_ic == 1) {
                    result = result + 1;
                }
            }
        }
        return result;
    }

    default int getNumberOfWriteIns(WriteIn S, int j) {
        int result = 0;
        int N = this.get_bold_S().getHeight();
        int z = this.get_bold_S().getWidth();
        for (int i = 1; i <= N; i++) {
            for (int k = 1; k <= z; k++) {
                var S_ik = this.get_bold_S().getValue(i, k);
                var t_ik = this.get_bold_T().getValue(i, k);
                if (S.equals(S_ik) && t_ik == j) {
                    result = result + 1;
                }
            }
        }
        return result;
    }

    default String toString(ArrayList<String> stringTable, int width, int indent) {
        var builder = new StringBuilder();
        var cellWidths = new int[width];
        int i = 0;
        for (String str : stringTable) {
            cellWidths[i] = java.lang.Math.max(cellWidths[i], str.length());
            i = (i + 1) % width;
        }
        i = 0;
        for (String str : stringTable) {
            if (i == 0) {
                builder.append("\n");
                builder.append(" ".repeat(indent));
            }
            int cellWidth = cellWidths[i];
            String formatedString;
            if (str.equals("")) {
                if (i == width - 1) {
                    formatedString = "-".repeat(cellWidth);
                } else {
                    formatedString = "-".repeat(cellWidth + 2);
                }
            } else if (str.matches("^[0-9]+$")) {
                formatedString = String.format("%" + cellWidth + "s  ", str);
            } else {
                formatedString = String.format("%-" + cellWidth + "s  ", str);
            }
            builder.append(formatedString);
            i = (i + 1) % width;
        }
        return builder.toString();
    }

}
