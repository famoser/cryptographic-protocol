/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.common;

import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

public class Query extends Pair<QuadraticResidue, QuadraticResidue> {

    public Query(QuadraticResidue a_1, QuadraticResidue a_2) {
        super(a_1, a_2);
    }

    public QuadraticResidue get_a_1() {
        return getFirst();
    }

    public QuadraticResidue get_a_2() {
        return getSecond();
    }

}
