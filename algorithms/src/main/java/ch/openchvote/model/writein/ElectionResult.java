/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.writein;

import ch.openchvote.util.IntMatrix;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.tuples.Quintuple;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ElectionResult extends Quintuple<IntVector, IntMatrix, IntMatrix, Matrix<WriteIn>, IntMatrix> implements ch.openchvote.model.common.ElectionResult {

    public ElectionResult(IntVector bold_u, IntMatrix bold_V, IntMatrix bold_W, Matrix<WriteIn> bold_S, IntMatrix bold_T) {
        super(bold_u, bold_V, bold_W, bold_S, bold_T);
    }

    @Override
    public IntVector get_bold_u() {
        return getFirst();
    }

    @Override
    public IntMatrix get_bold_V() {
        return getSecond();
    }

    @Override
    public IntMatrix get_bold_W() {
        return getThird();
    }

    @Override
    public Matrix<WriteIn> get_bold_S() {
        return getFourth();
    }

    @Override
    public IntMatrix get_bold_T() {
        return getFifth();
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof ElectionResult)) return false;
        var other = (ElectionResult) object;
        int n = this.get_bold_V().getWidth();
        if (other.get_bold_V().getWidth() != n) {
            return false;
        }
        int w = this.get_bold_W().getWidth();
        if (other.get_bold_W().getWidth() != w) {
            return false;
        }
        // we consider two election results as equal if all candidates have received the same amount of votes in every counting circle
        for (int k = 1; k <= n; k++) {
            for (int c = 1; c <= w; c++) {
                if (this.getNumberOfVotes(k, c) != other.getNumberOfVotes(k, c)) {
                    return false;
                }
            }
        }
        // plus if the sets of write-ins are the same and each of them has received the same amount of votes in every counting circle
        var J = this.getWriteInElections();
        if (!other.getWriteInElections().equals(J)) {
            return false;
        }
        for (int j : J) {
            var writeIns = this.getWriteIns(j);
            if (!writeIns.equals(other.getWriteIns(j))) {
                return false;
            }
            for (var writeIn : writeIns) {
                for (int c = 1; c <= w; c++) {
                    if (this.getNumberOfWriteIns(writeIn, c, j) != other.getNumberOfWriteIns(writeIn, c, j)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    @Override
    public String toString() {
        int n = this.get_bold_V().getWidth();
        int w = this.get_bold_W().getWidth();

        // initialize output table
        var stringTable = new ArrayList<String>();
        int rows = w + 2;

        // print table header
        stringTable.add("Candidate");
        for (int c = 1; c <= w; c++) {
            stringTable.add("CC" + c);
        }
        stringTable.add("Total");
        for (int c = 1; c <= rows; c++) {
            stringTable.add("");
        }

        // print main table
        for (int k = 1; k <= n; k++) {
            stringTable.add("Candidate-" + k);
            for (int c = 1; c <= w; c++) {
                stringTable.add("" + this.getNumberOfVotes(k, c));
            }
            stringTable.add("" + this.getNumberOfVotes(k));
        }
        for (int c = 1; c <= rows; c++) {
            stringTable.add("");
        }

        // print write-ins
        for (int j : this.getWriteInElections()) {
            for (var S: this.getWriteIns(j)) {
                stringTable.add(S.toString());
                for (int c = 1; c <= w; c++) {
                    stringTable.add("" + this.getNumberOfWriteIns(S, c, j));
                }
                stringTable.add("" + this.getNumberOfWriteIns(S, j));
            }
            for (int c = 1; c <= rows; c++) {
                stringTable.add("");
            }
        }

        // print table footer
        stringTable.add("Ballots");
        for (int c = 1; c <= w; c++) {
            stringTable.add("" + this.getTotalNumberOfVotes(c));
        }
        stringTable.add("" + this.getTotalNumberOfVotes());
        for (int c = 1; c <= rows; c++) {
            stringTable.add("");
        }

        // convert table to string
        return this.toString(stringTable, rows, 0);
    }

}
