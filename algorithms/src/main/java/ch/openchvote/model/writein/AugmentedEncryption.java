/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.writein;

import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Quadruple;

public class AugmentedEncryption extends Quadruple<QuadraticResidue, QuadraticResidue, Vector<QuadraticResidue>, QuadraticResidue> implements Comparable<AugmentedEncryption> {

    public AugmentedEncryption(QuadraticResidue a, QuadraticResidue b, Vector<QuadraticResidue> bold_a_prime, QuadraticResidue b_prime) {
        super(a, b, bold_a_prime, b_prime);
    }

    public QuadraticResidue get_a() {
        return this.getFirst();
    }

    public QuadraticResidue get_b() {
        return this.getSecond();
    }

    public Vector<QuadraticResidue> get_bold_a_prime() {
        return this.getThird();
    }

    public QuadraticResidue get_b_prime() {
        return this.getFourth();
    }

    @Override
    public int compareTo(AugmentedEncryption other) {
        int c = this.get_a().compareTo(other.get_a());
        if (c == 0) {
            c = this.get_b().compareTo(other.get_b());
        }
        if (c == 0) {
            c = Mod.prod(this.get_bold_a_prime()).compareTo(Mod.prod(other.get_bold_a_prime()));
        }
        if (c == 0) {
            c = this.get_b_prime().compareTo(other.get_b_prime());
        }
        return c;

    }
}
