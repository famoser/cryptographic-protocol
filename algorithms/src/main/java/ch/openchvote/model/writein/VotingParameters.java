/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.writein;

import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Undecuple;

public class VotingParameters extends Undecuple<String, Vector<String>, String, Vector<String>, IntVector, IntVector, IntVector, Integer, IntVector, IntVector, Integer> {

    public VotingParameters(String U, Vector<String> bold_c, String D_v, Vector<String> bold_e, IntVector bold_n, IntVector bold_k, IntVector bold_e_v, Integer w_v, IntVector bold_z, IntVector bold_v, int z_max) {
        super(U, bold_c, D_v, bold_e, bold_n, bold_k, bold_e_v, w_v, bold_z, bold_v, z_max);
    }

    public String get_U() {
        return this.getFirst();
    }

    public Vector<String> get_bold_c() {
        return this.getSecond();
    }

    public String get_D_v() {
        return this.getThird();
    }

    public Vector<String> get_bold_e() {
        return this.getFourth();
    }

    public IntVector get_bold_n() {
        return this.getFifth();
    }

    public IntVector get_bold_k() {
        return this.getSixth();
    }

    public IntVector get_bold_e_v() {
        return this.getSeventh();
    }

    public Integer get_w_v() {
        return this.getEighth();
    }

    public IntVector get_bold_z() {
        return getNinth();
    }

    public IntVector get_bold_v() {
        return getTenth();
    }

    public int get_z_max() {
        return getEleventh();
    }

}
