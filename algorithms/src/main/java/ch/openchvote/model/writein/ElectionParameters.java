/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.writein;

import ch.openchvote.util.IntMatrix;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Decuple;

public class ElectionParameters extends Decuple<String, Vector<String>, Vector<String>, Vector<String>, IntVector, IntVector, IntMatrix, IntVector, IntVector, IntVector> implements ch.openchvote.model.common.ElectionParameters{

    public ElectionParameters(String U, Vector<String> bold_c, Vector<String> bold_d, Vector<String> bold_e, IntVector bold_n, IntVector bold_k, IntMatrix bold_E, IntVector bold_w, IntVector bold_z, IntVector bold_v) {
        super(U, bold_c, bold_d, bold_e, bold_n, bold_k, bold_E, bold_w, bold_z, bold_v);
    }

    @Override
    public String get_U() {
        return this.getFirst();
    }

    @Override
    public Vector<String> get_bold_c() {
        return this.getSecond();
    }

    @Override
    public Vector<String> get_bold_d() {
        return this.getThird();
    }

    @Override
    public Vector<String> get_bold_e() {
        return this.getFourth();
    }

    @Override
    public IntVector get_bold_n() {
        return this.getFifth();
    }

    @Override
    public IntVector get_bold_k() {
        return this.getSixth();
    }

    @Override
    public IntMatrix get_bold_E() {
        return this.getSeventh();
    }

    @Override
    public IntVector get_bold_w() {
        return this.getEighth();
    }

    @Override
    public IntVector get_bold_z() {
        return getNinth();
    }

    @Override
    public IntVector get_bold_v() {
        return getTenth();
    }

}
