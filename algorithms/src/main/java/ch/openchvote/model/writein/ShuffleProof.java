/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.model.writein;

import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;
import ch.openchvote.util.tuples.Quadruple;
import ch.openchvote.util.tuples.Sextuple;

import java.math.BigInteger;

public class ShuffleProof extends Quadruple<BigInteger, Sextuple<BigInteger, BigInteger, BigInteger, Pair<BigInteger, BigInteger>, Vector<BigInteger>, Vector<BigInteger>>, Vector<QuadraticResidue>, Vector<QuadraticResidue>> {

    public ShuffleProof(BigInteger c, Sextuple<BigInteger, BigInteger, BigInteger, Pair<BigInteger, BigInteger>, Vector<BigInteger>, Vector<BigInteger>> s, Vector<QuadraticResidue> bold_c, Vector<QuadraticResidue> bold_c_hat) {
        super(c, s, bold_c, bold_c_hat);
    }

    public BigInteger get_c() {
        return this.getFirst();
    }

    public Sextuple<BigInteger, BigInteger, BigInteger, Pair<BigInteger, BigInteger>, Vector<BigInteger>, Vector<BigInteger>> get_s() {
        return this.getSecond();
    }

    public Vector<QuadraticResidue> get_bold_c() {
        return getThird();
    }

    public Vector<QuadraticResidue> get_bold_c_hat() {
        return getFourth();
    }

}
