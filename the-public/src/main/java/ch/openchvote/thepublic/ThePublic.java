/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.thepublic;

import ch.openchvote.framework.Party;
import ch.openchvote.framework.services.Logger;
import ch.openchvote.model.common.ElectionResult;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.Protocol;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;

/**
 * This class implements the 'The Public' party of the CHVote protocol. It is a direct sub-class of {@link Party}
 * with no particular extensions. The specific role of The Public in the protocol is implemented in the classes
 * {@link ch.openchvote.thepublic.plain.EventData} (plain protocol) and {@link ch.openchvote.thepublic.writein.EventData}
 * (write-in protocol) and in corresponding state and task classes.
 */
public class ThePublic extends Party {

    /**
     * Constructs a new voting client object.
     *
     * @param id   The public's party id
     * @param mode The logger's mode of operation
     */
    public ThePublic(String id, Logger.Mode mode) {
        super(id, PartyType.THE_PUBLIC, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
    }

    public void logElectionResult(ElectionResult ER) {
        this.logger.log(Logger.Level.INFO, ER.toString());
    }

    public ElectionResult getElectionResult(String eventId) {
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);
        var eventSetup = context.getEventSetup();
        var protocolId = eventSetup.getProtocolId();
        var protocol = Protocol.valueOf(protocolId);
        switch (protocol) {
            case PLAIN: {
                var eventData = (ch.openchvote.thepublic.plain.EventData) context.getEventData();
                this.persistenceService.unlockEvent(eventId);
                return eventData.ER.get();
            }
            case WRITEIN: {
                var eventData = (ch.openchvote.thepublic.writein.EventData) context.getEventData();
                this.persistenceService.unlockEvent(eventId);
                return eventData.ER.get();
            }
            default:
                this.persistenceService.unlockEvent(eventId);
                throw new RuntimeException();
        }
    }

}
