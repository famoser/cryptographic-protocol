# The Public

This module provides an implementation of the CHVote party *"The Public"*, which is an imaginary protcol party that receives the signed election result from the adminstrator at the end of the tallying process. 

The intention of this Maven module is to provide Java code that simulates the role of the public's behavior according to the protocol. As this is useful mainly for testing purposes, it will not be part of a real system implementation.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/ThePublic.svg)

## Messages

#### Incoming
Protocol | Message | Sender            | Signed | Encrypted | Content |    |
:-------:|:-------:|:------------------|:------:|:---------:|:--------|:---|
7.9      | MAT1    | Administrator     | yes    | no        | Election result| ![MAT1](img/MAT1.png) |

## Tasks

*not available*