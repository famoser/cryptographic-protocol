# Voter

This module provides an implementation of the CHVote party *"Voter"*. In the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the voter  is described as follows:

> The voters are the actual human users of the system. They are numbered with indices *i* =1,..,*N_E*. Prior to an election event, they receive the voting card from the printing authority, which they can use to cast and confirm a vote during the election period using their voting client.

The intention of this Maven module is to provide Java code that simulates the voter's behavior according to the protocol. As this is useful mainly for testing purposes, this module will not be part of a real system implementation. In a real implementation, essentially the same tasks are executed by real voters.

## Dependencies

The following UML component diagram illustrates the dependencies  to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/Voter.svg)

## Messages

#### Incoming
| Protocol | Message | Sender             | Signed | Encrypted | Content |    |
|:--------:|:--------:|:------------------|:------:|:---------:|:--------|:---|
| 7.3      | MPV1    | Printing authority | no     | no        | Voting card        | ![MPV1](img/MPV1.png) |
| 7.4      | MCV1    | Voting client      | no     | no        | Voting parameters  | ![MCV1](img/MCV1.png) |
| 7.6      | MCV2    | Voting client      | no     | no        | Verification codes | ![MCV2](img/MCV2.png) |
| 7.6      | MCV3    | Voting client      | no     | no        | Finalization codes | ![MCV3](img/MCV3.png) |
| 7.10     | MCV4    | Voting client      | no     | no        | Inspection code    | ![MCV4](img/MCV4.png) |
| –        | MXV1    | Coordinator        | yes    | no        | Start vote casting | – |
| –        | MXV2    | Coordinator        | yes    | no        | Start inspection   | – |

#### Outgoing
| Protocol | Message | Receiver           | Signed | Encrypted | Content |    |
|:--------:|:--------:|:------------------|:------:|:---------:|:--------|:---|
| 7.4      | MVC1    | Voting client      | no     | no        | Voter index               | ![MVC1](img/MVC1.png) |
| 7.4      | MVC2    | Voting client      | no     | no        | Voting code and selection | ![MVC2](img/MVC2.png) |
| 7.6      | MVC3    | Voting client      | no     | no        | Confirmation code         | ![MVC3](img/MVC3.png) |
| –        | MVC4    | Voting client      | no     | no        | Abstention                | – |
| 7.10     | MVC5    | Voting client      | no     | no        | Voter index               | ![MVC5](img/MVC5.png) |
| –        | MVX1    | Coordinator        | no     | no        | Pre-election done         | – |
| –        | MVX2    | Coordinator        | no     | no        | Vote casting done         | – | 
| –        | MVX3    | Coordinator        | no     | no        | Inspection done           | – | 

## Tasks

| Protocol | Task   | State | Description  |
|:--------:|:------:|:-----:|:-------------|
| 7.4      | T1     | S3    | ![TV1](img/T1.png) |
| 7.6      | T2     | S4    | ![TV2](img/T2.png) |
| 7.6      | T3     | S5    | ![TV3](img/T3.png) |
| 7.10     | T4     | S7    | ![TV4](img/T4.png) |