/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.plain.tasks;

import ch.openchvote.algorithms.common.CheckInspectionCode;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.voter.plain.EventData;

public class T4 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var pb = eventData.pb.get();
        var IC = eventData.IC.get();
        var V_c = eventData.VC_v.get();

        // perform task
        var FC_v = V_c.get_FC();
        var AC_v = V_c.get_AC();
        if (!CheckInspectionCode.run(pb, FC_v, AC_v, IC, params)) {
            throw new TaskException(T4.class, TaskException.Type.INSPECTION_CODE_MISMATCH);
        }
    }

}
