/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.plain.states;

import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MVC1;
import ch.openchvote.protocol.message.plain.MVC4;
import ch.openchvote.protocol.message.plain.MVX2;
import ch.openchvote.protocol.message.plain.MXV1;
import ch.openchvote.voter.Voter;
import ch.openchvote.voter.plain.EventData;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S2 extends State<Voter> {

    public S2(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MXV1:
                this.handleMXV1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMXV1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        this.party.checkAndGetContent(MXV1.class, message, eventSetup);

        // define participation bit
        int pb = this.party.getVotingStrategy().getParticipationBit();
        eventData.pb.set(pb);

        // get participant index of active party
        var i = eventSetup.getParticipantIndex(PartyType.VOTER, this.party.getId());

        if (pb == 1) { // vote

            // select event data
            var VC_v = eventData.VC_v.get();
            var v = VC_v.get_v();

            // send MVC1 message to voting client
            this.party.sendMessage(i, new MVC1(v), eventSetup);

            // update state
            context.setCurrentState(S3.class);

        } else { // abstain

            // send MVC4 message to voting client
            this.party.sendMessage(i, new MVC4(), eventSetup);

            // send MVX2 message to coordinator
            this.party.sendMessage(new MVX2(), eventSetup);

            // update state
            context.setCurrentState(S6.class);
        }
    }

}

