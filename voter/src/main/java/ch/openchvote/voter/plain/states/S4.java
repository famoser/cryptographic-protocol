/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.plain.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MCV2;
import ch.openchvote.protocol.message.plain.MVC3;
import ch.openchvote.voter.Voter;
import ch.openchvote.voter.plain.EventData;
import ch.openchvote.voter.plain.tasks.T2;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S4 extends State<Voter> {

    public S4(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCV2:
                this.handleMCV2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCV2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCV2.class, message, eventSetup);
        var bold_rc = messageContent.get_bold_rc();

        // update event data
        eventData.bold_rc.set(bold_rc);

        try {
            // perform task
            T2.run(eventData, params);

            // get participant index of active party
            var i = eventSetup.getParticipantIndex(PartyType.VOTER, this.party.getId());

            // select event data
            var VC_v = eventData.VC_v.get();
            var Y_v = VC_v.get_Y();

            // send MVC3 message to voting client
            this.party.sendMessage(i, new MVC3(Y_v), eventSetup);

            // update state
            context.setCurrentState(S5.class);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E3.class);
        }
    }

}
