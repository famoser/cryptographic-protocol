/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCV1;
import ch.openchvote.protocol.message.writein.MVC2;
import ch.openchvote.voter.Voter;
import ch.openchvote.voter.writein.EventData;
import ch.openchvote.voter.writein.states.E1;
import ch.openchvote.voter.writein.tasks.T1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S3 extends State<Voter> {

    public S3(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCV1:
                this.handleMCV1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCV1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCV1.class, message, eventSetup);
        var VP_v = messageContent.get_VP();

        // define selections
        var votingStrategy = this.party.getVotingStrategy();
        var S = votingStrategy.getSelection(VP_v);
        var bold_s_prime = votingStrategy.getWriteIns(S, VP_v);

        // update event data
        eventData.VP_v.set(VP_v);
        eventData.S.set(S);
        eventData.bold_s_prime.set(bold_s_prime);

        try {
            // perform task
            T1.run(eventData);

            // get participant index of active party
            var i = eventSetup.getParticipantIndex(PartyType.VOTER, this.party.getId());

            // select event data
            var VC_v = eventData.VC_v.get();
            var X_v = VC_v.get_X();
            var bold_s = eventData.bold_s.get();

            // send MVC2 message to voting client
            this.party.sendMessage(i, new MVC2(X_v, bold_s, bold_s_prime), eventSetup);

            // update state
            context.setCurrentState(S4.class);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E1.class);
        }
    }

}
