/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.writein.tasks;

import ch.openchvote.algorithms.common.CheckVerificationCodes;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.voter.writein.EventData;

public class T2 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var bold_rc = eventData.bold_rc.get();
        var bold_s = eventData.bold_s.get();
        var VC_v = eventData.VC_v.get();

        // perform task
        var bold_rc_v = VC_v.get_bold_rc();
        if (!CheckVerificationCodes.run(bold_rc_v, bold_rc, bold_s, params)) {
            throw new TaskException(T2.class, TaskException.Type.VERIFICATION_CODE_MISMATCH);
        }
    }

}
