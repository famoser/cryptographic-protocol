/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.writein;

import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.model.common.VotingCard;
import ch.openchvote.model.writein.VotingParameters;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.voter.writein.states.S1;

import java.util.Set;

public class EventData extends ch.openchvote.framework.context.EventData implements ch.openchvote.voter.common.EventData {

    // selections
    public final Value<Integer> pb = new Value<>();
    public final Value<Set<Integer>> S = new Value<>();
    public final Value<IntVector> bold_s = new Value<>();
    public final Value<Vector<WriteIn>> bold_s_prime = new Value<>();

    // voting card and parameters
    public final Value<VotingCard> VC_v = new Value<>();
    public final Value<VotingParameters> VP_v = new Value<>();

    // encryption randomization
    public final Value<Vector<String>> bold_rc = new Value<>();

    // finalization and inspection code
    public final Value<String> FC = new Value<>();
    public final Value<String> IC = new Value<>();

    @Override
    public Class<S1> getInitialState() {
        return S1.class;
    }

    @Override
    public void init(EventSetup eventSetup, String partyId) {
    }

    @Override
    public int getParticipationBit() {
        return this.pb.get();
    }

    @Override
    public int getVoterIndex() {
        return this.VC_v.get().get_v();
    }

    @Override
    public IntVector getSelections() {
        return this.bold_s.get();
    }

    @Override
    public Vector<WriteIn> getWriteIns() {
        return this.bold_s_prime.get();
    }

}
