/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCV3;
import ch.openchvote.protocol.message.writein.MVX2;
import ch.openchvote.voter.Voter;
import ch.openchvote.voter.writein.EventData;
import ch.openchvote.voter.writein.states.E4;
import ch.openchvote.voter.writein.states.E5;
import ch.openchvote.voter.writein.tasks.T3;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S5 extends State<Voter> {

    public S5(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCV3:
                this.handleMCV3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCV3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCV3.class, message, eventSetup);
        var FC = messageContent.get_FC();

        // update event data
        eventData.FC.set(FC);

        try {
            // perform task
            T3.run(eventData, params);

            // send MVX2 message to coordinator
            this.party.sendMessage(new MVX2(), eventSetup);

            // update state
            context.setCurrentState(S6.class);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E4.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E5.class);
        }
    }

}
