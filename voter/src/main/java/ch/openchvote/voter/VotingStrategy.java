/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter;

import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.RandomFactory;
import ch.openchvote.util.RandomMode;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.Math;

import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

/**
 * Instances of this class can be used by a {@link Voter} to select the candidates in a simulated election event.
 */
public class VotingStrategy {

    private final double p_participation;
    private final Random random;

    /**
     * Constructs a new voting strategy. Its behaviour is mainly determined by the node of operation of the
     * random generator, which is initalized by this constructor.
     *
     * @param p_participation The probability of the voter to participate in the election event
     * @param randomMode      The mode of operation for the random generator
     */
    public VotingStrategy(double p_participation, RandomMode randomMode) {
        this.p_participation = p_participation;
        this.random = RandomFactory.getInstance(randomMode);
    }

    /**
     * Determines a random participation bit, which determines wheter the voter will participate in an election event
     * or not.
     *
     * @return The random participation bit
     */
    public int getParticipationBit() {
        return (this.random.nextDouble() <= this.p_participation) ? 1 : 0;
    }

    /**
     * Returns a set of candidates in a plain election event.
     *
     * @param votingParameters The voter's voting parameters
     * @return The generated set of candidates
     */
    public Set<Integer> getSelection(ch.openchvote.model.plain.VotingParameters votingParameters) {
        return this.getSelection(votingParameters.get_bold_n(), votingParameters.get_bold_k(), votingParameters.get_bold_e_v(), null);
    }

    /**
     * Returns a set of candidates in a write-in election event.
     *
     * @param votingParameters The voter's voting parameters
     * @return The generated set of candidates
     */
    public Set<Integer> getSelection(ch.openchvote.model.writein.VotingParameters votingParameters) {
        return this.getSelection(votingParameters.get_bold_n(), votingParameters.get_bold_k(), votingParameters.get_bold_e_v(), votingParameters.get_bold_v());
    }

    // private helper method for generating the set of candidates
    private Set<Integer> getSelection(IntVector bold_n, IntVector bold_k, IntVector bold_e_v, IntVector bold_v) {
        var S = new TreeSet<Integer>();
        int t = bold_n.getLength();
        int offset = 0;
        for (int j = 1; j <= t; j++) {
            int n_j = bold_n.getValue(j);
            int k_j = bold_k.getValue(j);
            if (bold_e_v.getValue(j) == 1) {
                this.getSelection(S, n_j, k_j, offset, bold_v);
            }
            offset = offset + n_j;
        }
        return S;
    }

    // selecting the candidates in case of write-in elections must guarantee the ordering constraints described in Section 9.1.2.2
    private void getSelection(Set<Integer> S, int n_j, int k_j, int offset, IntVector bold_v) {
        int skipped = 0; // number of write-in candidates to skip
        int k = k_j; // remaining number of selections
        int n = n_j; // remaining number of candidates
        int i = offset + 1; // current candidate index
        while (k > 0) {
            int v_i = (bold_v == null) ? 0 : bold_v.getValue(i);
            if (v_i == 1 && skipped > 0) {
                skipped--;
            } else if (this.random.nextInt(n) < k) {
                S.add(i);
                k--;
                if (v_i == 0) {
                    skipped++;
                }
            }
            i++;
            n--;
        }
    }

    /**
     * Generates a random vector of write-in candidates for a given selection of candidates.
     *
     * @param S The given selection of candidates
     * @param votingParameters The voter's voting parameters
     * @return The generated vector of write-in candidates
     */
    public Vector<WriteIn> getWriteIns(Set<Integer> S, ch.openchvote.model.writein.VotingParameters votingParameters) {
        return this.getWriteIns(S, votingParameters.get_bold_n(), votingParameters.get_bold_k(), votingParameters.get_bold_e_v(), votingParameters.get_bold_v(), votingParameters.get_bold_z());
    }

    // private helper method for generating the write-in candidates
    private Vector<WriteIn> getWriteIns(Set<Integer> S, IntVector bold_n, IntVector bold_k, IntVector bold_e_v, IntVector bold_v, IntVector bold_z) {
        var z_prime = Math.intSumProd(bold_e_v, bold_k, bold_z);
        var builder_bold_s_prime = new Vector.Builder<WriteIn>(z_prime);
        int t = bold_n.getLength();
        int offset = 0;
        for (int j = 1; j <= t; j++) {
            int n_j = bold_n.getValue(j);
            if (bold_z.getValue(j) == 1 && bold_e_v.getValue(j) == 1) {
                for (int i = offset + 1; i <= offset + n_j; i++) {
                    if (S.contains(i)) {
                        if (bold_v.getValue(i) == 1) {
                            builder_bold_s_prime.addValue(new WriteIn(this.getRandomFirstName(), this.getRandomLastName()));
                        } else {
                            builder_bold_s_prime.addValue(WriteIn.EMPTY);
                        }
                    }
                }
            }
            offset = offset + n_j;
        }
        return builder_bold_s_prime.build();
    }

    // private helper method for generating random first names
    private String getRandomFirstName() {
        int i = this.random.nextInt(FIRST_NAMES.length);
        return FIRST_NAMES[i];
    }

    // private helper method for generating random last names
    private String getRandomLastName() {
        int i = this.random.nextInt(LAST_NAMES.length);
        return LAST_NAMES[i];
    }

    // two arrays of exemplary first and last names
    private static final String[] FIRST_NAMES = {"Eileen", "Lorretta", "Merle", "Brooke", "Sana", "Devorah", "Melaine", "Chantell", "Annamae", "Chong", "Toby", "Jeannie", "Genna", "Reita", "Pierre", "Kary", "Marta", "Josephine", "Glenda", "Suzanne", "Robyn", "Oma", "Tresa", "Eustolia", "Delma", "Tonia", "Kathy", "Lavenia", "Darrick", "Abe", "Patsy", "Eleni", "Lester"};
    private static final String[] LAST_NAMES = {"Jarvie", "Schillaci", "Brass", "Koss", "Birdwell", "Bauch", "Lineberger", "Mencer", "Mitchem", "Dews", "Moreland", "Catlin", "Millette", "Clements", "Bastin", "Riter", "Bhatt", "Aichele", "Funnell", "Frazer", "Bordelon", "Huggard", "Auger", "Comiskey", "Dacosta", "Renegar", "Ringler", "Rodarte", "Naval", "Haydel", "Briant"};

}