/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.voter;

import ch.openchvote.framework.Party;
import ch.openchvote.framework.services.Logger;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Triple;
import ch.openchvote.voter.common.EventData;

/**
 * This class implements the 'Voter' party of the CHVote protocol. It is a direct sub-class of {@link Party} with
 * with a single added field {@link Voter#votingStrategy}. This extension allows a voter to select the candidates
 * according to some {@link VotingStrategy}. The specific role of the voter in the protocol is implemented in the
 * classes {@link ch.openchvote.voter.plain.EventData} (plain protocol) and {@link ch.openchvote.voter.writein.EventData}
 * (write-in protocol) and in corresponding state and task classes.
 */
public class Voter extends Party {

    // the voter's voting strategy
    private final VotingStrategy votingStrategy;

    /**
     * Constructs a new instance of this class.
     *
     * @param id             The voter's party id
     * @param votingStrategy The voter's voting strategy
     * @param mode           The logger's mode of operation
     */
    public Voter(String id, VotingStrategy votingStrategy, Logger.Mode mode) {
        super(id, PartyType.VOTER, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
        this.votingStrategy = votingStrategy;
    }

    /**
     * Return the voter's voting strategy.
     *
     * @return The voter's voting stragegy
     */
    public VotingStrategy getVotingStrategy() {
        return this.votingStrategy;
    }

    /**
     * Returns the relevant information of a submitted vote. This method is mainly used
     * for testing purposes.
     *
     * @param eventId The given election event
     * @return The relevant information of a submitted vote
     */
    public Triple<Integer, IntVector, Vector<WriteIn>> getVote(String eventId) {
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);
        var eventData = (EventData) context.getEventData();
        var v = eventData.getVoterIndex();
        var bold_s = eventData.getSelections();
        var bold_s_prime = eventData.getWriteIns();
        this.persistenceService.unlockEvent(eventId);
        return new Triple<>(v, bold_s, bold_s_prime);
    }

    /**
     * Returns the voter's participation bit in an election event. This method is mainly used for testing
     * purposes.
     *
     * @param eventId The given election event
     * @return The voter's participation bit
     */
    public int getParticipationBit(String eventId) {
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);
        var eventData = (ch.openchvote.voter.common.EventData) context.getEventData();
        int pb = eventData.getParticipationBit();
        this.persistenceService.unlockEvent(eventId);
        return pb;
    }

}
