/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util

import ch.openchvote.util.math.QuadraticResidue
import ch.openchvote.util.tuples.*
import spock.lang.Specification

class SerializerSpec extends Specification {


    def "x = deserialize(serialize(x)) should hold for all Integer x"() {
        given:
        def serializer = new Serializer<Integer>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x                     | _
        Integer.MAX_VALUE     | _
        Integer.MIN_VALUE     | _
        Integer.valueOf(0)    | _
        Integer.valueOf(-237) | _
        Integer.valueOf(2857) | _
    }

    def "x = deserialize(serialize(x)) should hold for all BigInteger x"() {
        given:
        def serializer = new Serializer<BigInteger>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x                                                         | _
        BigInteger.TWO.pow(1024)                                  | _
        BigInteger.TWO.pow(1024).multiply(BigInteger.valueOf(-1)) | _
        BigInteger.valueOf(0)                                     | _
        BigInteger.valueOf(-237)                                  | _
        BigInteger.valueOf(2857)                                  | _
    }

    def "x = deserialize(serialize(x)) should hold for all QuadraticResidue x"() {
        given:
        def serializer = new Serializer<QuadraticResidue>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x                              | _
        new QuadraticResidue(121, 167) | _
        new QuadraticResidue(1, 167)   | _
        new QuadraticResidue(112, 167) | _
        new QuadraticResidue(276, 503) | _
        new QuadraticResidue(1, 503)   | _
        new QuadraticResidue(138, 503) | _
    }

    def "x = deserialize(serialize(x)) should hold for all ByteArray x"() {
        given:
        def serializer = new Serializer<ByteArray>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x                                                                     | _
        new ByteArray.FromSafeArray([] as byte[])                             | _
        new ByteArray.FromSafeArray([0xEA] as byte[])                         | _
        new ByteArray.FromSafeArray([0x00, 0x00, 0x00] as byte[])             | _
        new ByteArray.FromSafeArray([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[]) | _

    }

    def "x = deserialize(serialize(x)) should hold for all Boolean x"() {
        given:
        def serializer = new Serializer<Boolean>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x     | _
        true  | _
        false | _

    }

    def "x = deserialize(serialize(x)) should hold for all String x"() {
        given:
        def serializer = new Serializer<String>() {}

        expect:

        x == serializer.deserialize(serializer.serialize(x))

        where:
        x                                                                                                                                                                                                                                                                                                                                                                                                                                               | _
        ""                                                                                                                                                                                                                                                                                                                                                                                                                                              | _
        "Hello World"                                                                                                                                                                                                                                                                                                                                                                                                                                   | _
        "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquid ex ea commodi consequat. Quis aute iure reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint obcaecat cupiditat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum." | _
        "def serializer = new Serializer<String>() {}\n \n  expect:\n \n  x == serializer.deserialize(serializer.serialize(x))"                                                                                                                                                                                                                                                                                                                         | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntVector x"() {
        given:
        def serializer = new Serializer<IntVector>() {}

        def xBuilder = new IntVector.Builder(input.size())
        input.each {
            xBuilder.addValue(it)
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        input             | _
        []                | _
        [0, 1, 0]         | _
        [728, 5763, 7462] | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntVector x with indexes not beginning with 1"() {
        given:
        def serializer = new Serializer<IntVector>() {}

        def xBuilder = new IntVector.Builder(minIndex, maxIndex)
        input.each {
            xBuilder.addValue(it)
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        input             | minIndex | maxIndex
        [0]               | 0        | 0
        [0, 1, 0]         | 2        | 4
        [728, 5763, 7462] | 26       | 28
    }

    def "x = deserialize(serialize(x)) should hold for all Vector x"() {
        given:
        def serializer = new Serializer<Vector<String>>() {}

        def xBuilder = new Vector.Builder(input.size())
        input.each {
            xBuilder.addValue(it)
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        input                   | _
        []                      | _
        ["0", null, "0"]        | _
        ["728", "5763", "7462"] | _
    }

    def "x = deserialize(serialize(x)) should hold for all Vector x with indexes not beginning with 1"() {
        given:
        def serializer = new Serializer<Vector<BigInteger>>() {}

        def xBuilder = new Vector.Builder(minIndex, maxIndex)
        input.each {
            xBuilder.addValue(it)
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        input                                             | minIndex | maxIndex
        [BigInteger.valueOf(1736539)]                     | 0        | 0
        [BigInteger.ONE, BigInteger.ZERO, BigInteger.ONE] | 2        | 4
        [null, null, null]                                | 26       | 28
    }

    def "x = deserialize(serialize(x)) should hold for all IntMatrix x"() {
        given:
        def serializer = new Serializer<IntMatrix>() {}

        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()

        def xBuilder = new IntMatrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.setValue(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        inputMatrix        | _
        []                 | _
        [[11, 12, 13, 14],
         [21, 22, 23, 24],
         [31, 32, 33, 34]] | _
        [[11, 12, 13],
         [21, 22, 23],
         [31, 32, 33],
         [41, 42, 43]]     | _
    }

    def "x = deserialize(serialize(x)) should hold for all IntMatrix x with different indexes"() {
        given:
        def serializer = new Serializer<IntMatrix>() {}

        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()

        def xBuilder = new IntMatrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.addValue(inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix
        1           | 3           | 1           | 3           | [[11, 12, 13],
                                                                 [21, 22, 23],
                                                                 [31, 32, 33]]
        3           | 9           | 4           | 4           | [[11, 12, 13, 14, 15, 16, 17]]
        7           | 7           | 9           | 13          | [[11],
                                                                 [21],
                                                                 [31],
                                                                 [41],
                                                                 [51]]
    }

    def "x = deserialize(serialize(x)) should hold for all Matrix x"() {
        given:
        def serializer = new Serializer<Matrix<String>>() {}

        def height = inputMatrix.size()
        def width = (height == 0) ? 0 : inputMatrix.get(0).size()

        def xBuilder = new Matrix.Builder(height, width)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.setValue(i + 1, j + 1, inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        inputMatrix                | _
        []                         | _
        [["11", "12", "13", "14"],
         ["21", null, "23", "24"],
         ["31", "32", null, "34"]] | _
        [["11", "12", "13"],
         ["21", "22", "23"],
         ["31", "32", "33"],
         ["41", "42", "43"]]       | _
    }

    def "x = deserialize(serialize(x)) should hold for all Matrix x with different indexes"() {
        given:
        def serializer = new Serializer<Matrix<BigInteger>>() {}

        def height = maxColIndex - minColIndex
        def width = maxRowIndex - minRowIndex

        def xBuilder = new Matrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex)
        for (def i = 0; i < height; i++) {
            for (def j = 0; j < width; j++) {
                xBuilder.addValue(inputMatrix.get(i).get(j))
            }
        }
        def x = xBuilder.build()

        expect:

        x == serializer.deserialize(serializer.serialize(x))


        where:
        minRowIndex | maxRowIndex | minColIndex | maxColIndex | inputMatrix
        1           | 3           | 1           | 3           | [[BigInteger.valueOf(11), BigInteger.valueOf(12), BigInteger.valueOf(13)],
                                                                 [BigInteger.valueOf(21), BigInteger.valueOf(22), BigInteger.valueOf(23)],
                                                                 [BigInteger.valueOf(31), BigInteger.valueOf(32), BigInteger.valueOf(33)]]
        3           | 9           | 4           | 4           | [[null, null, null, null, null, null, null]]
        7           | 7           | 9           | 13          | [[BigInteger.valueOf(11)],
                                                                 [BigInteger.valueOf(21)],
                                                                 [BigInteger.valueOf(31)],
                                                                 [BigInteger.valueOf(41)],
                                                                 [BigInteger.valueOf(51)]]
    }

    def "x = deserialize(serialize(x)) should hold for all NullTuple x"() {
        given:
        def serializer = new Serializer<NullTuple>() {}
        def x = new NullTuple()
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Singleton x"() {
        given:
        def serializer = new Serializer<Singleton<String>>() {}
        def x = new Singleton("first")
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Pair x"() {
        given:
        def serializer = new Serializer<Pair<String, Integer>>() {}
        def x = new Pair("first", 2)
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Triple x"() {
        given:
        def serializer = new Serializer<Triple<String, Integer, BigInteger>>() {}
        def x = new Triple("first", 2, BigInteger.valueOf(3))
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Quadruple x"() {
        given:
        def serializer = new Serializer<Quadruple<String, Integer, BigInteger, Boolean>>() {}
        def x = new Quadruple("first", 2, BigInteger.valueOf(3), true)
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Quintuple x"() {
        given:
        def serializer = new Serializer<Quintuple<String, Integer, BigInteger, Boolean, Singleton<Integer>>>() {}
        def x = new Quintuple("first", 2, BigInteger.valueOf(3), true, new Singleton<Integer>(5))
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Sextuple x"() {
        given:
        def serializer = new Serializer<Sextuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>>>() {
        }
        def x = new Sextuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build()
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Septuple x"() {
        given:
        def serializer = new Serializer<Septuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer>>() {
        }
        def x = new Septuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Octuple x"() {
        given:
        def serializer = new Serializer<Octuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String>>() {
        }
        def x = new Octuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight"
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Nonuple x"() {
        given:
        def serializer = new Serializer<Nonuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger>>() {
        }
        def x = new Nonuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9)
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Decuple x"() {
        given:
        def serializer = new Serializer<Decuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger, Pair<String, String>>>() {
        }
        def x = new Decuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9),
                new Pair("first", "second")
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

    def "x = deserialize(serialize(x)) should hold for all Undecuple x"() {
        given:
        def serializer = new Serializer<Undecuple<String, Integer, BigInteger, Boolean, Singleton<Integer>, Vector<String>, Integer, String, BigInteger, Pair<String, String>, String>>() {
        }
        def x = new Undecuple("first",
                2,
                BigInteger.valueOf(3),
                true,
                new Singleton<Integer>(5),
                new Vector.Builder(4).fill("6").build(),
                7,
                "eight",
                BigInteger.valueOf(9),
                new Pair("first", "second"),
                "last"
        )
        expect:

        x == serializer.deserialize(serializer.serialize(x))

    }

}
