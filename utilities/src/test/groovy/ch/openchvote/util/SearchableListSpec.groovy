/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util

import spock.lang.Specification

class SearchableListSpec extends Specification {

    def "Combined Test of build and getter methods"() {

        given: "an empty List"
        def sList = new SearchableList()

        expect:
        sList.getLength() == 0
        sList.contains(26) == false
        sList.search(26) == null
        sList.getKeys().toArray() == [].toArray()
        sList.getValues().toArray() == [].toArray()
        sList.iterator().hasNext() == false

        when: "inserting the first element"
        def val1 = "first"
        sList.append(1, val1)

        then:
        sList.getLength() == 1
        sList.contains(31) == false
        sList.search(31) == null
        sList.contains(1) == true
        sList.search(1) == val1
        sList.getKeys().toArray() == [1].toArray()
        sList.getValues().toArray() == [val1].toArray()
        and: "iterator has one new element"
        def iter = sList.iterator()
        iter.hasNext() == true
        def pair1 = iter.next()
        pair1.getKey() == 1
        pair1.getValue() == val1
        iter.hasNext() == false

        when: "inserting 5 new elements with 5 new keys"
        def val2 = "second"
        sList.append(2, val2)
        def val3 = "third"
        sList.append(3, val3)
        def val4 = "fourth"
        sList.append(4, val4)
        def val5 = "fifth"
        sList.append(5, val5)
        def val6 = "sixth"
        sList.append(6, val6)

        then:
        sList.getLength() == 6
        sList.contains(42) == false
        sList.search(42) == null
        sList.contains(4) == true
        sList.search(4) == val4
        sList.getKeys().toArray() == [1, 2, 3, 4, 5, 6].toArray()
        sList.getValues().toArray() == [val1, val2, val3, val4, val5, val6].toArray()
        and: "iterator should travers the 6  elements in correct order"
        def iter2 = sList.iterator()
        def values2 = [val1, val2, val3, val4, val5, val6]
        for (int i = 1; i <= 6; i++) {
            assert iter2.hasNext() == true
            def pair = iter2.next()
            assert pair.getKey() == i
            assert pair.getValue() == values2.get(i - 1)
        }
        iter2.hasNext() == false

        when: "inserting existing key"
        def val7 = "sixth"
        sList.append(1, val7)

        then: "list should not change"

        sList.getLength() == 6
        sList.contains(42) == false
        sList.search(42) == null
        sList.contains(1) == true
        sList.search(1) == val1
        sList.getKeys().toArray() == [1, 2, 3, 4, 5, 6].toArray()
        sList.getValues().toArray() == [val1, val2, val3, val4, val5, val6].toArray()
        and: "iterator should travers the 6  elements in correct order"
        def iter3 = sList.iterator()
        def values3 = [val1, val2, val3, val4, val5, val6]
        for (int i = 1; i <= 6; i++) {
            assert iter3.hasNext() == true
            def pair = iter3.next()
            assert pair.getKey() == i
            assert pair.getValue() == values3.get(i - 1)
        }
        iter3.hasNext() == false

        when: "inserting inserting null value"
        sList.append(8, null)

        then: "IllegalArgumentException should be thrown"

        thrown(IllegalArgumentException)
        sList.getLength() == 6
        sList.contains(42) == false
        sList.search(42) == null
        sList.contains(1) == true
        sList.search(1) == val1
        sList.getKeys().toArray() == [1, 2, 3, 4, 5, 6].toArray()
        sList.getValues().toArray() == [val1, val2, val3, val4, val5, val6].toArray()
        and: "iterator should travers the 6  elements in correct order"
        def iter4 = sList.iterator()
        def values4 = [val1, val2, val3, val4, val5, val6]
        for (int i = 1; i <= 6; i++) {
            assert iter4.hasNext() == true
            def pair = iter4.next()
            assert pair.getKey() == i
            assert pair.getValue() == values4.get(i - 1)
        }
        iter4.hasNext() == false

    }
}
