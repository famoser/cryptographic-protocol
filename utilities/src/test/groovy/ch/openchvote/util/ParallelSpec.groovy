/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util


import ch.openchvote.util.math.Math
import spock.lang.Specification
import spock.lang.Unroll


class ParallelSpec extends Specification {

    def "forLoop(int startIndex, int endIndex, Consumer<Integer> consumer)"() {
        given:
        def endIndex = 9
        def boolArray = new boolean[endIndex + 1]
        def function = { x -> boolArray[x] = true }
        when:
        Parallel.forLoop(0, endIndex, function)
        then:
        for (boolean x : boolArray) {
            assert x
        }

    }


    @Unroll
    def "forEachLoop(Iterable<V> iterable, Consumer<V> consumer)"() {
        given:
        def vector = Vector.fromSafeArray(input.toArray())
        def expected = Vector.fromSafeArray(eInput.toArray())

        when:
        Parallel.forEachLoop(vector, function)
        then:
        vector.map(function) == expected
        vector.map(function).hashCode() == expected.hashCode()

        where:
        input                                                                       | function                       | eInput
        [1, 2, 3, 4, 5]                                                             | { n -> Math.powerOfTwo(n) }    | [2, 4, 8, 16, 32]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | { n -> n.add(BigInteger.ONE) } | [BigInteger.valueOf(124), BigInteger.valueOf(457), BigInteger.valueOf(790)]
        ["test", "null", "hello", "lorem"]                                          | { s -> s.replace("e", "") }    | ["tst", "null", "hllo", "lorm"]
    }

    def "forEachLoop(Stream<V> stream, Consumer<V> consumer)"() {
        given:
        def vector = Vector.fromSafeArray(input.toArray())
        def expected = Vector.fromSafeArray(eInput.toArray())

        when:
        Parallel.forEachLoop(vector.toStream(), function)
        then:
        vector.map(function) == expected
        vector.map(function).hashCode() == expected.hashCode()

        where:
        input                                                                       | function                       | eInput
        [1, 2, 3, 4, 5]                                                             | { n -> Math.powerOfTwo(n) }    | [2, 4, 8, 16, 32]
        [BigInteger.valueOf(123), BigInteger.valueOf(456), BigInteger.valueOf(789)] | { n -> n.add(BigInteger.ONE) } | [BigInteger.valueOf(124), BigInteger.valueOf(457), BigInteger.valueOf(790)]
        ["test", "null", "hello", "lorem"]                                          | { s -> s.replace("e", "") }    | ["tst", "null", "hllo", "lorm"]
    }
}
