/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.math

import ch.openchvote.util.Vector
import ch.openchvote.util.math.Mod
import ch.openchvote.util.math.QuadraticResidue
import spock.lang.Specification

class ModSpec extends Specification {


    def "pow(BigInteger x, BigInteger y, BigInteger n) should return x^y mod n"() {
        expect:
        Mod.pow(x, y, n) == expected
        where:
        x                       | y                      | n                       | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(0)  | BigInteger.valueOf(167) | BigInteger.valueOf(1)
        BigInteger.valueOf(124) | BigInteger.valueOf(1)  | BigInteger.valueOf(167) | BigInteger.valueOf(124)
        BigInteger.valueOf(34)  | BigInteger.valueOf(74) | BigInteger.valueOf(167) | BigInteger.valueOf(97)
        BigInteger.valueOf(84)  | BigInteger.valueOf(91) | BigInteger.valueOf(167) | BigInteger.valueOf(152)
        BigInteger.valueOf(74)  | BigInteger.valueOf(0)  | BigInteger.valueOf(503) | BigInteger.valueOf(1)
        BigInteger.valueOf(429) | BigInteger.valueOf(1)  | BigInteger.valueOf(503) | BigInteger.valueOf(429)
        BigInteger.valueOf(496) | BigInteger.valueOf(74) | BigInteger.valueOf(503) | BigInteger.valueOf(131)
        BigInteger.valueOf(397) | BigInteger.valueOf(91) | BigInteger.valueOf(503) | BigInteger.valueOf(271)
    }

    def "invert(BigInteger x, BigInteger n) should return the inverse to x based on modulo n"() {
        expect:
        Mod.invert(x, n) == expected
        where:
        x                       | n                       | expected
        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(1)
        BigInteger.valueOf(14)  | BigInteger.valueOf(167) | BigInteger.valueOf(12)
        BigInteger.valueOf(42)  | BigInteger.valueOf(167) | BigInteger.valueOf(4)
        BigInteger.valueOf(67)  | BigInteger.valueOf(167) | BigInteger.valueOf(5)
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(1)
        BigInteger.valueOf(4)   | BigInteger.valueOf(503) | BigInteger.valueOf(126)
        BigInteger.valueOf(151) | BigInteger.valueOf(503) | BigInteger.valueOf(10)
        BigInteger.valueOf(397) | BigInteger.valueOf(503) | BigInteger.valueOf(242)
    }

    def "add(BigInteger x, BigInteger y, BigInteger n) should return x+y mod n"() {
        expect:
        Mod.add(x, y, n) == expected
        where:
        x                       | y                      | n                       | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(0)  | BigInteger.valueOf(167) | BigInteger.valueOf(124)
        BigInteger.valueOf(124) | BigInteger.valueOf(1)  | BigInteger.valueOf(167) | BigInteger.valueOf(125)
        BigInteger.valueOf(34)  | BigInteger.valueOf(74) | BigInteger.valueOf(167) | BigInteger.valueOf(108)
        BigInteger.valueOf(84)  | BigInteger.valueOf(91) | BigInteger.valueOf(167) | BigInteger.valueOf(8)
        BigInteger.valueOf(74)  | BigInteger.valueOf(0)  | BigInteger.valueOf(503) | BigInteger.valueOf(74)
        BigInteger.valueOf(429) | BigInteger.valueOf(1)  | BigInteger.valueOf(503) | BigInteger.valueOf(430)
        BigInteger.valueOf(496) | BigInteger.valueOf(74) | BigInteger.valueOf(503) | BigInteger.valueOf(67)
        BigInteger.valueOf(397) | BigInteger.valueOf(91) | BigInteger.valueOf(503) | BigInteger.valueOf(488)
    }


    def "subtract(BigInteger x, BigInteger y, BigInteger n) should return x-y mod n"() {
        expect:
        Mod.subtract(x, y, n) == expected
        where:
        x                       | y                       | n                       | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(0)   | BigInteger.valueOf(167) | BigInteger.valueOf(124)
        BigInteger.valueOf(124) | BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(123)
        BigInteger.valueOf(34)  | BigInteger.valueOf(74)  | BigInteger.valueOf(167) | BigInteger.valueOf(127)
        BigInteger.valueOf(91)  | BigInteger.valueOf(84)  | BigInteger.valueOf(167) | BigInteger.valueOf(7)
        BigInteger.valueOf(74)  | BigInteger.valueOf(0)   | BigInteger.valueOf(503) | BigInteger.valueOf(74)
        BigInteger.valueOf(429) | BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(428)
        BigInteger.valueOf(496) | BigInteger.valueOf(74)  | BigInteger.valueOf(503) | BigInteger.valueOf(422)
        BigInteger.valueOf(97)  | BigInteger.valueOf(391) | BigInteger.valueOf(503) | BigInteger.valueOf(209)
    }


    def "minus(BigInteger n, BigInteger x) should return n-x mod n"() {
        expect:
        Mod.minus(x, n) == expected
        where:
        n                       | x                      | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(0)  | BigInteger.valueOf(0)
        BigInteger.valueOf(124) | BigInteger.valueOf(1)  | BigInteger.valueOf(123)
        BigInteger.valueOf(34)  | BigInteger.valueOf(74) | BigInteger.valueOf(28)
        BigInteger.valueOf(84)  | BigInteger.valueOf(91) | BigInteger.valueOf(77)
        BigInteger.valueOf(74)  | BigInteger.valueOf(0)  | BigInteger.valueOf(0)
        BigInteger.valueOf(429) | BigInteger.valueOf(1)  | BigInteger.valueOf(428)
        BigInteger.valueOf(496) | BigInteger.valueOf(74) | BigInteger.valueOf(422)
        BigInteger.valueOf(397) | BigInteger.valueOf(91) | BigInteger.valueOf(306)
    }

    def "multiply(BigInteger x, BigInteger y, BigInteger n) should return x*y mod n"() {
        expect:
        Mod.multiply(x, y, n) == expected
        where:
        x                       | y                      | n                       | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(0)  | BigInteger.valueOf(167) | BigInteger.valueOf(0)
        BigInteger.valueOf(124) | BigInteger.valueOf(1)  | BigInteger.valueOf(167) | BigInteger.valueOf(124)
        BigInteger.valueOf(34)  | BigInteger.valueOf(74) | BigInteger.valueOf(167) | BigInteger.valueOf(11)
        BigInteger.valueOf(9)   | BigInteger.valueOf(8)  | BigInteger.valueOf(167) | BigInteger.valueOf(72)
        BigInteger.valueOf(74)  | BigInteger.valueOf(0)  | BigInteger.valueOf(503) | BigInteger.valueOf(0)
        BigInteger.valueOf(429) | BigInteger.valueOf(1)  | BigInteger.valueOf(503) | BigInteger.valueOf(429)
        BigInteger.valueOf(496) | BigInteger.valueOf(74) | BigInteger.valueOf(503) | BigInteger.valueOf(488)
        BigInteger.valueOf(97)  | BigInteger.valueOf(3)  | BigInteger.valueOf(503) | BigInteger.valueOf(291)
    }

    def "divide(BigInteger x, BigInteger y, BigInteger n) should return x*(y^-1) mod n"() {
        expect:
        Mod.divide(x, y, n) == expected
        where:
        x                       | y                       | n                       | expected
        BigInteger.valueOf(124) | BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(124)
        BigInteger.valueOf(124) | BigInteger.valueOf(14)  | BigInteger.valueOf(167) | BigInteger.valueOf(152)
        BigInteger.valueOf(34)  | BigInteger.valueOf(42)  | BigInteger.valueOf(167) | BigInteger.valueOf(136)
        BigInteger.valueOf(9)   | BigInteger.valueOf(67)  | BigInteger.valueOf(167) | BigInteger.valueOf(45)
        BigInteger.valueOf(74)  | BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(74)
        BigInteger.valueOf(429) | BigInteger.valueOf(4)   | BigInteger.valueOf(503) | BigInteger.valueOf(233)
        BigInteger.valueOf(496) | BigInteger.valueOf(151) | BigInteger.valueOf(503) | BigInteger.valueOf(433)
        BigInteger.valueOf(97)  | BigInteger.valueOf(397) | BigInteger.valueOf(503) | BigInteger.valueOf(336)
    }

    def "sum(Vector<BigInteger> x_bold, BigInteger n) should return the sum of all numbers in x-bold mod n"() {
        given:
        def vectorBuilder = new Vector.Builder(x_bold.size())
        x_bold.each {
            vectorBuilder.addValue(BigInteger.valueOf(it))
        }
        def vector = vectorBuilder.build()

        expect:
        Mod.sum(vector, n) == expected

        where:
        x_bold                  | n   | expected
        [1, 2, 3, 4, 5]         | 167 | 15
        [379, 26387, 12]        | 167 | 58
        [32468, 123, 4857, 274] | 167 | 147
        [123]                   | 167 | 123
        []                      | 167 | 0
    }

    def "sumProd(Vector<BigInteger> x_bold, Vector<BigInteger> y_bold, BigInteger n)"() {
        given:
        def vector1Builder = new Vector.Builder(x.size())
        x.each {
            vector1Builder.addValue(BigInteger.valueOf(it))
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new Vector.Builder(y.size())
        y.each {
            vector2Builder.addValue(BigInteger.valueOf(it))
        }
        def vector2 = vector2Builder.build()
        expect:
        Mod.sumProd(vector1, vector2, n) == expected

        where:
        x                  | y               | n   | expected
        [1, 2, 3, 4, 5]    | [1, 2, 3, 4, 5] | 167 | 55
        [379, 26387]       | [1, 2]          | 167 | 47
        [32468, 123, 4857] | [3, 274, 4]     | 167 | 67
        []                 | []              | 167 | 0
    }

    def "prod(Vector<BigInteger> x_bold, BigInteger n) should return the product of all numbers in x-bold mod n"() {
        given:
        def vectorBuilder = new Vector.Builder(x_bold.size())
        x_bold.each {
            vectorBuilder.addValue(BigInteger.valueOf(it))
        }
        def vector = vectorBuilder.build()

        expect:
        Mod.prod(vector, n) == expected

        where:
        x_bold                  | n   | expected
        [1, 2, 3, 4, 5]         | 167 | 120
        [379, 26387, 12]        | 167 | 39
        [32468, 123, 4857, 274] | 167 | 36
        [123]                   | 167 | 123
        []                      | 167 | 1
    }


    def "prodPow(Vector<BigInteger> x_bold, Vector<BigInteger> y_bold, BigInteger n)"() {
        given:
        def vector1Builder = new Vector.Builder(x.size())
        x.each {
            vector1Builder.addValue(BigInteger.valueOf(it))
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new Vector.Builder(y.size())
        y.each {
            vector2Builder.addValue(BigInteger.valueOf(it))
        }
        def vector2 = vector2Builder.build()
        expect:
        Mod.prodPow(vector1, vector2, n) == expected

        where:
        x                  | y               | n   | expected
        [1, 2, 3, 4, 5]    | [1, 2, 3, 4, 5] | 167 | 45
        [379, 26387]       | [1, 2]          | 167 | 45
        [32468, 123, 4857] | [3, 274, 4]     | 167 | 103
        []                 | []              | 167 | 1
    }

    // QUADRATIC RESIDUE

    def "pow(QuadraticResidue x, BigInteger y)"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.pow(qd1, exp).equals(expected)
        Mod.pow(qd1, exp).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | exp                     | expectedValue | expectedModulo
        124    | 167     | BigInteger.valueOf(0)   | 1             | 167
        121    | 167     | BigInteger.valueOf(167) | 121           | 167
        124    | 167     | BigInteger.valueOf(2)   | 12            | 167
        205    | 503     | BigInteger.valueOf(15)  | 52            | 503
        1      | 503     | BigInteger.valueOf(3)   | 1             | 503
        255    | 503     | BigInteger.valueOf(67)  | 24            | 503
    }


    def "invert(QuadraticResidue x)"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.invert(qd1).equals(expected)
        Mod.invert(qd1).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | expectedValue | expectedModulo
        2      | 167     | 84            | 167
        56     | 167     | 3             | 167
        14     | 167     | 12            | 167
        252    | 503     | 2             | 503
        1      | 503     | 1             | 503
        63     | 503     | 8             | 503
    }

    def " multiply(QuadraticResidue x, QuadraticResidue y) should return #value1*#value2 mod modulo1"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.multiply(qd1, qd2).equals(expected)
        Mod.multiply(qd1, qd2).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | value2 | modulo2 | expectedValue | expectedModulo
        121    | 167     | 63     | 167     | 108           | 167
        1      | 167     | 115    | 167     | 115           | 167
        124    | 167     | 1      | 167     | 124           | 167
        205    | 503     | 83     | 503     | 416           | 503
        1      | 503     | 24     | 503     | 24            | 503
        255    | 503     | 1      | 503     | 255           | 503
    }

    def " multiply(QuadraticResidue x, QuadraticResidue y, QuadraticResidue z) should return #value1*#value2*#value3 mod modulo1"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        def qd3 = new QuadraticResidue(value3, modulo3)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.multiply(qd1, qd2, qd3).equals(expected)
        Mod.multiply(qd1, qd2, qd3).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | value2 | modulo2 | value3 | modulo3 | expectedValue | expectedModulo
        121    | 167     | 63     | 167     | 108    | 167     | 141           | 167
        1      | 167     | 115    | 167     | 24     | 167     | 88            | 167
        124    | 167     | 1      | 167     | 65     | 167     | 44            | 167
        205    | 503     | 83     | 503     | 301    | 503     | 472           | 503
        1      | 503     | 24     | 503     | 473    | 503     | 286           | 503
        255    | 503     | 1      | 503     | 73     | 503     | 4             | 503
    }


    def "divide(QuadraticResidue x, QuadraticResidue y)"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.divide(qd1, qd2).equals(expected)
        Mod.divide(qd1, qd2).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | value2 | modulo2 | expectedValue | expectedModulo
        121    | 167     | 2      | 167     | 144           | 167
        1      | 167     | 56     | 167     | 3             | 167
        124    | 167     | 14     | 167     | 152           | 167
        205    | 503     | 252    | 503     | 410           | 503
        1      | 503     | 63     | 503     | 8             | 503
        255    | 503     | 1      | 503     | 255           | 503
    }


    def "prod(Vector<QuadraticResidue> x_bold)"() {
        given:
        def vectorBuilder = new Vector.Builder(x_bold.size())
        x_bold.each {
            vectorBuilder.addValue(new QuadraticResidue(it, n))
        }
        def vector = vectorBuilder.build()
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.prod(vector) == expected

        where:
        x_bold           | n   | expectedValue | expectedModulo
        [1, 2, 14, 121]  | 167 | 48            | 167
        [21, 1, 144]     | 167 | 18            | 167
        [57, 99, 29, 93] | 167 | 127           | 167
        [99]             | 167 | 99            | 167
        []               | 167 | 1             | 167
    }


    def "prodPow(Vector<QuadraticResidue> x_bold, Vector<BigInteger> y_bold)"() {
        given:
        def vector1Builder = new Vector.Builder(x.size())
        x.each {
            vector1Builder.addValue(new QuadraticResidue(it, p))
        }
        def vector1 = vector1Builder.build()
        def vector2Builder = new Vector.Builder(y.size())
        y.each {
            vector2Builder.addValue(BigInteger.valueOf(it))
        }
        def vector2 = vector2Builder.build()

        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        Mod.prodPow(vector1, vector2) == expected

        where:
        x                 | y                 | p   | expectedValue | expectedModulo
        [1, 4, 9, 16, 25] | [1, 4, 9, 16, 25] | 167 | 63            | 167
        [21, 127]         | [1, 4]            | 167 | 28            | 167
        [57, 99, 29]      | [9, 44, 4]        | 167 | 93            | 167
        []                | []                | 167 | 1             | 167
    }


}
