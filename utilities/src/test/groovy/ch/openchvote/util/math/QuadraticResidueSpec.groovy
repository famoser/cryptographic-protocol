/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.math

import ch.openchvote.util.math.QuadraticResidue
import spock.lang.Specification
import spock.lang.Unroll

class QuadraticResidueSpec extends Specification {

    def "QuadraticResidue(long value, long modulo) should throw IllegalArgumentException for case: #caseDesc"() {
        when:
        new QuadraticResidue(value, modulo)
        then:
        thrown(IllegalArgumentException)

        where:
        caseDesc                       | value | modulo
        "value is zero"                | 0     | 167
        "value is greater then modulo" | 168   | 167
    }

    def "QuadraticResidue(long value, long modulo) should create a QuadraticResidue with #expectedValue and expectedModulo"() {
        when:
        def qd = new QuadraticResidue(value, modulo)
        then:
        qd.getValue() == expectedValue
        qd.getSqrtValue() == expectedSqrtValue
        qd.getModulus() == expectedModulo
        qd.hasModulus(expectedModulo)

        where:
        value | modulo | expectedValue           | expectedSqrtValue       | expectedModulo
        121   | 167    | BigInteger.valueOf(121) | BigInteger.valueOf(11)  | BigInteger.valueOf(167)
        1     | 167    | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        112   | 167    | BigInteger.valueOf(112) | BigInteger.valueOf(46)  | BigInteger.valueOf(167)
        276   | 503    | BigInteger.valueOf(276) | BigInteger.valueOf(205) | BigInteger.valueOf(503)
        1     | 503    | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        138   | 503    | BigInteger.valueOf(138) | BigInteger.valueOf(248) | BigInteger.valueOf(503)
    }

    def "QuadraticResidue(BigInteger value, BigInteger modulo) should throw IllegalArgumentException for case: #caseDesc"() {
        when:
        new QuadraticResidue(value, modulo)
        then:
        thrown(IllegalArgumentException)

        where:
        caseDesc                       | value                   | modulo
        "value is zero"                | BigInteger.valueOf(0)   | BigInteger.valueOf(167)
        "value is greater then modulo" | BigInteger.valueOf(168) | BigInteger.valueOf(167)
    }

    def "QuadraticResidue(BigInteger value, BigInteger modulo) should create a QuadraticResidue with #expectedValue and expectedModulo"() {
        when:
        def qd = new QuadraticResidue(value, modulo)
        then:
        qd.getValue() == expectedValue
        qd.getSqrtValue() == expectedSqrtValue
        qd.getModulus() == expectedModulo
        qd.hasModulus(expectedModulo)

        where:
        value                   | modulo                  | expectedValue           | expectedSqrtValue       | expectedModulo
        BigInteger.valueOf(121) | BigInteger.valueOf(167) | BigInteger.valueOf(121) | BigInteger.valueOf(11)  | BigInteger.valueOf(167)
        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        BigInteger.valueOf(112) | BigInteger.valueOf(167) | BigInteger.valueOf(112) | BigInteger.valueOf(46)  | BigInteger.valueOf(167)
        BigInteger.valueOf(276) | BigInteger.valueOf(503) | BigInteger.valueOf(276) | BigInteger.valueOf(205) | BigInteger.valueOf(503)
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        BigInteger.valueOf(138) | BigInteger.valueOf(503) | BigInteger.valueOf(138) | BigInteger.valueOf(248) | BigInteger.valueOf(503)
    }

    def "QuadraticResidue(BigInteger value, BigInteger sqrtValue, BigInteger modulo) should throw IllegalArgumentException for case: #caseDesc"() {
        when:
        new QuadraticResidue(value, sqrtValue, modulo)
        then:
        thrown(IllegalArgumentException)

        where:
        caseDesc                                                | value                   | sqrtValue               | modulo
        "value is zero"                                         | BigInteger.valueOf(0)   | BigInteger.valueOf(1)   | BigInteger.valueOf(167)
        "value is greater then modulo"                          | BigInteger.valueOf(168) | BigInteger.valueOf(1)   | BigInteger.valueOf(167)
        "sqrtvalue is zero"                                     | BigInteger.valueOf(1)   | BigInteger.valueOf(0)   | BigInteger.valueOf(167)
        "sqrtvalue is greater then modulo"                      | BigInteger.valueOf(1)   | BigInteger.valueOf(168) | BigInteger.valueOf(167)
        "value is not equal to sqrtvalue*sqrtvalue mod modulo " | BigInteger.valueOf(19)  | BigInteger.valueOf(3)   | BigInteger.valueOf(167)
    }

    def "QuadraticResidue(BigInteger value, BigInteger sqrtValue, BigInteger modulo) should create a QuadraticResidue with #expectedValue and expectedModulo"() {
        when:
        def qd = new QuadraticResidue(value, sqrtValue, modulo)
        then:
        qd.getValue() == expectedValue
        qd.getSqrtValue() == expectedSqrtValue
        qd.getModulus() == expectedModulo
        qd.hasModulus(expectedModulo)

        where:
        value                   | sqrtValue               | modulo                  | expectedValue           | expectedSqrtValue       | expectedModulo
        BigInteger.valueOf(121) | BigInteger.valueOf(11)  | BigInteger.valueOf(167) | BigInteger.valueOf(121) | BigInteger.valueOf(11)  | BigInteger.valueOf(167)
        BigInteger.valueOf(1)   | BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        BigInteger.valueOf(112) | BigInteger.valueOf(46)  | BigInteger.valueOf(167) | BigInteger.valueOf(112) | BigInteger.valueOf(46)  | BigInteger.valueOf(167)
        BigInteger.valueOf(276) | BigInteger.valueOf(205) | BigInteger.valueOf(503) | BigInteger.valueOf(276) | BigInteger.valueOf(205) | BigInteger.valueOf(503)
        BigInteger.valueOf(1)   | BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.ONE          | BigInteger.valueOf(1)   | null
        BigInteger.valueOf(138) | BigInteger.valueOf(248) | BigInteger.valueOf(503) | BigInteger.valueOf(138) | BigInteger.valueOf(248) | BigInteger.valueOf(503)
    }

    def "isOne() should return if the QuadraticResidue has value one or not"() {
        given:
        def qd = new QuadraticResidue(value, modulo)
        expect:
        qd.isOne() == expected
        where:
        value                   | modulo                  | expected

        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | true
        BigInteger.valueOf(205) | BigInteger.valueOf(503) | false
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | true
        BigInteger.valueOf(255) | BigInteger.valueOf(503) | false
    }


    def "toBigInteger() should return the value of QuadraticResidue as a BigInteger"() {
        given:
        def qd = new QuadraticResidue(value, modulo)
        expect:
        qd.getValue() == expected
        where:
        value                   | modulo                  | expected

        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(1)
        BigInteger.valueOf(112) | BigInteger.valueOf(167) | BigInteger.valueOf(112)
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(1)
        BigInteger.valueOf(255) | BigInteger.valueOf(503) | BigInteger.valueOf(255)
    }

    @Unroll
    def "hasModulo(BigInteger p) should return if the QuadraticResidues modulo is equal to p"() {
        given:
        def qd = new QuadraticResidue(value, modulo)
        expect:
        qd.hasModulus(p) == expected
        where:
        value                   | modulo                  | p                       | expected

        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(167) | true
        BigInteger.valueOf(112) | BigInteger.valueOf(167) | BigInteger.valueOf(167) | true
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(503) | true
        BigInteger.valueOf(255) | BigInteger.valueOf(503) | BigInteger.valueOf(503) | true
        BigInteger.valueOf(1)   | BigInteger.valueOf(167) | BigInteger.valueOf(83)  | true
        BigInteger.valueOf(112) | BigInteger.valueOf(383) | BigInteger.valueOf(112) | false
        BigInteger.valueOf(1)   | BigInteger.valueOf(503) | BigInteger.valueOf(167) | true
        BigInteger.valueOf(255) | BigInteger.valueOf(503) | BigInteger.valueOf(479) | false
    }

    def "modMultiply(QuadraticResidue other) should throw a IllegalArgumentException if modulo1 not equal modulo2"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        when:
        qd1.modMultiply(qd2)
        then:
        thrown(IllegalArgumentException)
        where:
        value1 | modulo1 | value2 | modulo2
        121    | 167     | 63     | 83
        205    | 503     | 83     | 179

    }

    @Unroll
    def "modMultiply(QuadraticResidue other)  should return #value1*#value2 mod modulo1"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        qd1.modMultiply(qd2).equals(expected)
        qd1.modMultiply(qd2).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | value2 | modulo2 | expectedValue | expectedModulo
        121    | 167     | 63     | 167     | 108           | 167
        1      | 167     | 115    | 167     | 115           | 167
        124    | 167     | 1      | 167     | 124           | 167
        205    | 503     | 83     | 503     | 416           | 503
        1      | 503     | 24     | 503     | 24            | 503
        255    | 503     | 1      | 503     | 255           | 503
    }


    @Unroll
    def "modPow(BigInteger exp)  should return #value1^exp mod modulo1"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        qd1.modPow(exp).equals(expected)
        qd1.modPow(exp).hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | exp                     | expectedValue | expectedModulo
        124    | 167     | BigInteger.valueOf(0)   | 1             | 167
        121    | 167     | BigInteger.valueOf(167) | 121           | 167
        124    | 167     | BigInteger.valueOf(2)   | 12            | 167
        205    | 503     | BigInteger.valueOf(15)  | 52            | 503
        1      | 503     | BigInteger.valueOf(3)   | 1             | 503
        255    | 503     | BigInteger.valueOf(67)  | 24            | 503
    }

    @Unroll
    def "modInverse() should return the inverse element of #value1 in regards to #modulo1"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def expected = new QuadraticResidue(expectedValue, expectedModulo)
        expect:
        qd1.modInverse().equals(expected)
        qd1.modInverse().hashCode() == expected.hashCode()
        where:
        value1 | modulo1 | expectedValue | expectedModulo
        2      | 167     | 84            | 167
        56     | 167     | 3             | 167
        14     | 167     | 12            | 167
        252    | 503     | 2             | 503
        1      | 503     | 1             | 503
        63     | 503     | 8             | 503
    }

    @Unroll
    def "toString() should return the expected String representation"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)

        expect:
        qd1.toString() == expected
        where:
        value1 | modulo1 | expected
        2      | 167     | "2"
        56     | 167     | "56"
        14     | 167     | "14"
        252    | 503     | "252"
        1      | 503     | "1"
        63     | 503     | "63"
    }


    def "compareTo(QuadraticResidue other) throws IllegalArgumentException if the QuadraticResidue dont have the same modulo"() {
        given:
        def qd1 = new QuadraticResidue(value1, modulo1)
        def qd2 = new QuadraticResidue(value2, modulo2)
        when:
        qd1.compareTo(qd2)
        then:
        thrown(IllegalArgumentException)
        where:
        value1 | modulo1 | value2 | modulo2
        121    | 167     | 63     | 83
        205    | 503     | 83     | 179

    }

    def "compareTo, equals and hashCode test"() {
        given:
        def qd1 = new QuadraticResidue(2, 167)
        def qd2 = new QuadraticResidue(2, 167)
        def qd3 = new QuadraticResidue(84, 167)
        expect:
        qd1.compareTo(qd2) == 0
        qd1.equals(qd2) == true
        qd1.hashCode() == qd2.hashCode()
        and:
        qd1.compareTo(qd3) <= 0
        qd1.equals(qd3) == false
        qd1.hashCode() != qd3.hashCode()
        and:
        qd3.compareTo(qd2) >= 0
        qd3.equals(qd2) == false
        qd3.hashCode() != qd2.hashCode()

    }


}
