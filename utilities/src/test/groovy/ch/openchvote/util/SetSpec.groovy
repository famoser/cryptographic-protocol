/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util

import ch.openchvote.util.math.QuadraticResidue
import ch.openchvote.util.tuples.*
import spock.lang.Specification
import spock.lang.Unroll

class SetSpec extends Specification {

    def static final p = BigInteger.valueOf(167)
    def static final q = BigInteger.valueOf(83)
    def static final e = BigInteger.valueOf(7)

    @Unroll
    def "contains(T x) #x"() {
        expect:
        set.contains(x) == expected

        where:
        set                                                                                 | x                                                                                                 | expected
        Set.NN                                                                              | BigInteger.valueOf(0)                                                                             | true
        Set.NN                                                                              | BigInteger.valueOf(Integer.MAX_VALUE)                                                             | true
        Set.NN                                                                              | BigInteger.valueOf(-1)                                                                            | false
        Set.NN                                                                              | BigInteger.valueOf(-98264)                                                                        | false
        Set.NN_plus                                                                         | BigInteger.ONE                                                                                    | true
        Set.NN_plus                                                                         | BigInteger.valueOf(982374)                                                                        | true
        Set.NN_plus                                                                         | BigInteger.valueOf(0)                                                                             | false
        Set.NN_plus                                                                         | BigInteger.valueOf(-928374)                                                                       | false
        Set.ZZ(p)                                                                           | BigInteger.valueOf(0)                                                                             | true
        Set.ZZ(p)                                                                           | p.add(BigInteger.valueOf(-1))                                                                     | true
        Set.ZZ(p)                                                                           | p                                                                                                 | false
        Set.ZZ(p)                                                                           | BigInteger.valueOf(-1)                                                                            | false
        Set.ZZ_star(p)                                                                      | BigInteger.valueOf(1)                                                                             | true
        Set.ZZ_star(p)                                                                      | p.add(BigInteger.valueOf(-1))                                                                     | true
        Set.ZZ_star(p)                                                                      | p                                                                                                 | false
        Set.ZZ_star(p)                                                                      | BigInteger.valueOf(-1)                                                                            | false
        Set.ZZ_powerOfTwo(e)                                                                | BigInteger.valueOf(0)                                                                             | true
        Set.ZZ_powerOfTwo(e)                                                                | BigInteger.TWO.pow(e).add(BigInteger.valueOf(-1))                                                 | true
        Set.ZZ_powerOfTwo(e)                                                                | BigInteger.valueOf(-1)                                                                            | false
        Set.ZZ_powerOfTwo(e)                                                                | BigInteger.TWO.pow(e)                                                                             | false
        Set.GG(q)                                                                           | new QuadraticResidue(BigInteger.valueOf(1), q)                                                    | true
        Set.GG(q)                                                                           | new QuadraticResidue(BigInteger.valueOf(81), q)                                                   | true
        Set.GG(q)                                                                           | new QuadraticResidue(BigInteger.valueOf(100), BigInteger.valueOf(107))                            | false
        Set.GG(q)                                                                           | new QuadraticResidue(q, BigInteger.valueOf(107))                                                  | false
        Set.GG(p, q)                                                                        | BigInteger.ONE                                                                                    | true
        Set.GG(p, q)                                                                        | BigInteger.valueOf(28)                                                                            | true
        Set.GG(p, q)                                                                        | BigInteger.valueOf(-1)                                                                            | false
        Set.GG(p, q)                                                                        | q                                                                                                 | false
        Set.B(5)                                                                            | new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])                             | true
        Set.B(5)                                                                            | new ByteArray.FromSafeArray([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[])                             | true
        Set.B(5)                                                                            | new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04] as byte[])                                   | false
        Set.B(5)                                                                            | new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])                       | false
        Set.B_star                                                                          | new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])                             | true
        Set.B_star                                                                          | new ByteArray.FromSafeArray([0xFF, 0xFF, 0xFF, 0xFF, 0xFF] as byte[])                             | true
        Set.B_star                                                                          | new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04] as byte[])                                   | true
        Set.B_star                                                                          | null                                                                                              | false
        Set.UCS(5)                                                                          | "12345"                                                                                           | true
        Set.UCS(5)                                                                          | "zzzzz"                                                                                           | true
        Set.UCS(5)                                                                          | "1234"                                                                                            | false
        Set.UCS(5)                                                                          | "123456"                                                                                          | false
        Set.UCS_star                                                                        | "12345"                                                                                           | true
        Set.UCS_star                                                                        | "zzzzz"                                                                                           | true
        Set.UCS_star                                                                        | null                                                                                              | false
        Set.String(Alphabet.LETTERS)                                                        | "abcde"                                                                                           | true
        Set.String(Alphabet.LETTERS)                                                        | "vwxyz"                                                                                           | true
        Set.String(Alphabet.LETTERS)                                                        | "abc34"                                                                                           | false
        Set.String(Alphabet.LETTERS)                                                        | "abcd5"                                                                                           | false
        Set.String(Alphabet.LETTERS, 5)                                                     | "abcde"                                                                                           | true
        Set.String(Alphabet.LETTERS, 5)                                                     | "vwxyz"                                                                                           | true
        Set.String(Alphabet.LETTERS, 5)                                                     | "abc"                                                                                             | false
        Set.String(Alphabet.LETTERS, 5)                                                     | "abcd5"                                                                                           | false
        Set.String(Alphabet.LETTERS, 3, 5)                                                  | "abcde"                                                                                           | true
        Set.String(Alphabet.LETTERS, 3, 5)                                                  | "vwxyz"                                                                                           | true
        Set.String(Alphabet.LETTERS, 3, 5)                                                  | "ab"                                                                                              | false
        Set.String(Alphabet.LETTERS, 3, 5)                                                  | "abcd5"                                                                                           | false
        Set.Vector(Set.UCS(5))                                                              | (new Vector.Builder<String>(3)).fill("Lorem").build()                                             | true
        Set.Vector(Set.UCS(5))                                                              | (new Vector.Builder<String>(1)).fill("Lorem Ipsum").build()                                       | false
        Set.Vector(Set.UCS(5), 5)                                                           | (new Vector.Builder<String>(5)).fill("Lorem").build()                                             | true
        Set.Vector(Set.UCS(5), 5)                                                           | (new Vector.Builder<String>(5)).fill("Lorem Ipsum").build()                                       | false
        Set.Matrix(Set.UCS(5), 5, 3)                                                        | (new Matrix.Builder<String>(5, 3)).fill("Lorem").build()                                          | true
        Set.Matrix(Set.UCS(5), 5, 3)                                                        | (new Matrix.Builder<String>(5, 3)).fill("Lorem Ipsum").build()                                    | false
        Set.IntVector(IntSet.NN, 5)                                                         | (new IntVector.Builder(5)).fill(647).build()                                                      | true
        Set.IntVector(IntSet.NN, 5)                                                         | (new IntVector.Builder(3)).fill(4).build()                                                        | false
        Set.IntMatrix(IntSet.NN, 5, 3)                                                      | (new IntMatrix.Builder(5, 3)).fill(34).build()                                                    | true
        Set.IntMatrix(IntSet.NN, 5, 3)                                                      | (new IntMatrix.Builder(3, 5)).fill(0).build()                                                     | false
        Set.Pair(Set.UCS(5), Set.UCS(5))                                                    | new Pair<>("Lorem", "Ipsum")                                                                      | true
        Set.Pair(Set.UCS(5), Set.UCS(5))                                                    | new Pair<>("Hello", "World!")                                                                     | false
        Set.Triple(Set.UCS(5), Set.NN, Set.UCS(5))                                          | new Triple<>("Lorem", BigInteger.valueOf(24), "Ipsum")                                            | true
        Set.Triple(Set.UCS(5), Set.NN, Set.UCS(5))                                          | new Triple<>("Hello", BigInteger.valueOf(-1), "World!")                                           | false
        Set.Quadruple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star)                         | new Quadruple<>("Lorem", BigInteger.valueOf(24), "Ipsum", "dolor")                                | true
        Set.Quadruple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star)                         | new Quadruple<>("Hello", BigInteger.valueOf(-1), "World!", "Bye!")                                | false
        Set.Quintuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.ZZ(7))              | new Quintuple<>("Lorem", BigInteger.valueOf(24), "Ipsum", "dolor", BigInteger.valueOf(4))         | true
        Set.Quintuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.ZZ(7))              | new Quintuple<>("Hello", BigInteger.valueOf(-1), "World!", "Bye!", BigInteger.valueOf(7))         | false
        Set.Sextuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.ZZ(7), Set.UCS_star) | new Sextuple<>("Lorem", BigInteger.valueOf(24), "Ipsum", "dolor", BigInteger.valueOf(4), "sit")   | true
        Set.Sextuple(Set.UCS(5), Set.NN, Set.UCS(5), Set.UCS_star, Set.ZZ(7), Set.UCS_star) | new Sextuple<>("Hello", BigInteger.valueOf(-1), "World!", ",Good", BigInteger.valueOf(7), "Bye!") | false
    }

    @Unroll
    def "contains(T x) for Phi set #x"() {
        given:
        def permutationBuilder = new IntVector.Builder(x.size())
        x.each {
            permutationBuilder.addValue(it)
        }
        def permutation = permutationBuilder.build()
        expect:
        set.contains(permutation) == expected

        where:
        set        | x               | expected
        Set.Phi(5) | [1, 2, 3, 4, 5] | true
        Set.Phi(5) | [5, 2, 3, 1, 4] | true
        Set.Phi(5) | [5, 2, 3, 1]    | false
        Set.Phi(5) | [5, 2, 3, 1, 5] | false

    }

    @Unroll
    def "containsAll(Stream<T> stream) #x"() {
        given:

        def stream = Arrays.stream(x.toArray())
        expect:
        set.containsAll(stream) == expected

        where:
        set                             | x                                                                                                                                              | expected
        Set.NN                          | []                                                                                                                                             | true
        Set.NN                          | [BigInteger.valueOf(0), BigInteger.valueOf(876213), BigInteger.valueOf(32984)]                                                                 | true
        Set.NN                          | [BigInteger.valueOf(Integer.MAX_VALUE), BigInteger.valueOf(21368), BigInteger.valueOf(9287)]                                                   | true
        Set.NN                          | [BigInteger.valueOf(-1), BigInteger.valueOf(13897), BigInteger.valueOf(0)]                                                                     | false
        Set.NN                          | [BigInteger.valueOf(123), BigInteger.valueOf(-98264)]                                                                                          | false
        Set.NN_plus                     | []                                                                                                                                             | true
        Set.NN_plus                     | [BigInteger.valueOf(21412), BigInteger.ONE, BigInteger.valueOf(324987)]                                                                        | true
        Set.NN_plus                     | [BigInteger.valueOf(324), BigInteger.valueOf(526873), BigInteger.valueOf(982374)]                                                              | true
        Set.NN_plus                     | [BigInteger.valueOf(213), BigInteger.valueOf(4879), BigInteger.valueOf(0)]                                                                     | false
        Set.NN_plus                     | [BigInteger.valueOf(0), BigInteger.valueOf(-928374), BigInteger.valueOf(0)]                                                                    | false
        Set.ZZ(p)                       | []                                                                                                                                             | true
        Set.ZZ(p)                       | [BigInteger.valueOf(0), BigInteger.valueOf(166), BigInteger.valueOf(23)]                                                                       | true
        Set.ZZ(p)                       | [p.add(BigInteger.valueOf(-1)), BigInteger.ONE]                                                                                                | true
        Set.ZZ(p)                       | [BigInteger.valueOf(14), BigInteger.valueOf(0), p]                                                                                             | false
        Set.ZZ(p)                       | [BigInteger.valueOf(123), BigInteger.valueOf(-1), BigInteger.valueOf(154)]                                                                     | false
        Set.ZZ_powerOfTwo(e)            | []                                                                                                                                             | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(0)]                                                                                                                        | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.TWO.pow(e).add(BigInteger.valueOf(-1)), BigInteger.valueOf(127)]                                                                   | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(-1)]                                                                                                                       | false
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(173), BigInteger.TWO.pow(e)]                                                                                               | false
        Set.GG(q)                       | []                                                                                                                                             | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(1), q), new QuadraticResidue(BigInteger.valueOf(25), q)]                                              | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(81), q), new QuadraticResidue(BigInteger.valueOf(64), q)]                                             | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(100), BigInteger.valueOf(107))]                                                                       | false
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(81), q), new QuadraticResidue(q, BigInteger.valueOf(107))]                                            | false
        Set.GG(p, q)                    | []                                                                                                                                             | true
        Set.GG(p, q)                    | [BigInteger.ONE, BigInteger.valueOf(7), BigInteger.valueOf(116)]                                                                               | true
        Set.GG(p, q)                    | [BigInteger.valueOf(28), BigInteger.valueOf(49), BigInteger.valueOf(31)]                                                                       | true
        Set.GG(p, q)                    | [BigInteger.valueOf(96), BigInteger.valueOf(-1), BigInteger.valueOf(2)]                                                                        | false
        Set.GG(p, q)                    | [q, BigInteger.valueOf(5)]                                                                                                                     | false
        Set.B(5)                        | []                                                                                                                                             | true
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[]), new ByteArray.FromSafeArray([0xA4, 0x7F, 0xCE, 0x3D, 0x32] as byte[])] | true
        Set.B(5)                        | []                                                                                                                                             | true
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04] as byte[]), new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])]       | false
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])]                                                                  | false
        Set.UCS(5)                      | []                                                                                                                                             | true
        Set.UCS(5)                      | ["12345", "12345", "12345"]                                                                                                                    | true
        Set.UCS(5)                      | ["zzzzz", "Z5DEQ", "Fe4T7"]                                                                                                                    | true
        Set.UCS(5)                      | ["1234", "12345", "jde749d"]                                                                                                                   | false
        Set.UCS(5)                      | ["123456", "15f"]                                                                                                                              | false
        Set.String(Alphabet.LETTERS, 5) | []                                                                                                                                             | true
        Set.String(Alphabet.LETTERS, 5) | ["abcde", "uehte", "eWtZd"]                                                                                                                    | true
        Set.String(Alphabet.LETTERS, 5) | ["vwxyz", "irhts", "eurht"]                                                                                                                    | true
        Set.String(Alphabet.LETTERS, 5) | ["djebt", "heurh", "abc"]                                                                                                                      | false
        Set.String(Alphabet.LETTERS, 5) | ["abcd5", "e8gh4", "ehwk"]                                                                                                                     | false

    }

    @Unroll
    def "containsAll(Stream<T> stream) for Phi set #x"() {
        given:
        def permutationList = new LinkedList<IntVector>()
        x.each { array ->
            def permutationBuilder = new IntVector.Builder(array.size())
            array.each {
                permutationBuilder.addValue(it)
            }
            def permutation = permutationBuilder.build()
            permutationList.add(permutation)
        }
        def stream = Arrays.stream(permutationList.toArray())
        expect:
        set.containsAll(stream) == expected

        where:
        set        | x                                                                | expected
        Set.Phi(5) | [[1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [2, 4, 1, 3, 5]]              | true
        Set.Phi(5) | [[5, 2, 3, 1, 4], [2, 5, 1, 3, 4], [5, 1, 2, 4, 3]]              | true
        Set.Phi(5) | [[35, 47, 19, 371, 21], [5, 2, 3, 1], [1, 83, 485, 1287, 78]]    | false
        Set.Phi(5) | [[1, 2, 3, 4, 5], [2398, 9457, 2648, 1983, 35], [5, 2, 3, 1, 5]] | false

    }

    @Unroll
    def "contains(Collection<T> values) #x"() {
        given:
        def collection = new ArrayList()
        x.each {
            collection.add(it)
        }

        expect:
        set.containsAll(collection) == expected

        where:
        set                             | x                                                                                                                                              | expected
        Set.NN                          | []                                                                                                                                             | true
        Set.NN                          | [BigInteger.valueOf(0), BigInteger.valueOf(876213), BigInteger.valueOf(32984)]                                                                 | true
        Set.NN                          | [BigInteger.valueOf(Integer.MAX_VALUE), BigInteger.valueOf(21368), BigInteger.valueOf(9287)]                                                   | true
        Set.NN                          | [BigInteger.valueOf(-1), BigInteger.valueOf(13897), BigInteger.valueOf(0)]                                                                     | false
        Set.NN                          | [BigInteger.valueOf(123), BigInteger.valueOf(-98264)]                                                                                          | false
        Set.NN_plus                     | []                                                                                                                                             | true
        Set.NN_plus                     | [BigInteger.valueOf(21412), BigInteger.ONE, BigInteger.valueOf(324987)]                                                                        | true
        Set.NN_plus                     | [BigInteger.valueOf(324), BigInteger.valueOf(526873), BigInteger.valueOf(982374)]                                                              | true
        Set.NN_plus                     | [BigInteger.valueOf(213), BigInteger.valueOf(4879), BigInteger.valueOf(0)]                                                                     | false
        Set.NN_plus                     | [BigInteger.valueOf(0), BigInteger.valueOf(-928374), BigInteger.valueOf(0)]                                                                    | false
        Set.ZZ(p)                       | []                                                                                                                                             | true
        Set.ZZ(p)                       | [BigInteger.valueOf(0), BigInteger.valueOf(166), BigInteger.valueOf(23)]                                                                       | true
        Set.ZZ(p)                       | [p.add(BigInteger.valueOf(-1)), BigInteger.ONE]                                                                                                | true
        Set.ZZ(p)                       | [BigInteger.valueOf(14), BigInteger.valueOf(0), p]                                                                                             | false
        Set.ZZ(p)                       | [BigInteger.valueOf(123), BigInteger.valueOf(-1), BigInteger.valueOf(154)]                                                                     | false
        Set.ZZ_powerOfTwo(e)            | []                                                                                                                                             | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(0)]                                                                                                                        | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.TWO.pow(e).add(BigInteger.valueOf(-1)), BigInteger.valueOf(127)]                                                                   | true
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(-1)]                                                                                                                       | false
        Set.ZZ_powerOfTwo(e)            | [BigInteger.valueOf(173), BigInteger.TWO.pow(e)]                                                                                               | false
        Set.GG(q)                       | []                                                                                                                                             | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(1), q), new QuadraticResidue(BigInteger.valueOf(25), q)]                                              | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(81), q), new QuadraticResidue(BigInteger.valueOf(64), q)]                                             | true
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(100), BigInteger.valueOf(107))]                                                                       | false
        Set.GG(q)                       | [new QuadraticResidue(BigInteger.valueOf(81), q), new QuadraticResidue(q, BigInteger.valueOf(107))]                                            | false
        Set.GG(p, q)                    | []                                                                                                                                             | true
        Set.GG(p, q)                    | [BigInteger.ONE, BigInteger.valueOf(7), BigInteger.valueOf(116)]                                                                               | true
        Set.GG(p, q)                    | [BigInteger.valueOf(28), BigInteger.valueOf(49), BigInteger.valueOf(31)]                                                                       | true
        Set.GG(p, q)                    | [BigInteger.valueOf(96), BigInteger.valueOf(-1), BigInteger.valueOf(2)]                                                                        | false
        Set.GG(p, q)                    | [q, BigInteger.valueOf(5)]                                                                                                                     | false
        Set.B(5)                        | []                                                                                                                                             | true
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[]), new ByteArray.FromSafeArray([0xA4, 0x7F, 0xCE, 0x3D, 0x32] as byte[])] | true
        Set.B(5)                        | []                                                                                                                                             | true
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04] as byte[]), new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05] as byte[])]       | false
        Set.B(5)                        | [new ByteArray.FromSafeArray([0x01, 0x02, 0x03, 0x04, 0x05, 0x06] as byte[])]                                                                  | false
        Set.UCS(5)                      | []                                                                                                                                             | true
        Set.UCS(5)                      | ["12345", "12345", "12345"]                                                                                                                    | true
        Set.UCS(5)                      | ["zzzzz", "Z5DEQ", "Fe4T7"]                                                                                                                    | true
        Set.UCS(5)                      | ["1234", "12345", "jde749d"]                                                                                                                   | false
        Set.UCS(5)                      | ["123456", "15f"]                                                                                                                              | false
        Set.String(Alphabet.LETTERS, 5) | []                                                                                                                                             | true
        Set.String(Alphabet.LETTERS, 5) | ["abcde", "uehte", "eWtZd"]                                                                                                                    | true
        Set.String(Alphabet.LETTERS, 5) | ["vwxyz", "irhts", "eurht"]                                                                                                                    | true
        Set.String(Alphabet.LETTERS, 5) | ["djebt", "heurh", "abc"]                                                                                                                      | false
        Set.String(Alphabet.LETTERS, 5) | ["abcd5", "e8gh4", "ehwk"]                                                                                                                     | false

    }

    @Unroll
    def "containsAll(Collection<T> values) for Phi set #x"() {

        given:
        def collection = new ArrayList()
        x.each { array ->
            def permutationBuilder = new IntVector.Builder(array.size())
            array.each {
                permutationBuilder.addValue(it)
            }
            def permutation = permutationBuilder.build()
            collection.add(permutation)
        }

        expect:
        set.containsAll(collection) == expected


        where:
        set        | x                                                                | expected
        Set.Phi(5) | [[1, 2, 3, 4, 5], [1, 2, 3, 4, 5], [2, 4, 1, 3, 5]]              | true
        Set.Phi(5) | [[5, 2, 3, 1, 4], [2, 5, 1, 3, 4], [5, 1, 2, 4, 3]]              | true
        Set.Phi(5) | [[35, 47, 19, 371, 21], [5, 2, 3, 1], [1, 83, 485, 1287, 78]]    | false
        Set.Phi(5) | [[1, 2, 3, 4, 5], [2398, 9457, 2648, 1983, 35], [5, 2, 3, 1, 5]] | false

    }

    def "orNull()"() {
        expect:
        set.contains(null) == false
        set.orNull().contains(null) == true
        where:
        set                             | _
        Set.NN                          | _
        Set.NN_plus                     | _
        Set.ZZ(p)                       | _
        Set.ZZ_powerOfTwo(e)            | _
        Set.GG(q)                       | _
        Set.GG(p, q)                    | _
        Set.B(5)                        | _
        Set.UCS(5)                      | _
        Set.String(Alphabet.LETTERS, 5) | _

    }


    def "ZZ(BigInteger n): should throw IllegalArgumentException for n = null and n <= 0 "() {
        when:
        Set.ZZ(x)
        then:
        thrown(IllegalArgumentException)

        where:
        x                     | _
        null                  | _
        BigInteger.valueOf(0) | _
    }


    def "ZZ_powerOfTwo(int e): should throw ArithmeticException fore < 0 "() {
        when:
        Set.ZZ_powerOfTwo(x)
        then:
        thrown(ArithmeticException)

        where:
        x  | _
        -1 | _
    }

    def "GG(BigInteger p): should throw IllegalArgumentException for p = null and p <= 0 "() {
        when:
        Set.GG(x)
        then:
        thrown(IllegalArgumentException)

        where:
        x                     | _
        null                  | _
        BigInteger.valueOf(0) | _
    }

    def "GG(BigInteger p, BigInteger q): should throw IllegalArgumentException case: #caseDesc"() {
        when:
        Set.GG(x, y)
        then:
        thrown(IllegalArgumentException)

        where:
        caseDesc                  | x                      | y
        "p is null"               | null                   | q
        "q is null"               | p                      | null
        "p <= q"                  | q                      | p
        "q does not divide p -1 " | BigInteger.valueOf(83) | BigInteger.valueOf(17)
    }

    def "String(Alphabet alphabet, int n): should throw IllegalArgumentException for alphabet = null "() {
        when:
        Set.String(alphabet, n)
        then:
        thrown(IllegalArgumentException)

        where:
        alphabet | n
        null     | 5
    }
}
