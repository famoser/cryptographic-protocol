/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.*;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/**
 * Objects of this class can be used to serialize arbitrary objects of various types into strings and back. The supported
 * types are: {@link Integer}, {@link BigInteger}, {@link QuadraticResidue}, {@link ByteArray}, {@link String},
 * {@link Boolean}, {@link IntVector}, {@link IntMatrix}, {@link Vector}, {@link Matrix}, {@link Tuple} ({@link Singleton},
 * {@link Pair}, {@link Triple}, etc), Sub-classes of {@link Tuple}.
 *
 * The construction of a serializer must always respect the following pattern:
 * <pre>{@code
 *    var s1 = new Serializer<BigInteger>() {};
 *    var s2 = new Serializer<Pair<String, Integer>>() {};
 *    var s3 = new Serializer<Matrix<String>>() {};
 * }</pre>
 * The additional curly brackets after calling the regular constructor are required to create an instance of an
 * anonymous sub-class. This is a technical trick to circumvent the type erasure meachnism of Java generics, which
 * normally disallows access to actual types parameters.
 *
 * @param <T> The generic type of the objects to serialize or abtained from deserialization.
 */
public class Serializer<T> {

    // various characters used for prefixing, delimiting, suffixing, or escaping
    private static final char OPEN_CHAR = '[';
    private static final char CLOSE_CHAR = ']';
    private static final char DELIMITER_CHAR = ',';
    private static final char ESCAPE_CHAR = '\\';
    private static final String OPEN = "" + OPEN_CHAR;
    private static final String CLOSE = "" + CLOSE_CHAR;
    private static final String DELIMITER = "" + DELIMITER_CHAR;
    private static final String ESCAPED_OPEN = "" + ESCAPE_CHAR + OPEN_CHAR;
    private static final String ESCAPED_CLOSE = "" + ESCAPE_CHAR + CLOSE_CHAR;

    // some constants
    private static final String TRUE = "1";
    private static final String FALSE = "0";
    private static final String NULL = "*";
    private static final SubString NULL_SUBSTRING = new SubString(NULL);

    // the actual type of the serializer
    private final Type type;

    /**
     * Constructs a new serializer object for the given type. The constructor must always be called as shown in the
     * examples given under {@link Serializer}. A {@link Exception} is thrown otherwise.
     *
     * @throws Exception if the constructor is called without following the prescribed pattern
     */
    public Serializer() throws Exception {
        Type superClass = this.getClass().getGenericSuperclass();
        if (superClass instanceof ParameterizedType) {
            // the super-class of an anonymous class always has exactly one type argument
            this.type = ((ParameterizedType) superClass).getActualTypeArguments()[0];
        } else {
            throw new Exception();
        }
    }

    /**
     * Serializes the given object of type {@code T} into a string.
     *
     * @param obj The object to serialize
     * @return The resulting serialization string
     * @throws Exception if the object consists of types that are not supported (see list of supported types
     * under {@link Serializer})
     */
    public String serialize(T obj) throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        this.serialize(stringBuilder, obj);
        return stringBuilder.toString();
    }

    /**
     * Deserializes the given string into an object of type {@code T}.
     *
     * @param str The string to deserialze
     * @return The deserialized object
     * @throws Exception if the input string cannot be deserialized into an object of type {@code T}
     */
    @SuppressWarnings("unchecked")
    public T deserialize(String str) throws Exception {
        return (T) this.deserialize(new SubString(str), this.type);
    }

    // internal deserialzation method, which passes a StringBuilder object through the recursion
    private void serialize(StringBuilder stringBuilder, Object object) {
        if (object == null) {
            this.serializeNull(stringBuilder);
        } else if (object instanceof Integer) {
            this.serializeInteger(stringBuilder, (Integer) object);
        } else if (object instanceof BigInteger) {
            this.serializeBigInteger(stringBuilder, (BigInteger) object);
        } else if (object instanceof QuadraticResidue) {
            this.serializeQuadraticResidue(stringBuilder, (QuadraticResidue) object);
        } else if (object instanceof ByteArray) {
            this.serializeByteArray(stringBuilder, (ByteArray) object);
        } else if (object instanceof String) {
            this.serializeString(stringBuilder, (String) object);
        } else if (object instanceof Boolean) {
            this.serializeBoolean(stringBuilder, (Boolean) object);
        } else if (object instanceof IntVector) {
            this.serializeIntVector(stringBuilder, (IntVector) object);
        } else if (object instanceof IntMatrix) {
            this.serializeIntMatrix(stringBuilder, (IntMatrix) object);
        } else if (object instanceof Vector) {
            this.serializeVector(stringBuilder, (Vector<?>) object);
        } else if (object instanceof Matrix) {
            this.serializeMatrix(stringBuilder, (Matrix<?>) object);
        } else if (object instanceof Tuple) {
            this.serializeTuple(stringBuilder, (Tuple) object);
        } else
            throw new Exception(object.getClass());
    }

    private Object deserialize(SubString subString, Type type) {
        if (subString.equals(NULL_SUBSTRING))
            return null;
        if (type instanceof Class<?>) {
            Class<?> clazz = (Class<?>) type;
            if (clazz == Integer.class)
                return deserializeInteger(subString);
            if (clazz == BigInteger.class)
                return deserializeBigInteger(subString);
            if (clazz == QuadraticResidue.class)
                return deserializeQuadraticResidue(subString);
            if (clazz == ByteArray.class)
                return deserializeByteArray(subString);
            if (clazz == String.class)
                return deserializeString(subString);
            if (clazz == Boolean.class)
                return deserializeBoolean(subString);
            if (clazz == IntVector.class)
                return deserializeIntVector(subString);
            if (clazz == IntMatrix.class)
                return deserializeIntMatrix(subString);
            if (Tuple.class.isAssignableFrom(clazz))
                return deserializeTuple(subString, clazz);
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            var rawType = (Class<?>) parameterizedType.getRawType();
            Type[] subTypes = parameterizedType.getActualTypeArguments();
            if (rawType == Vector.class)
                return deserializeVector(subString, subTypes[0]); // a vector will always have exactly one subtype
            if (rawType == Matrix.class)
                return deserializeMatrix(subString, subTypes[0]); // a matrix will always have exactly one subtype
            if (Tuple.class.isAssignableFrom(rawType))
                return deserializeTuple(subString, rawType, subTypes);
        }
        throw new Exception(subString.toString(), type);
    }

    private void serializeNull(StringBuilder stringBuilder) {
        stringBuilder.append(NULL);
    }

    private void serializeInteger(StringBuilder stringBuilder, Integer value) {
        stringBuilder.append(value >= 0 ? Integer.toHexString(value) : "-" + Integer.toHexString(-value));
    }

    private int deserializeInteger(SubString subString) {
        String string = subString.toString();
        try {
            return Integer.valueOf(string, 16);
        } catch (java.lang.Exception e) {
            throw new Exception(string, Integer.class);
        }
    }

    private void serializeBigInteger(StringBuilder stringBuilder, BigInteger value) {
        stringBuilder.append(value.toString(16));
    }

    private BigInteger deserializeBigInteger(SubString subString) {
        String string = subString.toString();
        try {
            return new BigInteger(string, 16);
        } catch (java.lang.Exception e) {
            throw new Exception(string, BigInteger.class);
        }
    }

    private void serializeQuadraticResidue(StringBuilder stringBuilder, QuadraticResidue quadraticResidue) {
        stringBuilder.append(OPEN);
        this.serializeBigInteger(stringBuilder, quadraticResidue.getSqrtValue());
        stringBuilder.append(DELIMITER);
        if (quadraticResidue.isOne()) {
            this.serializeNull(stringBuilder);
        } else {
            this.serializeBigInteger(stringBuilder, quadraticResidue.getModulus());
        }
        stringBuilder.append(CLOSE);
    }

    private QuadraticResidue deserializeQuadraticResidue(SubString subString) {
        var subStrings = split(subString);
        if (subStrings.size() == 2) {
            BigInteger sqrtValue = this.deserializeBigInteger(subStrings.get(0));
            if (sqrtValue.equals(BigInteger.ONE)) {
                if (subStrings.get(1).equals(NULL_SUBSTRING)) {
                    return QuadraticResidue.ONE;
                } // else throw exception in last line
            } else {
                BigInteger modulus = this.deserializeBigInteger(subStrings.get(1));
                BigInteger value = Mod.multiply(sqrtValue, sqrtValue, modulus);
                return new QuadraticResidue(value, sqrtValue, modulus);
            }
        }
        throw new Exception(subString.toString(), QuadraticResidue.class);
    }

    private void serializeByteArray(StringBuilder stringBuilder, ByteArray byteArray) {
        stringBuilder.append(OPEN);
        stringBuilder.append(byteArray);
        stringBuilder.append(CLOSE);
    }

    private ByteArray deserializeByteArray(SubString subString) {
        String string = subString.toString();
        int length = string.length();
        if (length >= 2 && string.charAt(0) == OPEN_CHAR && string.charAt(length - 1) == CLOSE_CHAR) {
            try {
                byte[] bytes = hexStringToBytes(string.substring(1, length - 1));
                return new ByteArray.FromSafeArray(bytes);
            } catch (java.lang.Exception e) {
                throw new Exception(string, ByteArray.class);
            }
        } else {
            throw new Exception(string, ByteArray.class);
        }
    }

    private void serializeString(StringBuilder stringBuilder, String string) {
        stringBuilder.append(OPEN);
        stringBuilder.append(addEscaping(string));
        stringBuilder.append(CLOSE);
    }

    private String deserializeString(SubString subString) {
        String string = subString.toString();
        int length = string.length();
        if (length >= 2 && string.charAt(0) == OPEN_CHAR && string.charAt(length - 1) == CLOSE_CHAR) {
            return removeEscaping(string.substring(1, length - 1));
        } else {
            throw new Exception(string, String.class);
        }
    }

    private void serializeBoolean(StringBuilder stringBuilder, Boolean b) {
        stringBuilder.append(b ? TRUE : FALSE);
    }

    private boolean deserializeBoolean(SubString subString) {
        String string = subString.toString();
        if (string.equals(TRUE)) {
            return true;
        }
        if (string.equals(FALSE)) {
            return false;
        }
        throw new Exception(string, Boolean.class);
    }

    private void serializeIntVector(StringBuilder stringBuilder, IntVector vector) {
        stringBuilder.append(OPEN);
        this.serializeInteger(stringBuilder, vector.getMinIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, vector.getMaxIndex());
        stringBuilder.append(DELIMITER);
        this.serializeStream(vector.toStream().boxed(), stringBuilder);
        stringBuilder.append(CLOSE);
    }

    private IntVector deserializeIntVector(SubString subString) {
        var subStrings = split(subString);
        if (subStrings.size() == 3) {
            int minIndex = this.deserializeInteger(subStrings.get(0));
            int maxIndex = this.deserializeInteger(subStrings.get(1));
            int length = maxIndex - minIndex + 1;
            var values = split(subStrings.get(2));
            if (values.size() == length) {
                var builder = new IntVector.Builder(minIndex, maxIndex);
                for (var value : values) {
                    builder.addValue(Integer.valueOf(value.toString(), 16));
                }
                return builder.build();
            }
        }
        throw new Exception(subString.toString(), IntVector.class);
    }

    private void serializeIntMatrix(StringBuilder stringBuilder, IntMatrix matrix) {
        stringBuilder.append(OPEN);
        this.serializeInteger(stringBuilder, matrix.getMinRowIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMaxRowIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMinColIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMaxColIndex());
        stringBuilder.append(DELIMITER);
        this.serializeStream(matrix.toStream().boxed(), stringBuilder);
        stringBuilder.append(CLOSE);
    }

    private IntMatrix deserializeIntMatrix(SubString subString) {
        var subStrings = split(subString);
        if (subStrings.size() == 5) {
            int minRowIndex = this.deserializeInteger(subStrings.get(0));
            int maxRowIndex = this.deserializeInteger(subStrings.get(1));
            int minColIndex = this.deserializeInteger(subStrings.get(2));
            int maxColIndex = this.deserializeInteger(subStrings.get(3));
            int height = maxRowIndex - minRowIndex + 1;
            int width = maxColIndex - minColIndex + 1;
            var values = split(subStrings.get(4));
            if (values.size() == height * width) {
                var builder = new IntMatrix.Builder(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
                for (var value : values) {
                    builder.addValue(Integer.valueOf(value.toString(), 16));
                }
                return builder.build();
            }
        }
        throw new Exception(subString.toString(), IntMatrix.class);
    }

   private void serializeVector(StringBuilder stringBuilder, Vector<?> vector) {
        stringBuilder.append(OPEN);
        this.serializeInteger(stringBuilder, vector.getMinIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, vector.getMaxIndex());
        stringBuilder.append(DELIMITER);
        this.serializeStream(vector.toStream(), stringBuilder);
        stringBuilder.append(CLOSE);
    }

    private Vector<?> deserializeVector(SubString subString, Type subType) {
        var subStrings = split(subString);
        if (subStrings.size() == 3) {
            int minIndex = this.deserializeInteger(subStrings.get(0));
            int maxIndex = this.deserializeInteger(subStrings.get(1));
            int length = maxIndex - minIndex + 1;
            var values = split(subStrings.get(2));
            if (values.size() == length) {
                var builder = new Vector.Builder<>(minIndex, maxIndex);
                for (var value : values) {
                    builder.addValue(this.deserialize(value, subType));
                }
                return builder.build();
            }
        }
        throw new Exception(subString.toString(), Vector.class);
    }

    private void serializeMatrix(StringBuilder stringBuilder, Matrix<?> matrix) {
        stringBuilder.append(OPEN);
        this.serializeInteger(stringBuilder, matrix.getMinRowIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMaxRowIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMinColIndex());
        stringBuilder.append(DELIMITER);
        this.serializeInteger(stringBuilder, matrix.getMaxColIndex());
        stringBuilder.append(DELIMITER);
        this.serializeStream(matrix.toStream(), stringBuilder);
        stringBuilder.append(CLOSE);
    }

    private Matrix<?> deserializeMatrix(SubString subString, Type subType) {
        var subStrings = split(subString);
        if (subStrings.size() == 5) {
            int minRowIndex = this.deserializeInteger(subStrings.get(0));
            int maxRowIndex = this.deserializeInteger(subStrings.get(1));
            int minColIndex = this.deserializeInteger(subStrings.get(2));
            int maxColIndex = this.deserializeInteger(subStrings.get(3));
            int height = maxRowIndex - minRowIndex + 1;
            int width = maxColIndex - minColIndex + 1;
            var values = split(subStrings.get(4));
            if (values.size() == height * width) {
                var builder = new Matrix.Builder<>(minRowIndex, maxRowIndex, minColIndex, maxColIndex);
                for (var value : values) {
                    builder.addValue(this.deserialize(value, subType));
                }
                return builder.build();
            }
        }
        throw new Exception(subString.toString(), Matrix.class);
    }

    private void serializeTuple(StringBuilder stringBuilder, Tuple tuple) {
        this.serializeStream(tuple.toStream(), stringBuilder);
    }

    private Tuple deserializeTuple(SubString subString, Class<?> clazz) {
        Type[] subTypes;
        if (NullTuple.class.isAssignableFrom(clazz)) {
            subTypes = new Type[0];
        } else {
            ParameterizedType superType = (ParameterizedType) clazz.getGenericSuperclass();
            subTypes = superType.getActualTypeArguments();
        }
        return deserializeTuple(subString, clazz, subTypes);
    }

    private Tuple deserializeTuple(SubString subString, Class<?> rawType, Type[] subTypes) {
        var subStrings = split(subString);
        if (subTypes.length == subStrings.size()) {
            Object[] values = new Object[subTypes.length];
            for (int i = 0; i < subTypes.length; i++) {
                values[i] = this.deserialize(subStrings.get(i), subTypes[i]);
            }
            try {
                return (Tuple) rawType.getConstructors()[0].newInstance(values);
            } catch (java.lang.Exception e) {
                throw new Exception(subString.toString(), rawType);
            }
        }
        throw new Exception(subString.toString(), rawType);
    }

    private void serializeStream(Stream<?> stream, StringBuilder stringBuilder) {
        stringBuilder.append(OPEN);
        int oldLength = stringBuilder.length();
        stream.forEach(v -> {
            this.serialize(stringBuilder, v);
            stringBuilder.append(DELIMITER);
        });
        int newLength = stringBuilder.length() - 1; // without last comma
        stringBuilder.setLength(Math.max(oldLength, newLength));
        stringBuilder.append(CLOSE);
    }

    // expects a string representation of a tree with balanced parentheses defined by open and close, splits the
    // string into a list of top-level children defined by delim
    private static List<SubString> split(SubString subString) {
        List<SubString> result = new ArrayList<>();
        int length = subString.length();
        if (length < 2 || subString.charAt(0) != OPEN_CHAR || subString.charAt(length - 1) != CLOSE_CHAR)
            throw new Exception(subString.toString());
        if (length > 2) {
            int counter = 0;
            int i = 1;
            for (int j = 1; j < length; j++) {
                if (counter < 0)
                    throw new Exception(subString.toString());
                char c = subString.charAt(j);
                if (c == DELIMITER_CHAR || j == length - 1) {
                    if (counter == 0) {
                        result.add(subString.substring(i, j));
                        i = j + 1;
                    }
                } else {
                    if (c == OPEN_CHAR && subString.charAt(j - 1) != ESCAPE_CHAR)
                        counter++;
                    if (c == CLOSE_CHAR && subString.charAt(j - 1) != ESCAPE_CHAR)
                        counter--;
                }
            }
            if (counter < 0)
                throw new Exception(subString.toString());
        }
        return result;
    }

    private static byte[] hexStringToBytes(String str) {
        if (str.length() % 2 != 0)
            throw new NumberFormatException();
        int length = str.length() / 2;
        byte[] bytes = new byte[length];
        for (int i = 0; i < length; i++) {
            bytes[i] = (byte) Integer.parseInt(str.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }

    private static String addEscaping(String str) {
        return str.replace(OPEN, ESCAPED_OPEN).replace(CLOSE, ESCAPED_CLOSE);
    }

    private static String removeEscaping(String str) {
        return str.replace(ESCAPED_OPEN, OPEN).replace(ESCAPED_CLOSE, CLOSE);
    }

    /**
     * This class is used for throwing specific exceptions while executing the serialization algorithms. The different types of
     * serialization exceptions are represented as an internal enum type.
     */
    public static class Exception extends RuntimeException {

        public enum Type {
            CONSTRUCTION_FAILED,
            SERIALIZATION_FAILED,
            DESERIALIZATION_FAILED
        }

        private final Type exceptionType;
        private final String string;
        private final java.lang.reflect.Type type;

        public Exception() {
            this(Type.CONSTRUCTION_FAILED, "", null);
        }

        public Exception(java.lang.reflect.Type type) {
            this(Type.SERIALIZATION_FAILED, "", type);
        }

        public Exception(String string) {
            this(Type.DESERIALIZATION_FAILED, string, null);
        }

        public Exception(String string, java.lang.reflect.Type type) {
            this(Type.DESERIALIZATION_FAILED, string, type);
        }

        private Exception(Type exceptionType, String string, java.lang.reflect.Type type) {
            super("Error with serialization/deserialization: " + exceptionType.name() + ": " + string);
            this.exceptionType = exceptionType;
            this.string = string;
            this.type = type;
        }

        public Type getExceptionType() {
            return this.exceptionType;
        }

        public String getString() {
            return this.string;
        }

        public java.lang.reflect.Type getType() {
            return this.type;
        }

    }

}
