/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.Iterator;
import java.util.Objects;
import java.util.SortedMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Objects of this generic class represent immutable matrices of a fixed height {@code n} and width {@code m}. Their
 * elements are indexed by two indices from {@code minRowIndex} (usually {@code 1}) to {@code maxRowIndex} (usually {@code n})
 * and {@code minColIndex} (usually {@code 1}) to {@code maxColIndex} (usually {@code m}). Since this class is abstract, it does not
 * offer public constructors. New instances of this class are created using the static member classes {@link Matrix.Builder},
 * {@link Matrix.RowBuilder}, and {@link Matrix.ColBuilder}. During the building process, values can be added to or placed
 * into an initially empty matrix containing {@code null} values. After the building process, accessing the values in
 * the resulting matrix is restricted to read-only. The class implements the {@link Iterable} interface.
 *
 * @param <V> The generic type of the values stored in the matrix
 */
public abstract class Matrix<V> extends Hashable implements Iterable<V> {

    /**
     * Returns the value at the given row and column index. For an invalid indices, an exception is thrown.
     *
     * @param rowIndex The row index
     * @param colIndex The column index
     * @return The value at this row and column index
     * @throws IndexOutOfBoundsException if invalid indices are given
     */
    public abstract V getValue(int rowIndex, int colIndex);

    /**
     * Returns the minimal row index of this matrix.
     *
     * @return The minimal row index
     */
    public abstract int getMinRowIndex();

    /**
     * Returns the maximal row index of this matrix.
     *
     * @return The maximal row index
     */
    public abstract int getMaxRowIndex();

    /**
     * Returns the minimal column index of this matrix.
     *
     * @return The minimal column index
     */
    public abstract int getMinColIndex();

    /**
     * Returns the maximal column index of this matrix.
     *
     * @return The maximal column index
     */
    public abstract int getMaxColIndex();

    /**
     * Returns the height of the matrix
     *
     * @return The height of the matrix
     */
    public int getHeight() {
        return this.getMaxRowIndex() - this.getMinRowIndex() + 1;
    }

    /**
     * Returns the width of the matrix
     *
     * @return The width of the matrix
     */
    public int getWidth() {
        return this.getMaxColIndex() - this.getMinColIndex() + 1;
    }

    /**
     * Returns a vector consisting of the matrice's row vectors.
     *
     * @return The vector of row vectors
     */
    public Vector<Vector<V>> getRows() {
        Matrix<V> matrix = this;
        return new Vector<>() {

            @Override
            public Vector<V> getValue(int index) {
                return matrix.getRow(index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxRowIndex();
            }
        };
    }

    /**
     * Returns a vector consisting of the matrice's column vectors.
     *
     * @return The vector of column vectors
     */
    public Vector<Vector<V>> getCols() {
        Matrix<V> matrix = this;
        return new Vector<>() {

            @Override
            public Vector<V> getValue(int index) {
                return matrix.getCol(index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the row vector at the given index.
     *
     * @param rowIndex The given row index
     * @return The row vector at the given index
     * @throws IndexOutOfBoundsException if an invalid row index is given
     */
    public Vector<V> getRow(int rowIndex) {
        if (rowIndex < this.getMinRowIndex() || rowIndex > this.getMaxRowIndex())
            throw new IndexOutOfBoundsException();
        Matrix<V> matrix = this;
        return new Vector<>() {

            @Override
            public V getValue(int index) {
                return matrix.getValue(rowIndex, index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the column vector at the given index.
     *
     * @param colIndex The given column index
     * @return The column vector at the given index
     * @throws IndexOutOfBoundsException if an invalid column index is given
     */
    public Vector<V> getCol(int colIndex) {
        if (colIndex < this.getMinColIndex() || colIndex > this.getMaxColIndex())
            throw new IndexOutOfBoundsException();
        Matrix<V> matrix = this;
        return new Vector<>() {

            @Override
            public V getValue(int index) {
                return matrix.getValue(index, colIndex);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxRowIndex();
            }
        };
    }

    /**
     * Creates a new matrix by applying a function to each value of the matrix. Depending on the specified function, the
     * type {@code W} of the returned matrix may be different from the type {@code V} of the original matrix. {@code
     * null} values are mapped into the specified value {@code defaultValue}. By returning a wrapper object which
     * performs the mapping lazily, this method runs ins constant time.
     *
     * @param function     The function that maps the values of the matrix
     * @param defaultValue the default value that replaces {@code null} values
     * @param <W>          The type of the returned matrix
     * @return A matrix containing all mapped values
     */
    public <W> Matrix<W> map(Function<? super V, ? extends W> function, W defaultValue) {
        Matrix<V> matrix = this;
        return new Matrix<>() {

            @Override
            public W getValue(int rowIndex, int colIndex) {
                V value = matrix.getValue(rowIndex, colIndex);
                return value == null ? defaultValue : function.apply(value);
            }

            @Override
            public int getMinRowIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return matrix.getMaxRowIndex();
            }

            @Override
            public int getMinColIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxColIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the transposed matrix, in which the row and column indices and the height and width are swapped.
     *
     * @return The transposed matrix
     */
    public Matrix<V> transpose() {
        Matrix<V> matrix = this;
        return new Matrix<>() {

            @Override
            public V getValue(int rowIndex, int colIndex) {
                return matrix.getValue(colIndex, rowIndex);
            }

            @Override
            public int getMinRowIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return matrix.getMaxColIndex();
            }

            @Override
            public int getMinColIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxColIndex() {
                return matrix.getMaxRowIndex();
            }

            @Override
            public Matrix<V> transpose() {
                return matrix;
            }
        };
    }

    /**
     * Returns a stream containing all values of the matrix. The values in the stream are ordered row-wise from
     * left to right.
     *
     * @return The stream of values obtained from the matrix
     */
    public Stream<V> toStream() {
        return this.getIndices().mapToObj(this::getValue);
    }

    /**
     * Checks if all values of the matrix are equal.
     *
     * @return {@code true}, if all values are equals, {@code false}, otherwise
     */
    public boolean isUniform() {
        return this.toStream().skip(1).allMatch(value -> Objects.equals(value, this.getValue(0)));
    }

    // private helper method for generating a stream of all matrix indices
    private IntStream getIndices() {
        return IntStream.rangeClosed(0, this.getHeight() * this.getWidth() - 1);
    }

    // private helper method for mapping a matrix index into a row and columns index
    private V getValue(int index) {
        int rowIndex = index / this.getWidth() + this.getMinRowIndex();
        int colIndex = index % this.getWidth() + this.getMinColIndex();
        return this.getValue(rowIndex, colIndex);
    }

    @Override
    public Iterator<V> iterator() {
        Matrix<V> matrix = this;
        return new Iterator<>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return this.currentIndex <= matrix.getWidth() * matrix.getHeight() - 1;
            }

            @Override
            public V next() {
                return matrix.getValue(this.currentIndex++);
            }
        };
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Matrix)) return false;

        Matrix<?> other = (Matrix<?>) object;
        if (this.getMinRowIndex() != other.getMinRowIndex()) return false;
        if (this.getMaxRowIndex() != other.getMaxRowIndex()) return false;
        if (this.getMinColIndex() != other.getMinColIndex()) return false;
        if (this.getMaxColIndex() != other.getMaxColIndex()) return false;

        return this.getIndices().allMatch(index -> Objects.equals(this.getValue(index), other.getValue(index)));
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.getMinRowIndex();
        initValue = 31 * initValue + this.getMaxRowIndex();
        initValue = 31 * initValue + this.getMinColIndex();
        initValue = 31 * initValue + this.getMaxColIndex();
        return this.toStream().mapToInt(Objects::hashCode).reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        if (this.getHeight() == 0 || this.getWidth() == 0) {
            return "[]\n";
        }
        int maxLength = this.toStream().map(Objects::toString).mapToInt(String::length).max().orElse(0);
        String valueFormat = "%" + maxLength + "s";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.getHeight(); i++) {
            String prefix, suffix;
            if (this.getHeight() == 1) {
                prefix = "[";
                suffix = "]";
            } else if (i == 0) {
                prefix = "⌈";
                suffix = "⌉";
            } else if (i == this.getHeight() - 1) {
                prefix = "⌊";
                suffix = "⌋";
            } else {
                prefix = "|";
                suffix = "|";
            }
            String row = this.getRow(this.getMinRowIndex() + i).toStream().map(value -> String.format(valueFormat, value)).collect(Collectors.joining(" ", prefix, suffix));
            stringBuilder.append(row);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * This static builder class is the main tool for constructing matrices from scratch. The height and width of the
     * matrix are defined at the beginning of the building process. All values are initially all set to {@code null}.
     * Values can be added either incrementally (row-wise from left to right) or in arbitrary order. At the end of the
     * building process, the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix
     * is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    public static class Builder<V> {

        // flag that determines whether the matrix has already been built or not
        private boolean built;
        // the first and the last valid row indices
        private final int minRowIndex;
        private final int maxRowIndex;
        // the first and the last valid column indices
        private final int minColIndex;
        private final int maxColIndex;
        // an array for storing the added values during the building process
        private final V[][] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public Builder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and width
         * {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        @SuppressWarnings("unchecked")
        public Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.built = false;
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
            this.values = (V[][]) new Object[maxRowIndex - minRowIndex + 1][maxColIndex - minColIndex + 1];
            this.indexCounter = 0;
        }

        /**
         * Fills up the matrix with a single value.
         *
         * @param value The value used for filling up the matrix
         * @return The matrix builder itself
         */
        public Matrix.Builder<V> fill(V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                for (int i = this.minRowIndex; i <= this.maxRowIndex; i++) {
                    for (int j = this.minColIndex; j <= this.maxColIndex; j++) {
                        this.setValue(i, j, value);
                    }
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given row and column indices. The matrix builder object itself is returned to
         * allow pipeline notation when multiple values are added to the matrix.
         *
         * @param rowIndex The row index of the value to be added
         * @param colIndex The column index of the value to be added
         * @param value The value to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public Builder<V> setValue(int rowIndex, int colIndex, V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex || colIndex < this.minColIndex || colIndex > this.maxColIndex)
                    throw new IndexOutOfBoundsException();
                this.values[rowIndex - this.minRowIndex][colIndex - this.minColIndex] = value;
                return this;
            }
        }

        /**
         * Sets the next value in the matrix and increases the internal index counter by 1. In this way, values are
         * added row-wise from left to right. The matrix builder object itself is returned to allow pipeline notation
         * when multiple values are added to the matrix.
         *
         * @param value The value to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder<V> addValue(V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                int width = this.maxColIndex - this.minColIndex + 1;
                int currentRowIndex, currentColIndex;
                currentRowIndex = this.minRowIndex + this.indexCounter / width;
                currentColIndex = this.minColIndex + this.indexCounter % width;
                this.indexCounter = this.indexCounter + 1;
                return this.setValue(currentRowIndex, currentColIndex, value);
            }
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public Matrix<V> build() {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                this.built = true;
                return new Matrix<>() {
                    @Override
                    public V getValue(int rowIndex, int colIndex) {
                        if (rowIndex < minRowIndex || rowIndex > maxRowIndex || colIndex < minColIndex || colIndex > maxColIndex)
                            throw new IndexOutOfBoundsException();
                        return values[rowIndex - minRowIndex][colIndex - minColIndex];
                    }

                    @Override
                    public int getMinRowIndex() {
                        return minRowIndex;
                    }

                    @Override
                    public int getMaxRowIndex() {
                        return maxRowIndex;
                    }

                    @Override
                    public int getMinColIndex() {
                        return minColIndex;
                    }

                    @Override
                    public int getMaxColIndex() {
                        return maxColIndex;
                    }
                };
            }
        }
    }

    /**
     * This static builder class can be used for constructing matrices from given row vectors. The height and width of the
     * matrix are defined at the beginning of the building process. All values are initially all set to {@code null}.
     * Rows can be added either incrementally or in arbitrary order. At the end of the building process, the matrix can
     * be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    public static class RowBuilder<V> {

        // uses a vector builder internally
        private final Vector.Builder<Vector<V>> vectorBuilder;
        private final int minColIndex;
        private final int maxColIndex;

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public RowBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.vectorBuilder = new Vector.Builder<>(minRowIndex, maxRowIndex);
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
        }

        /**
         * Sets the given row vector at the given index. The matrix builder object itself is returned to
         * allow pipeline notation when multiple rows are added to the matrix.
         *
         * @param rowIndex The row index of the row vector to be added
         * @param row The row vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public RowBuilder<V> setRow(int rowIndex, Vector<V> row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.setValue(rowIndex, row);
            return this;
        }

        /**
         * Sets the next row vector in the matrix and increases the internal index counter by 1. In this way, row vectors
         * are added incrementally. The matrix builder object itself is returned to allow pipeline notation
         * when multiple row vectors are added to the matrix.
         *
         * @param row The row vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public RowBuilder<V> addRow(Vector<V> row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.addValue(row);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public Matrix<V> build() {
            Vector<Vector<V>> vector = this.vectorBuilder.build();
            return new Matrix<>() {

                @Override
                public V getValue(int rowIndex, int colIndex) {
                    Vector<V> row = vector.getValue(rowIndex);
                    return row == null ? null : row.getValue(colIndex);
                }

                @Override
                public int getMinRowIndex() {
                    return vector.getMinIndex();
                }

                @Override
                public int getMaxRowIndex() {
                    return vector.getMaxIndex();
                }

                @Override
                public int getMinColIndex() {
                    return minColIndex;
                }

                @Override
                public int getMaxColIndex() {
                    return maxColIndex;
                }
            };
        }
    }

    /**
     * This static builder class can be used for constructing matrices from given column vectors. The height and width of the
     * matrix are defined at the beginning of the building process. All values are initially all set to {@code null}.
     * Rows can be added either incrementally or in arbitrary order. At the end of the building process, the matrix can
     * be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     *
     * @param <V> The generic type of the matrix to build
     */
    public static class ColBuilder<V> {

        // uses a vector builder internally
        private final Vector.Builder<Vector<V>> vectorBuilder;
        private final int minRowIndex;
        private final int maxRowIndex;

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public ColBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.vectorBuilder = new Vector.Builder<>(minColIndex, maxColIndex);
        }

        /**
         * Sets the given column vector at the given index. The matrix builder object itself is returned to
         * allow pipeline notation when multiple rows are added to the matrix.
         *
         * @param colIndex The column index of the column vector to be added
         * @param col The column vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public ColBuilder<V> setCol(int colIndex, Vector<V> col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.setValue(colIndex, col);
            return this;
        }

        /**
         * Sets the next column vector in the matrix and increases the internal index counter by 1. In this way, column
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation
         * when multiple column vectors are added to the matrix.
         *
         * @param col The column vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public ColBuilder<V> addCol(Vector<V> col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.addValue(col);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public Matrix<V> build() {
            Vector<Vector<V>> vector = this.vectorBuilder.build();
            return new Matrix<>() {

                @Override
                public V getValue(int rowIndex, int colIndex) {
                    Vector<V> col = vector.getValue(colIndex);
                    return col == null ? null : col.getValue(rowIndex);
                }

                @Override
                public int getMinRowIndex() {
                    return minRowIndex;
                }

                @Override
                public int getMaxRowIndex() {
                    return maxRowIndex;
                }

                @Override
                public int getMinColIndex() {
                    return vector.getMinIndex();
                }

                @Override
                public int getMaxColIndex() {
                    return vector.getMaxIndex();
                }
            };
        }
    }

    /**
     * This static method allows constructing new matrices from a given map of keys of type {@code Integer} and values of
     * type {@code Vector<V>}. The map must be safe to use without copying its values. This method must therefore be used
     * with maximal care. Each of the vectors in the map defines a column vector in the resulting matrix. The keys of
     * the map are the column indices in the newly created matrix, i.e., the indexing of the column vectors in the
     * matrix goes from the smallest to the largest key in the map.
     *
     * @param colMap The given map of column vectors
     * @return The constructed matrix
     */
    public static <V> Matrix<V> fromSafeColMap(SortedMap<Integer, Vector<V>> colMap) {
        return Matrix.fromColVector(Vector.fromSafeMap(colMap));
    }

    // private helper method for constructing a matrix from given vector of column vectors
    private static <V> Matrix<V> fromColVector(Vector<Vector<V>> colVector) {
        if (!colVector.map(Vector::getMinIndex).isUniform() || !colVector.map(Vector::getMaxIndex).isUniform())
            throw new IllegalArgumentException();
        return new Matrix<>() {

            @Override
            public V getValue(int rowIndex, int colIndex) {
                var col = colVector.getValue(colIndex);
                return col == null ? null : col.getValue(rowIndex);
            }

            @Override
            public int getMinRowIndex() {
                return colVector.getLength() == 0 ? 1 : colVector.getValue(colVector.getMinIndex()).getMinIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return colVector.getLength() == 0 ? 0 : colVector.getValue(colVector.getMinIndex()).getMaxIndex();
            }

            @Override
            public int getMinColIndex() {
                return colVector.getMinIndex();
            }

            @Override
            public int getMaxColIndex() {
                return colVector.getMaxIndex();
            }
        };

    }

}
