/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.Iterator;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Objects of this class represent immutable matrices of {@code int} values of a fixed height {@code n} and width
 * {@code m}. Their elements are indexed by two indices from {@code minRowIndex} (usually {@code 1}) to {@code maxRowIndex}
 * (usually {@code n}) and {@code minColIndex} (usually {@code 1}) to {@code maxColIndex} (usually {@code m}). Since this
 * class is abstract, it does not offer public constructors. New instances of this class are created using the static member
 * classes {@link IntMatrix.Builder}, {@link IntMatrix.RowBuilder}, and {@link IntMatrix.ColBuilder}. During the building
 * process, values can be added to or placed into an initially empty matrix containing zeros. After the building process,
 * accessing the values in the resulting vector is restricted to read-only. The class implements the {@link Iterable}
 * interface.
 */
public abstract class IntMatrix extends Hashable implements Iterable<Integer> {

    /**
     * Returns the {@code int} value at the given row and column index. For an invalid indices, an exception is thrown.
     *
     * @param rowIndex The row index
     * @param colIndex The column index
     * @return The {@code int} value at this row and column index
     * @throws IndexOutOfBoundsException if invalid indices are given
     */
    public abstract int getValue(int rowIndex, int colIndex);

    /**
     * Returns the minimal row index of this matrix.
     *
     * @return The minimal row index
     */
    public abstract int getMinRowIndex();

    /**
     * Returns the maximal row index of this matrix.
     *
     * @return The maximal row index
     */
    public abstract int getMaxRowIndex();

    /**
     * Returns the minimal column index of this matrix.
     *
     * @return The minimal column index
     */
    public abstract int getMinColIndex();

    /**
     * Returns the maximal column index of this matrix.
     *
     * @return The maximal column index
     */
    public abstract int getMaxColIndex();

    /**
     * Returns the height of the matrix
     *
     * @return The height of the matrix
     */
    public int getHeight() {
        return this.getMaxRowIndex() - this.getMinRowIndex() + 1;
    }

    /**
     * Returns the width of the matrix
     *
     * @return The width of the matrix
     */
    public int getWidth() {
        return this.getMaxColIndex() - this.getMinColIndex() + 1;
    }

    /**
     * Returns a vector consisting of the matrice's row vectors.
     *
     * @return The vector of row vectors
     */
    public Vector<IntVector> getRows() {
        IntMatrix matrix = this;
        return new Vector<>() {

            @Override
            public IntVector getValue(int index) {
                return matrix.getRow(index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxRowIndex();
            }
        };
    }

    /**
     * Returns a vector consisting of the matrice's column vectors.
     *
     * @return The vector of column vectors
     */
    public Vector<IntVector> getCols() {
        IntMatrix matrix = this;
        return new Vector<>() {

            @Override
            public IntVector getValue(int index) {
                return matrix.getCol(index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the row vector at the given index.
     *
     * @param rowIndex The given row index
     * @return The row vector at the given index
     * @throws IndexOutOfBoundsException if an invalid row index is given
     */
    public IntVector getRow(int rowIndex) {
        if (rowIndex < this.getMinRowIndex() || rowIndex > this.getMaxRowIndex())
            throw new IndexOutOfBoundsException();
        IntMatrix matrix = this;
        return new IntVector() {

            @Override
            public int getValue(int index) {
                return matrix.getValue(rowIndex, index);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the column vector at the given index.
     *
     * @param colIndex The given column index
     * @return The column vector at the given index
     * @throws IndexOutOfBoundsException if an invalid column index is given
     */
    public IntVector getCol(int colIndex) {
        if (colIndex < this.getMinColIndex() || colIndex > this.getMaxColIndex())
            throw new IndexOutOfBoundsException();
        IntMatrix matrix = this;
        return new IntVector() {

            @Override
            public int getValue(int index) {
                return matrix.getValue(index, colIndex);
            }

            @Override
            public int getMinIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxIndex() {
                return matrix.getMaxRowIndex();
            }
        };
    }

    /**
     * Creates a new {@code int} matrix by applying a function to each {@code int} value of the matrix. By returning a
     * wrapper object which performs the mapping lazily, this method runs ins constant time.
     *
     * @param function     The function that maps the values of the matrix
     * @return A matrix containing all mapped values
     */
    public IntMatrix map(IntUnaryOperator function) {
        IntMatrix matrix = this;
        return new IntMatrix() {

            @Override
            public int getValue(int rowIndex, int colIndex) {
                return function.applyAsInt(matrix.getValue(rowIndex, colIndex));
            }

            @Override
            public int getMinRowIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return matrix.getMaxRowIndex();
            }

            @Override
            public int getMinColIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxColIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Creates a new matrix by applying a function to each value of the matrix. The type {@code W} of the returned matrix
     * depends on the specified function. By returning a wrapper object which performs the mapping lazily, this method
     * runs ins constant time.
     *
     * @param function     The function that maps the values of the matrix
     * @param <W>          The type of the returned matrix
     * @return A matrix containing all mapped values
     */

    public <W> Matrix<W> map(IntFunction<? extends W> function) {
        IntMatrix matrix = this;
        return new Matrix<>() {

            @Override
            public W getValue(int rowIndex, int colIndex) {
                return function.apply(matrix.getValue(rowIndex, colIndex));
            }

            @Override
            public int getMinRowIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return matrix.getMaxRowIndex();
            }

            @Override
            public int getMinColIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxColIndex() {
                return matrix.getMaxColIndex();
            }
        };
    }

    /**
     * Returns the transposed matrix, in which the row and column indices and the height and width are swapped.
     *
     * @return The transposed matrix
     */
    public IntMatrix transpose() {
        IntMatrix matrix = this;
        return new IntMatrix() {

            @Override
            public int getValue(int rowIndex, int colIndex) {
                return matrix.getValue(colIndex, rowIndex);
            }

            @Override
            public int getMinRowIndex() {
                return matrix.getMinColIndex();
            }

            @Override
            public int getMaxRowIndex() {
                return matrix.getMaxColIndex();
            }

            @Override
            public int getMinColIndex() {
                return matrix.getMinRowIndex();
            }

            @Override
            public int getMaxColIndex() {
                return matrix.getMaxRowIndex();
            }

            @Override
            public IntMatrix transpose() {
                return matrix;
            }
        };
    }

    /**
     * Returns a new int matrix containing the same rows sorted in ascending order.
     *
     * @return The sorted matrix
     */
    public IntMatrix rowSort() {
        var builder = new RowBuilder(this.getMinRowIndex(), this.getMaxRowIndex(), this.getMinColIndex(), this.getMaxColIndex());
        this.getRows().toStream().sorted().forEach(builder::addRow);
        return builder.build();
    }

    /**
     * Returns a new int matrix containing the same columns sorted in ascending order.
     *
     * @return The sorted matrix
     */
    public IntMatrix colSort() {
        var builder = new ColBuilder(this.getMinRowIndex(), this.getMaxRowIndex(), this.getMinColIndex(), this.getMaxColIndex());
        this.getCols().toStream().sorted().forEach(builder::addCol);
        return builder.build();
    }

    /**
     * Returns a {@code int} stream containing all {@code int} values of the matrix. The values in the stream are
     * ordered row-wise from left to right.
     *
     * @return The stream of values obtained from the matrix
     */
    public IntStream toStream() {
        return this.getIndices().map(this::getValue);
    }

    /**
     * Checks if all values of the matrix are equal.
     *
     * @return {@code true}, if all values are equals, {@code false}, otherwise
     */
    public boolean isUniform() {
        return this.toStream().skip(1).allMatch(value -> value == this.getValue(0));
    }

    // private helper method for generating a stream of all matrix indices
    private IntStream getIndices() {
        return IntStream.rangeClosed(0, this.getHeight() * this.getWidth() - 1);
    }

    // private helper method for mapping a matrix index into a row and columns index
    private int getValue(int index) {
        int rowIndex = index / this.getWidth() + this.getMinRowIndex();
        int colIndex = index % this.getWidth() + this.getMinColIndex();
        return this.getValue(rowIndex, colIndex);
    }

    @Override
    public Iterator<Integer> iterator() {
        IntMatrix matrix = this;
        return new Iterator<>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return this.currentIndex <= matrix.getWidth() * matrix.getHeight() - 1;
            }

            @Override
            public Integer next() {
                return matrix.getValue(this.currentIndex++);
            }
        };
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof IntMatrix)) return false;

        IntMatrix other = (IntMatrix) object;
        if (this.getMinRowIndex() != other.getMinRowIndex()) return false;
        if (this.getMaxRowIndex() != other.getMaxRowIndex()) return false;
        if (this.getMinColIndex() != other.getMinColIndex()) return false;
        if (this.getMaxColIndex() != other.getMaxColIndex()) return false;

        return this.getIndices().allMatch(index -> this.getValue(index) == other.getValue(index));
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.getMinRowIndex();
        initValue = 31 * initValue + this.getMaxRowIndex();
        initValue = 31 * initValue + this.getMinColIndex();
        initValue = 31 * initValue + this.getMaxColIndex();
        return this.toStream().reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        if (this.getHeight() == 0 || this.getWidth() == 0) {
            return "[]\n";
        }
        int maxLength = this.toStream().mapToObj(Integer::toString).mapToInt(String::length).max().orElse(0);
        String valueFormat = "%" + maxLength + "s";
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.getHeight(); i++) {
            String prefix, suffix;
            if (this.getHeight() == 1) {
                prefix = "[";
                suffix = "]";
            } else if (i == 0) {
                prefix = "⌈";
                suffix = "⌉";
            } else if (i == this.getHeight() - 1) {
                prefix = "⌊";
                suffix = "⌋";
            } else {
                prefix = "|";
                suffix = "|";
            }
            String row = this.getRow(this.getMinRowIndex() + i).toStream().mapToObj(value -> String.format(valueFormat, value)).collect(Collectors.joining(" ", prefix, suffix));
            stringBuilder.append(row);
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    /**
     * This static builder class is the main tool for constructing {@code int} matrices from scratch. The height and width
     * of the matrix are defined at the beginning of the building process. All values are initially all set to {@code 0}.
     * Values can be added either incrementally (row-wise from left to right) or in arbitrary order. At the end of the
     * building process, the matrix can be built exactly once. The builder class is threadsafe and the resulting matrix
     * is immutable.
     */
    public static class Builder {

        // flag that determines whether the matrix has already been built or not
        private boolean built;
        // the first and the last valid row indices
        private final int minRowIndex;
        private final int maxRowIndex;
        // the first and the last valid column indices
        private final int minColIndex;
        private final int maxColIndex;
        // an array for storing the added values during the building process
        private final int[][] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public Builder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and width
         * {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public Builder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.built = false;
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
            this.values = new int[maxRowIndex - minRowIndex + 1][maxColIndex - minColIndex + 1];
            this.indexCounter = 0;
        }

        /**
         * Fills up the matrix with a single value.
         *
         * @param value The value used for filling up the matrix
         * @return The matrix builder itself
         */
        public IntMatrix.Builder fill(int value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                for (int i = this.minRowIndex; i <= this.maxRowIndex; i++) {
                    for (int j = this.minColIndex; j <= this.maxColIndex; j++) {
                        this.setValue(i, j, value);
                    }
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given row and column indices. The matrix builder object itself is returned to
         * allow pipeline notation when multiple values are added to the matrix.
         *
         * @param rowIndex The row index of the value to be added
         * @param colIndex The column index of the value to be added
         * @param value The value to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public Builder setValue(int rowIndex, int colIndex, int value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                if (rowIndex < this.minRowIndex || rowIndex > this.maxRowIndex || colIndex < this.minColIndex || colIndex > this.maxColIndex)
                    throw new IndexOutOfBoundsException();
                this.values[rowIndex - this.minRowIndex][colIndex - this.minColIndex] = value;
                return this;
            }
        }

        /**
         * Sets the next value in the matrix and increases the internal index counter by 1. In this way, values are
         * added row-wise from left to right. The matrix builder object itself is returned to allow pipeline notation
         * when multiple values are added to the matrix.
         *
         * @param value The value to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder addValue(int value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                int width = this.maxColIndex - this.minColIndex + 1;
                int currentRowIndex, currentColIndex;
                currentRowIndex = this.minRowIndex + this.indexCounter / width;
                currentColIndex = this.minColIndex + this.indexCounter % width;
                this.indexCounter = this.indexCounter + 1;
                return this.setValue(currentRowIndex, currentColIndex, value);
            }
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public IntMatrix build() {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                this.built = true;
                return new IntMatrix() {
                    @Override
                    public int getValue(int rowIndex, int colIndex) {
                        if (rowIndex < minRowIndex || rowIndex > maxRowIndex || colIndex < minColIndex || colIndex > maxColIndex)
                            throw new IndexOutOfBoundsException();
                        return values[rowIndex - minRowIndex][colIndex - minColIndex];
                    }

                    @Override
                    public int getMinRowIndex() {
                        return minRowIndex;
                    }

                    @Override
                    public int getMaxRowIndex() {
                        return maxRowIndex;
                    }

                    @Override
                    public int getMinColIndex() {
                        return minColIndex;
                    }

                    @Override
                    public int getMaxColIndex() {
                        return maxColIndex;
                    }
                };
            }
        }
    }

    /**
     * This static builder class can be used for constructing {@code int} matrices from given row vectors. The height and
     * width of the matrix are defined at the beginning of the building process. All values are initially all set to {@code 0}.
     * Rows can be added either incrementally or in arbitrary order. At the end of the building process, the matrix can
     * be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     */
    public static class RowBuilder {

        // uses a vector builder internally
        private final Vector.Builder<IntVector> vectorBuilder;
        private final int minColIndex;
        private final int maxColIndex;

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public RowBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new row-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public RowBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.vectorBuilder = new Vector.Builder<>(minRowIndex, maxRowIndex);
            this.minColIndex = minColIndex;
            this.maxColIndex = maxColIndex;
        }

        /**
         * Sets the given row vector at the given index. The matrix builder object itself is returned to
         * allow pipeline notation when multiple rows are added to the matrix.
         *
         * @param rowIndex The row index of the row vector to be added
         * @param row The row vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public RowBuilder setRow(int rowIndex, IntVector row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.setValue(rowIndex, row);
            return this;
        }

        /**
         * Sets the next row vector in the matrix and increases the internal index counter by 1. In this way, row vectors
         * are added incrementally. The matrix builder object itself is returned to allow pipeline notation
         * when multiple row vectors are added to the matrix.
         *
         * @param row The row vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public RowBuilder addRow(IntVector row) {
            if (row == null || row.getMinIndex() != this.minColIndex || row.getMaxIndex() != this.maxColIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.addValue(row);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public IntMatrix build() {
            Vector<IntVector> vector = this.vectorBuilder.build();
            return new IntMatrix() {

                @Override
                public int getValue(int rowIndex, int colIndex) {
                    IntVector row = vector.getValue(rowIndex);
                    return row == null ? 0 : row.getValue(colIndex);
                }

                @Override
                public int getMinRowIndex() {
                    return vector.getMinIndex();
                }

                @Override
                public int getMaxRowIndex() {
                    return vector.getMaxIndex();
                }

                @Override
                public int getMinColIndex() {
                    return minColIndex;
                }

                @Override
                public int getMaxColIndex() {
                    return maxColIndex;
                }
            };
        }
    }

    /**
     * This static builder class can be used for constructing {@code int} matrices from given column vectors. The height and
     * width of the matrix are defined at the beginning of the building process. All values are initially all set to {@code 0}.
     * Columns can be added either incrementally or in arbitrary order. At the end of the building process, the matrix can
     * be built exactly once. The builder class is threadsafe and the resulting matrix is immutable.
     */
    public static class ColBuilder {

        // uses a vector builder internally
        private final Vector.Builder<IntVector> vectorBuilder;
        private final int minRowIndex;
        private final int maxRowIndex;

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed {@code height} and {@code width}. Indexing
         * of both the rows and columns starts from 1.
         *
         * @param height The height of the matrix to construct
         * @param width The width of the matrix to construct
         */
        public ColBuilder(int height, int width) {
            this(1, height, 1, width);
        }

        /**
         * Constructs a new column-wise matrix builder for a matrix of fixed height {@code maxRowIndex-minRowIndex+1} and
         * width {@code maxColIndex-minColIndex+1}.
         *
         * @param minRowIndex The minimal row index
         * @param maxRowIndex The maximal row index
         * @param minColIndex The minimal column index
         * @param maxColIndex The maximal column index
         */
        public ColBuilder(int minRowIndex, int maxRowIndex, int minColIndex, int maxColIndex) {
            if (minRowIndex < 0 || minRowIndex > maxRowIndex + 1 || minColIndex < 0 || minColIndex > maxColIndex + 1)
                throw new IllegalArgumentException();
            this.minRowIndex = minRowIndex;
            this.maxRowIndex = maxRowIndex;
            this.vectorBuilder = new Vector.Builder<>(minColIndex, maxColIndex);
        }

        /**
         * Sets the given column vector at the given index. The matrix builder object itself is returned to
         * allow pipeline notation when multiple rows are added to the matrix.
         *
         * @param colIndex The column index of the column vector to be added
         * @param col The column vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public ColBuilder setCol(int colIndex, IntVector col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.setValue(colIndex, col);
            return this;
        }

        /**
         * Sets the next column vector in the matrix and increases the internal index counter by 1. In this way, column
         * vectors are added incrementally. The matrix builder object itself is returned to allow pipeline notation
         * when multiple column vectors are added to the matrix.
         *
         * @param col The column vector to be added
         * @return The matrix builder itself
         * @throws IllegalStateException     if the matrix has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public ColBuilder addCol(IntVector col) {
            if (col == null || col.getMinIndex() != this.minRowIndex || col.getMaxIndex() != this.maxRowIndex)
                throw new IllegalArgumentException();
            this.vectorBuilder.addValue(col);
            return this;
        }

        /**
         * Terminates the building process and returns the constructed matrix.
         *
         * @return The constructed matrix
         * @throws IllegalStateException if the matrix has already been build
         */
        public IntMatrix build() {
            Vector<IntVector> vector = this.vectorBuilder.build();
            return new IntMatrix() {

                @Override
                public int getValue(int rowIndex, int colIndex) {
                    IntVector col = vector.getValue(colIndex);
                    return col == null ? 0 : col.getValue(rowIndex);
                }

                @Override
                public int getMinRowIndex() {
                    return minRowIndex;
                }

                @Override
                public int getMaxRowIndex() {
                    return maxRowIndex;
                }

                @Override
                public int getMinColIndex() {
                    return vector.getMinIndex();
                }

                @Override
                public int getMaxColIndex() {
                    return vector.getMaxIndex();
                }
            };
        }
    }

}
