/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.stream.IntStream;

/**
 * Objects of this class represent substrings of existing Java string. Instead of copying the characters of the
 * substring, they keep a reference to the parent string and two indices that indicate the start and the end position
 * of the substring. The purpose of this class is to avoid copying substrings from large strings.
 */
public class SubString {

    private final String string; // parent string
    private final int from; // index of first character
    private final int to; // index of last character + 1

    /**
     * Constructs a substring object that is identical to the given parent string.
     *
     * @param string The given parent string
     */
    public SubString(String string) {
        this(string, 0, string.length());
    }

    /**
     * Constructs a substring object based on the characters of the given parent string. The substring begins at the
     * specified index {@code from} and extends to the character at index {@code to - 1}. Thus the length of the
     * substring is {@code to - from}.
     *
     * @param string The given parent string
     * @param from   The index of the first substring character
     * @param to     The index of the last substring character plus 1
     */
    public SubString(String string, int from, int to) {
        if (from < 0 || to > string.length() || to < from)
            throw new IllegalArgumentException();
        this.string = string;
        this.from = from;
        this.to = to;
    }

    /**
     * Returns the length of this substring.
     *
     * @return The length of this substring
     */
    public int length() {
        return this.to - this.from;
    }

    /**
     * Returns the character at the specified index. An index ranges from {@code 0} to {@code length() - 1}.
     *
     * @param index The index of the character
     * @return The character at the specified index of this substring
     */
    public char charAt(int index) {
        if (index < 0 || index > this.length())
            throw new IndexOutOfBoundsException();
        return this.string.charAt(this.from + index);
    }

    /**
     * Returns a new substring of an existing substring. The new substring begins at the specified index {@code from}
     * and extends to the character at index {@code to - 1}. Thus the length of the new substring is {@code to - from}.
     *
     * @param from The index of the first substring character
     * @param to   The index of the last substring character plus 1
     * @return The specified substring
     */
    public SubString substring(int from, int to) {
        return new SubString(string, this.from + from, this.from + to);
    }

    @Override
    public String toString() {
        return this.string.substring(this.from, this.to);
    }

    @Override
    public int hashCode() {
        return this.toString().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof SubString))
            return false;
        SubString other = (SubString) obj;
        if (this.length() != other.length())
            return false;
        return IntStream.range(0, this.length()).allMatch(i -> this.charAt(i) == other.charAt(i));
    }

}
