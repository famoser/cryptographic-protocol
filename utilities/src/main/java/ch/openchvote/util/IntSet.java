/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class implements the generic interface {@link Set} for values of type {@code int}. The main
 * purpose of this interface is to avoid unnecessary boxing/unboxing operations between the primitive type {@code int}
 * and the wrapper class {@code Integer} while conducting membership tests. Since this class is abstract, it does not
 * offer public constructors.
 */
public abstract class IntSet implements Set<Integer> {

    /**
     * Creates an integer set containing the values from the specified range. The range itself is specified by its lower
     * and upper bounds, i.e., it corresponds to the set {lb,...,up}.
     *
     * @param lb The lower bound of the integer range
     * @param ub The upper bound of the integer range
     * @return The set of integers within the given range
     */
    public static IntSet range(int lb, int ub) {
        return new IntSet() {
            @Override
            public boolean contains(int x) {
                return lb <= x && x <= ub;
            }

            @Override
            public IntStream toStream() {
                return IntStream.rangeClosed(lb, ub);
            }
        };
    }

    /**
     * The set of non-negative integers {0,1,2,...}. The finite domain of the {@code int} data type limits this set to
     * values smaller or equal to {@code Integer.MAX_VALUE}.
     */
    public static final IntSet NN = IntSet.range(0, Integer.MAX_VALUE);

    /**
     * The set of positive integers {1,2,3,...}. The finite domain of the {@code int} data type limits this set to
     * values smaller or equal to {@code Integer.MAX_VALUE}.
     */
    public static final IntSet NN_plus = IntSet.range(1, Integer.MAX_VALUE);

    /**
     * Creates the set of positive integers smaller or equal to {@code n}.
     *
     * @param n The upper bound of the range
     * @return The set of positive integers smaller or equal to {@code n}
     * @throws IllegalArgumentException if {@code n} is negative
     */
    public static IntSet NN_plus(int n) {
        if (n < 0)
            throw new IllegalArgumentException();
        return IntSet.range(1, n);
    }

    /**
     * Returns the set {0,...,n-1} of integers modulo {@code n}.
     *
     * @param n The modulo
     * @return The set of integers modulo {@code n}
     * @throws IllegalArgumentException if {@code n} is negative
     */
    public static IntSet ZZ(int n) {
        if (n < 0)
            throw new IllegalArgumentException();
        return IntSet.range(0, n - 1);
    }

    /**
     * Creates the set of all integers contained in the given varargs parameter {@code values}.
     *
     * @param values An array of integers
     * @return The set of integers contained in {@code values}
     */
    public static IntSet of(Integer... values) {
        var set = java.util.Set.of(values);
        return new IntSet() {
            @Override
            public boolean contains(int x) {
                return set.contains(x);
            }

            @Override
            public IntStream toStream() {
                return set.stream().mapToInt(Integer::intValue);
            }
        };
    }

    /**
     * The set of binary values {0,1}.
     */
    public static final IntSet BB = IntSet.of(0, 1);

    /**
     * Creates the set of all integers contained in the given {@code java.util.Set} of integers.
     *
     * @param values A set of integers
     * @return The set of integers contained in {@code values}
     */
    public static IntSet of(java.util.Set<Integer> values) {
        var intArray = values.toArray(new Integer[0]);
        return IntSet.of(intArray);
    }

    /**
     * Performs the membership test on a single value {@code x} of type {@code int}. Returns {@code true}, if {@code x}
     * is a member of the set, and {@code false} otherwise.
     *
     * @param x The given value
     * @return {@code true} if {@code x} is a member of the set
     */
    public abstract boolean contains(int x);

    // protected helper method to create an integer stream of all the values in the set
    protected abstract IntStream toStream();

    /**
     * Performs the membership test on all values of a given integer stream. Returns {@code true}, if all values are
     * members of the set. If the stream itself is {@code null}, the membership test returns {@code false}.
     *
     * @param stream The given integer stream
     * @return {@code true} if all values are members of the set
     */
    public boolean containsAll(IntStream stream) {
        return stream != null && stream.allMatch(this::contains);
    }

    /**
     * Performs the membership test on all objects of a given vector. Returns {@code true}, if all values are members of
     * the set. If the vector itself is {@code null}, the membership test returns {@code false}.
     *
     * @param vector The given vector
     * @return {@code true} if all values are members of the set
     */
    public boolean containsAll(IntVector vector) {
        return vector != null && vector.toStream().allMatch(this::contains);
    }

    /**
     * Performs the membership test on all objects of a given matrix. Returns {@code true}, if all values are members of
     * the set. If the matrix itself is {@code null}, the membership test returns {@code false}.
     *
     * @param matrix The given matrix
     * @return {@code true} if all values are members of the set
     */
    public boolean containsAll(IntMatrix matrix) {
        return matrix != null && matrix.getRows().toStream().allMatch(this::containsAll);
    }

    @Override
    public boolean contains(Integer x) {
        return x != null && this.contains(x.intValue());
    }

    @Override
    public String toString() {
        return this.toStream().mapToObj(Integer::toString).collect(Collectors.joining(",", "{", "}"));
    }

}