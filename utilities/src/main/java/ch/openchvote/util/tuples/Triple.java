/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a triple of non-null values of different generic types.
 *
 * @param <V1> The generic type of the first value
 * @param <V2> The generic type of the second value
 * @param <V3> The generic type of the third value
 */
public class Triple<V1, V2, V3> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;

    /**
     * Constructs a new triple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first  First value
     * @param second Second value
     * @param third  Third value
     */
    public Triple(V1 first, V2 second, V3 third) {
        if (first == null || second == null || third == null)
            throw new IllegalArgumentException();
        this.first = first;
        this.second = second;
        this.third = third;
    }

    /**
     * Returns the first element of the triple
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the triple
     *
     * @return The second element
     */
    public V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the triple
     *
     * @return The third element
     */
    public V3 getThird() {
        return this.third;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Triple<?, ?, ?> other = (Triple<?, ?, ?>) object;

        if (!this.first.equals(other.first)) return false;
        if (!this.second.equals(other.second)) return false;
        return this.third.equals(other.third);
    }

    @Override
    public int hashCode() {
        int result = this.first.hashCode();
        result = 31 * result + this.second.hashCode();
        result = 31 * result + this.third.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s,%s)", this.first, this.second, this.third);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .build();
    }
}
