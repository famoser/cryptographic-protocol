/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a quadruple of non-null values of different generic types.
 *
 * @param <V1> The generic type of the first value
 * @param <V2> The generic type of the second value
 * @param <V3> The generic type of the third value
 * @param <V4> The generic type of the fourth value
 */
public class Quadruple<V1, V2, V3, V4> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;
    private final V4 fourth;

    /**
     * Constructs a new quadruple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first  First value
     * @param second Second value
     * @param third  Third value
     * @param fourth Fourth value
     */
    public Quadruple(V1 first, V2 second, V3 third, V4 fourth) {
        if (first == null || second == null || third == null || fourth == null)
            throw new IllegalArgumentException();
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
    }

    /**
     * Returns the first element of the quadruple
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the quadruple
     *
     * @return The second element
     */
    public V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the quadruple
     *
     * @return The third element
     */
    public V3 getThird() {
        return this.third;
    }

    /**
     * Returns the fourth element of the quadruple
     *
     * @return The fourth element
     */
    public V4 getFourth() {
        return this.fourth;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Quadruple<?, ?, ?, ?> other = (Quadruple<?, ?, ?, ?>) object;

        if (!this.first.equals(other.first)) return false;
        if (!this.second.equals(other.second)) return false;
        if (!this.third.equals(other.third)) return false;
        return this.fourth.equals(other.fourth);
    }

    @Override
    public int hashCode() {
        int result = this.first.hashCode();
        result = 31 * result + this.second.hashCode();
        result = 31 * result + this.third.hashCode();
        result = 31 * result + this.fourth.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s,%s,%s)", this.first, this.second, this.third, this.fourth);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .add(this.fourth)
                .build();
    }
}
