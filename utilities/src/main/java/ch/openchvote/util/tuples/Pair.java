/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a pair of non-null values of different generic types.
 *
 * @param <V1> The generic type of the first value
 * @param <V2> The generic type of the second value
 */
public class Pair<V1, V2> extends Tuple {

    private final V1 first;
    private final V2 second;

    /**
     * Constructs a new pair for the given non-null values. An exception is thrown if one of the given values is {@code
     * null}.
     *
     * @param first  First value
     * @param second Second value
     */
    public Pair(V1 first, V2 second) {
        if (first == null || second == null)
            throw new IllegalArgumentException();
        this.first = first;
        this.second = second;
    }

    /**
     * Returns the first element of the pair
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the pair
     *
     * @return The second element
     */
    public V2 getSecond() {
        return this.second;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Pair<?, ?> other = (Pair<?, ?>) object;

        if (!this.first.equals(other.first)) return false;
        return this.second.equals(other.second);
    }

    @Override
    public int hashCode() {
        int result = this.first.hashCode();
        result = 31 * result + this.second.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", this.first, this.second);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .build();
    }
}
