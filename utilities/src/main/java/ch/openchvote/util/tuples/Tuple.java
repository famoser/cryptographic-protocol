/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import ch.openchvote.util.Hashable;
import ch.openchvote.util.Vector;

import java.util.stream.Stream;

/**
 * This abstract class is the base class for implementing tuples (pairs, triples, etc.) of different lengths. Tuples are
 * immutable objects.
 */
public abstract class Tuple extends Hashable {

    /**
     * Returns a vector of type {@code Object} containing all elements of the tuple.
     *
     * @return A vector of type {@code Object} containing all elements of the tuple
     */
    public Vector<Object> toVector() {
        return Vector.fromSafeArray(this.toArray());
    }

    /**
     * Returns a stream of type {@code Object} containing all elements of the tuple.
     *
     * @return A stream of type {@code Object} containing all elements of the tuple
     */
    public abstract Stream<Object> toStream();

    // Private method to convert the tuple into an array of values in the given order
    private Object[] toArray() {
        return this.toStream().toArray(Object[]::new);
    }

}
