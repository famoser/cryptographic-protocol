/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a singleton of a non-null value of a generic type.
 *
 * @param <V1> The generic type of the value
 */
public class Singleton<V1> extends Tuple {

    private final V1 first;

    /**
     * Constructs a new singleton for the given non-null value. An exception is thrown if the given values is {@code
     * null}.
     *
     * @param first First value
     */
    public Singleton(V1 first) {
        if (first == null)
            throw new IllegalArgumentException();
        this.first = first;
    }

    /**
     * Returns the first element of the singleton
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Singleton<?> other = (Singleton<?>) object;

        return this.first.equals(other.first);
    }

    @Override
    public int hashCode() {
        return this.first.hashCode();
    }

    @Override
    public String toString() {
        return String.format("(%s)", this.first);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .build();
    }
}
