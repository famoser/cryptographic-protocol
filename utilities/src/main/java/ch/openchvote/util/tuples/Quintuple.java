/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a quintuple of non-null values of different generic types.
 *
 * @param <V1> The generic type of the first value
 * @param <V2> The generic type of the second value
 * @param <V3> The generic type of the third value
 * @param <V4> The generic type of the fourth value
 * @param <V5> The generic type of the fifth value
 */
public class Quintuple<V1, V2, V3, V4, V5> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;
    private final V4 fourth;
    private final V5 fifth;

    /**
     * Constructs a new quintuple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first  First value
     * @param second Second value
     * @param third  Third value
     * @param fourth Fourth value
     * @param fifth  Fifth value
     */
    public Quintuple(V1 first, V2 second, V3 third, V4 fourth, V5 fifth) {
        if (first == null || second == null || third == null || fourth == null || fifth == null)
            throw new IllegalArgumentException();
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
    }

    /**
     * Returns the first element of the quintuple
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the quintuple
     *
     * @return The second element
     */
    public V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the quintuple
     *
     * @return The third element
     */
    public V3 getThird() {
        return this.third;
    }

    /**
     * Returns the fourth element of the quintuple
     *
     * @return The fourth element
     */
    public V4 getFourth() {
        return this.fourth;
    }

    /**
     * Returns the fifth element of the quintuple
     *
     * @return The fifth element
     */
    public V5 getFifth() {
        return this.fifth;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Quintuple<?, ?, ?, ?, ?> other = (Quintuple<?, ?, ?, ?, ?>) object;

        if (!this.first.equals(other.first)) return false;
        if (!this.second.equals(other.second)) return false;
        if (!this.third.equals(other.third)) return false;
        if (!this.fourth.equals(other.fourth)) return false;
        return this.fifth.equals(other.fifth);
    }

    @Override
    public int hashCode() {
        int result = this.first.hashCode();
        result = 31 * result + this.second.hashCode();
        result = 31 * result + this.third.hashCode();
        result = 31 * result + this.fourth.hashCode();
        result = 31 * result + this.fifth.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s,%s,%s,%s)", this.first, this.second, this.third, this.fourth, this.fifth);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .add(this.fourth)
                .add(this.fifth)
                .build();
    }
}
