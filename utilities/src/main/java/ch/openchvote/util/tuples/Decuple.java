/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.tuples;

import java.util.stream.Stream;

/**
 * This class implements a decuple of non-null values of different generic types.
 *
 * @param <V1>  The generic type of the first value
 * @param <V2>  The generic type of the second value
 * @param <V3>  The generic type of the third value
 * @param <V4>  The generic type of the fourth value
 * @param <V5>  The generic type of the fifth value
 * @param <V6>  The generic type of the sixth value
 * @param <V7>  The generic type of the seventh value
 * @param <V8>  The generic type of the eighth value
 * @param <V9>  The generic type of the ninth value
 * @param <V10> The generic type of the tenth value
 */
public class Decuple<V1, V2, V3, V4, V5, V6, V7, V8, V9, V10> extends Tuple {

    private final V1 first;
    private final V2 second;
    private final V3 third;
    private final V4 fourth;
    private final V5 fifth;
    private final V6 sixth;
    private final V7 seventh;
    private final V8 eighth;
    private final V9 ninth;
    private final V10 tenth;

    /**
     * Constructs a new decuple for the given non-null values. An exception is thrown if one of the given values is
     * {@code null}.
     *
     * @param first   First value
     * @param second  Second value
     * @param third   Third value
     * @param fourth  Fourth value
     * @param fifth   Fifth value
     * @param sixth   Sixth value
     * @param seventh Seventh value
     * @param eighth  Eighth value
     * @param ninth   Ninth value
     * @param tenth   Tenth value
     */
    public Decuple(V1 first, V2 second, V3 third, V4 fourth, V5 fifth, V6 sixth, V7 seventh, V8 eighth, V9 ninth, V10 tenth) {
        if (first == null || second == null || third == null || fourth == null || fifth == null || sixth == null || seventh == null || eighth == null || ninth == null || tenth == null)
            throw new IllegalArgumentException();
        this.first = first;
        this.second = second;
        this.third = third;
        this.fourth = fourth;
        this.fifth = fifth;
        this.sixth = sixth;
        this.seventh = seventh;
        this.eighth = eighth;
        this.ninth = ninth;
        this.tenth = tenth;
    }

    /**
     * Returns the first element of the decuple
     *
     * @return The first element
     */
    public V1 getFirst() {
        return this.first;
    }

    /**
     * Returns the second element of the decuple
     *
     * @return The second element
     */
    public V2 getSecond() {
        return this.second;
    }

    /**
     * Returns the third element of the decuple
     *
     * @return The third element
     */
    public V3 getThird() {
        return this.third;
    }

    /**
     * Returns the fourth element of the decuple
     *
     * @return The fourth element
     */
    public V4 getFourth() {
        return this.fourth;
    }

    /**
     * Returns the fifth element of the decuple
     *
     * @return The fifth element
     */
    public V5 getFifth() {
        return this.fifth;
    }

    /**
     * Returns the sixth element of the decuple
     *
     * @return The sixth element
     */
    public V6 getSixth() {
        return this.sixth;
    }

    /**
     * Returns the seventh element of the decuple
     *
     * @return The seventh element
     */
    public V7 getSeventh() {
        return this.seventh;
    }

    /**
     * Returns the eighth element of the decuple
     *
     * @return The eighth element
     */
    public V8 getEighth() {
        return this.eighth;
    }

    /**
     * Returns the ninth element of the decuple
     *
     * @return The ninth element
     */
    public V9 getNinth() {
        return this.ninth;
    }

    /**
     * Returns the tenth element of the decuple
     *
     * @return The tenth element
     */
    public V10 getTenth() {
        return this.tenth;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (object == null || this.getClass() != object.getClass()) return false;

        Decuple<?, ?, ?, ?, ?, ?, ?, ?, ?, ?> other = (Decuple<?, ?, ?, ?, ?, ?, ?, ?, ?, ?>) object;

        if (!this.first.equals(other.first)) return false;
        if (!this.second.equals(other.second)) return false;
        if (!this.third.equals(other.third)) return false;
        if (!this.fourth.equals(other.fourth)) return false;
        if (!this.fifth.equals(other.fifth)) return false;
        if (!this.sixth.equals(other.sixth)) return false;
        if (!this.seventh.equals(other.seventh)) return false;
        if (!this.eighth.equals(other.eighth)) return false;
        if (!this.ninth.equals(other.ninth)) return false;
        return this.tenth.equals(other.tenth);
    }

    @Override
    public int hashCode() {
        int result = this.first.hashCode();
        result = 31 * result + this.second.hashCode();
        result = 31 * result + this.third.hashCode();
        result = 31 * result + this.fourth.hashCode();
        result = 31 * result + this.fifth.hashCode();
        result = 31 * result + this.sixth.hashCode();
        result = 31 * result + this.seventh.hashCode();
        result = 31 * result + this.eighth.hashCode();
        result = 31 * result + this.ninth.hashCode();
        result = 31 * result + this.tenth.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", this.first, this.second, this.third, this.fourth, this.fifth, this.sixth, this.seventh, this.eighth, this.ninth, this.tenth);
    }

    @Override
    public Stream<Object> toStream() {
        return Stream.builder()
                .add(this.first)
                .add(this.second)
                .add(this.third)
                .add(this.fourth)
                .add(this.fifth)
                .add(this.sixth)
                .add(this.seventh)
                .add(this.eighth)
                .add(this.ninth)
                .add(this.tenth)
                .build();
    }
}
