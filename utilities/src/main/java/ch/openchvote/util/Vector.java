/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.Function;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Objects of this generic class represent immutable vectors of a fixed length {@code n}. Their elements are indexed from {@code
 * minIndex} (usually {@code 1}) to {@code maxIndex} (usually {@code n}). Since this class is abstract, it does not
 * offer public constructors. New instances of this class are created using the static member class {@link Builder}.
 * During the building process, values can be added to or placed into an initially empty vector containing {@code null}
 * values. After the building process, accessing the values in the resulting vector is restricted to read-only. The class
 * implements the {@link Iterable} interface.
 *
 * @param <V> The generic type of the values stored in the vector
 */
public abstract class Vector<V> extends Hashable implements Iterable<V> {

    /**
     * Returns the value at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The value at this index
     * @throws IndexOutOfBoundsException if an invalid index is given
     */
    public abstract V getValue(int index);

    /**
     * Returns the minimal index of this vector.
     *
     * @return The minimal index
     */
    public abstract int getMinIndex();

    /**
     * Returns the maximal index of this vector.
     *
     * @return The maximal index
     */
    public abstract int getMaxIndex();

    /**
     * Returns the length of the vector
     *
     * @return The length of the vector
     */
    public int getLength() {
        return this.getMaxIndex() - this.getMinIndex() + 1;
    }

    /**
     * Creates a new vector by applying a function to each value of the vector. Depending on the specified function, the
     * type {@code W} of the returned vector may be different from the type {@code V} of the original vector. {@code
     * null} values are mapped into {@code null} values. By returning a wrapper object which performs the mapping
     * lazily, this method runs ins constant time.
     *
     * @param function The function that maps the values of the vector
     * @param <W>      The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W> Vector<W> map(Function<? super V, ? extends W> function) {
        return this.map(function, null);
    }

    /**
     * Creates a new vector by applying a function to each value of the vector. Depending on the specified function, the
     * type {@code W} of the returned vector may be different from the type {@code V} of the original vector. {@code
     * null} values are mapped into the specified value {@code defaultValue}. By returning a wrapper object which
     * performs the mapping lazily, this method runs ins constant time.
     *
     * @param function     The function that maps the values of the vector
     * @param defaultValue the default value that replaces {@code null} values
     * @param <W>          The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W> Vector<W> map(Function<? super V, ? extends W> function, W defaultValue) {
        Vector<V> vector = this;
        return new Vector<>() {
            @Override
            public W getValue(int index) {
                V value = vector.getValue(index);
                return value == null ? defaultValue : function.apply(value);
            }

            @Override
            public int getMinIndex() {
                return vector.getMinIndex();
            }

            @Override
            public int getMaxIndex() {
                return vector.getMaxIndex();
            }
        };
    }

    /**
     * Creates a new {@code IntVector} by applying a {@code ToIntFunction} to each value of the vector. {@code
     * null} values are mapped into 0. By returning a wrapper object which performs the mapping lazily, this method
     * runs ins constant time.
     *
     * @param function The function that maps the values of the vector
     * @return A integer vector containing all mapped values
     */
    public IntVector mapToInt(ToIntFunction<? super V> function) {
        Vector<V> vector = this;
        return new IntVector() {
            @Override
            public int getValue(int index) {
                V value = vector.getValue(index);
                return value == null ? 0 : function.applyAsInt(value);
            }

            @Override
            public int getMinIndex() {
                return vector.getMinIndex();
            }

            @Override
            public int getMaxIndex() {
                return vector.getMaxIndex();
            }
        };
    }

    /**
     * Creates a new vector by selecting the values specified by the given {@code indexSet}. By taking the given indices
     * in ascending order, the order of the values in the returned vector corresponds to the order in the original
     * vector. The length of the returned vector is equal to the size of {@code indexSet}, and indexing starts at 1.
     *
     * @param indexSet The set of indices of the values to be selected
     * @return A new vector containing the selected values
     */
    public Vector<V> select(IntSet indexSet) {
        if (indexSet == null)
            throw new IndexOutOfBoundsException();
        var indexArray = this.getIndices().filter(indexSet::contains).toArray();
        Vector<V> vector = this;
        return new Vector<>() {
            @Override
            public V getValue(int index) {
                return vector.getValue(indexArray[index - 1]);
            }

            @Override
            public int getMinIndex() {
                return 1;
            }

            @Override
            public int getMaxIndex() {
                return indexArray.length;
            }
        };
    }

    // private helper method for generating a stream of all vector indices
    private IntStream getIndices() {
        return IntStream.rangeClosed(this.getMinIndex(), this.getMaxIndex());
    }

    /**
     * Checks if all values of the vector are equal.
     *
     * @return {@code true}, if all values are equals, {@code false}, otherwise
     */
    public boolean isUniform() {
        return this.getIndices().skip(1).allMatch(index -> Objects.equals(this.getValue(index - 1), this.getValue(index)));
    }

    /**
     * Returns a stream containing all values of the vector in the same order.
     *
     * @return The stream of values obtained from the vector
     */
    public Stream<V> toStream() {
        return this.getIndices().mapToObj(this::getValue);
    }

    /**
     * Returns an array containing all of the values in this vector in proper sequence (from first to the last value).
     * To support the internal creation of a generic array, the class object of of the generic type must be passed as
     * argument.
     *
     * @param clazz The class object of the generic type
     * @return An array containing all of the values in this vector in proper sequence
     */
    public V[] toArray(Class<V> clazz) {
        V[] values = (V[]) Array.newInstance(clazz, this.getLength());
        this.getIndices().forEach(i -> values[i - this.getMinIndex()] = this.getValue(i));
        return values;
    }

    @Override
    public Iterator<V> iterator() {
        Vector<V> vector = this;
        return new Iterator<>() {

            private int currentIndex = vector.getMinIndex();

            @Override
            public boolean hasNext() {
                return this.currentIndex <= vector.getMaxIndex();
            }

            @Override
            public V next() {
                return vector.getValue(this.currentIndex++);
            }
        };
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Vector)) return false;

        Vector<?> other = (Vector<?>) object;
        if (this.getMinIndex() != other.getMinIndex()) return false;
        if (this.getMaxIndex() != other.getMaxIndex()) return false;

        return this.getIndices().allMatch(index -> Objects.equals(this.getValue(index), other.getValue(index)));
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.getMinIndex();
        initValue = 31 * initValue + this.getMaxIndex();
        return this.toStream().mapToInt(Objects::hashCode).reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        return this.toStream().map(Objects::toString).collect(Collectors.joining(",", "[", "]"));
    }

    /**
     * This static builder class is the main tool for constructing vectors from scratch. If the length of the vector to
     * construct is defined at the beginning of the building process, then the values are initially all set to {@code
     * null}. If the length is not specified, then the vector grows when new values are added. In both cases, values can
     * be added either incrementally or in arbitrary order. At the end of the building process, the vector can be built
     * exactly once. The builder class is threadsafe and the resulting vector is immutable.
     *
     * @param <V> The generic type of the vector to build
     */
    public static class Builder<V> {

        // flag that determines whether the vector has already been built or not
        private boolean built;
        // flag that determines whether the vector length is fixed or not during building process
        private final boolean growable;
        // the first and the last valid indices
        private int minIndex;
        private int maxIndex;
        // an array for storing the added values during the building process
        private V[] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a vector builder for a vector with undetermined length. The length of the vector grows
         * automatically to the necessary length when values are added. Indexing starts at 1.
         */
        public Builder() {
            this.built = false;
            this.growable = true;
            this.minIndex = 1;
            this.maxIndex = 0;
            this.values = null;
            this.indexCounter = 0;
        }

        /**
         * Constructs a vector builder for a vector of fixed {@code length}. Indexing starts from 1.
         *
         * @param length The length of the vector to construct
         */
        public Builder(int length) {
            this(1, length);
        }

        /**
         * Constructs a vector builder for a vector of fixed length {@code maxIndex-minIndex+1}. Indexing starts from
         * {@code minIndex} and goes up {@code maxIndex}.
         *
         * @param minIndex The minimal index
         * @param maxIndex The maximal index
         */
        public Builder(int minIndex, int maxIndex) {
            if (minIndex < 0 || minIndex > maxIndex + 1)
                throw new IllegalArgumentException();
            this.built = false;
            this.growable = false;
            this.minIndex = minIndex;
            this.maxIndex = maxIndex;
            this.values = null;
            this.indexCounter = 0;
        }

        /**
         * Fills up the vector with a single value. In case of a vector with undetermined length, the current length
         * remains unchanged.
         *
         * @param value The value used for filling up the vector
         * @return The vector builder itself
         */
        public Builder<V> fill(V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                for (int i = minIndex; i <= this.maxIndex; i++) {
                    this.setValue(i, value);
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given index. If the vector length is undetermined, increases the length if
         * necessary. The vector builder object itself is returned to allow pipeline notation when multiple values are
         * added to the vector.
         *
         * @param index The index of the value to be added
         * @param value The value to be added
         * @return The vector builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        @SuppressWarnings("unchecked")
        public Builder<V> setValue(int index, V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                if (!this.growable && (index < this.minIndex || index > this.maxIndex))
                    throw new IndexOutOfBoundsException();
                if (this.growable) {
                    this.minIndex = Math.min(this.minIndex, index);
                    this.maxIndex = Math.max(this.maxIndex, index);
                }
                if (this.values != null || value != null) {
                    int length = this.maxIndex - this.minIndex + 1;
                    if (this.values == null) {
                        this.values = (V[]) Array.newInstance(value.getClass(), length);
                    } else {
                        int arraylength = this.values.length;
                        while (length > arraylength) {
                            arraylength = 2 * arraylength + 1;
                        }
                        if (arraylength > this.values.length) {
                            this.values = Arrays.copyOf(this.values, arraylength);
                        }
                    }
                    this.values[index - this.minIndex] = value;
                }
                return this;
            }
        }

        /**
         * Sets the next value in the vector and increases the internal index counter by 1. If the vector length is
         * undetermined, increases the length if necessary. The vector builder object itself is returned to allow
         * pipeline notation when multiple values are added to the vector.
         *
         * @param value The value to be added
         * @return The vector builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder<V> addValue(V value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                int currentIndex = this.minIndex + this.indexCounter;
                this.indexCounter = this.indexCounter + 1;
                return this.setValue(currentIndex, value);
            }
        }

        /**
         * Terminates the building process and returns the constructed vector.
         *
         * @return The constructed vector
         * @throws IllegalStateException if the vector has already been build
         */
        public Vector<V> build() {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                this.built = true;
                return Vector.fromSafeArray(this.values, this.minIndex, this.maxIndex);
            }
        }
    }

    /**
     * This static method allows constructing new vectors from a given map of keys of type {@code Integer} and values of
     * type {@code V}. The map must be safe to use without copying its values. This method must therefore be used with
     * maximal care. The keys of the map are the indices of the newly created vector, i.e., the indexing of the vector goes
     * from the smallest to the largest key in the map. Keys in between with no entry in the map are interpreted as
     * {@code null} values.
     *
     * @param valueMap The given map of (index,value)-pairs
     * @return The constructed vector
     */
    public static <V> Vector<V> fromSafeMap(SortedMap<Integer, V> valueMap) {
        int minIndex = valueMap.isEmpty() ? 0 : valueMap.firstKey();
        int maxIndex = valueMap.isEmpty() ? -1 : valueMap.lastKey();
        return fromSafeMap(valueMap, minIndex, maxIndex);
    }

    // private helper method for vectors with indices from given range
    private static <V> Vector<V> fromSafeMap(Map<Integer, V> valueMap, int minIndex, int maxIndex) {
        return new Vector<>() {
            @Override
            public V getValue(int index) {
                if (index < minIndex || index > maxIndex)
                    throw new IndexOutOfBoundsException();
                return valueMap.get(index);
            }

            @Override
            public int getMinIndex() {
                return minIndex;
            }

            @Override
            public int getMaxIndex() {
                return maxIndex;
            }
        };
    }

    /**
     * This static method allows constructing new vectors from a given array of values of type {@code V}. The array must
     * be safe to use without copying its values. This method must therefore be used with maximal care. The indexing in
     * the newly created vector starts from 1.
     *
     * @param values The given values
     * @return The constructed vector
     * @throws IllegalArgumentException if the given array of values is null
     */
    public static <V> Vector<V> fromSafeArray(V... values) {
        if (values == null)
            throw new IllegalArgumentException();
        return Vector.fromSafeArray(values, 1, values.length);
    }

    // private helper method for vectors with indices from given range
    private static <V> Vector<V> fromSafeArray(V[] values, int minIndex, int maxIndex) {
        return new Vector<>() {

            @Override
            public V getValue(int index) {
                if (index < minIndex || index > maxIndex)
                    throw new IndexOutOfBoundsException();
                return values == null ? null : values[index - minIndex];
            }

            @Override
            public int getMinIndex() {
                return minIndex;
            }

            @Override
            public int getMaxIndex() {
                return maxIndex;
            }

            @Override
            public V[] toArray(Class<V> clazz) {
                if (values == null) {
                    return (V[]) Array.newInstance(clazz, this.getLength());
                }
                if (values.length == this.getLength()) {
                    return values;
                } else {
                    return Arrays.copyOf(values, this.getLength());
                }
            }
        };
    }

    /**
     * Returns a new vector containing the same values sorted relative to the natural ordering of the given type.
     *
     * @param vector The input vector to be sorted
     * @param <V>    The type of the values in the vector
     * @return The sorted vector
     */
    public static <V extends Comparable<V>> Vector<V> sort(Vector<V> vector) {
        if (vector == null)
            throw new IllegalArgumentException();
        var builder = new Vector.Builder<V>(vector.getMinIndex(), vector.getMaxIndex());
        vector.toStream().sorted().forEach(builder::addValue);
        return builder.build();
    }

}
