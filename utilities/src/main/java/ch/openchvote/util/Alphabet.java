/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.HashMap;
import java.util.Map;

/**
 * This class implements the concept of an alphabet, which consists of an ordered finite set of characters. The position
 * of the characters in the order is called rank. The rank of the first character in an alphabet of size n is 0 and the
 * rank of the last element is n-1. By implementing the {@link Set} interface, an alphabet also inherits the
 * properties of a set (of characters).
 */
public class Alphabet implements Set<Character> {

    /**
     * The binary alphabet (digits 0 and 1)
     */
    public static final Alphabet BASE2 = new Alphabet("01");

    /**
     * The octal alphabet (digits 0 to 7)
     */
    public static final Alphabet BASE8 = new Alphabet("01234567");

    /**
     * The decimal alphabet (digits 0 to 9)
     */
    public static final Alphabet BASE10 = new Alphabet("0123456789");

    /**
     * The hexadecimal alphabet (digits 0 to E)
     */
    public static final Alphabet BASE16 = new Alphabet("0123456789ABCDEF");

    /**
     * 32-character alphabet with (digits 0 to V)
     */
    public static final Alphabet BASE32HEX = new Alphabet("0123456789ABCDEFGHIJKLMNOPQRSTUV");

    /**
     * 32-character alphabet (digits A to Z, 2 to 6)
     */
    public static final Alphabet BASE32 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ234567");

    /**
     * 64-character alphabet (digits A to Z, a to z, 0 to 9, +, /)
     */
    public static final Alphabet BASE64 = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/");

    /**
     * Lower-case letters alphabet (digits a to z)
     */
    public static final Alphabet LOWER_CASE = new Alphabet("abcdefghijklmnopqrstuvwxyz");

    /**
     * Upper-case letters alphabet (digits A to z)
     */
    public static final Alphabet UPPER_CASE = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZ");

    /**
     * Upper-case and lower-case letters alphabet (digits A to Z, a to z)
     */
    public static final Alphabet LETTERS = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");

    /**
     * Alphanumeric alphabet (digits A to Z, a to z, 0 to 9)
     */
    public static final Alphabet ALPHANUMERIC = new Alphabet("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789");

    /**
     * UCS alphabet (all Java Unicode characters)
     */
    public static final Alphabet UCS = new Alphabet() {

        @Override
        public String getCharacters() {
            throw new UnsupportedOperationException();
        }

        @Override
        public int getSize() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean contains(Character c) {
            return true;
        }

        @Override
        public int getRank(char c) {
            return c;
        }

        @Override
        public char getChar(int rank) {
            return (char) rank;
        }
    };

    private Map<Character, Integer> charMap;
    private String characters;

    // private constructor used for Alphabet.UCS
    private Alphabet() {
    }

    /**
     * General public constructor for arbitrary alphabets. The characters of the alphabet are given by a string in which
     * each character is contained at most once. The positions of the characters in the string determine their ranks.
     * Internally, all characters are stored in a HashMap, which enables computing their ranks efficiently. The minimal
     * size of the alphabet is 2.
     *
     * @param characters The set of characters given as a string
     */
    public Alphabet(String characters) {
        if (characters == null || characters.length() < 2)
            throw new IllegalArgumentException();
        this.charMap = new HashMap<>(characters.length());
        for (int index = 0; index < characters.length(); index++) {
            this.charMap.put(characters.charAt(index), index);
        }
        if (this.charMap.size() != characters.length())
            throw new IllegalArgumentException();
        this.characters = characters;
    }

    /**
     * Returns all characters as a string in ascending rank order.
     *
     * @return All characters of the alphabet
     */
    public String getCharacters() {
        return this.characters;
    }

    /**
     * Returns the size (number of characters) of the alphabet.
     *
     * @return The size of the alphabet
     */
    public int getSize() {
        return this.characters.length();
    }

    /**
     * Returns the rank of the character in the alphabet. If the character is not an element of the alphabet, an
     * exception is thrown.
     *
     * @param c The character
     * @return The rank of the character
     */
    public int getRank(char c) {
        if (!this.charMap.containsKey(c)) {
            throw new IllegalArgumentException();
        }
        return this.charMap.get(c);
    }

    /**
     * Returns the character with the given rank in the alphabet. For invalid ranks, an exception is thrown.
     *
     * @param rank The rank
     * @return The character with the rank
     */
    public char getChar(int rank) {
        if (rank < 0 || rank >= this.characters.length())
            throw new IndexOutOfBoundsException();
        return this.characters.charAt(rank);
    }

    /**
     * Adds a new character to an alphabet. The returned alphabet is a new object, which has been created without
     * modifying the given alphabet.
     *
     * @param c The character to be added
     * @return The extended alphabet
     */
    public Alphabet addCharacter(char c) {
        if (this.contains(c)) {
            return this;
        } else {
            return new Alphabet(this.characters + c);
        }
    }

    /**
     * Checks if all characters from a given string are elements of the alphabet. This method complements the {@link
     * Set#containsAll} methods from the {@link Set} interface.
     *
     * @param s The string
     * @return {@code true}, if all characters from {@code s} are in the alphabet, {@code false} otherwise
     */
    public boolean containsAll(String s) {
        return this.containsAll(s.chars().mapToObj(c -> (char) c));
    }

    @Override
    public boolean contains(Character c) {
        return this.charMap.containsKey(c);
    }

}
