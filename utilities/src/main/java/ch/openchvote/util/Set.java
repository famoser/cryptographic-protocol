/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.Mod;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.*;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * This is a functional interface for implementing set membership tests in a simple and consistent way for different
 * types of sets. Classes implementing this interface are therefore not to be understood as data structures such as
 * {@link java.util.HashSet} or {@link java.util.TreeSet}. Based on the basic membership test implemented by {@link
 * Set#contains}, the interface offers default methods for performing membership tests over all values from a given
 * stream, vector, matrix, or array.
 *
 * @param <T> The generic type of the values contained in the set
 */
public interface Set<T> {

    /**
     * Performs the membership test on a single object {@code x} of the given generic type. Returns {@code true}, if
     * {@code x} is a member of the set, and {@code false} otherwise.
     *
     * @param x The given object
     * @return {@code true} if {@code x} is a member of the set
     */
    boolean contains(T x);

    /**
     * Performs the membership test on all objects of a given stream. Returns {@code true}, if all values are members of
     * the set. If the stream itself is {@code null}, the membership test returns {@code false}.
     *
     * @param stream The given stream
     * @return {@code true} if all values are members of the set
     */
    default boolean containsAll(Stream<T> stream) {
        return stream != null && stream.allMatch(this::contains);
    }

    /**
     * Performs the membership test on all objects of a given collection. Returns {@code true}, if all values are
     * members of the set. If the collection itself is {@code null}, the membership test returns {@code false}.
     *
     * @param values The given collection of values
     * @return {@code true} if all values are members of the set
     */
    default boolean containsAll(Collection<T> values) {
        return values != null && this.containsAll(values.stream());
    }

    /**
     * Extends the given set with the value {@code null}.
     *
     * @return The set extended with {@code null}
     */
    default Set<T> orNull() {
        Set<T> set = this;
        return x -> x == null || set.contains(x);
    }

    /**
     * The set of non-negative integers {0,1,2,...}.
     */
    Set<BigInteger> NN = x -> x != null && x.signum() >= 0;

    /**
     * The set of positive integers {1,2,3,...}.
     */
    Set<BigInteger> NN_plus = x -> x != null && x.signum() > 0;

    /**
     * Returns the set {0,...,n-1} of integers modulo {@code n}.
     *
     * @param n The modulo
     * @return The set of integers modulo {@code n}
     * @throws IllegalArgumentException if {@code n == null} or {@code n <= 0}
     */
    static Set<BigInteger> ZZ(BigInteger n) {
        if (n == null || n.signum() <= 0)
            throw new IllegalArgumentException();
        return x -> x != null && x.signum() >= 0 && x.compareTo(n) < 0;
    }

    /**
     * Returns the set {0,...,2^e-1} of integers modulo {@code 2^e}.
     *
     * @param e The exponent of the modulo
     * @return The set of integers modulo {@code 2^e}
     * @throws ArithmeticException if {@code e < 0}
     */
    static Set<BigInteger> ZZ_powerOfTwo(int e) {
        return ZZ(BigInteger.TWO.pow(e));
    }

    /**
     * Returns the set Z*_n = {1<=x<n: gcd(x,n)=1} of integers relatively prime to {@code n}.
     *
     * @param n The modulo
     * @return The set of integers relatively prime to {@code n}
     * @throws IllegalArgumentException if {@code n == null} or {@code n <= 0}
     */
    static Set<BigInteger> ZZ_star(BigInteger n) {
        if (n == null || n.signum() < 0)
            throw new IllegalArgumentException();
        return x -> x != null && x.signum() == 1 && x.compareTo(n) < 0 && n.gcd(x).equals(BigInteger.ONE);
    }

    /**
     * Returns the set of quadratic residues modulo {@code p}, where {@code p} is an assumed safe prime (not tested
     * explicitly).
     *
     * @param p The given safe prime modulo
     * @return The set of quadratic residues modulo {@code p}
     * @throws IllegalArgumentException if {@code p == null} or {@code p <= 0}
     */
    static Set<QuadraticResidue> GG(BigInteger p) {
        if (p == null || p.signum() <= 0)
            throw new IllegalArgumentException();
        return x -> x != null && x.hasModulus(p);
    }

    /**
     * Returns the {@code q}-order subgroup of integers modulo {@code p}, where {@code p} and {@code q} are assumed
     * primes (not tested explicitly) of the form {@code p=kq+1}.
     *
     * @param p The given prime modulo
     * @param q The prime order of the subgroup
     * @return The {@code q}-order subgroup of integers modulo {@code p}
     * @throws IllegalArgumentException if {@code p == null}, {@code q == null}, {@code p <= q}, or {@code q} does not
     *                                  divide {@code p-1}
     */
    static Set<BigInteger> GG(BigInteger p, BigInteger q) {
        if (p == null || q == null || p.compareTo(q) <= 0 || !Math.divides(q, p.subtract(BigInteger.ONE)))
            throw new IllegalArgumentException();
        return x -> x != null
                && x.signum() > 0
                && x.compareTo(p) < 0
                && Mod.pow(x, q, p).equals(BigInteger.ONE);
    }

    /**
     * Returns the set of all permutations of length {@code n}. Permutations are represented by integer vectors
     * containing the values {@code 1,...,n} in permuted order.
     *
     * @param n The given length
     * @return The set of permutations of length {@code n}
     * @throws IllegalArgumentException if {@code n < 0}
     */
    static Set<IntVector> Phi(int n) {
        if (n < 0)
            throw new IllegalArgumentException();
        return phi -> phi != null
                && phi.getLength() == n
                && phi.toStream().allMatch(x -> 1 <= x && x <= n)
                && phi.toStream().distinct().count() == n;
    }

    /**
     * Returns the set of all byte arrays of length {@code length}.
     *
     * @param length The given length
     * @return The set of byte arrays of length {@code length}
     * @throws IllegalArgumentException if {@code length < 0}
     */
    static Set<ByteArray> B(int length) {
        if (length < 0)
            throw new IllegalArgumentException();
        return byteArray -> byteArray != null && byteArray.getLength() == length;
    }

    /**
     * The set of all byte arrays of arbitrary length.
     */
    Set<ByteArray> B_star = Objects::nonNull;

    /**
     * The set of all UCS strings.
     */
    Set<String> UCS_star = string -> string != null && Alphabet.UCS.containsAll(string);

    /**
     * Returns the set of all UCS strings of length {@code n}.
     *
     * @param length The given length
     * @return The set of strings of length {@code n}
     */
    static Set<String> UCS(int length) {
        return Set.String(Alphabet.UCS, length);
    }

    /**
     * Returns the set of all strings of arbitrary length with characters from the given alphabet.
     *
     * @param alphabet The given alphabet
     * @return The set of strings of with characters from the given alphabet
     * @throws IllegalArgumentException if {@code alphabet == null}
     */
    static Set<String> String(Alphabet alphabet) {
        if (alphabet == null)
            throw new IllegalArgumentException();
        return string -> string != null && alphabet.containsAll(string);
    }

    /**
     * Returns the set of all strings of length {@code length} with characters from the given alphabet.
     *
     * @param alphabet The given alphabet
     * @param length   The given length
     * @return The set of strings of length {@code length} with characters from the given alphabet
     * @throws IllegalArgumentException if {@code alphabet == null} or {@code length < 0}
     */
    static Set<String> String(Alphabet alphabet, int length) {
        if (alphabet == null || length < 0)
            throw new IllegalArgumentException();
        return string -> string != null
                && string.length() == length
                && alphabet.containsAll(string);
    }

    /**
     * Returns the set of all strings of length greater or equal to {@code minLength} and smaller or equal to
     * {@code maxLength} with characters from the given alphabet.
     *
     * @param alphabet  The given alphabet
     * @param minLength The given minimal length
     * @param maxLength The given maximal length
     * @return The set of strings of length {@code minLength <= n <= maxLength} with characters from the given alphabet
     * @throws IllegalArgumentException if {@code alphabet == null}, {@code minLength < 0}, or {@code maxLength < 0}
     */
    static Set<String> String(Alphabet alphabet, int minLength, int maxLength) {
        if (alphabet == null || minLength < 0 || maxLength < 0)
            throw new IllegalArgumentException();
        return string -> string != null
                && string.length() >= minLength
                && string.length() <= maxLength
                && alphabet.containsAll(string);
    }

    /**
     * The set of all byte arrays representing a valid UTF8 encoding.
     */
    Set<ByteArray> UTF8 = B -> {
        var decoder = StandardCharsets.UTF_8.newDecoder();
        decoder.onMalformedInput(CodingErrorAction.REPORT);
        decoder.onUnmappableCharacter(CodingErrorAction.REPORT);
        try {
            decoder.decode(ByteBuffer.wrap(B.toByteArray()));
            return true;
        } catch (CharacterCodingException e) {
            return false;
        }
    };

    /**
     * Returns the set of all vectors with elements from the given set.
     *
     * @param set The given set
     * @param <T> The generic type of both the vctor and the set
     * @return The set of all vectors with elements from the given set
     */
    static <T> Set<Vector<? extends T>> Vector(Set<T> set) {
        if (set == null)
            throw new IllegalArgumentException();
        return vector -> vector.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all vectors of length {@code length} with elements from the given set.
     *
     * @param set    The given set
     * @param length The length of the vectors
     * @param <T>    The generic type of both the vectors and the set
     * @return The set of all vectors of length {@code length} with elements from the given set
     */
    static <T> Set<Vector<? extends T>> Vector(Set<T> set, int length) {
        if (set == null || length < 0)
            throw new IllegalArgumentException();
        return vector -> (vector.getLength() == length) && vector.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all matrices of the given height and width with elements from the given set.
     *
     * @param set    The given set
     * @param height The height of the matrices
     * @param width  The width of the matrices
     * @param <T>    The generic type of both the matrices and the set
     * @return The set of all matrices of the given height and width with elements from the given set
     */
    static <T> Set<Matrix<? extends T>> Matrix(Set<T> set, int height, int width) {
        if (set == null || height < 0 || width < 0)
            throw new IllegalArgumentException();
        return matrix -> (matrix.getHeight() == height) && (matrix.getWidth() == width) && matrix.toStream().allMatch(set::contains);
    }

    /**
     * Returns the set of all {@code int} vectors of length {@code length} with elements from the given {@code int} set.
     *
     * @param set    The given {@code int} set
     * @param length The length of the {@code int} vectors
     * @return The set of all {@code int} vectors of length {@code length} with elements from the given {@code int} set
     */
    static Set<IntVector> IntVector(IntSet set, int length) {
        if (set == null || length < 0)
            throw new IllegalArgumentException();
        return vector -> (vector.getLength() == length) && set.containsAll(vector);
    }

    /**
     * Returns the set of all {@code int} matrices of the given height and width with elements from the given {@code int} set.
     *
     * @param set    The given {@code int} set
     * @param height The height of the matrices
     * @param width  The width of the matrices
     * @return The set of all {@code int} matrices of the given height and width with elements from the given {@code int} set
     */
    static Set<IntMatrix> IntMatrix(IntSet set, int height, int width) {
        if (set == null || height < 0 || width < 0)
            throw new IllegalArgumentException();
        return matrix -> (matrix.getHeight() == height) && (matrix.getWidth() == width) && set.containsAll(matrix);
    }

    /**
     * Creates the Cartesian product of two input sets.
     * @param set1 The first set
     * @param set2 The second set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @return The Cartesian product of two sets
     */
    static <T1, T2> Set<Pair<? extends T1, ? extends T2>> Pair(Set<T1> set1, Set<T2> set2) {
        if (set1 == null || set2 == null)
            throw new IllegalArgumentException();
        return pair -> set1.contains(pair.getFirst()) && set2.contains(pair.getSecond());
    }

    /**
     * Creates the Cartesian product of three input sets.
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @return The Cartesian product of three sets
     */
    static <T1, T2, T3> Set<Triple<? extends T1, ? extends T2, ? extends T3>> Triple(Set<T1> set1, Set<T2> set2, Set<T3> set3) {
        if (set1 == null || set2 == null || set3 == null)
            throw new IllegalArgumentException();
        return triple -> set1.contains(triple.getFirst()) && set2.contains(triple.getSecond()) && set3.contains(triple.getThird());
    }

    /**
     * Creates the Cartesian product of four input sets.
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @return The Cartesian product of four sets
     */
    static <T1, T2, T3, T4> Set<Quadruple<? extends T1, ? extends T2, ? extends T3, ? extends T4>> Quadruple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4) {
        if (set1 == null || set2 == null || set3 == null || set4 == null)
            throw new IllegalArgumentException();
        return quadruple -> set1.contains(quadruple.getFirst()) && set2.contains(quadruple.getSecond()) && set3.contains(quadruple.getThird()) && set4.contains(quadruple.getFourth());
    }

    /**
     * Creates the Cartesian product of five input sets.
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @return The Cartesian product of five sets
     */
    static <T1, T2, T3, T4, T5> Set<Quintuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5>> Quintuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null)
            throw new IllegalArgumentException();
        return quintuple -> set1.contains(quintuple.getFirst()) && set2.contains(quintuple.getSecond()) && set3.contains(quintuple.getThird()) && set4.contains(quintuple.getFourth()) && set5.contains(quintuple.getFifth());
    }

    /**
     * Creates the Cartesian product of six input sets.
     * @param set1 The first set
     * @param set2 The second set
     * @param set3 The third set
     * @param set4 The fourth set
     * @param set5 The fifth set
     * @param set6 The sixth set
     * @param <T1> The generic type of the first set
     * @param <T2> The generic type of the second set
     * @param <T3> The generic type of the third set
     * @param <T4> The generic type of the fourth set
     * @param <T5> The generic type of the fifth set
     * @param <T6> The generic type of the sixth set
     * @return The Cartesian product of six sets
     */
    static <T1, T2, T3, T4, T5, T6> Set<Sextuple<? extends T1, ? extends T2, ? extends T3, ? extends T4, ? extends T5, ? extends T6>> Sextuple(Set<T1> set1, Set<T2> set2, Set<T3> set3, Set<T4> set4, Set<T5> set5, Set<T6> set6) {
        if (set1 == null || set2 == null || set3 == null || set4 == null || set5 == null || set6 == null)
            throw new IllegalArgumentException();
        return sextuple -> set1.contains(sextuple.getFirst()) && set2.contains(sextuple.getSecond()) && set3.contains(sextuple.getThird()) && set4.contains(sextuple.getFourth()) && set5.contains(sextuple.getFifth()) && set6.contains(sextuple.getSixth());
    }

}
