/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.IntFunction;
import java.util.function.IntUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Objects of this class represent immutable vectors of {@code int} values of a fixed length {@code n}. Their elements
 * are indexed from {@code minIndex} (usually {@code 1}) to {@code maxIndex} (usually {@code n}). Since this class is
 * abstract, it does not offer public constructors. New instances of this class are created using the static member
 * class {@link Builder}. During the building process, values can be added to or placed into an initially empty vector
 * containing zeros. After the building process, accessing the values in the resulting vector is restricted to
 * read-only. The class implements the {@link Iterable} interface.
 */
public abstract class IntVector extends Hashable implements Iterable<Integer>, Comparable<IntVector> {

    /**
     * Returns the value at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The value at this index
     * @throws IndexOutOfBoundsException if an invalid index is given
     */
    public abstract int getValue(int index);

    /**
     * Returns the minimal index of this array.
     *
     * @return The minimal index
     */
    public abstract int getMinIndex();

    /**
     * Returns the maximal index of this array.
     *
     * @return The maximal index
     */
    public abstract int getMaxIndex();

    /**
     * Returns the length of the vector
     *
     * @return The length of the vector
     */
    public int getLength() {
        return this.getMaxIndex() - this.getMinIndex() + 1;
    }

    /**
     * Creates a new vector of type {@code W} by applying a function to each {@code int} value of the vector.
     * By returning a wrapper object which performs the mapping lazily, this method runs ins constant time.
     *
     * @param function The function that maps the values of the vector
     * @param <W>      The type of the returned vector
     * @return A vector containing all mapped values
     */
    public <W> Vector<W> mapToObj(IntFunction<? extends W> function) {
        IntVector vector = this;
        return new Vector<>() {

            @Override
            public W getValue(int index) {
                return function.apply(vector.getValue(index));
            }

            @Override
            public int getMinIndex() {
                return vector.getMinIndex();
            }

            @Override
            public int getMaxIndex() {
                return vector.getMaxIndex();
            }
        };
    }

    /**
     * Creates a new {@code IntVector} by applying a function to each {@code int} value of the vector.
     * By returning a wrapper object which performs the mapping lazily, this method runs ins constant time.
     *
     * @param function The function that maps the values of the vector
     * @return A vector containing all mapped values
     */
    public IntVector map(IntUnaryOperator function) {
        IntVector vector = this;
        return new IntVector() {
            @Override
            public int getValue(int index) {
                return function.applyAsInt(vector.getValue(index));
            }

            @Override
            public int getMinIndex() {
                return vector.getMinIndex();
            }

            @Override
            public int getMaxIndex() {
                return vector.getMaxIndex();
            }
        };
    }

    /**
     * Creates a new vector by selecting the values specified by the given {@code indexSet}. By taking the given indices
     * in ascending order, the order of the values in the returned vector corresponds to the order in the original
     * vector. The length of the returned vector is equal to the size of {@code indexSet}, and indexing starts at 1.
     *
     * @param indexSet The indices of the values to be selected
     * @return A new vector containing the selected values
     */
    public IntVector select(IntSet indexSet) {
        if (indexSet == null)
            throw new IndexOutOfBoundsException();
        var indexArray = this.getIndices().filter(indexSet::contains).toArray();
        IntVector vector = this;
        return new IntVector() {
            @Override
            public int getValue(int index) {
                return vector.getValue(indexArray[index - 1]);
            }

            @Override
            public int getMinIndex() {
                return 1;
            }

            @Override
            public int getMaxIndex() {
                return indexArray.length;
            }
        };
    }

    // private helper method for generating a stream of all indices
    private IntStream getIndices() {
        return IntStream.rangeClosed(this.getMinIndex(), this.getMaxIndex());
    }

    /**
     * Checks if all values of the vector are equal.
     *
     * @return {@code true}, if all values are equals, {@code false}, otherwise
     */
    public boolean isUniform() {
        return this.getIndices().skip(1).allMatch(index -> this.getValue(index - 1) == this.getValue(index));
    }

    /**
     * Checks that every value of this int vector is strictly less than the corresponding value of another int vector of
     * the same length. Returns {@code true}, if this the case.
     *
     * @param other The other vector
     * @return {@code true}, if every value of the this vector is strictly less than the corresponding value of the
     * other vector, {@code false}, otherwise
     * @throws IllegalArgumentException if the vectors are not of equal length
     */
    public boolean isLess(IntVector other) {
        if (other == null || this.getLength() != other.getLength())
            throw new IllegalArgumentException();
        return this.getIndices().allMatch(index -> this.getValue(index) <= other.getValue(index));
    }

    /**
     * Returns a new int vector containing the same values sorted in ascending order.
     *
     * @return The sorted vector
     */
    public IntVector sort() {
        var builder = new IntVector.Builder(this.getMinIndex(), this.getMaxIndex());
        this.toStream().sorted().forEach(builder::addValue);
        return builder.build();
    }

    /**
     * Checks if the int vector is sorted in ascending order.
     *
     * @return {@code true}, if the vector is sorted, {@code false}, otherwise
     */
    public boolean isSorted() {
        return this.getIndices().skip(1).allMatch(index -> this.getValue(index - 1) < this.getValue(index));
    }

    /**
     * Returns a stream containing all values of the vector in the same order.
     *
     * @return The stream obtained from the vector
     */
    public IntStream toStream() {
        return this.getIndices().map(this::getValue);
    }

    /**
     * Returns an array containing all values of the vector in the same order.
     *
     * @return The array of values obtained from the vector
     */
    public int[] toArray() {
        int[] values = new int[this.getLength()];
        this.getIndices().forEach(i -> values[i - this.getMinIndex()] = this.getValue(i));
        return values;
    }

    @Override
    public Iterator<Integer> iterator() {
        IntVector vector = this;
        return new Iterator<>() {

            private int currentIndex = vector.getMinIndex();

            @Override
            public boolean hasNext() {
                return this.currentIndex <= vector.getMaxIndex();
            }

            @Override
            public Integer next() {
                return vector.getValue(this.currentIndex++);
            }
        };
    }

    @Override
    public int compareTo(IntVector other) {
        int c;
        // compare length
        c = this.getLength() - other.getLength();
        if (c != 0) return c;

        // compare minIndex
        c = this.getMinIndex() - other.getMinIndex();
        if (c != 0) return c;

        // compare values
        for (int i = this.getMinIndex(); i <= this.getMaxIndex(); i++) {
            c = this.getValue(i) - other.getValue(i);
            if (c != 0) return c;
        }
        // equality
        return 0;
    }


    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof IntVector)) return false;

        IntVector other = (IntVector) object;
        if (this.getMinIndex() != other.getMinIndex()) return false;
        if (this.getMaxIndex() != other.getMaxIndex()) return false;

        return this.getIndices().allMatch(index -> this.getValue(index) == other.getValue(index));
    }

    @Override
    public int hashCode() {
        int initValue = 0;
        initValue = 31 * initValue + this.getMinIndex();
        initValue = 31 * initValue + this.getMaxIndex();
        return this.toStream().reduce(initValue, (result, hash) -> 31 * result + hash);
    }

    @Override
    public String toString() {
        return this.toStream().mapToObj(Integer::toString).collect(Collectors.joining(",", "[", "]"));
    }

    /**
     * This static builder class is the main tool for constructing vectors from scratch. If the length of the vector to
     * construct is defined at the beginning of the building process, then the values are initially all set to {@code
     * null}. If the length is not specified, then the vector grows when new values are added. In both cases, values can
     * be added either incrementally or in arbitrary order. At the end of the building process, the vector can be built
     * exactly once. The builder class is threadsafe and the resulting vector is immutable.
     */
    public static class Builder {

        // flag that determines whether the vector has already been built or not
        private boolean built;
        // flag that determines whether the vector length is fixed or not during building process
        private final boolean growable;
        // the first and the last valid index
        private int minIndex;
        private int maxIndex;
        // an array for storing the added values during the building process
        private int[] values;
        // an internal index counter for adding values incrementally
        private int indexCounter;

        /**
         * Constructs a vector builder for a vector with undetermined length. The length of the vector grows
         * automatically to the necessary length when values are added. Indexing starts at 1.
         */
        public Builder() {
            this.built = false;
            this.growable = true;
            this.minIndex = 1;
            this.maxIndex = 0;
            this.values = new int[0];
            this.indexCounter = 0;
        }

        /**
         * Constructs a vector builder for a vector of fixed length {@code length}. Indexing starts from 1 and goes up
         * to {@code length}.
         *
         * @param length The length of the vector to construct
         */
        public Builder(int length) {
            this(1, length);
        }

        /**
         * Constructs a vector builder for a vector of fixed length {@code maxIndex-minIndex+1}. Indexing starts from
         * {@code minIndex} and goes up {@code maxIndex}.
         *
         * @param minIndex The minimal index
         * @param maxIndex The maximal index
         */
        public Builder(int minIndex, int maxIndex) {
            if (minIndex < 0 || minIndex > maxIndex + 1)
                throw new IllegalArgumentException();
            this.built = false;
            this.growable = false;
            this.minIndex = minIndex;
            this.maxIndex = maxIndex;
            this.values = new int[this.maxIndex - this.minIndex + 1];
            this.indexCounter = 0;
        }

        /**
         * Fills up the int vector with a single value. In case of a vector with undetermined length, the current length
         * remains unchanged.
         *
         * @param value The value used for filling up the vector
         * @return The vector builder itself
         */
        public IntVector.Builder fill(int value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                for (int i = this.minIndex; i <= this.maxIndex; i++) {
                    this.setValue(i, value);
                }
                return this;
            }
        }

        /**
         * Sets the given value at the given index. If the vector length is undetermined, increases the length if
         * necessary. The vector builder object itself is returned to allow pipeline notation when multiple values are
         * added to the vector.
         *
         * @param index The index of the value to be added
         * @param value The value to be added
         * @return The vector builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if an invalid index is given
         */
        public Builder setValue(int index, int value) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                if (!this.growable && (index < this.minIndex || index > this.maxIndex))
                    throw new IndexOutOfBoundsException();
                if (this.growable) {
                    this.minIndex = Math.min(this.minIndex, index);
                    this.maxIndex = Math.max(this.maxIndex, index);
                }
                int length = this.maxIndex - this.minIndex + 1;
                int arraylength = this.values.length;
                while (length > arraylength) {
                    arraylength = 2 * arraylength + 1;
                }
                if (arraylength > this.values.length) {
                    this.values = Arrays.copyOf(this.values, arraylength);
                }
                this.values[index - this.minIndex] = value;
                return this;
            }
        }

        /**
         * Sets the next value in the vector and increases the internal index counter by 1. If the vector length is
         * undetermined, increases the length if necessary. The vector builder object itself is returned to allow
         * pipeline notation when multiple values are added to the vector.
         *
         * @param value The value to be added
         * @return The vector builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder addValue(int value) {
            synchronized (this) {
                int currentIndex;
                currentIndex = this.minIndex + this.indexCounter;
                this.indexCounter = this.indexCounter + 1;
                return this.setValue(currentIndex, value);
            }
        }

        /**
         * Terminates the building process and returns the constructed vector.
         *
         * @return The constructed vector
         * @throws IllegalStateException if the vector has already been build
         */
        public IntVector build() {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                this.built = true;
                return IntVector.fromSafeArray(this.values, this.minIndex, this.maxIndex);
            }
        }

    }

    /**
     * This static method allows constructing new vectors from a given vararg array of integers. The array must
     * be safe to use without copying its values. This method must therefore be used with maximal care. The indexing in
     * the newly created vector starts from 1.
     *
     * @param values The given int values
     * @return The constructed vector
     * @throws IllegalArgumentException if the given array of int values is null
     */
    public static IntVector fromSafeArray(int... values) {
        if (values == null)
            throw new IllegalArgumentException();
        return IntVector.fromSafeArray(values, 1, values.length);
    }

    // private helper method for vectors with arbitrary indices
    private static IntVector fromSafeArray(int[] values, int minIndex, int maxIndex) {
        return new IntVector() {

            @Override
            public int getValue(int index) {
                if (index < minIndex || index > maxIndex)
                    throw new IndexOutOfBoundsException();
                return values[index - minIndex];
            }

            @Override
            public int getMinIndex() {
                return minIndex;
            }

            @Override
            public int getMaxIndex() {
                return maxIndex;
            }

            @Override
            public int[] toArray() {
                if (values == null) {
                    return new int[0];
                }
                if (values.length == this.getLength()) {
                    return values;
                } else {
                    return Arrays.copyOf(values, this.getLength());
                }
            }
        };
    }

}
