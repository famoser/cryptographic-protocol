/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.math;

import ch.openchvote.util.Vector;

import java.math.BigInteger;
import java.util.Objects;

/**
 * The elements of this class represent quadratic residues modulo a safe prime. Internally, quadratic residues are represented
 * by the smaller of their square roots. This way of representing quadratic residues offers efficient group membership
 * tests. {@link QuadraticResidue#ONE} represents the identify element independently of the modulus, which is convenient
 * for implementing "empty products" without knowing the modulus (see {@link Mod#prod(Vector)}).
 */
public class QuadraticResidue implements Comparable<QuadraticResidue> {

    /**
     * The identity element 1 for an arbitrary modulus.
     */
    public static final QuadraticResidue ONE = new QuadraticResidue();

    private final BigInteger modulus; // safe prime
    private final BigInteger sqrtValue; // always the smaller of te two square roots
    private BigInteger value; // the actual group element (or null)

    /**
     * This is a convenience constructor for creating quadratic residues modulo a safe prime in examples with small values
     * of type {@link long}. These values are translated into BigIntegers, which are then handed over to the main constructor
     * {@link QuadraticResidue#QuadraticResidue(BigInteger, BigInteger)}.
     *
     * @param value The given quadratic residue
     * @param modulus The safe prime modulus
     * @throws IllegalArgumentException if the given value is not an element of the corresponding subgroup
     */
    public QuadraticResidue(long value, long modulus) {
        this(BigInteger.valueOf(value), BigInteger.valueOf(modulus));
    }

    /**
     * This is the main constructor for creating quadratic residues modulo a safe prime. It computes the square root of
     * the given value for internal representation. An exception is thrown, if the given value is not an element of
     * the corresponding subgroup.
     *
     * @param value The given quadratic residue
     * @param modulus The safe prime modulus
     * @throws IllegalArgumentException if the given value is not an element of the corresponding subgroup
     */
    public QuadraticResidue(BigInteger value, BigInteger modulus) {
        this(value, Mod.pow(value, modulus.add(BigInteger.ONE).shiftRight(2), modulus), modulus);
    }

    /**
     * This is another constructor for creating quadratic residues modulo a safe prime for cases where the square root
     * representation is already known. An exception is thrown, if the given value is not {@code null} and not an element of
     * the corresponding subgroup.
     *
     * @param value The given quadratic residue
     * @param sqrtValue The given square root of the quadratic residue
     * @param modulus The safe prime modulus
     * @throws IllegalArgumentException if the given value is not {@code null} and not an element of the corresponding subgroup
     */
    public QuadraticResidue(BigInteger value, BigInteger sqrtValue, BigInteger modulus) {
        if (sqrtValue.compareTo(BigInteger.ZERO) <= 0 || sqrtValue.compareTo(modulus) >= 0)
            throw new IllegalArgumentException();
        if (value != null && !Mod.multiply(sqrtValue, sqrtValue, modulus).equals(value))
            throw new IllegalArgumentException();
        if (sqrtValue.equals(BigInteger.ONE)) {
            this.modulus = null;
            this.sqrtValue = BigInteger.ONE;
            this.value = BigInteger.ONE;
        } else {
            this.modulus = modulus;
            this.sqrtValue = sqrtValue.min(Mod.minus(sqrtValue, modulus));
            this.value = value;
        }
    }

    // private for constructing the identity element 1 for an arbitrary modulo
    private QuadraticResidue() {
        this.modulus = null;
        this.sqrtValue = BigInteger.ONE;
        this.value = BigInteger.ONE;
    }

    /**
     * Multiplies the quadratic residue with another one.
     *
     * @param other The other quadratic residue
     * @return The product of the two quadratic residues
     * @throws IllegalArgumentException, if the modulo of the other quadratic residue is not the same
     */
    public QuadraticResidue modMultiply(QuadraticResidue other) {
        if (this.isOne()) return other;
        if (other.isOne()) return this;
        if (!this.modulus.equals(other.modulus))
            throw new IllegalArgumentException();
        return new QuadraticResidue(null, Mod.multiply(this.sqrtValue, other.sqrtValue, this.modulus), this.modulus);
    }

    /**
     * Raises this quadratic residue to the power of the given exponent.
     *
     * @param exp The given exponent
     * @return The quadratic residue raised to the power of the given exponent
     * @throws IllegalArgumentException, if the modulo of the other quadratic residue is not the same
     */
    public QuadraticResidue modPow(BigInteger exp) {
        if (this.isOne() || exp.signum() == 0) return ONE;
        if (exp.signum() > 0) {
            return new QuadraticResidue(null, Mod.pow(this.sqrtValue, exp, this.modulus), this.modulus);
        } else {
            return this.modPow(exp.abs()).modInverse();
        }
    }

    /**
     * Returns the multiplicative inverse of the quadratic residue.
     *
     * @return The multiplicative inverse of the quadratic residue
     */
    public QuadraticResidue modInverse() {
        if (this.isOne()) return ONE;
        return new QuadraticResidue(null, Mod.invert(this.sqrtValue, this.modulus), this.modulus);
    }

    /**
     * Checks if this instance is the identity element 1.
     *
     * @return {@code true}, if this instance is the identity element 1, {@code false} otherwise
     */
    public boolean isOne() {
        return this.modulus == null;
    }

    /**
     * Checks if the modulus is equal to {@code p}.
     *
     * @return {@code true}, if the modulus is equal to {@code p}, {@code false} otherwise
     */
    public boolean hasModulus(BigInteger p) {
        return this.modulus == null || this.modulus.equals(p);
    }

    /**
     * Returns the modulus of this quadratic residue.
     *
     * @return The modulus
     */
    public BigInteger getModulus() {
        return this.modulus;
    }

    /**
     * Returns the square root representation of this quadratic residue.
     *
     * @return The square root of the quadratic residue
     */
    public BigInteger getSqrtValue() {
        return this.sqrtValue;
    }

    /**
     * Returns the quadratic residue as BigInteger.
     *
     * @return The quadratic residue as BigInteger
     */
    public BigInteger getValue() {
        if (this.value == null) {
            this.value = this.isOne() ? BigInteger.ONE : Mod.multiply(this.sqrtValue, this.sqrtValue, this.modulus);
        }
        return this.value;
    }

    @Override
    public String toString() {
        return getValue().toString();
    }

    @Override
    public int compareTo(QuadraticResidue other) {
        if (!Objects.equals(this.modulus, other.modulus))
            throw new IllegalArgumentException();
        return this.getValue().compareTo(other.getValue());
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof QuadraticResidue)) return false;
        QuadraticResidue other = (QuadraticResidue) object;
        return this.sqrtValue.equals(other.sqrtValue) && Objects.equals(this.modulus, other.modulus);
    }

    @Override
    public int hashCode() {
        int result = this.sqrtValue.hashCode();
        result = 31 * result + (this.modulus == null ? 0 : this.modulus.hashCode());
        return result;
    }

}
