/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.math;

import ch.openchvote.util.IntVector;
import ch.openchvote.util.Vector;

import java.math.BigInteger;
import java.util.stream.IntStream;

/**
 * This class provides some additional methods for mathematical functions not included in standard Java libraries. The
 * methods deal with integer values of type {@code byte}, {@code int} or {@link BigInteger}.
 */
public class Math {

    /**
     * Converts a given value {@code 0<=x<=255} of type {@code int} into a value {@code -128<=b<=127} of type {@code
     * byte}. For an invalid input, the return value is not specified.
     *
     * @param x The given value of type {@code int}
     * @return The converted value of type {@code byte}
     */
    public static byte intToByte(int x) {
        return (byte) x;
    }

    /**
     * Converts a given value {@code -128<=b<=127} of type {@code byte} into a value {@code 0<=x<=255} of type {@code
     * int}. This method implements the inverse functions of {@code intToByte}.
     *
     * @param b The given value of type {@code byte}
     * @return The converted value of type {@code int}
     */
    public static int byteToInt(byte b) {
        return b < 0 ? b + 256 : b;
    }

    /**
     * Computes the bit-wise logical AND-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical AND-operation
     */
    public static byte and(byte b1, byte b2) {
        return (byte) (b1 & b2);
    }

    /**
     * Computes the bit-wise logical OR-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical OR-operation
     */
    public static byte or(byte b1, byte b2) {
        return (byte) (b1 | b2);
    }

    /**
     * Computes the bit-wise logical XOR-operation of two bytes.
     *
     * @param b1 The first byte
     * @param b2 The second byte
     * @return The result of applying the bit-wise logical XOR-operation
     */
    public static byte xor(byte b1, byte b2) {
        return (byte) (b1 ^ b2);
    }

    /**
     * Computes the fraction {@code x/y} rounded up to the next integer, where {@code x>=0} and {@code y>=1} are the
     * given input values of type {@code int}. For invalid inputs, the return value is not specified.
     *
     * @param x The numerator
     * @param y The denominator
     * @return The fraction {@code x/y} rounded up to the next integer
     */
    public static int ceilDiv(int x, int y) {
        return x / y + (x % y == 0 ? 0 : 1);
    }

    /**
     * Computes the number of necessary bits to represent {@code x>=0} of type {@code int}. For negative inputs, the
     * return value is not specified.
     *
     * @param x The given value
     * @return The number of necessary bits to represent {@code x>=0}
     */
    public static int bitLength(int x) {
        return Integer.SIZE - Integer.numberOfLeadingZeros(java.lang.Math.abs(x));
    }

    /**
     * Checks if {@code x>0} is an integer divisor of {@code y>=0}. For invalid inputs, the return value is not
     * specified.
     *
     * @param x The first integer
     * @param y The second integer
     * @return {@code true}, if {@code x} is an integer divisor of {@code y}, {@code false} otherwise
     */
    public static boolean divides(int x, int y) {
        return y % x == 0;
    }

    /**
     * Computes {@code 2^y} for a given integer {@code 0<=y<=30}. For an invalid input, the return value is not
     * specified.
     *
     * @param y The given exponent
     * @return The {@code y}-th power of 2
     */
    public static int powerOfTwo(int y) {
        return 1 << y;
    }

    /**
     * Computes {@code 10^y} for a given integer {@code 0<=y<=9}. For an invalid input, the return value is not
     * specified.
     *
     * @param y The given exponent
     * @return The {@code y}-th power of 10
     */
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public static int powerOfTen(int y) {
        return IntStream.iterate(1, x -> x * 10).skip(y).findFirst().getAsInt();
    }

    /**
     * Computes the sum of all integer values from a given vector of type {@code IntVector}. The behaviour of this
     * method is unspecified for vectors containing {@code null} values.
     *
     * @param vector The given vector of integer values
     * @return The sum all integer values
     */
    public static int intSum(IntVector vector) {
        return intSum(vector, 1, vector.getLength());
    }

    /**
     * Computes the sum of all integer values from a given vector of type {@code IntVector} with indices between
     * {@code startIndex} and {@code endIndex}. The behaviour of this method is unspecified for vectors containing
     * {@code null} values.
     *
     * @param vector     The given vector of integer values
     * @param startIndex The lowest index of the values to be included
     * @param endIndex   The highest index of the values to be included
     * @return The sum all integer values with indices between {@code startIndex} and {@code endIndex}
     */
    public static int intSum(IntVector vector, int startIndex, int endIndex) {
        return IntStream.rangeClosed(startIndex, endIndex).map(vector::getValue).sum();
    }

    /**
     * Computes the sum of products of all integer pairs from two input vectors of type {@code IntVector}. An
     * exception is thrown for input vectors of different length.
     *
     * @param vector1 The first vector of integer values
     * @param vector2 The second vector of integer values
     * @return The sum of products all integer pairs
     * @throws IndexOutOfBoundsException if the vectors are not equally long
     */
    public static int intSumProd(IntVector vector1, IntVector vector2) {
        if (vector1.getLength() != vector2.getLength())
            throw new IndexOutOfBoundsException();
        return IntStream.rangeClosed(1, vector1.getLength()).map(i -> vector1.getValue(i) * vector2.getValue(i)).sum();
    }

    /**
     * Computes the sum of products of all integer triples from three input vectors of type {@code IntVector}. An
     * exception is thrown for input vectors of different length.
     *
     * @param vector1 The first vector of integer values
     * @param vector2 The second vector of integer values
     * @param vector3 The third vector of integer values
     * @return The sum of products all integer pairs
     * @throws IndexOutOfBoundsException if the vectors are not equally long
     */
    public static int intSumProd(IntVector vector1, IntVector vector2, IntVector vector3) {
        if (vector1.getLength() != vector2.getLength() || vector1.getLength() != vector3.getLength())
            throw new IndexOutOfBoundsException();
        return IntStream.rangeClosed(1, vector1.getLength()).map(i -> vector1.getValue(i) * vector2.getValue(i) * vector3.getValue(i)).sum();
    }

    /**
     * Computes the maximum of all integer values from a given vector of type {@code IntVector}. The method
     * returns 0 for vectors of length 0. The behaviour of this method is unspecified for vectors containing {@code
     * null} values.
     *
     * @param vector The given vector of integer values
     * @return The maximum all integer values or 0 if the input vector is empty
     */
    public static int intMax(IntVector vector) {
        return IntStream.rangeClosed(1, vector.getLength()).map(vector::getValue).max().orElse(0);
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded up to the next integer, where {@code x>=1} and
     * {@code b>=2} are the given integer values. For invalid inputs, the return value is not specified.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded up to the next integer
     */
    public static int ceilLog(BigInteger x, int b) {
        int result = 0;
        BigInteger base = BigInteger.valueOf(b);
        BigInteger z = BigInteger.ONE;
        while (z.compareTo(x) < 0) {
            result++;
            z = z.multiply(base);
        }
        return result;
    }

    /**
     * Computes the logarithm of {@code x} to the base {@code b} rounded down to the next integer, where {@code x>=1}
     * and {@code b>=2} are the given integer values. For invalid inputs, the output is not specified.
     *
     * @param x The given integer
     * @param b The base
     * @return The logarithm of {@code x} to the base {@code b} rounded down to the next integer
     */
    public static int floorLog(BigInteger x, int b) {
        int result = 0;
        BigInteger base = BigInteger.valueOf(b);
        BigInteger z = base;
        while (z.compareTo(x) <= 0) {
            result++;
            z = z.multiply(base);
        }
        return result;
    }

    /**
     * Checks if {@code x>0} is an integer divisor of {@code y>=0}. For invalid inputs, the return value is not
     * specified.
     *
     * @param x The first integer
     * @param y The second integer
     * @return {@code true}, if {@code x} is an integer divisor of {@code y}, {@code false} otherwise
     */
    public static boolean divides(BigInteger x, BigInteger y) {
        return y.mod(x).equals(BigInteger.ZERO);
    }

    /**
     * Computes the sum of all integer values from a given vector of type {@code Vector<BigInteger>}. The behaviour of
     * this method is unspecified for vectors containing {@code null} values.
     *
     * @param vector The given vector of integer values
     * @return The sum all integer values
     */
    public static BigInteger sum(Vector<BigInteger> vector) {
        return vector.toStream().reduce(BigInteger.ZERO, BigInteger::add);
    }

    /**
     * Computes the product of all integer values from a given vector of type {@code Vector<BigInteger>}. The behaviour
     * of this method is unspecified for vectors containing {@code null} values.
     *
     * @param vector The given vector of integer values
     * @return The product all integer values
     */
    public static BigInteger prod(Vector<BigInteger> vector) {
        return vector.toStream().reduce(BigInteger.ONE, BigInteger::multiply);
    }

    /**
     * Computes the Jacobi symbol {@code J(x,n)} of a positive integer {@code x} and an odd positive integer {@code n}
     * satisfying {@code 1<=x<=n}. The method implements the algorithm as described in Eric Bach and Jeffrey Shallit's
     * book on "Algorithmic Number Theory". The behaviour of this method and its return value are unspecified for
     * invalid input values.
     * <p>
     * Reference: Eric Bach and Jeffrey Shallit, "Algorithmic Number Theory", Vol.1: Efficient Algorithms", p.113
     *
     * @param x A positive integer
     * @param n An odd positive integer
     * @return The Jacobi symbol of {@code x} and {@code n}
     */
    public static int jacobiSymbol(BigInteger x, BigInteger n) {
        int result = 1;
        BigInteger z;
        while (x.signum() != 0) {
            while (!x.testBit(0)) {
                x = Math.div2(x);
                if (n.testBit(0) && n.testBit(1) == !n.testBit(2)) {
                    result = -result;
                }
            }
            z = x;
            x = n;
            n = z;
            if (x.testBit(0) && x.testBit(1) && n.testBit(0) && n.testBit(1)) {
                result = -result;
            }
            x = x.mod(n);
        }
        return n.equals(BigInteger.ONE) ? result : 0;
    }

    /**
     * Computes {@code x mod 256}. Returns the result as a value of type {@code int}.
     *
     * @param x The input value
     * @return {@code x} modulo 256
     */
    public static int mod256(BigInteger x) {
        return x.intValue() % 256;
    }

    /**
     * Computes {@code x/256}.
     *
     * @param x The input value
     * @return {@code x} divided by 256
     */
    public static BigInteger div256(BigInteger x) {
        return x.shiftRight(8);
    }

    /**
     * Computes {@code 256*x}.
     *
     * @param x The input value
     * @return {@code x} times 256
     */
    public static BigInteger mult256(BigInteger x) {
        return x.shiftLeft(8);
    }

    /**
     * Computes {@code x/2}.
     *
     * @param x The input value
     * @return {@code x} divided by 2
     */
    public static BigInteger div2(BigInteger x) {
        return x.shiftRight(1);
    }

    /**
     * Computes {@code 2*x}.
     *
     * @param x The input value
     * @return {@code x} times 2
     */
    public static BigInteger mult2(BigInteger x) {
        return x.shiftLeft(1);
    }

}

