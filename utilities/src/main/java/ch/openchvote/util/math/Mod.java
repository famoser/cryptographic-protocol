/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.math;

import ch.openchvote.util.Vector;

import java.math.BigInteger;
import java.util.Objects;
import java.util.stream.IntStream;

/**
 * This class provides static methods for modular arithmetic with objects of type {@link BigInteger} and {@link
 * QuadraticResidue}. One of the main goal of this class is to offer a consistent interface to all modular operations
 * and to bundle all calls to {@link BigInteger#modPow} in one single place.
 */
public class Mod {

    // PART 1: BigInteger

    /**
     * Computes the modular sum of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @param n The modulus
     * @return The modular sum {@code x + y mod n}
     */
    public static BigInteger add(BigInteger x, BigInteger y, BigInteger n) {
        return x.add(y).mod(n);
    }

    /**
     * Computes the modular difference of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @param n The modulus
     * @return The modular difference {@code x - y mod n}
     */
    public static BigInteger subtract(BigInteger x, BigInteger y, BigInteger n) {
        return x.subtract(y).mod(n);
    }

    /**
     * Computes the additive inverse of {@code x} modulo {@code n}.
     *
     * @param x The given integer
     * @param n The modulus
     * @return The additive inverse of {@code -x mod n}
     */
    public static BigInteger minus(BigInteger x, BigInteger n) {
        return n.subtract(x).mod(n);
    }

    /**
     * Computes the modular sum of a vector of integers. Returns 0 if the vector is empty.
     *
     * @param bold_x The vector of integers
     * @param n      The modulus
     * @return The modular sum of the given vector of integers
     */
    public static BigInteger sum(Vector<BigInteger> bold_x, BigInteger n) {
        return bold_x.toStream().reduce(BigInteger.ZERO, (x, y) -> Mod.add(x, y, n));
    }

    /**
     * Computes the modular product of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @param n The modulus
     * @return The modular product {@code x * y mod n}
     */
    public static BigInteger multiply(BigInteger x, BigInteger y, BigInteger n) {
        return x.multiply(y).mod(n);
    }

    /**
     * Computes {@code x} to the power of {@code y} modulo {@code n}.
     *
     * @param x The base
     * @param y The exponent
     * @param n The modulus
     * @return {@code x} to the power of {@code y} modulo {@code n}
     */
    public static BigInteger pow(BigInteger x, BigInteger y, BigInteger n) {
        if (VMGJFassade.isLoaded()) {
            return VMGJFassade.modPow(x, y, n);
        } else {
            return x.modPow(y, n);
        }
    }

    /**
     * Computes the multiplicative inverse of {@code x} modulo {@code n}.
     *
     * @param x The given integer
     * @param n The modulus
     * @return The multiplicative inverse of {@code x} modulo {@code n}
     * @throws ArithmeticException if {@code x} is not relatively prime to {@code n}
     */
    public static BigInteger invert(BigInteger x, BigInteger n) {
        return x.modInverse(n);
    }

    /**
     * Computes the modular division of two integers {@code x} and {@code y}.
     *
     * @param x The first integer
     * @param y The second integer
     * @param n The modulus
     * @return The modular division {@code x / y mod n}
     * @throws ArithmeticException if {@code x} is not relatively prime to {@code n}
     */
    public static BigInteger divide(BigInteger x, BigInteger y, BigInteger n) {
        return x.multiply(y.modInverse(n)).mod(n);
    }

    /**
     * Computes the modular sum of the pairwise products of the values from two equally long vectors. Returns 0 if
     * the vectors are empty.
     *
     * @param bold_x The first vector of integers
     * @param bold_y The second vector of integers
     * @param n      The modulus
     * @return The modular sum of products
     * @throws IndexOutOfBoundsException if the two vectors are not equally long
     */
    public static BigInteger sumProd(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, BigInteger n) {
        if (bold_x.getLength() != bold_y.getLength())
            throw new IndexOutOfBoundsException();
        return IntStream.rangeClosed(1, bold_x.getLength())
                .mapToObj(i -> Mod.multiply(bold_x.getValue(i), bold_y.getValue(i), n))
                .reduce(BigInteger.ZERO, (x, y) -> Mod.add(x, y, n));
    }

    /**
     * Computes the modular product of a vector of integers. Returns 1 if the vector is empty.
     *
     * @param bold_x The vector of integers
     * @param n      The modulus
     * @return The modular product of the given vector of integers
     */
    public static BigInteger prod(Vector<BigInteger> bold_x, BigInteger n) {
        return bold_x.toStream().reduce(BigInteger.ONE, (x, y) -> Mod.multiply(x, y, n));
    }

    /**
     * Computes the modular product of the modular powers obtained from combining the values of two equally long vectors
     * of bases and exponents. Returns 1 if the vectors are empty. Tries to delegate the computation to the VMGJ library.
     *
     * @param bold_x The first vector of integers
     * @param bold_y The second vector of integers
     * @param n      The modulus
     * @return The modular product of powers
     * @throws IndexOutOfBoundsException if the two vectors are not equally long
     */
    public static BigInteger prodPow(Vector<BigInteger> bold_x, Vector<BigInteger> bold_y, BigInteger n) {
        if (bold_x.getLength() != bold_y.getLength())
            throw new IndexOutOfBoundsException();
        if (VMGJFassade.isLoaded()) {
            return VMGJFassade.modProdPow(bold_x.toArray(BigInteger.class), bold_y.toArray(BigInteger.class), n);
        }
        return IntStream.rangeClosed(1, bold_x.getLength())
                .mapToObj(i -> Mod.pow(bold_x.getValue(i), bold_y.getValue(i), n))
                .reduce(BigInteger.ONE, (x, y) -> Mod.multiply(x, y, n));
    }

    // PART 2: QuadraticResidue

    /**
     * Computes the modular product of two quadratic residues {@code x} and {@code y}.
     *
     * @param x The first quadratic residue
     * @param y The second quadratic residue
     * @return The modular product {@code x * y mod n}
     * @throws IllegalArgumentException, if the moduli of the quadratic residues are not the same
     */
    public static QuadraticResidue multiply(QuadraticResidue x, QuadraticResidue y) {
        return x.modMultiply(y);
    }

    /**
     * Computes the modular product of three quadratic residues {@code x}, {@code y}, and {@code z}.
     *
     * @param x The first quadratic residue
     * @param y The second quadratic residue
     * @param z The third quadratic residue
     * @return The modular product {@code x * y * z mod n}
     * @throws IllegalArgumentException, if the moduli of the quadratic residues are not the same
     */
    public static QuadraticResidue multiply(QuadraticResidue x, QuadraticResidue y, QuadraticResidue z) {
        return x.modMultiply(y).modMultiply(z);
    }

    /**
     * Computes {@code x} to the power of {@code y} modulo {@code n}.
     *
     * @param x The base
     * @param y The exponent
     * @return {@code x} to the power of {@code y} modulo {@code n}
     */
    public static QuadraticResidue pow(QuadraticResidue x, BigInteger y) {
        return x.modPow(y);
    }

    /**
     * Computes the multiplicative inverse of {@code x} modulo {@code n}.
     *
     * @param x The given integer
     * @return The multiplicative inverse of {@code x} modulo {@code n}
     * @throws ArithmeticException if {@code x} is not relatively prime to {@code n}
     */
    public static QuadraticResidue invert(QuadraticResidue x) {
        return x.modInverse();
    }

    /**
     * Computes the modular division of two quadratic residues {@code x} and {@code y}.
     *
     * @param x The first quadratic residue
     * @param y The second quadratic residue
     * @return The modular division {@code x / y mod n}
     * @throws IllegalArgumentException, if the moduli of the quadratic residues are not the same
     */
    public static QuadraticResidue divide(QuadraticResidue x, QuadraticResidue y) {
        return x.modMultiply(y.modInverse());
    }

    /**
     * Computes the modular product of a vector of quadratic residues. Returns 1 if the vector is empty.
     *
     * @param bold_x The vector of quadratic residues
     * @return The modular product of the given vector of quadratic residues
     */
    public static QuadraticResidue prod(Vector<QuadraticResidue> bold_x) {
        return bold_x.toStream().reduce(QuadraticResidue.ONE, QuadraticResidue::modMultiply);
    }

    /**
     * Computes the modular product of the modular powers obtained from combining the values of two equally long
     * vectors of bases and exponents. Returns 1 if the vectors are empty. Tries to delegate the computation to the
     * VMGJ library.
     *
     * @param bold_x The first vector of integers
     * @param bold_y The second vector of integers
     * @return The modular product of powers
     * @throws IndexOutOfBoundsException if the two vectors are not equally long
     */
    public static QuadraticResidue prodPow(Vector<QuadraticResidue> bold_x, Vector<BigInteger> bold_y) {
        if (bold_x.getLength() == 0) {
            return QuadraticResidue.ONE;
        }
        BigInteger modulus = bold_x.toStream().map(QuadraticResidue::getModulus).filter(Objects::nonNull).findFirst().orElse(null);
        if (modulus == null) {
            return QuadraticResidue.ONE;
        }
        BigInteger sqrtValue = Mod.prodPow(bold_x.map(QuadraticResidue::getSqrtValue), bold_y, modulus);
        return new QuadraticResidue(null, sqrtValue, modulus);
    }

}
