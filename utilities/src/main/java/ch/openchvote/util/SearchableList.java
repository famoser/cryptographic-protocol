/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import ch.openchvote.util.tuples.Pair;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Implements an append-only list of (key,value)-pairs with unique keys. The type of the keys is fixed to {@code int},
 * whereas the type of the values is generic ({@code null} values are not permitted). Searching for keys and appending
 * new entries is implemented efficiently in constant time using an instance of {@link LinkedHashMap} as an internal
 * data structure, which also keeps track of the insertion order. By implementing the {@link Iterable} interface, the
 * list can be used in for-each loops.
 *
 * @param <V> The generic type of the values stored in the list
 */
public class SearchableList<V> implements Iterable<SearchableList.Entry<V>> {

    // the internal linked hashMap for storing the (key,value)-pairs
    private final LinkedHashMap<Integer, V> map;

    /**
     * Constructs a new empty searchable list.
     */
    public SearchableList() {
        this.map = new LinkedHashMap<>();
    }

    /**
     * Returns the current number of entries in the searchable list.
     *
     * @return The current number of entries
     */
    public int getLength() {
        return this.map.size();
    }

    /**
     * Appends a new (key,value)-pair to the searchable list. If the key already exists, the list remains unchanged. An
     * exception is thrown in attempts to add {@code null} to the list.
     *
     * @param key   The key to be added
     * @param value The value to be added
     * @throws IllegalArgumentException if the valued to be added is {@code null}
     */
    public void append(int key, V value) {
        if (value == null)
            throw new IllegalArgumentException();
        this.map.putIfAbsent(key, value);
    }

    /**
     * Checks if the searchable list contains an entry for a given key.
     *
     * @param key The given key
     * @return {@code true}, if the list contains {@code key}, {@code false} otherwise
     */
    public boolean contains(int key) {
        return this.map.containsKey(key);
    }

    /**
     * If the searchable list contains an entry for a given key, the corresponding value is returned. If no entry for
     * {@code key} exists in the list, the value {@code null} is returned.
     *
     * @param key The given key
     * @return The value associated with {@code key} in the list or {@code null}
     */
    public V search(int key) {
        return this.map.get(key);
    }

    /**
     * Returns a stream of all keys currently stored in the list. The stream of keys respects the insertion order of the
     * keys in the searchable list.
     *
     * @return The stream of all keys
     */
    public IntStream getKeys() {
        return this.map.keySet().stream().mapToInt(Integer::intValue);
    }

    /**
     * Returns a stream of all values currently stored in the list. The stream of values respects the insertion order of
     * the values in the searchable list.
     *
     * @return The stream of all values
     */
    public Stream<V> getValues() {
        return this.map.values().stream();
    }

    @Override
    public Iterator<Entry<V>> iterator() {
        var keyIterator = this.map.keySet().iterator();
        return new Iterator<>() {
            @Override
            public boolean hasNext() {
                return keyIterator.hasNext();
            }

            @Override
            public Entry<V> next() {
                var key = keyIterator.next();
                return new Entry<>(key, map.get(key));
            }
        };
    }

    /**
     * Helper class to store (key,value)-pairs during iterations.
     *
     * @param <V> The generic type of the value stored in an entry
     */
    public static class Entry<V> extends Pair<Integer, V> {

        /**
         * Public constructor for creating (key,value)-pairs.
         *
         * @param key The given key
         * @param value The given value
         */
        public Entry(Integer key, V value) {
            super(key, value);
        }

        /**
         * Returns the key of an entry.
         *
         * @return The key of an entry
         */
        public int getKey() {
            return this.getFirst();
        }

        /**
         * Returns the value of an entry.
         *
         * @return The value of an entry
         */
        public V getValue() {
            return this.getSecond();
        }
    }

}
