/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util.crypto;

import ch.openchvote.util.ByteArray;

import java.security.MessageDigest;

/**
 * This class provides an abstraction of the SHA3-256 hash algorithm. It consists of a methods for
 * computing hash values of arbitrary input messages, which are given as instances of the class {@link ByteArray}.
 * Internally, the existing Java security class {@link MessageDigest} is used to perform the actual hash value
 * computation.
 */
public class SHA3 implements HashAlgorithm {

    private static final int HASH_LENGTH = 32; // number of bytes

    // package-private constructor to allow singleton instantiation in HashAlgorithm
    public SHA3() {
    }

    @Override
    public int getLength() {
        return HASH_LENGTH;
    }

    @Override
    public ByteArray hash(ByteArray message) {
        try {
            byte[] bytes = MessageDigest.getInstance("SHA3-256").digest(message.toByteArray());
            return new ByteArray.FromSafeArray(bytes);
        } catch (Exception e) {
            throw new RuntimeException("Error during SHA3 setup", e);
        }
    }

}
