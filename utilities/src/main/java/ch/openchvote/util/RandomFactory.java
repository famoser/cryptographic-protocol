/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import java.util.Random;

/**
 * This class provides a factory for instances of the Java class {@link Random}. Various modes from the enum class
 * {@link RandomMode} are supported. Using {@link RandomMode#RANDOM}, regular {@link Random} objects are returned. Using
 * {@link RandomMode#DETERMINISTIC}, the returned {@link Random} objects are seeded wit a series of deterministic
 * values starting from 1. Using {@link RandomMode#IDENTICAL}, the returned {@link Random} objects are all seeded with
 * the same value {@link RandomFactory#DEFAULT_SEED}. Using {@link RandomMode#IDENTICAL_MIN} or
 * {@link RandomMode#IDENTICAL_MAX}, the returned {@link Random} will always return the the smallest respectively the
 * largest value from the range of possible values.
 */
public class RandomFactory {

    private static final long DEFAULT_SEED = 0;
    private static long currentSeed = 0;

    /**
     * Return a new instance of the Java class {@link Random}. Its behaviour is determined by the specified mode of the
     * enum class {@link RandomMode}.
     *
     * @param mode The mode of the returned {@link Random} object
     * @return The constructed {@link Random} object
     */
    public static Random getInstance(RandomMode mode) {
        switch (mode) {
            case RANDOM:
                return new Random();
            case DETERMINISTIC:
                currentSeed++;
                return new Random(currentSeed);
            case IDENTICAL:
                return new Random(DEFAULT_SEED);
            case IDENTICAL_MIN:
                return new Random() {
                    @Override
                    public double nextDouble() {
                        return 0.0;
                    }
                    @Override
                    public int nextInt(int bound) {
                        if (bound <= 0)
                            throw new IllegalArgumentException("Bound must be positive");
                        return 0;
                    }
                    // other methods derived from Random are not needed
                };
            case IDENTICAL_MAX:
                return new Random() {
                    @Override
                    public double nextDouble() {
                        return 1.0;
                    }
                    @Override
                    public int nextInt(int bound) {
                        if (bound <= 0)
                            throw new IllegalArgumentException("Bound must be positive");
                        return bound - 1;
                    }
                    // other methods derived from Random are not needed
                };
            default: throw new IllegalArgumentException();
        }
    }

}
