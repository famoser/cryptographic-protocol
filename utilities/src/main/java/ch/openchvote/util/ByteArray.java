/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.util;

import ch.openchvote.util.math.Math;

import java.util.Iterator;
import java.util.function.BinaryOperator;
import java.util.stream.IntStream;

/**
 * Objects of this class represent immutable byte arrays of a fixed length n. Their elements are indexed from 0 to n-1.
 * The class provides methods for various operations on such byte arrays (skipping or truncating elements, bit-wise
 * logical operations, concatenation, etc.). All these methods are implemented to run in constant time. Since this class
 * is abstract, it does not offer public constructors. New instances of this class are created using the local {@link
 * Builder} class. During the building process, the bytes can be added to or placed into an initially empty byte array
 * consisting of 0s. After the building process, accessing the bytes in the resulting byte array is restricted to
 * read-only. This class implements the {@link Iterable} interface.
 */
public abstract class ByteArray implements Iterable<Byte> {

    /**
     * The empty byte array of length 0.
     */
    public static final ByteArray EMPTY = new ByteArray.Builder(0).build();

    /**
     * Returns the length of the byte array.
     *
     * @return The length of the byte array
     */
    public abstract int getLength();

    /**
     * Returns the byte at the given index. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The byte at this index
     * @throws IndexOutOfBoundsException if the index is not valid
     */
    public abstract byte getByte(int index);

    /**
     * Returns the byte at the given index as an {@code int} value. For an invalid index, an exception is thrown.
     *
     * @param index The index
     * @return The byte at this index cast to {@code int}
     * @throws IndexOutOfBoundsException if the index is not valid
     */
    public int getInteger(int index) {
        return Math.byteToInt(this.getByte(index));
    }

    /**
     * Creates and returns a new byte array by skipping the first {@code k<=n} bytes of the given byte array of length
     * {@code n}. The length of the returned byte array is {@code n-k}. An exception is thrown for an invalid {@code
     * k}.
     *
     * @param k The number of bytes to skip
     * @return The byte array obtained from skipping the first {@code k} bytes
     * @throws IllegalArgumentException if {@code k} is not valid
     */
    public ByteArray skip(int k) {
        if (k < 0 || k > this.getLength())
            throw new IllegalArgumentException();
        if (k == 0)
            return this;
        ByteArray byteArray = this;
        return new ByteArray() {

            @Override
            public int getLength() {
                return byteArray.getLength() - k;
            }

            @Override
            public byte getByte(int index) {
                if (index < 0 || index >= this.getLength())
                    throw new IllegalArgumentException();
                return byteArray.getByte(k + index);
            }

        };
    }

    /**
     * Creates and returns a new byte array, which contains the {@code k} first bytes of the given byte array. An
     * exception is thrown for an invalid {@code k}.
     *
     * @param k The length of the returned byte array
     * @return The byte array obtained from keeping the first {@code k} bytes
     * @throws IllegalArgumentException if {@code k} is not valid
     */
    public ByteArray truncate(int k) {
        if (k < 0 || k > this.getLength())
            throw new IllegalArgumentException();
        if (k == this.getLength())
            return this;
        ByteArray byteArray = this;
        return new ByteArray() {

            @Override
            public int getLength() {
                return k;
            }

            @Override
            public byte getByte(int index) {
                if (index < 0 || index >= this.getLength())
                    throw new IllegalArgumentException();
                return byteArray.getByte(index);
            }

        };
    }

    /**
     * Returns the concatenation of the given byte array with another byte array.
     *
     * @param byteArray2 The other byte array
     * @return The concatenated byte array
     */
    public ByteArray concatenate(ByteArray byteArray2) {
        ByteArray byteArray1 = this;
        if (byteArray1.getLength() == 0)
            return byteArray2;
        if (byteArray2.getLength() == 0)
            return byteArray1;
        return new ByteArray() {
            private final int length = byteArray1.getLength() + byteArray2.getLength();

            @Override
            public int getLength() {
                return length;
            }

            @Override
            public byte getByte(int index) {
                if (index < byteArray1.getLength())
                    return byteArray1.getByte(index);
                else
                    return byteArray2.getByte(index - byteArray1.getLength());
            }

            @Override
            protected void toByteArray(byte[] bytes, int offset) {
                byteArray1.toByteArray(bytes, offset);
                byteArray2.toByteArray(bytes, offset + byteArray1.getLength());
            }
        };
    }

    /**
     * Returns the byte array obtained from applying the logical AND-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the AND-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public ByteArray and(ByteArray other) {
        return applyBitWise(this, other, Math::and);
    }

    /**
     * Returns the byte array obtained from applying the logical OR-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the OR-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public ByteArray or(ByteArray other) {
        return applyBitWise(this, other, Math::or);
    }

    /**
     * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the two byte
     * arrays. An exception is thrown if their lengths are incompatible.
     *
     * @param other The other byte array
     * @return The result from applying the XOR-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public ByteArray xor(ByteArray other) {
        return applyBitWise(this, other, Math::xor);
    }

    // private helper method for applying a binary operator bit-wise.
    private static ByteArray applyBitWise(ByteArray byteArray1, ByteArray byteArray2, BinaryOperator<Byte> operator) {
        if (byteArray1.getLength() != byteArray2.getLength())
            throw new IllegalArgumentException();
        return new ByteArray() {

            @Override
            public int getLength() {
                return byteArray1.getLength();
            }

            @Override
            public byte getByte(int index) {
                return operator.apply(byteArray1.getByte(index), byteArray2.getByte(index));
            }

        };
    }

    /**
     * Computes the concatenation of multiple byte arrays in the given order. The byte arrays are given as a vector.
     *
     * @param byteArrays The vector of byteArrays
     * @return The concatenation of the given byte arrays
     */
    public static ByteArray concatenate(Vector<ByteArray> byteArrays) {
        int length = byteArrays.toStream().mapToInt(ByteArray::getLength).sum();
        return new ByteArray() {

            @Override
            public int getLength() {
                return length;
            }

            @Override
            public byte getByte(int index) {
                for (int i = byteArrays.getMinIndex(); i <= byteArrays.getMaxIndex(); i++) {
                    var byteArray = byteArrays.getValue(i);
                    if (index < byteArray.getLength()) {
                        return byteArray.getByte(index);
                    }
                    index = index - byteArray.getLength();
                }
                throw new IndexOutOfBoundsException();
            }

            @Override
            public void toByteArray(byte[] bytes, int offset) {
                for (var byteArray : byteArrays) {
                    byteArray.toByteArray(bytes, offset);
                    offset = offset + byteArray.getLength();
                }
            }
        };
    }

    /**
     * Returns the byte array obtained from applying the logical AND-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible. If the
     * given vector is empty, a byte array of length 0 is returned.
     *
     * @param byteArrays The vector of byteArrays
     * @return The result from applying the AND-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public static ByteArray and(Vector<ByteArray> byteArrays) {
        return byteArrays.toStream().reduce(ByteArray::and).orElse(ByteArray.EMPTY);
    }

    /**
     * Returns the byte array obtained from applying the logical OR-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible. If the
     * given vector is empty, a byte array of length 0 is returned.
     *
     * @param byteArrays The vector of byteArrays
     * @return The result from applying the AND-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public static ByteArray or(Vector<ByteArray> byteArrays) {
        return byteArrays.toStream().reduce(ByteArray::or).orElse(ByteArray.EMPTY);
    }

    /**
     * Returns the byte array obtained from applying the logical XOR-operator bit-wise to all bytes of the given byte
     * arrays. The byte arrays are given as a vector. An exception is thrown if their lengths are incompatible. If the
     * given vector is empty, a byte array of length 0 is returned.
     *
     * @param byteArrays The vector of byteArrays
     * @return The result from applying the AND-operator
     * @throws IllegalArgumentException if the byte arrays are not of equal length
     */
    public static ByteArray xor(Vector<ByteArray> byteArrays) {
        return byteArrays.toStream().reduce(ByteArray::xor).orElse(ByteArray.EMPTY);
    }

    /**
     * Creates and returns a new byte array with the same bytes, except for the byte at the specified index, which
     * receives a new value. An exception is thrown if the specified index is invalid. The new byte is given as an
     * {@code int} value.
     *
     * @param byteIndex The index of the changed byte
     * @param x         The new byte given as an {@code int} value
     * @return The new byte array with the byte changed at the specified index
     * @throws IndexOutOfBoundsException if the index is not valid
     */
    public ByteArray setByte(int byteIndex, int x) {
        return setByte(byteIndex, Math.intToByte(x));
    }

    /**
     * Creates and returns a new byte array with the same bytes, except for the byte at the specified index, which
     * receives a new value. An exception is thrown if the specified index is invalid. The new byte is given as an
     * {@code int} value.
     *
     * @param byteIndex The index of the changed byte
     * @param b         The new byte
     * @return The new byte array with the byte changed at the specified index
     * @throws IndexOutOfBoundsException if the index is not valid
     */
    public ByteArray setByte(int byteIndex, byte b) {
        if (byteIndex < 0 || byteIndex >= this.getLength())
            throw new IndexOutOfBoundsException();
        if (b == this.getByte(byteIndex))
            return this;
        ByteArray byteArray = this;
        return new ByteArray() {
            @Override
            public int getLength() {
                return byteArray.getLength();
            }

            @Override
            public byte getByte(int index) {
                return index == byteIndex ? b : byteArray.getByte(index);
            }

            @Override
            protected void toByteArray(byte[] bytes, int offset) {
                byteArray.toByteArray(bytes, offset);
                bytes[offset + byteIndex] = b;
            }
        };
    }

    /**
     * Returns a Java {@code byte[]} containing the bytes of the byte array in the same order.
     *
     * @return The resulting instance of {@code byte[]}
     */
    public byte[] toByteArray() {
        int length = this.getLength();
        byte[] bytes = new byte[length];
        this.toByteArray(bytes, 0);
        return bytes;
    }

    /**
     * Returns a Java {@code IntStream} containing the bytes of the byte array as {@code int} values.
     *
     * @return The resulting instance of {@code IntStream}
     */
    public IntStream toStream() {
        return this.getIndices().map(this::getInteger);
    }

    // private helper method for creating an IntStream of all indices
    private IntStream getIndices() {
        return IntStream.rangeClosed(0, this.getLength() - 1);
    }

    // protected helper method for transforming the byte array in a byte[]
    protected void toByteArray(byte[] bytes, int offset) {
        this.getIndices().forEach(index -> bytes[offset + index] = this.getByte(index));
    }

    @Override
    public Iterator<Byte> iterator() {
        ByteArray byteArray = this;
        return new Iterator<>() {

            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return this.currentIndex < byteArray.getLength();
            }

            @Override
            public Byte next() {
                return byteArray.getByte(this.currentIndex++);
            }

        };
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof ByteArray)) return false;
        ByteArray other = (ByteArray) object;
        if (this.getLength() != other.getLength()) return false;
        return this.getIndices().allMatch(index -> this.getByte(index) == other.getByte(index));
    }

    @Override
    public int hashCode() {
        int initValue = this.getLength();
        return this.toStream().reduce(initValue, (result, value) -> result + 31 * value);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Byte b : this) {
            stringBuilder.append(String.format("%02x", Math.byteToInt(b)).toUpperCase());
        }
        return stringBuilder.toString();
    }

    /**
     * This local class allows building new byte arrays of a given length. Initially, all bytes of the byte array are
     * set to 0. The bytes can then be set either in arbitrary order (using {@link Builder#setByte} or in ascending
     * order (using {@link Builder#addByte}). When the building process is over, the construction of the byte array can
     * be finalized (using {@link Builder#build}). While the resulting byte array offers read-only access, the builder
     * offers write-only access. The builder itself is threadsafe.
     */
    public static class Builder {

        // flag to indicate the end of the building process
        private boolean built;
        // array of bytes used during building process
        private final byte[] bytes;
        // an internal index counter for adding bytes incrementally
        private int indexCounter;

        /**
         * Constructs a new builder for a byte array of a given length. Returns an exception if the length is negative.
         *
         * @param length The length of the byte array
         * @throws IllegalArgumentException if the length is negative
         */
        public Builder(int length) {
            if (length < 0)
                throw new IllegalArgumentException();
            this.built = false;
            this.bytes = new byte[length];
            this.indexCounter = 0;
        }

        /**
         * Sets the byte at the specified index to {@code x}. This is a convenience method for setting bytes in the
         * byte array in form of {@code int} values. For an invalid value {@code x} or an invalid index, an exception
         * is thrown. The builder object is returned to enable pipeline notation.
         *
         * @param index The index
         * @param x     The byte (given as {@code int}) to be stored at the index
         * @return The builder object
         * @throws IndexOutOfBoundsException if the index is not valid
         */
        public Builder setByte(int index, int x) {
            return this.setByte(index, Math.intToByte(x));
        }

        /**
         * Sets the byte at the specified index to {@code b}. For an invalid index, an exception is thrown. The builder
         * object is returned to enable pipeline notation.
         *
         * @param index The index
         * @param b     The byte to be stored at the index
         * @return The builder object
         * @throws IndexOutOfBoundsException if the index is not valid
         */
        public Builder setByte(int index, byte b) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                if (index < 0 || index >= this.bytes.length)
                    throw new IndexOutOfBoundsException();
                bytes[index] = b;
                return this;
            }
        }

        /**
         * Sets the next byte in the byte array to {@code x} and increases the internal index counter by 1. This is
         * a convenience method for adding bytes to the byte array in form of {@code int} values. The byte array builder
         * object itself is returned to allow pipeline notation when multiple bytes are added to the byte array.
         *
         * @param x The int to be added
         * @return The byte array builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder addByte(int x) {
            return this.addByte(Math.intToByte(x));
        }

        /**
         * Sets the next byte in the byte array to {@code b} and increases the internal index counter by 1. The byte array builder
         * object itself is returned to allow pipeline notation when multiple bytes are added to the byte array.
         *
         * @param b The byte to be added
         * @return The byte array builder itself
         * @throws IllegalStateException     if the vector has already been build
         * @throws IndexOutOfBoundsException if the maximal index had been reached before
         */
        public Builder addByte(byte b) {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                int currentIndex;
                currentIndex = this.indexCounter;
                this.indexCounter = this.indexCounter + 1;
                return this.setByte(currentIndex, b);
            }
        }

        /**
         * Terminates the building process and returns the constructed byte array.
         *
         * @return The constructed byte array
         * @throws IllegalStateException if the byte array has already been build
         */
        public ByteArray build() {
            synchronized (this) {
                if (this.built)
                    throw new IllegalStateException();
                this.built = true;
                return new ByteArray.FromSafeArray(this.bytes);
            }
        }

    }

    /**
     * This local class allows constructing new byte arrays from a given {@code byte[]}, which is safe to use without
     * copying its values. Therefore, this constructor must always be used with maximal care.
     */
    public static class FromSafeArray extends ByteArray {

        // private instance variable for string the bytes
        private final byte[] bytes;

        /**
         * General constructor for creating byte arrays from a given {@code byte[]}.
         *
         * @param bytes The given (array of) bytes
         * @throws IllegalArgumentException if the given array is {@code null}
         */
        public FromSafeArray(byte... bytes) {
            if (bytes == null)
                throw new IllegalArgumentException();
            this.bytes = bytes;
        }

        @Override
        public int getLength() {
            return this.bytes.length;
        }

        @Override
        public byte getByte(int index) {
            if (index < 0 || index >= bytes.length)
                throw new IndexOutOfBoundsException();
            return bytes[index];
        }

        @Override
        protected void toByteArray(byte[] bytes, int offset) {
            System.arraycopy(this.bytes, 0, bytes, offset, this.bytes.length);
        }

    }

}
