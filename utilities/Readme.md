# Utilities

This module provides a collection of utility classes, which implement various mathematical concepts such as vectors, matrices, tuples, or modular arithmetic. It also offers convenient wrapper classes for some cryptographic primitives.

The goal of these utility classes is to increase the convenience of programming mathematical and cryptographic algoritms in Java. Many of the available classes are generic and can be nested, which offers strong type saftey even for complex mathematical types like:
```java
Triple<BigInteger, Vector<String>, Pair<Integer, ByteArray>> aTriple
```
Providing these classes have been crucial for implementing the CHVote pseudo-code algorithms as closely as possible to their description in the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325) document.

## Dependencies

As shown in the following UML component diagram, this module is independent of any other module of the OpenCHVote project. However, there is an optional dependency to the [Verificatum Multiplicative Groups Library for Java (VMGJ)](https://github.com/verificatum/verificatum-vmgj), for which [this license](LICENSE_VMGLJ.md) applies. At runtime, a wrapper tries to resolve the dependency to VMGJ. No harm occurs if the dependency can not be resolved. A warning is displayed in that case. Without VMGJ, some of the computations are less efficient. 

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

This module can be built as follows from its root:

````shell script
$ mvn clean install
-- a lot of output
````
