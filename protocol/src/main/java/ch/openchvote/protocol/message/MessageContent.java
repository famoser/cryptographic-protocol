/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message;

import ch.openchvote.framework.Message;
import ch.openchvote.util.Serializer;

/**
 * This sub-interface implements two methods from the super-interface {@link Message.Content} as default methods. The
 * reason for not providing an abstract class is the lack of multi-inheritance for Java classes (all classes implementing
 * this interface already inherit from another class).
 *
 * @param <T> The generic type parameter defining the actual message content class
 */
public interface MessageContent<T extends MessageContent<T>> extends Message.Content {

    /**
     * Provides a serializer for objects of the generic type {@code <T>}.
     *
     * @return A serializer for objects of the generic type {@code <T>}
     */
    default Serializer<T> getSerializer() {
        return new Serializer<>() {};
    }

    @Override
    @SuppressWarnings("unchecked")
    default String serialize() {
        return this.getSerializer().serialize((T) this);
    }

    @Override
    default MessageType getType() {
        return MessageType.valueOf(this.getClass().getSimpleName());
    }

    @SuppressWarnings("unchecked")
    class Factory implements Message.ContentFactory {

        @Override
        public <T extends Message.Content> T create(Class<T> clazz, String serializedContent) {
            try {
                // every MessageContent class is expected to possess a final static variable SERIALIZER
                var serializer = (Serializer<T>) clazz.getDeclaredField("SERIALIZER").get(null);
                return serializer.deserialize(serializedContent);
            } catch (ReflectiveOperationException exception) {
                throw new RuntimeException(exception);
            }
        }

    }

}
