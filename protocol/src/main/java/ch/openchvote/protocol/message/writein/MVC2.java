/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Triple;

public class MVC2 extends Triple<String, IntVector, Vector<WriteIn>> implements MessageContent<MVC2> {

    public static final Serializer<MVC2> SERIALIZER = new Serializer<>() {};

    public MVC2(String X, IntVector bold_s, Vector<WriteIn> bold_s_prime) {
        super(X, bold_s, bold_s_prime);
    }

    public String get_X() {
        return getFirst();
    }

    public IntVector get_bold_s() {
        return getSecond();
    }

    public Vector<WriteIn> get_bold_s_prime() {
        return getThird();
    }

}
