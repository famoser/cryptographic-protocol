/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.plain;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.tuples.Singleton;

public class MVC1 extends Singleton<Integer> implements MessageContent<MVC1> {

    public static final Serializer<MVC1> SERIALIZER = new Serializer<>() {};

    public MVC1(Integer v) {
        super(v);
    }

    public Integer get_v() {
        return getFirst();
    }
}
