/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message;

import ch.openchvote.protocol.PartyType;
import ch.openchvote.framework.Message;

import static ch.openchvote.protocol.PartyType.*;

/**
 * This enum class defines a {@link Message.Type} object for every message of the CHVote protocol.
 */
public enum MessageType implements Message.Type {

    // from administrator (all messages are signed)
    MAE1(ADMINISTRATOR, ELECTION_AUTHORITY, true, false),
    MAP1(ADMINISTRATOR, PRINTING_AUTHORITY, true, false),
    MAC1(ADMINISTRATOR, VOTING_CLIENT, true, false),
    MAT1(ADMINISTRATOR, THE_PUBLIC, true, false),
    MAX1(ADMINISTRATOR, COORDINATOR, true, false),

    // from election authority (all messages are signed)
    MEP1(ELECTION_AUTHORITY, PRINTING_AUTHORITY, true, true), // the only encrypted message
    MEA1(ELECTION_AUTHORITY, ADMINISTRATOR, true, false),
    MEC1(ELECTION_AUTHORITY, VOTING_CLIENT, true, false),
    MEC2(ELECTION_AUTHORITY, VOTING_CLIENT, true, false),
    MEC3(ELECTION_AUTHORITY, VOTING_CLIENT, true, false),
    MEC4(ELECTION_AUTHORITY, VOTING_CLIENT, true, false),
    MEE1(ELECTION_AUTHORITY, ELECTION_AUTHORITY, true, false),
    MEE2(ELECTION_AUTHORITY, ELECTION_AUTHORITY, true, false),
    MEE3(ELECTION_AUTHORITY, ELECTION_AUTHORITY, true, false),
    MEE4(ELECTION_AUTHORITY, ELECTION_AUTHORITY, true, false),
    MEX1(ELECTION_AUTHORITY, COORDINATOR, true, false),
    MEX2(ELECTION_AUTHORITY, COORDINATOR, true, false),

    // from printing authority
    MPV1(PRINTING_AUTHORITY, VOTER, false, false),

    // from voting client
    MCA1(VOTING_CLIENT, ADMINISTRATOR, false, false),
    MCE1(VOTING_CLIENT, ELECTION_AUTHORITY, false, false),
    MCE2(VOTING_CLIENT, ELECTION_AUTHORITY, false, false),
    MCE3(VOTING_CLIENT, ELECTION_AUTHORITY, false, false),
    MCE4(VOTING_CLIENT, ELECTION_AUTHORITY, false, false),
    MCV1(VOTING_CLIENT, VOTER, false, false),
    MCV2(VOTING_CLIENT, VOTER, false, false),
    MCV3(VOTING_CLIENT, VOTER, false, false),
    MCV4(VOTING_CLIENT, VOTER, false, false),

    // from voter
    MVC1(VOTER, VOTING_CLIENT, false, false),
    MVC2(VOTER, VOTING_CLIENT, false, false),
    MVC3(VOTER, VOTING_CLIENT, false, false),
    MVC4(VOTER, VOTING_CLIENT, false, false),
    MVC5(VOTER, VOTING_CLIENT, false, false),
    MVX1(VOTER, COORDINATOR, false, false),
    MVX2(VOTER, COORDINATOR, false, false),
    MVX3(VOTER, COORDINATOR, false, false),

    // from coordinator (all messages are signed)
    MXA1(COORDINATOR, ADMINISTRATOR, true, false),
    MXA2(COORDINATOR, ADMINISTRATOR, true, false),
    MXA3(COORDINATOR, ADMINISTRATOR, true, false),
    MXE1(COORDINATOR, ELECTION_AUTHORITY, true, false),
    MXE2(COORDINATOR, ELECTION_AUTHORITY, true, false),
    MXE3(COORDINATOR, ELECTION_AUTHORITY, true, false),
    MXV1(COORDINATOR, VOTER, true, false),
    MXV2(COORDINATOR, VOTER, true, false);

    private final PartyType senderType;
    private final PartyType receiverType;
    private final boolean isSigned;
    private final boolean isEncrypted;

    // private constructor not available from the outside
    MessageType(PartyType senderType, PartyType receiverType, boolean isSigned, boolean isEncrypted) {
        this.senderType = senderType;
        this.receiverType = receiverType;
        this.isSigned = isSigned;
        this.isEncrypted = isEncrypted;
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public PartyType getSenderType() {
        return this.senderType;
    }

    @Override
    public PartyType getReceiverType() {
        return this.receiverType;
    }

    @Override
    public boolean isSigned() {
        return this.isSigned;
    }

    @Override
    public boolean isEncrypted() {
        return this.isEncrypted;
    }

    /**
     * This factory class can be used to select the right {@link MessageType} for a given type name. The selection is
     * based on the names of the enum objects.
     */
    public static class Factory implements Message.TypeFactory {

        @Override
        public MessageType create(String typeName) {
            return MessageType.valueOf(typeName);
        }

    }

}
