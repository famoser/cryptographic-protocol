/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.model.writein.KeyPairProof;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

public class MEC1 extends Triple<QuadraticResidue, Vector<QuadraticResidue>, KeyPairProof> implements MessageContent<MEC1> {

    public static final Serializer<MEC1> SERIALIZER = new Serializer<>() {};

    public MEC1(QuadraticResidue pk, Vector<QuadraticResidue> bold_pk_prime, KeyPairProof pi) {
        super(pk, bold_pk_prime, pi);
    }

    public QuadraticResidue get_pk() {
        return getFirst();
    }

    public Vector<QuadraticResidue> get_bold_pk_prime() {
        return getSecond();
    }

    public KeyPairProof get_pi() {
        return getThird();
    }

}
