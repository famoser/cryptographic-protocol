/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.model.common.CredentialProof;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Quadruple;

import java.math.BigInteger;

public class MEE2 extends Quadruple<Vector<BigInteger>, Vector<BigInteger>, Vector<BigInteger>, CredentialProof> implements MessageContent<MEE2> {

    public static final Serializer<MEE2> SERIALIZER = new Serializer<>() {};

    public MEE2(Vector<BigInteger> bold_x_hat, Vector<BigInteger> bold_y_hat, Vector<BigInteger> bold_z_hat, CredentialProof pi_hat) {
        super(bold_x_hat, bold_y_hat, bold_z_hat, pi_hat);
    }

    public Vector<BigInteger> get_bold_x_hat() {
        return this.getFirst();
    }

    public Vector<BigInteger> get_bold_y_hat() {
        return this.getSecond();
    }

    public Vector<BigInteger> get_bold_z_hat() {
        return this.getThird();
    }

    public CredentialProof get_pi_hat() {
        return this.getFourth();
    }

}
