/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.model.writein.AugmentedEncryption;
import ch.openchvote.model.writein.ShuffleProof;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Pair;

public class MEE3 extends Pair<Vector<AugmentedEncryption>, ShuffleProof> implements MessageContent<MEE3> {

    public static final Serializer<MEE3> SERIALIZER = new Serializer<>() {};

    public MEE3(Vector<AugmentedEncryption> bold_e_tilde, ShuffleProof pi_tilde) {
        super(bold_e_tilde, pi_tilde);
    }

    public Vector<AugmentedEncryption> get_bold_e_tilde() {
        return getFirst();
    }

    public ShuffleProof get_pi_tilde() {
        return getSecond();
    }

}
