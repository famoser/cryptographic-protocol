/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.model.writein.ElectionParameters;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.tuples.Singleton;

public class MAP1 extends Singleton<ElectionParameters> implements MessageContent<MAP1> {

    public static final Serializer<MAP1> SERIALIZER = new Serializer<>() {};

    public MAP1(ElectionParameters EP) {
        super(EP);
    }

    public ElectionParameters get_EP() {
        return this.getFirst();
    }

}
