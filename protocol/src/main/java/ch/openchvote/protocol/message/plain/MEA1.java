/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.plain;

import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

public class MEA1 extends Pair<Vector<QuadraticResidue>, Vector<Encryption>> implements MessageContent<MEA1> {

    public static final Serializer<MEA1> SERIALIZER = new Serializer<>() {};

    public MEA1(Vector<QuadraticResidue> bold_c, Vector<Encryption> bold_e_tilde_s) {
        super(bold_c, bold_e_tilde_s);
    }

    public Vector<QuadraticResidue> get_bold_c() {
        return getFirst();
    }

    public Vector<Encryption> get_bold_e_tilde_s() {
        return getSecond();
    }

}
