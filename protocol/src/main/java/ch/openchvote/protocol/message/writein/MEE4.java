/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.message.writein;

import ch.openchvote.model.writein.DecryptionProof;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Triple;

public class MEE4 extends Triple<Vector<QuadraticResidue>, Matrix<QuadraticResidue>, DecryptionProof> implements MessageContent<MEE4> {

    public static final Serializer<MEE4> SERIALIZER = new Serializer<>() {};

    public MEE4(Vector<QuadraticResidue> bold_c, Matrix<QuadraticResidue> bold_D, DecryptionProof pi_prime) {
        super(bold_c, bold_D, pi_prime);
    }

    public Vector<QuadraticResidue> get_bold_c() {
        return getFirst();
    }

    public Matrix<QuadraticResidue> get_bold_D() {
        return getSecond();
    }

    public DecryptionProof get_pi_prime() {
        return getThird();
    }

}
