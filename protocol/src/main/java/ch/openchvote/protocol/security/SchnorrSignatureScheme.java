/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.security;

import ch.openchvote.algorithms.general.CheckSignature;
import ch.openchvote.algorithms.general.GenSignature;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.security.SignatureScheme;
import ch.openchvote.model.general.Signature;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Serializer;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

/**
 * This class implements the Schnorr signature scheme. The group parameters are selected from the {@link Parameters}
 * class. It uses the signature algorithm {@link GenSignature}.
 */
public class SchnorrSignatureScheme implements SignatureScheme {

    // used for serializing the resulting signatures
    private static final Serializer<Signature> SIGNATURE_SERIALIZER = new Serializer<>() {};

    // used for serializing private and public keys
    private static final Serializer<BigInteger> KEY_SERIALIZER = new Serializer<>() {};

    @Override
    public String sign(Message.Content messageContent, String messageType, Object aux, String privateKey, int securityLevel) {
        var params = new Parameters(securityLevel);
        var sk = KEY_SERIALIZER.deserialize(privateKey);
        var aux_prime = (aux == null) ? messageType : new Pair<>(messageType, aux);
        var sigma = GenSignature.run(sk, messageContent, aux_prime, params);
        var signature = SIGNATURE_SERIALIZER.serialize(sigma);
        return signature;
    }

    @Override
    public boolean verify(String signature, Message.Content messageContent, String messageType, Object aux, String publicKey, int securityLevel) {
        var params = new Parameters(securityLevel);
        Signature sigma = SIGNATURE_SERIALIZER.deserialize(signature);
        var pk = KEY_SERIALIZER.deserialize(publicKey);
        var aux_prime = (aux == null) ? messageType : new Pair<>(messageType, aux);
        return CheckSignature.run(sigma, pk, messageContent, aux_prime, params);
    }

}
