/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.security;

import ch.openchvote.algorithms.general.GenSchnorrKeyPair;
import ch.openchvote.framework.security.Certificate;
import ch.openchvote.framework.security.KeyGenerator;
import ch.openchvote.framework.services.Keystore;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Serializer;

import java.math.BigInteger;
import java.util.Set;

/**
 * This class implements a {@link KeyGenerator} for generating key pairs of a given Schnorr group. The group parameters
 * are selected from the {@link Parameters} class. It uses the key generation algorithm {@link GenSchnorrKeyPair}.
 */
public class SchnorrKeyGenerator implements KeyGenerator {

    // used for serializing the resulting private and public keys
    private static final Serializer<BigInteger> KEY_SERIALIZER = new Serializer<>() {};

    @Override
    public Set<Integer> getSecurityLevels() {
        return Parameters.SECURITY_LEVELS;
    }

    @Override
    public Keystore.Entry generateKeystoreEntry(String subject, int securityLevel) {
        if (!this.getSecurityLevels().contains(securityLevel) || subject == null)
            throw new IllegalArgumentException();
        var params = new Parameters(securityLevel);
        var pair = GenSchnorrKeyPair.run(params);
        var privateKey = KEY_SERIALIZER.serialize(pair.getFirst());
        var publicKey = KEY_SERIALIZER.serialize(pair.getSecond());
        var certificate = new Certificate() {

            @Override
            public String getSubject() {
                return subject;
            }

            @Override
            public String getPublicKey() {
                return publicKey;
            }

            @Override
            public int getSecurityLevel() {
                return securityLevel;
            }
        };
        return new Keystore.Entry() {

            @Override
            public String getPrivateKey() {
                return privateKey;
            }

            @Override
            public Certificate getCertificate() {
                return certificate;
            }
        };
    }

}
