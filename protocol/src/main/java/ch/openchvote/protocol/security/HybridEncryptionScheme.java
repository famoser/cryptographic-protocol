/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol.security;

import ch.openchvote.algorithms.general.*;
import ch.openchvote.framework.security.EncryptionScheme;
import ch.openchvote.model.general.Ciphertext;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.Serializer;

import java.math.BigInteger;

/**
 * This class implements a hybrid encrpytion scheme for encrypting arbitrarily large messages. The group parameters
 * are selected from the {@link Parameters} class. It uses the hybrid encryption algorithm {@link GenCiphertext}.
 */
public class HybridEncryptionScheme implements EncryptionScheme {

    // used for serializing the resulting ciphertext
    private static final Serializer<Ciphertext> CIPHERTEXT_SERIALIZER = new Serializer<>() {};

    // used for serializing private and public keys
    private static final Serializer<BigInteger> KEY_SERIALIZER = new Serializer<>() {};

    @Override
    public String encrypt(String plaintext, String publicKey, int securityLevel) {
        var params = new Parameters(securityLevel);
        var pk = KEY_SERIALIZER.deserialize(publicKey);
        var e = GenCiphertext.run(pk, ToByteArray.run(plaintext), params);
        var ciphertext = CIPHERTEXT_SERIALIZER.serialize(e);
        return ciphertext;
    }

    @Override
    public String decrypt(String ciphertext, String privateKey, int securityLevel) {
        var params = new Parameters(securityLevel);
        var sk = KEY_SERIALIZER.deserialize(privateKey);
        var e = CIPHERTEXT_SERIALIZER.deserialize(ciphertext);
        var plainText = ToString.run(GetPlaintext.run(sk, e, params));
        return plainText;
    }

}
