/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.protocol;

import ch.openchvote.framework.Party;

/**
 * This enum class defines all party types of the CHVote protocol. Some of them have cryptographic keys for
 * signing (or encrypting) messages, and some of them don't.
 */
public enum PartyType implements Party.Type {

    // parties with keys
    ADMINISTRATOR(true),
    ELECTION_AUTHORITY(true),
    COORDINATOR(true),
    PRINTING_AUTHORITY(true),

    // parties without keys
    VOTER(false),
    VOTING_CLIENT(false),
    THE_PUBLIC(false);

    private final boolean hasKeys;

    // private constructor not available from the outside
    PartyType(boolean hasKeys) {
        this.hasKeys = hasKeys;
    }

    @Override
    public String getName() {
        return this.name();
    }

    @Override
    public boolean hasKeys() {
        return hasKeys;
    }

}
