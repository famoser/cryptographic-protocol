# Protocol

This module provides classes necessary for instantiating the generic <code>framework</code> module to the particularities of the CHVote protocol. It consists of the following elements:

- An enum class <code>MessageType</code>, which defines the sender, the receiver, and the properties (signed, encrypted) of every protocol message. It also defines a unique name for each message, for example <code>MCE2</code> for the unsigned plaintext message from the voting client to the election authorities during vote casting. A complete list of all message types is given [here](#messages).

- A set of <code>MessageContent</code> classes, which defines the exact Java type of the message content for every protocol message. Note that a set of such classes exist for both protocol versions.As an example, consider the message <code>MCE2</code>, which contains a voter index and a ballot. The corresponding class is defined as follows:
  ```java
  public class MCE2 extends Pair<Integer, Ballot> implements MessageContent<MCE2> {}
  ```
- Two other enum classes <code>PartyType</code> and <code>Protocol</code> for specifying the CHVote parties and protocol versions, respectively. 

- Implementations of the security interfaces <code>SignatureScheme</code>, <code>EncryptionScheme</code>, <code>KeyGenerator</code>, and <code>Certificate</code> from the module <code>framework</code> according to the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325).

The intention of this module is to provide production-quality Java code, that can be used without changes in a real system implementation.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)


## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## Messages

#### Pre-Election
| Protocol | Message | Sender             |  Receiver          | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:-------------------|:------:|:---------:|:--------|:---|
| 7.1      | MAE1    | Administrator      | Election authority | yes    | no        | Election parameters          | ![MAE1](img/MAE1.png) |
| 7.1      | MEE1    | Election authority | Election authority | yes    | no        | Public key share             | ![MEE1](img/MEE1.png) |
| 7.2      | MEE2    | Election authority | Election authority | yes    | no        | Public voting credentials    | ![MEE2](img/MEE2.png) |
| 7.2      | MAP1    | Administrator      | Printing authority | yes    | no        | Election parameters          | ![MAP1](img/MAP1.png) |
| 7.3      | MEP1    | Election authority | Printing authority | yes    | yes       | Voting card data             | ![MEP1](img/MEP1.png) |
| 7.3      | MPV1    | Printing authority | Voter              | no     | no        | Voting cards                 | ![MPV1](img/MPV1.png) |

#### Election
| Protocol | Message | Sender             | Receiver           | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:-------------------|:------:|:---------:|:--------|:---|
| 7.4      | MVC1    | Voter              | Voting client      | no     | no        | Voter index                  | ![MVC1](img/MVC1.png) |
| 7.4      | MCA1    | Voting client      | Administrator      | no     | no        | Voter index                  | ![MCA1](img/MCA1.png) |
| 7.4      | MAC1    | Administrator      | Voting client      | yes    | no        | Voting parameters, public key share | ![MAC1](img/MAC1.png) |
| 7.4      | MCV1    | Voting client      | Voter              | no     | no        | Voting parameters            | ![MCV1](img/MCV1.png) |
| 7.4      | MVC2    | Voter              | Voting client      | no     | no        | Voting code and selection    | ![MVC2](img/MVC2.png) |
| 7.5      | MCE1    | Voting client      | Election authority | no     | no        | Voter index                  | ![MCE1](img/MCE1.png) |
| 7.5      | MEC1    | Election authority | Voting client      | yes    | no        | Public key share             | ![MEC1](img/MEC1.png) |
| 7.5      | MCE2    | Voting client      | Election authority | no     | no        | Ballot                       | ![MCE2](img/MCE2.png) |
| 7.5      | MEC2    | Election authority | Voting client      | yes    | no        | Response                     | ![MEC2](img/MEC2.png) |
| 7.6      | MCV2    | Voting client      | Voter              | no     | no        | Verification codes           | ![MCV2](img/MCV2.png) |
| 7.6      | MVC3    | Voter              | Voting client      | no     | no        | Confirmation code            | ![MVC3](img/MVC3.png) |
| 7.6      | MCE3    | Voting client      | Election authority | no     | no        | Confirmation                 | ![MCE3](img/MCE3.png) |
| 7.6      | MEC3    | Election authority | Voting client      | yes    | no        | Finalization                 | ![MEC3](img/MEC3.png) |
| 7.6      | MCV3    | Voting client      | Voter              | no     | no        | Finalization codes           | ![MCV3](img/MCV3.png) |

#### Post-Election
| Protocol | Message | Sender             | Receiver           | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:-------------------|:------:|:---------:|:--------|:---|
| 7.7      | MEE3    | Election authority | Election authority | yes    | no        | Shuffled encryptions         | ![MEE3](img/MEE3.png) |
| 7.8      | MEE4    | Election authority | Election authority | yes    | no        | Partial decryptions          | ![MEE4](img/MEE4.png) |
| 7.9      | MEA1    | Election authority | Administrator      | yes    | no        | Combined partial decryptions | ![MEA1](img/MEA1.png) |
| 7.9      | MAT1    | Administrator      | The public         | yes    | no        | Election result              | ![MAT1](img/MAT1.png) |
| 7.10     | MVC5    | Voter              | Voting client      | no     | no        | Voter index                  | ![MVC5](img/MVC5.png) |
| 7.10     | MCE4    | Voting client      | Election authority | no     | no        | Voter index                  | ![MCE4](img/MCE4.png) |
| 7.10     | MEC4    | Election authority | Voting client      | yes    | no        | Inspection                   | ![MEC4](img/MEC4.png) |
| 7.10     | MCV4    | Voting client      | Voter              | no     | no        | Inspection code              | ![MCV4](img/MCV4.png) |
| –        | MVC4    | Voter              | Voting client      | no     | no        | Abstention                   | –                     |

#### Coordination

| Message | Sender             |  Receiver          | Signed | Purpose                 |
|:-------:|:-------------------|:-------------------|:------:|:------------------------|
| MXA1    | Coordinator        | Administrator      | yes    | Start pre-election      |
| MAX1    | Administrator      | Coordinator        | yes    | Pre-election done       |
| MEX1    | Election authority | Coordinator        | yes    | Pre-election done       |
| MVX1    | Voter              | Coordinator        | no     | Pre-election done       |
| MXA2    | Coordinator        | Administrator      | yes    | Start vote casting      |
| MXE1    | Coordinator        | Election authority | yes    | Start vote casting      |
| MXV1    | Coordinator        | Voter              | yes    | Start vote casting      |
| MVX2    | Voter              | Coordinator        | no     | Vote casting done       | 
| MXA3    | Coordinator        | Administrator      | yes    | Start post-election     |
| MXE2    | Coordinator        | Election authority | yes    | Start post-election     |
| MEX2    | Election authority | Coordinator        | yes    | Post-election done      |
| MXV2    | Coordinator        | Voter              | yes    | Start inspection        |
| MVX3    | Voter              | Coordinator        | no     | Inspection done         | 
| MXE3    | Coordinator        | Election authority | yes    | Election event finished |
