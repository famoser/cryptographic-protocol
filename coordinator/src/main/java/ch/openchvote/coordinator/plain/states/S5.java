/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.coordinator.plain.states;

import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MVX3;
import ch.openchvote.protocol.message.plain.MXE3;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.Message;
import ch.openchvote.coordinator.Coordinator;
import ch.openchvote.coordinator.plain.EventData;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S5 extends State<Coordinator> {
    public S5(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MVX3:
                this.handleMVX3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMVX3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();

        // check and get message content
        this.party.checkAndGetContent(MVX3.class, message, eventSetup);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var eventMessages = context.getEventMessages();

        // check if all MVX3 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MVX3)) {

            // select event data
            var U = eventData.U.get();

            // send MXE3 message to election authorities
            this.party.sendMessage(new MXE3(), U, eventSetup);

            // update state
            context.setCurrentState(S6.class);
        }
    }

}