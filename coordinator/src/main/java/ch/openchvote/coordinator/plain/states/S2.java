/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.coordinator.plain.states;

import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.plain.*;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.coordinator.Coordinator;
import ch.openchvote.coordinator.plain.EventData;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S2 extends State<Coordinator> {

    public S2(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MAX1:
                this.handleMAX1(message, context);
                break;
            case MEX1:
                this.handleMEX1(message, context);
                break;
            case MVX1:
                this.handleMVX1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMAX1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var U = eventData.U.get();

        // check and get message content
        this.party.checkAndGetContent(MAX1.class, message, U, eventSetup);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    private void handleMEX1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var U = eventData.U.get();

        // check and get message content
        this.party.checkAndGetContent(MEX1.class, message, U, eventSetup);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    private void handleMVX1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();

        // check and get message content
        this.party.checkAndGetContent(MVX1.class, message, eventSetup);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var eventMessages = context.getEventMessages();

        // check if all MAX1, MEX1, and MVX1 messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MAX1) && eventMessages.hasAllMessages(eventSetup, MessageType.MEX1) && eventMessages.hasAllMessages(eventSetup, MessageType.MVX1)) {

            // select event data
            var U = eventData.U.get();

            // send MXA2 message to administrator
            this.party.sendMessage(new MXA2(), U, eventSetup);

            // send MXE1 message to election authorities
            this.party.sendMessage(new MXE1(), U, eventSetup);

            // send MXV1 message to voters
            this.party.sendMessage(new MXV1(), eventSetup);

            // update state
            context.setCurrentState(S3.class);

            // send internal message
            this.party.sendInternalMessage(eventSetup);
        }
    }

}
