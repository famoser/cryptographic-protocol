/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.coordinator;

import ch.openchvote.administrator.Administrator;
import ch.openchvote.framework.exceptions.EventSetupException;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.framework.services.Logger;
import ch.openchvote.voter.Voter;
import ch.openchvote.printingauthority.PrintingAuthority;
import ch.openchvote.thepublic.ThePublic;
import ch.openchvote.votingclient.VotingClient;

import java.util.*;

import static ch.openchvote.framework.services.Logger.Level.INFO;

/**
 * This class implements the 'Coordinator' party, which is explicitly included in the CHVote protocol. Its purpose is
 * send coordination messages to the parties for keeping the protocol running. This class is a direct sub-class of
 * {@link Party} with with a single added field {@link Coordinator#eventSetups}. This extension allows the coordinator
 * to keep track of the event setups of previous and current election events. The specific role of the coordinator in
 * the protocol is implemented in the classes {@link ch.openchvote.voter.plain.EventData} (plain protocol) and
 * {@link ch.openchvote.voter.writein.EventData} (write-in protocol) and in corresponding state and task classes.
 */
public class Coordinator extends Party {

    // a private hashmap for storing the event setups of previous and current election events
    private final Map<String, EventSetup> eventSetups;

    /**
     * Constructs a new instance of this class.
     *
     * @param id   The coordinator's party id
     * @param mode The logger's mode of operation
     */
    public Coordinator(String id, Logger.Mode mode) {
        super(id, PartyType.COORDINATOR, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
        this.eventSetups = new HashMap<>();
    }

    /**
     * Initializes a new election event by creating an {@link EventSetup} consisting of all given parties and adds it
     * to the private hashmap.
     *
     * @param eventId             The id of the new election event
     * @param protocolId          The protocol id of the new eleciton event
     * @param securityLevel       The selected security level
     * @param administrator       An administrator
     * @param printingAuthority   A printing authority
     * @param electionAuthorities A list of election authorities
     * @param votingClients       A list of voting clients
     * @param voters              A list of voter
     * @param thePublic           The public
     */
    public void initEvent(String eventId, String protocolId, int securityLevel, Administrator administrator, PrintingAuthority printingAuthority, List<ElectionAuthority> electionAuthorities, List<VotingClient> votingClients, List<Voter> voters, ThePublic thePublic) {

        // check uniqueness of eventId
        if (this.eventSetups.containsKey(eventId)) {
            this.log(Logger.Level.ERROR, eventId, "Event id already exists");
            throw new EventSetupException(EventSetupException.Type.DUPLICATE_EVENT_ID);
        }

        // create eventSetup
        EventSetup eventSetup = new EventSetup(eventId, protocolId, securityLevel);
        this.eventSetups.put(eventId, eventSetup);

        // only necessary for calling initEvent (remove when eventSetup-message is introduced)
        var parties = new ArrayList<Party>();

        // election administrator
        eventSetup.addParticipant(administrator);
        parties.add(administrator);

        // printing authority
        eventSetup.addParticipant(printingAuthority);
        parties.add(printingAuthority);

        // election authorities
        for (ElectionAuthority electionAuthority : electionAuthorities) {
            eventSetup.addParticipant(electionAuthority);
            parties.add(electionAuthority);
        }

        // voting Clients
        for (VotingClient votingClient : votingClients) {
            eventSetup.addParticipant(votingClient);
            parties.add(votingClient);
        }

        // voters
        for (Voter voter : voters) {
            eventSetup.addParticipant(voter);
            parties.add(voter);
        }

        // the public
        eventSetup.addParticipant(thePublic);
        parties.add(thePublic);

        // coordinator
        eventSetup.addParticipant(this);
        parties.add(this);

        // setup parties
        for (Party party : parties) {
            party.initEvent(eventSetup);
        }
        this.log(INFO, eventId, "Event setup created");
    }

    /**
     * Starts an election event by sending an internal message to itself.
     *
     * @param eventId The id of the election event
     */
    public void startEvent(String eventId) {
        this.log(INFO, eventId, "Event started");
        this.onInternalMessage(eventId);
    }

}
