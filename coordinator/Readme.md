# Coordinator

This module provides an implementation of an additional  protocol party called *"Coordinator"*. Its purpose is to send configuration messages to the other parties, for example for announcing the start or the end of the election phases. The coordination messages have not content, and the coordinator accumulates not data during a protocol run. 

This party has been introduced mainly for testing purposes. It simplifies the problem of keeping the election process 'alive' during a simulated election event. In a real system implementation, this module will therefore not be part of the system in exactly this form. However, there might be a similar coordinator implementation with a similar role.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/Coordinator.svg)

## Messages

#### Incoming
| Message | Sender             | Signed | Encrypted | Content |    |
|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| MAX1    | Administrator      | yes    | no        | Pre-election done  | |
| MEX1    | Election authority | yes    | no        | Pre-election done  | |
| MEX2    | Election authority | yes    | no        | Post-election done | |
| MVX1    | Voter              | no     | no        | Pre-election done  | |
| MVX2    | Voter              | no     | no        | Vote casting done  | | 
| MVX3    | Voter              | no     | no        | Inspection done    | | 

#### Outgoing
| Message |  Receiver          | Signed | Encrypted | Content |    |
|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| MXA1    | Administrator      | yes    | no        |  Start pre-election     | |
| MXA2    | Administrator      | yes    | no        | Start vote casting      | |
| MXA3    | Administrator      | yes    | no        | Start post-election     | |
| MXE1    | Election authority | yes    | no        | Start vote casting      | |
| MXE2    | Election authority | yes    | no        | Start post-election     | |
| MXE3    | Election authority | yes    | no        | Election event finished | |
| MXV1    | Voter              | yes    | no        | Start vote casting      | |
| MXV2    | Voter              | yes    | no        | Start inspection        | |

## Tasks

*not available*
