# Framework

This module offers a generic framework for implementing cryptographic protocols. While the CHVote protocol is its principal use case, the framework is largely independent of any CHVote particularities. Achieving this [independence](#dependencies) has been an important design goal.

## Introduction

In the CHVote e-voting protocol, there are different parties such as the administrator, the election authorities, or the printing authority. During a protocol execution, they perform certain tasks and exchange messages. Since the required properties of the communication channels are not always the same, parties must be flexible to send and receive messages that are either signed or unsigned respectively encrypted or not encrypted. Because election events may overlap in time, it must be possible for the parties to participate in different protocol executions simultaneously. In such simultaneous protocol runs, support for different protocol versions and security levels must be provided. Currently, CHVote defines two protocol versions *plain* and *writein* and four security levels 0, 1, 2, 3 (see [CHVote Protocol Specification](https://eprint.iacr.org/2017/325)).

This module offers a common abstraction for the implementation of such a setting. In particular, the framework provides a solution for the following questions:

- How can a party participate in multiple protocol executions simultaneously?
- How can support for different protocol versions be provided?
- How can the required flexibility with respect to the properties of the communication channels be realized?

More general design questions result from the goal of making the  parties independent of concrete infrastructure services. This independence is the key for achieving a strict separation between cryptographically relevant code and cryptographically irrelevant code. This separation defines the boundaries of the current OpenCHVote project.

## General Concepts

A party is essentially a state-event-machine. Each state represents a particular position in the protocol flow of an election event, and state transitions are triggered by receiving messages from other parties. Upon receiving a message, a party executes different tasks such as cryptographic computations, the sending of new messages to other parties, or the transition to a new state. The data required for these tasks is stored as shared event data, which is available to all states. This basic principle is illustrated in the following diagram:

![state-event](img/state-event.svg)

The infrastructure information for a given election event is stored in an *event setup* object. The main content of an event setup is the list of participating parties. Other elements are a unique event id and the desired security level.

To run multiple election events simultaneously, the parties maintain a context for each event using their persistence service. Each context contains the event setup and the *event data* used by the party to process a message and to conduct a state transition. In this way, the event data objects are strictly separated over the different election events. The event context also accumulates all messages received during the protocol run.

To support different protocol versions, the event setup contains a protocol identifier. This identifier is used as package name to load the state classes of the corresponding protocol version. This solution is implemented using Java reflection.

## Design

In the OOP design that follows from the above general concepts, the classes <code>Party</code>, <code>State</code>, <code>Message</code>, <code>EventContext</code>, <code>EventSetup</code>, <code>EventData</code>, and <code>Participant</code> are the most important ones. They are shown in the following UML class diagram. For improved readability, the diagram is slightly simplified.

![class-diagram](img/class-diagram.svg)

Some additional interfaces exist for making the framework code independent of infrastructure services and specific cryptographic schemes are included in the above class diagram:
- Service interfaces: <code>MessagingService</code>, <code>PersistenceService</code>, <code>Keystore</code>, <code>Logger</code>
- Cryptographic interface: <code>EncryptionScheme</code>, <code>SignatureScheme</code>, <code>KeyGenerator</code>, <code>Certificate</code>
    
## Technical Aspects

#### Message Handling: <code>Party.onMessage()</code>

The central method of the <code>party</code> is the <code>onMessage()</code> method that is called in the event of an incoming message from another party. Here is a summary of the tasks that are triggered in such a case:
1. Lock the event and load the event context using the persistence service for the identifier specified in the message.
2. Check if the message has already been processed and cancel processing if so.
3. Check if the sender, the receiver, and the type of the message are correct and cancel processing if not.
4. Get the identifier of the current state from the context and load the state object.
5. Pass the message and context to the state's <code>handleMessage()</code> method.
6. Save the updated event context using the persistence service and unlock the event.

A similar procedure exists for handling *internal messages*, which the party can send to itself for triggering possible subsequent state transitions after processing a message.

#### Message Handling: <code>State.handleMessage()</code>

The <code>State</code> class in the framework has a default <code>handleMessage()</code> method, which throws an exceptions to indicate that the message cannot be processed. This default behaviour is inherited for example by the final states, in which no further message events are possible.

In specific state classes that inherit from <code>State</code>, the <code>handleMessage()</code> method usually follows the following procedure: 
1. If the message content is encrypted, decrypt it.
3. If the message is signed, deserialize and verify the signature.
3. Deserialize the message content and retrieve its data.
4. Use the message and context data to perform some computational tasks.
5. Update the event data.
6. Send messages to other parties
7. If necessary, update the current state in the event context.

#### Message Serialization

For keeping the <code>MessagingService</code> interface as simple as possible, and for making corresponding implementation as straightforward as possible, all fields of a <code>Message</code> object are strings. This includes the message content and its signature. This means that the domain objects must be serialized before sending a message and deserialized upon receiving one. This functionality is delegated to an internal <code>Message.Content</code> interface, which can be implemented in different ways for specific protocols. In the case of OpenCHVote, a generic <code>Serializer</code> class is used for that purpose. 

#### Synchronization

Since the messaging service delivers messages asynchronously, the <code>onMessage()</code> method of a party may be invoked in simultaneously. Therefore, the persistence service must provide the possibility to lock an event context. To prevent an event context from being locked too long, a non-locked context is passed to the <code>onMessage()</code> method. It's the method's own responsibility to invoke the locking and unlocking mechanism whenever needed.

#### Transactionality

If an error occurs during message processing, an exception is thrown by the <code>handleMessage()</code> method or by methods called from <code>handleMessage()</code>. There are three main cases of exceptions: <code>MessageException</code>, <code>AlgorithmException</code>, and <code>TaskException</code>. In the first two cases, the exception is caught by the party's <code>onMessage</code> method and the event is unlocked without saving it. In case of a <code>TaskException</code>, the party usually moves into a final error state.

#### Implementation of Protocols

To implement a new protocol version, for example a protocol called "Protocol2", each party must implement the following classes in  corresponding packages:
 
- <code>protocol2.EventData</code>: a sub-class of <code>EventData</code> implementing the specific event data
- <code>protocol2.states.State_1</code>, ..., <code>protocol2.states.State_n</code>: sub-classes of <code>State</code> implementing the specific states of the party's state machine.

The abstract method <code>EventData.getInitialState()</code> must be overridden to define the initial state of the party's state machine.  

## Dependencies

As shown in the following UML component diagram, this module is independent of any other module of the OpenCHVote project.

![Maven dependency](img/maven-dependency.svg)


## Installation of Maven Artefact

This module can be built as follows from its root:

````shell script
$ mvn clean install
-- a lot of output
````
