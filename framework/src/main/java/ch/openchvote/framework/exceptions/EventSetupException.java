/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.exceptions;

/**
 * This class is used for throwing specific exceptions related to an event setup. The different types of
 * event setup exceptions are represented as an internal enum class.
 */
public class EventSetupException extends RuntimeException {

    public enum Type {
        PARTICIPANT_ALREADY_EXISTS,
        PARTICIPANT_DOES_NOT_EXIST,
        DUPLICATE_EVENT_ID
    }

    private final Type type;

    public EventSetupException(Type type) {
        super(type.name());
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

}
