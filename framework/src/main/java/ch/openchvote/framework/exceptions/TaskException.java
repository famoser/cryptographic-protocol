/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.exceptions;

/**
 * This class is used for throwing specific exceptions that happen when executing a task. The different types of task
 * exceptions are represented as an internal enum class.
 */
public class TaskException extends RuntimeException {

    public enum Type {
        INVALID_ZKP_PROOF,
        INVALID_BALLOT,
        INVALID_CONFIRMATION,
        VERIFICATION_CODE_MISMATCH,
        FINALIZATION_CODE_MISMATCH,
        INSPECTION_CODE_MISMATCH,
        VALUES_NOT_EQUAL
    }

    private final String taskName;
    private final Type type;

    public TaskException(Class<?> taskClass, Type type) {
        this(taskClass.getName(), type);
    }

    public TaskException(String taskName, Type type) {
        super(taskName + ": " + type);
        this.taskName = taskName;
        this.type = type;
    }

    public String getTaskName() {
        return this.taskName;
    }

    public Type getType() {
        return this.type;
    }

}
