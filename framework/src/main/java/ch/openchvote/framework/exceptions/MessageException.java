/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.exceptions;

import ch.openchvote.framework.Message;

/**
 * This class is used for throwing specific exceptions related the sending and receiving messages. The different types of
 * message exceptions are represented as an internal enum class.
 */
public class MessageException extends RuntimeException {

    public enum Type {
        UNKNOWN_TYPE,
        WRONG_RECEIVER,
        WRONG_RECEIVER_TYPE,
        WRONG_SENDER_TYPE,
        UNKNOWN_SENDER,
        UNKNOWN_RECEIVER,
        ALREADY_PROCESSED,
        INVALID_SIGNATURE,
        INVALID_CONTENT
    }

    private final Type type;

    public MessageException(Type type) {
        super("INT: " + type);
        this.type = type;
    }

    public MessageException(Message message, Type type) {
        super(message.getTypeName() + " (" + message.getId() + "): " + type);
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

}
