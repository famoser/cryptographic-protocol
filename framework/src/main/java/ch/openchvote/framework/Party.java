/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework;

import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.security.*;
import ch.openchvote.framework.services.*;

import java.util.Optional;

import static ch.openchvote.framework.exceptions.MessageException.Type.*;
import static ch.openchvote.framework.services.Logger.Level.*;

/**
 * This is a base class for the parties participating in a protocol. It receives a {@link MessagingService}
 * at the time of construction, through which the party is connected with other parties participating in a protocol. To
 * enable to party to sign, verify, encrypt, and decrypt messages, the constructor also receives a {@link KeyGenerator},
 * a {@link SignatureScheme}, and an {@link EncryptionScheme}, which must be the same for all parties participating in a
 * protocol. The constructor furthermore reads a configuration file to install a {@link PersistenceService},
 * a {@link Logger}, and a {@link Keystore}. In these cases, the parties are free to install components of their own
 * choice.
 * <p>
 * The central methods of this class deal with the sending and receiving of messages. If one or the other version of
 * {@link Party#sendMessage} is called, the messages to be sent is first prepared (serialized, signed, encrypted) and
 * the sent over the {@link MessagingService} to the indented receiver. At the receiver, the receipt of this message
 * triggers calling the method {@link Party#onMessage(Message)}. The main purpose of this method is to load the right
 * {@link EventContext} from the {@link PersistenceService} and to create a {@link State} object for the current state.
 * The actual processing of the message content is then delegated to the state's
 * {@link State#handleMessage(Message, EventContext)} method.
 */
public class Party {

    protected final String id;
    protected final Type type;
    protected final MessagingService messagingService;
    protected final Message.ContentFactory messageContentFactory;
    protected final Message.TypeFactory messageTypeFactory;
    protected final PersistenceService persistenceService;
    protected final Logger logger;
    protected final Keystore keystore;
    protected final SignatureScheme signatureScheme;
    protected final EncryptionScheme encryptionScheme;
    protected final KeyGenerator keyGenerator;

    /**
     * This is the base constructor of this class, which must be called by corresponding constructors of all
     * sub-class. It setups the party's services and cryptographic schemes.
     *  @param id A unique party id
     * @param type The party's type
     * @param messageContentFactory A factory for creating message contents
     * @param messageTypeFactory A factory for creating message types
     * @param keyGenerator A key generator for the party's signature and encryption schemes
     * @param signatureScheme The protocols' common signature scheme
     * @param encryptionScheme The protocols' common signature scheme
     * @param loggerMode A logger
     */
    protected Party(String id, Type type, Message.ContentFactory messageContentFactory, Message.TypeFactory messageTypeFactory, KeyGenerator keyGenerator, SignatureScheme signatureScheme, EncryptionScheme encryptionScheme, Logger.Mode loggerMode) {
        this.id = id;
        this.type = type;

        // protocol-specific message content and type factories
        this.messageContentFactory = messageContentFactory;
        this.messageTypeFactory = messageTypeFactory;

        // protocol-specific cryptographic schemes
        this.keyGenerator = keyGenerator;
        this.signatureScheme = signatureScheme;
        this.encryptionScheme = encryptionScheme;

        // get services from configuration file
        var configuration = new Configuration(this);
        this.messagingService = configuration.getMessagingService();
        this.messagingService.registerParty(this);
        this.persistenceService = configuration.getPersistenceService();
        this.logger = configuration.getLogger();
        this.logger.setMode(loggerMode);
        this.keystore = configuration.getKeystore();
        if (this.getType().hasKeys()) {
            this.keystore.generateKeys(this.id, this.keyGenerator);
        }

    }

    /**
     * Return the party's id
     *
     * @return The party's id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Returns the party's type
     *
     * @return The party's type
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Returns the party's current certificate for a given security level. The certificate is loaded from the
     * party's {@link Keystore}.
     *
     * @param securityLevel The security level (ex. 1, 2, etc.)
     * @return A party's current certificate for the security level
     */
    public Optional<Certificate> getCertificate(int securityLevel) {
        return this.keystore.getCertificate(securityLevel);
    }

    /**
     *  Initializes a new election event for a given event setup. For that, a new {@link EventContext} object is
     *  created and stored using the party's persistence service. The the new event context, the party's state is set
     *  to the protocol's initial state.
     *
     * @param eventSetup The given election setup
     */
    public void initEvent(EventSetup eventSetup) {

        // check if participant names match with certificate subjects and if certificates are valid
        if (!this.checkCertificates(eventSetup))
            throw new RuntimeException("Invalid certificate.");
        // register all participants  in the messaging Service
        for (Participant participant : eventSetup.getParticipants()){
            this.messagingService.registerParticipant(participant.getPartyId());
        }
        // create and save new event context
        var context = new EventContext(this, eventSetup);
        var eventId = eventSetup.getEventId();
        this.persistenceService.lockEvent(eventId);
        this.persistenceService.saveContext(context);
        this.persistenceService.unlockEvent(eventId);

    }

    /**
     * This method handles an incoming external message from another party. It loads the right {@link EventContext}
     * and creates a {@link State} object for the current state. The actual processing of the message content is
     * delegated to the state's {@link State#handleMessage(Message, EventContext)} method. If the message
     * handling terminates successfully, possibly by moving the context into a new state, the updated
     * {@link EventContext} is persisted using the party's {@link PersistenceService}.
     *
     * @param message The received message to handle
     */
    public void onMessage(Message message) {

        // lock event and load context
        var eventId = message.getEventId();
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);

        // read data from context
        var stateId = context.getCurrentStateId();
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();

        // create current state
        var protocolId = eventSetup.getProtocolId();
        var state = State.create(protocolId, stateId, this);

        try {
            var messageType = this.messageTypeFactory.create(message.getTypeName());

            // check if correct message receiver
            if (!this.id.equals(message.getReceiverId())) {
                throw new MessageException(message, WRONG_RECEIVER);
            }

            // check if correct receiver type
            if (this.type != messageType.getReceiverType()) {
                throw new MessageException(message, WRONG_RECEIVER_TYPE);
            }

            // check is sender exists in setup
            if (!eventSetup.hasParticipant(message.getSenderId())) {
                throw new MessageException(message, UNKNOWN_SENDER);
            }

            // check if correct sender type
            if (!messageType.getSenderType().getName().equals(eventSetup.getParticipant(message.getSenderId()).getPartyType())) {
                throw new MessageException(message, WRONG_SENDER_TYPE);
            }

            // check if message already exists
            if (eventMessages.hasMessage(message)) {
                throw new MessageException(message, ALREADY_PROCESSED);
            }

            // check if message contains signature (if needed)
            if (messageType.isSigned() != message.isSigned()) {
                throw new MessageException(message, INVALID_SIGNATURE);
            }

            // add message to eventMessages
            eventMessages.addMessage(message);
            this.log(DEBUG, eventId, message.getTypeName() + " received from " + message.getSenderId() + " (state=" + stateId + ")");

            // handle message
            state.handleMessage(message, context);

            // save context and unlock event
            this.persistenceService.saveContext(context);
            this.persistenceService.unlockEvent(eventId);

            // log success
            this.logMessageHandled(protocolId, eventId, message.getTypeName(), stateId, context.getCurrentStateId());

        } catch (MessageException exception) {

            // unlock event
            this.persistenceService.unlockEvent(eventId);

            // log failure
            this.log(WARNING, eventId, message.getTypeName() + " received from " + message.getSenderId() + ", handling failed: " + exception.getType() + " (state=" + stateId + ")");

        }
    }

    /**
     * The method handles internal messages. It is similar to {@link Party#onMessage(Message)}, but fewer checks are
     * required before calling {@link State#handleMessage(Message, EventContext)} triggering the actual processing.
     * If the handling of the internal message terminates successfully, possibly by moving the context into a new state,
     * the updated {@link EventContext} is persisted using the party's {@link PersistenceService}.
     *
     * @param eventId The given election event identifier
     */
    public void onInternalMessage(String eventId) {

        // lock event and load context
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);

        // read data from context
        var stateId = context.getCurrentStateId();
        var eventSetup = context.getEventSetup();

        // create current state
        var protocolId = eventSetup.getProtocolId();
        var state = State.create(protocolId, stateId, this);

        // handle internal message
        state.handleInternalMessage(context);

        // save context and unlock event
        this.persistenceService.saveContext(context);
        this.persistenceService.unlockEvent(eventId);

        // log internal message handled
        var newStateId = context.getCurrentStateId();
        this.logMessageHandled(protocolId, eventId, "INT", stateId, newStateId);
    }

    /**
     * Creates a new message for the given message content and sends it to all intended receivers. The list of receivers
     * is derived from the event setup and from the message type, which is known to the message content.
     *
     * @param messageContent The message content
     * @param aux An auxiliary information to include in the signature
     * @param eventSetup The event setup
     */
    public void sendMessage(Message.Content messageContent, Object aux, EventSetup eventSetup) {
        var messageType = messageContent.getType();
        var receivers = eventSetup.getParticipants(messageType.getReceiverType());
        for (var receiver : receivers) {
            var receiverId = receiver.getPartyId();
            if (!receiverId.equals(this.id)) {
                this.sendMessage(receiverId, messageContent, aux, eventSetup);
            }
        }
    }

    /**
     * Creates a new message for the given message content and sends it to all intended receivers. The list of receivers
     * is derived from the event setup and from the message type, which is known to the message content. This is a short
     * version of {@link Party#sendMessage(Message.Content, Object, EventSetup)} for unsigned messages or for
     * messages without an auxiliary information to include in the signature.
     *
     * @param messageContent The message content
     * @param eventSetup The event setup
     */
    public void sendMessage(Message.Content messageContent, EventSetup eventSetup) {
        this.sendMessage(messageContent, null, eventSetup);
    }

    /**
     * Creates a new message for the given message content and sends it to the receiver with a given participant index.
     * For establishing a unique indexing, the event setup keeps the the participants in an ordered list.
     *
     * @param participantIndex The participant index of the intended receiver
     * @param messageContent The message content
     * @param aux An auxiliary information to include in the signature
     * @param eventSetup The event setup
     */
    public void sendMessage(int participantIndex, Message.Content messageContent, Object aux, EventSetup eventSetup) {
        var messageType = messageContent.getType();
        var receiverId = eventSetup.getParticipant(messageType.getReceiverType(), participantIndex).getPartyId();
        this.sendMessage(receiverId, messageContent, aux, eventSetup);
    }

    /**
     * Creates a new message for the given message content and sends it to the receiver with a given participant index.
     * For establishing a unique indexing, the event setup keeps the the participants in an ordered list. This is a short
     * version of {@link Party#sendMessage(int, Message.Content, Object, EventSetup)} for unsigned messages or for
     * messages without auxiliary information to include in the signature.
     *
     * @param participantIndex The participant index of the intended receiver
     * @param messageContent The message content
     * @param eventSetup The event setup
     */
    public void sendMessage(int participantIndex, Message.Content messageContent, EventSetup eventSetup) {
        sendMessage(participantIndex, messageContent, null, eventSetup);
    }

    /**
     * Creates a new message for the given message content and sends it to the receiver with a given party id.
     *
     * @param receiverId The party id of the intended receiver
     * @param messageContent The message content
     * @param aux An auxiliary information to include in the signature
     * @param eventSetup The event setup
     */
    public void sendMessage(String receiverId, Message.Content messageContent, Object aux, EventSetup eventSetup) {
        this.sendMessage(this.id, receiverId, messageContent, aux, eventSetup);
    }

    /**
     * Creates a new message for the given message content and sends it to the receiver with a given party id. This is
     * a short version of {@link Party#sendMessage(String, Message.Content, Object, EventSetup)} for unsigned messages
     * or for messages without auxiliary information to include in the signature.
     *
     * @param receiverId The party id of the intended receiver
     * @param messageContent The message content
     * @param eventSetup The event setup
     */
    public void sendMessage(String receiverId, Message.Content messageContent, EventSetup eventSetup) {
        this.sendMessage(receiverId, messageContent, null, eventSetup);
    }

    /**
     * Sends an internal message to the party itself. Since internal messages are not carrying any information, they
     * are not created explicitly. Instead, the method {@link MessagingService#sendInternalMessage(String, String)} is
     * invoked, which initiates the handling the internal message.
     *
     * @param eventSetup The event setup
     */
    public void sendInternalMessage(EventSetup eventSetup) {
        var eventId = eventSetup.getEventId();
        this.messagingService.sendInternalMessage(this.id, eventId);
        this.log(DEBUG, eventId, "INT sent to " + this.id);
    }

    /**
     * First, the received message is deserialized and (if necessary) decrypted using the party's {@link EncryptionScheme}.
     * Second, if necessary, the signature of the message content is verified using the party's {@link SignatureScheme}.
     * If all operations and checks are successful, the deserialized content is returned. Otherwise, corresponding
     * exceptions are thrown.
     *
     * @param clazz The class object of the message content
     * @param message The received message
     * @param aux An auxiliary information included in the signature
     * @param eventSetup The event setup
     * @param <T> The generic type parameter specifying the type of the message content
     * @return The deserialized message content.
     */
    public <T extends Message.Content> T checkAndGetContent(Class<T> clazz, Message message, Object aux, EventSetup eventSetup) {
        var content = message.getContent();
        var messageType = this.messageTypeFactory.create(message.getTypeName());
        if (messageType.isEncrypted()) {
            content = this.decryptMessageContent(content, eventSetup);
        }
        var messageContent = this.messageContentFactory.create(clazz, content);
        if (messageType.isSigned()) {
            this.verifySignature(message, messageContent, aux, eventSetup);
        }
        return messageContent;
    }

    /**
     * First, the received message is deserialized and (if necessary) decrypted using the party's {@link EncryptionScheme}.
     * Second, if necessary, the signature of the message content is verified using the party's {@link SignatureScheme}.
     * If all operations and checks are successful, the deserialized content is returned. Otherwise, corresponding
     * exceptions are thrown. This is a short version of {@link Party#checkAndGetContent(Class, Message, Object, EventSetup)}
     * for unsigned messages or for messages without auxiliary information included in the signature.
     *
     * @param clazz The class object of the message content
     * @param message The received message
     * @param eventSetup The event setup
     * @param <T> The generic type parameter specifying the type of the message content
     * @return The deserialized message content.
     */
    public <T extends Message.Content> T checkAndGetContent(Class<T> clazz, Message message, EventSetup eventSetup) {
        return this.checkAndGetContent(clazz, message, null, eventSetup);
    }

    // needed for checking the party's state in a given election event

    /**
     * Returns the type of the party's current state for a given event id.
     *
     * @param eventId The given event id
     * @return The type of the party's current state
     */
    public State.Type getCurrentStateType(String eventId) {
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);
        var protocolId = context.getEventSetup().getProtocolId();
        var stateId = context.getCurrentStateId();
        var state = State.create(protocolId, stateId, this);
        this.persistenceService.unlockEvent(eventId);
        return state.getType();
    }

    // private method for sending a single message
    private void sendMessage(String senderId, String receiverId, Message.Content messageContent, Object aux, EventSetup eventSetup) {
        var eventId = eventSetup.getEventId();
        var content = messageContent.serialize();
        var messageType = messageContent.getType();
        content = messageType.isEncrypted() ? this.encryptMessageContent(receiverId, content, eventSetup) : content;
        var signature = (messageType.isSigned()) ? this.signMessageContent(messageContent, messageType.getName(), aux, eventSetup) : null;
        var message = new Message(messageType.getName(), senderId, receiverId, eventId, content, signature);
        this.messagingService.sendMessage(message);
        this.log(DEBUG, eventId, message.getTypeName() + " sent to " + message.getReceiverId());
    }

    // private method for encrypting a string message content
    private String encryptMessageContent(String receiverId, String content, EventSetup eventSetup) {
        var certificate = eventSetup.getParticipant(receiverId).getCertificate();
        var publicKey = certificate.getPublicKey();
        int securityLevel = certificate.getSecurityLevel();
        return this.encryptionScheme.encrypt(content, publicKey, securityLevel);
    }

    // private method for decrypting a string message content
    private String decryptMessageContent(String encryptedContent, EventSetup eventSetup) {
        var certificate = eventSetup.getParticipant(this.id).getCertificate();
        var privateKey = this.keystore.getPrivateKey(certificate);
        if (privateKey.isPresent()) {
            int securityLevel = certificate.getSecurityLevel();
            return this.encryptionScheme.decrypt(encryptedContent, privateKey.get(), securityLevel);
        }
        throw new RuntimeException("No private key for certificate available");
    }

    // private method for signing a message content
    private String signMessageContent(Message.Content messageContent, String messageType, Object aux, EventSetup eventSetup) {
        var certificate = eventSetup.getParticipant(this.id).getCertificate();
        var privateKey = this.keystore.getPrivateKey(certificate);
        if (privateKey.isPresent()) {
            int securityLevel = certificate.getSecurityLevel();
            return this.signatureScheme.sign(messageContent, messageType, aux, privateKey.get(), securityLevel);
        }
        throw new RuntimeException("No private key for certificate available");
    }

    // private method for verifying the signature of a message content
    private void verifySignature(Message message, Message.Content messageContent, Object aux, EventSetup eventSetup) {
        var messageType = message.getTypeName();
        var signature = message.getSignature();
        var senderId = message.getSenderId();
        var certificate = eventSetup.getParticipant(senderId).getCertificate();
        var publicKey = certificate.getPublicKey();
        int securityLevel = certificate.getSecurityLevel();
        if (!this.signatureScheme.verify(signature, messageContent, messageType, aux, publicKey, securityLevel)) {
            throw new MessageException(message, INVALID_SIGNATURE);
        }
    }

    // check all participant certificates from an eventSetup
    private boolean checkCertificates(EventSetup eventSetup) {
        return eventSetup.getParticipants().stream()
                .filter(Participant::hasCertificate)
                .allMatch(this::checkCertificate);
    }

    // check a single certificate of a participant
    private boolean checkCertificate(Participant participant) {
        var certificate = participant.getCertificate();
        return participant.getPartyId().equals(certificate.getSubject());
        // TODO implement real certificate validity check
    }

    // private method for logging message and internal message handling
    private void logMessageHandled(String protocolId, String eventId, String messageType, String stateId, String newStateId) {
        this.log(DEBUG, eventId, messageType + " handled (oldState=" + stateId + ", newState=" + newStateId + ")");
        if (!newStateId.equals(stateId)) {
            var newState = State.create(protocolId, newStateId, this);
            if (newState.getType() == State.Type.FINAL) {
                this.log(INFO, eventId, "Final state reached");
            }
            if (newState.getType() == State.Type.ERROR) {
                this.log(WARNING, eventId, "Error state reached (" + newState.getClass().getSimpleName() +")" );
            }
        }
    }

    // protected helper method for making consistent logging entries
    protected void log(Logger.Level level, String eventId, String logString) {
        this.logger.log(level, String.format("%-14s %-17s : %s", eventId, this.id, logString));
    }

    @Override
    public String toString() {
        return this.getType() + "-" + this.id;
    }

    /**
     * To model the fact that certain parties in a protocol have exactly the same role and tasks, this interface
     * introduces the concept of a <b>party type</b>. In a specific implementation of a protocol, party types can
     * be defined most conveniently using an enum class that implements this interface.
     */
    public interface Type {

        /**
         * Return the name of the party type.
         * @return The pary type's name
         */
        String getName();

        /**
         * Checks if a party type requires the  possession of private and public keys for signing and decrypting
         * messages.
         * @return {@code true}, if the party type requires key, {@code false} otherwise
         */
        boolean hasKeys();

    }

}
