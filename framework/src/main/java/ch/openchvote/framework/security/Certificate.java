/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.security;

/**
 * This interface models the basic functionality of a certificate as a container for assigning a public key to a
 * subject. Both the subject and the public key are given as strings. The current version of this interface ignores
 * several important aspects of 'real' certificates, for example the signature of the issuing CA or the expiration date.
 */
public interface Certificate {

    /**
     * Returns the certificate's subject
     *
     * @return The certificate's subject
     */
    String getSubject();

    /**
     * Returns the certificate's public key
     *
     * @return The certificate's public key
     */
    String getPublicKey();

    /**
     * Returns the security level of the certificate's public key
     *
     * @return The public key's security level
     */
    int getSecurityLevel();

    // TODO add more methods for checking the certificate's validity

}
