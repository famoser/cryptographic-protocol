/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.security;

import ch.openchvote.framework.Message;
/**
 * This interface defines the basic functionality of a signature scheme by providing two methods for signing a given
 * {@link Message.Content} and for verifying the resulting signature. The resulting signature is represented by a
 * string.
 */
public interface SignatureScheme {

    /**
     * Signs the given message content using the sender's private key. The given security level defines the strength of
     * the signature, for example by using it to select corresponding cryptographic parameters. Two additional elements
     * {@code messageType} and {@code aux} are linked into the signature for making the signature dependent on the the
     * message type and on some auxiliary information.
     *
     * @param messageContent The message content to sign
     * @param messageType The message type
     * @param aux Some auxiliary information
     * @param privateKey The sender's private key
     * @param securityLevel The security level of the signature
     * @return The generated signature
     */
    String sign(Message.Content messageContent, String messageType, Object aux, String privateKey, int securityLevel);

    /**
     * Verifies a given signature using the sender's public key. This operation is the inverse of
     * {@link SignatureScheme#sign(String, Message.Content, String, Object, String, int)}.
     *
     * @param signature The given signature
     * @param messageContent The signed message content
     * @param messageType The message type
     * @param aux Some auxiliary information
     * @param publicKey The sender's public key
     * @param securityLevel The security level of the signature
     * @return {@code true}, if the signature is valid, {@code false} otherwise
     */
    boolean verify(String signature, Message.Content messageContent, String messageType, Object aux, String publicKey, int securityLevel);

}
