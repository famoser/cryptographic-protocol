/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.security;

/**
 * This interface defines the basic functionality of an asymmetric encryption scheme. All parameters of the two methods 
 * {@link EncryptionScheme#encrypt(String, String, int)} and {@link EncryptionScheme#decrypt(String, String, int)} are
 * strings representations of corresponding objects.
 */
public interface EncryptionScheme {

    /**
     * Encrypts a given plaintext with the receiver's public key. The given security level defines the strength of the
     * encryption, for example by using it to select corresponding cryptographic parameters. The plaintext and the
     * resulting ciphertext are strings, as well as the public key.
     *
     * @param plaintext The plaintext to encrypt
     * @param publicKey The receiver's public key
     * @param securityLevel The security level of the encryption
     * @return The ciphertext
     */
    String encrypt(String plaintext, String publicKey, int securityLevel);

    /**
     * Decrypts a given ciphertext with the receiver's private key into the original plaintext. This operation is the
     * inverse of {@link EncryptionScheme#encrypt(String, String, int)}.
     *
     * @param ciphertext The ciphertext to decrypt
     * @param privateKey The receiver's private key
     * @param securityLevel The security level of the encryption
     * @return The ciphertext
     */
    String decrypt(String ciphertext, String privateKey, int securityLevel);

}
