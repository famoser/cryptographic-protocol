/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework;

import ch.openchvote.framework.services.MessagingService;

import java.util.UUID;

/**
 * Instances of this class contain the necessary information of a message to be transmitted over the
 * {@link MessagingService}. All fields are of type {@link String}. The class has no methods other than the
 * getter methods for accessing the fields.
 */
public class Message {

    protected final String id;
    protected final String typeName;
    protected final String senderId;
    protected final String receiverId;
    protected final String eventId;
    protected final String content;
    protected final String signature;

    /**
     * Constructs a new message object from the given parameters.
     *
     * @param typeName The name of the message type
     * @param senderId The sender id
     * @param receiverId The receiver id
     * @param eventId The event id
     * @param content The message content
     * @param signature The message's signature (or {@link null})
     */
    public Message(String typeName, String senderId, String receiverId, String eventId, String content, String signature) {
        this.id = UUID.randomUUID().toString();
        this.typeName = typeName;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.eventId = eventId;
        this.content = content;
        this.signature = signature;
    }

    /**
     * Constructs a new message object from the given parameters inluding the id.
     * This constructor is only used for reconstructing a message after it was sent.
     *
     * @param id The id of the message
     * @param typeName The name of the message type
     * @param senderId The sender id
     * @param receiverId The receiver id
     * @param eventId The event id
     * @param content The message content
     * @param signature The message's signature (or {@link null})
     */
    public Message(String id, String typeName, String senderId, String receiverId, String eventId, String content, String signature) {
        this.id = id;
        this.typeName = typeName;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.eventId = eventId;
        this.content = content;
        this.signature = signature;
    }

    /**
     * Returns the message's unique id.
     *
     * @return The message id.
     */
    public String getId() {
        return this.id;
    }

    /**
     * Return the name of the message's type.
     *
     * @return The name of the message's type
     */
    public String getTypeName() {
        return this.typeName;
    }

    /**
     * Returns the sender id.
     *
     * @return The sender id
     */
    public String getSenderId() {
        return this.senderId;
    }

    /**
     * Returns the receiver id.
     *
     * @return The receiver id
     */
    public String getReceiverId() {
        return this.receiverId;
    }

    /**
     * Returns the event id.
     *
     * @return The event id
     */
    public String getEventId() {
        return this.eventId;
    }

    /**
     * Returns the message content.
     *
     * @return The message content
     */
    public String getContent() {
        return this.content;
    }

    /**
     * Returns the message's signature, or {@code null} if the message is unsigned
     *
     * @return The message's signature (or {@link null})
     */
    public String getSignature() {
        return this.signature;
    }

    /**
     * Checks if the message is signed.
     *
     * @return {@code true}, if the message is signed, {@code false} otherwise
     */
    public boolean isSigned() {
        return this.signature != null;
    }

    @Override
    public String toString() {
        var shortId = this.id.substring(0, 4) + "..." + this.id.substring(this.id.length() - 4);
        return "{id=" + shortId + ", type=" + this.typeName + ", sender=" + this.senderId + ", receiver=" + this.receiverId + ", event=" + this.eventId + "}";
    }

    /**
     * The content of each message needs to be serialized before sending it to the receiver as part of a
     * {@link Message} object. This interface defines a serialization method, which needs to be provided in each
     * instantiation. Furthermore, every message content is linked to a particular message type.
     */
    public interface Content {

        /**
         * Generates a string that represents the serialized message content.
         *
         * @return The serialized message content
         */
        String serialize();

        /**
         * Returns the message type linked to the message content.
         *
         * @return The message type linked to the message content
         */
        Message.Type getType();

    }

    /**
     * This factory enables the construction of {@link Message.Content} objects by deserializing them from
     * corresponding strings representing the message content.
     */
    public interface ContentFactory {

        /**
         * Constructs a {@link Message.Content} object from a serialization string.
         *
         * @param clazz The class of the constructed message type
         * @param serializedContent The string representing the message content
         * @param <T> The generic type of the message content
         * @return The constructed message content object
         */
        <T extends Message.Content> T create(Class<T> clazz, String serializedContent);

    }

    /**
     * There are different types of messages in a protocol, depending on who sends a message to whom and for what
     * purpose. This also determines whether the message is signed, encrpyted, or signed and encrypted. Instances of
     * {@link Message.Type} define all these basic properties of a message. In an actual protocol implementation, such
     * types can be defined most conveniently using an enum class that implements this interface.
     */
    public interface Type {

        /**
         * Returns the name of the message type
         *
         * @return The name of the message type
         */
        String getName();

        /**
         * Returns the sender's party type
         *
         * @return The sender's party type
         */
        Party.Type getSenderType();

        /**
         * Returns the receiver's party type
         *
         * @return The receiver's party type
         */
        Party.Type getReceiverType();

        /**
         * Defines whether messages of that type are signed.
         *
         * @return {@code true}, if messages of that type are signed, {@code false} otherwise
         */
        boolean isSigned();

        /**
         * Defines whether messages of that type are encrypted.
         *
         * @return {@code true}, if messages of that type are encrypted, {@code false} otherwise
         */
        boolean isEncrypted();

    }

    /**
     * This factory enables the construction of {@link Message.Type} objects based on their names.
     */
    public interface TypeFactory {

        /**
         * Returns a {@link Message.Type} objects based with the give name
         * @param typeName The name of the message type
         * @return The constructed message type
         */
        Type create(String typeName);

    }

}
