/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.context;

import ch.openchvote.framework.Participant;
import ch.openchvote.framework.Message;

import java.util.*;
import java.util.stream.Stream;

/**
 * This is a container class for accumulating all the messages a party receives during the execution of an
 * election event. The message ids are stored in a {@link Set} to enable the detection of duplicate messages. The
 * messages themselves are stored in a hashmap for offering an efficient access.
 */
public class EventMessages {

    protected final Set<String> messageIds;
    protected final Map<String, Map<String, Message>> messages;

    /**
     * Constructs a new {@link EventMessages} instance by initializing the internal data structures.
     */
    public EventMessages() {
        this.messageIds = new HashSet<>();
        this.messages = new HashMap<>();
    }

    /**
     * Checks if a given message already exists. This is the case if either the message id already exists or if there is
     * a message with another id but of the same type.
     *
     * @param message The given message
     * @return {@code true}, if the message already exists, {@code false} otherwise
     */
    public boolean hasMessage(Message message) {
        if (this.messageIds.contains(message.getId())) {
            return true;
        }
        var messageMap = this.messages.get(message.getTypeName());
        if (messageMap == null) {
            return false;
        }
        return messageMap.containsKey(message.getSenderId());
    }

    /**
     * Adds a new message to the list of messages. If a message with the same id or of the same type already exists,
     * then the new message is ignored.
     *
     * @param message The message to add
     */
    public void addMessage(Message message) {
        this.messageIds.add(message.getId());
        var messageMap = this.messages.computeIfAbsent(message.getTypeName(), k -> new HashMap<>());
        messageMap.putIfAbsent(message.getSenderId(), message);
    }

    /**
     * Checks if a message from a given type has been received from every affected participant in the event setup.
     *
     * @param eventSetup The given event setup
     * @param messageType The given message type
     * @return {@code true}, if a message has been received form every participant, {@code false} otherwise
     */
    public boolean hasAllMessages(EventSetup eventSetup, Message.Type messageType) {
        var senderType = messageType.getSenderType();
        var participants = eventSetup.getParticipants(senderType);
        return this.hasAllMessages(messageType, participants);
    }

    /**
     * Checks if a message from a given type has been received from every affected participant in the event setup,
     * except from the party with the given party id.
     *
     * @param eventSetup The given event setup
     * @param messageType The given message type
     * @param partyId The id of the party to exclude
     * @return {@code true}, if a message has been received form every other participant, {@code false} otherwise
     */
    public boolean hasAllOtherMessages(EventSetup eventSetup, Message.Type messageType, String partyId) {
        var senderType = messageType.getSenderType();
        var participants = eventSetup.getOtherParticipants(senderType, partyId);
        return this.hasAllMessages(messageType, participants);
    }

    /**
     * Checks if a message from a given type has been received from every participant preceding the party with the
     * given party id in the event setup's list of participants.
     *
     * @param eventSetup The given event setup
     * @param messageType The given message type
     * @param partyId The id of the party to exclude
     * @return {@code true}, if a message has been received form every preceding participant, {@code false} otherwise
     */
    public boolean hasAllPrecedingMessages(EventSetup eventSetup, Message.Type messageType, String partyId) {
        var senderType = messageType.getSenderType();
        var participants = eventSetup.getPrecedingParticipants(senderType, partyId);
        return this.hasAllMessages(messageType, participants);
    }

    // helper method to check if a messages from all participants exists
    private boolean hasAllMessages(Message.Type messageType, List<Participant> participants) {
        var messageMap = this.messages.getOrDefault(messageType.getName(), new HashMap<>());
        return participants.stream().map(Participant::getPartyId).allMatch(messageMap::containsKey);
    }

    /**
     * Returns a shallow copy of all messages by iterating through all the existing messages and adding them into
     * a new instance. This method is called from {@link EventContext#getCopy()} to obtain a copy of the event data
     * object.
     *
     * @return A copy of all messages
     */

    public EventMessages getCopy() {
        var eventMessages = new EventMessages();
        this.getMessages().forEach(eventMessages::addMessage);
        return eventMessages;
    }

    // private helper method to obtain a stream of all messages
    private Stream<Message> getMessages() {
        return this.messages.keySet().stream()
                .map(this.messages::get)
                .flatMap(messageMap -> messageMap.keySet().stream().map(messageMap::get));
    }

}
