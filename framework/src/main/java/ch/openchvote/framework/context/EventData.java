/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.context;

import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.exceptions.EventDataException;

import java.lang.reflect.Field;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;

/**
 * This is an abstract base class for different event data classes of specific parties and protocols.
 */
public abstract class EventData {

    /**
     * Returns the id of the protocol's initial state.
     *
     * @return The id of the initial state
     */
    public String getInitialStateId() {
        return this.getInitialState().getSimpleName();
    }

    // abstract helper method to obtain the class of the protocol's initial state
    protected abstract Class<? extends State<?>> getInitialState();

    /**
     * This methdod is initalizes the event data based on a given event setup. In certain cases, it may be necessary
     * to extract from an event setup some information needed in the context of the event data.
     *
     * @param eventSetup The given event setup
     */
    public abstract void init(EventSetup eventSetup, String partyId);

    /**
     * Creates a new {@link EventData} object for a given party and protocol id. A corresponding {@link EventData}
     * sub-class must exist at the right location in the party's package structure. The name of this subclass must
     * be {@code EventData} and the package name must include the protocol id.
     *
     * @param party      The party requesting a new event data object
     * @param protocolId The given protocol id
     * @return The new event data object
     */
    public static EventData create(Party party, String protocolId) {
        try {
            var packageName = party.getClass().getPackageName();
            var clazz = Class.forName(packageName + "." + protocolId.toLowerCase() + "." + EventData.class.getSimpleName());
            return (EventData) clazz.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * Returns a shallow copy of this event data by iterating through all instance variable using Java reflection. This
     * method is called from {@link EventContext#getCopy()} to obtain a copy of the event data object.
     *
     * @return A shallow copy of this event data
     */
    public EventData getCopy() {
        try {
            EventData copy = this.getClass().getDeclaredConstructor().newInstance();
            for (Field field : this.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                field.set(copy, field.get(this));
            }
            return copy;
        } catch (ReflectiveOperationException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * This is a generic container class for event data values. A newly created container is empty until its value has
     * been set. After setting the container's value, it cannot be overridden.
     *
     * @param <V> The generic type of the value
     */
    public static class Value<V> {

        private V value;

        /**
         * Returns the value stored in the container.
         *
         * @return The value stored in the container
         * @throws EventDataException if the value has not been set
         */
        public V get() {
            if (this.value == null) {
                throw new EventDataException(EventDataException.Type.INVALID_READ_ACCESS);
            } else {
                return value;
            }
        }

        /**
         * Stores the given value into the container.
         *
         * @param value The given value
         * @throws EventDataException if the value already exist
         */
        public void set(V value) {
            if (this.value == null) {
                this.value = value;
            } else {
                throw new EventDataException(EventDataException.Type.INVALID_WRITE_ACCESS);
            }
        }

        /**
         * Stores the given value into the container unless a value already exist.
         *
         * @param value The given value
         */
        public void setIfAbsent(V value) {
            if (this.value == null) {
                this.value = value;
            }
        }

        /**
         * Checks if a value is present in the container.
         *
         * @return {@code true}, if the value is present, {@code false} otherwise
         */
        public boolean isPresent() {
            return this.value != null;
        }

    }

    /**
     * This is a generic container class for indexed event data values. Newly created containers are empty. When a
     * value is set for a given index, in cannot be overriden. The range of the permitted indices is unrestricted. When
     * all values are set, the collection of values can be mapped into a target data structure (list, set, vector, etc.).
     * The function responsible for this mapping must be defined at construction. Once the target data structure has
     * been generated, it is no longer permitted to add further values to the container.
     *
     * @param <V> The generic type of the values
     * @param <W> The generic type of the target data structure
     */
    public static class ValueMap<V, W> {

        private final SortedMap<Integer, V> valueMap;
        private final Function<? super SortedMap<Integer, V>,? extends W> finalMapping;
        private W finalValue;

        public ValueMap(Function<? super SortedMap<Integer, V>,? extends W> finalMapping){
            this.valueMap = new TreeMap<>();
            this.finalMapping = finalMapping;
        }

        public V get(int index) {
            var value = this.valueMap.get(index);
            if (value == null) {
                throw new EventDataException(EventDataException.Type.INVALID_READ_ACCESS);
            } else {
                return value;
            }
        }

        public W get() {
            if (this.finalValue == null) {
                this.finalValue = this.finalMapping.apply(this.valueMap);
            }
            return this.finalValue;
        }

        public void set(int index, V value) {
            if (this.finalValue == null && !this.valueMap.containsKey(index)) {
                this.valueMap.put(index, value);
            } else {
                throw new EventDataException(EventDataException.Type.INVALID_WRITE_ACCESS);
            }
        }

        public boolean isPresent(int i) {
            return this.valueMap.containsKey(i);
        }

        public boolean isPresent() {
            return this.finalValue != null;
        }

    }

}
