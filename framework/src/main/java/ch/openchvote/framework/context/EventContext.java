/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.context;

import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;

/**
 * Instances of this class represent a party's data related to an election event. It stores the current {@link State}, the
 * {@link EventSetup}, the {@link EventData}, and all successfully handled messages. While the event setup is fixed
 * when the context is created, both the event data and the list of messages accumulate more and more information during
 * the execution of the election event.
 */
public class EventContext {

    protected String currentStateId;
    protected final EventSetup eventSetup;
    protected final EventData eventData;
    protected final EventMessages eventMessages;

    /**
     * Constructs a new event context for a given {@link EventSetup}. All fields are initialized accordingly.
     *
     * @param party The party requesting the creation of an event context
     * @param eventSetup The given event setup
     */
    public EventContext(Party party, EventSetup eventSetup) {
        // create event data
        var eventData = EventData.create(party, eventSetup.getProtocolId());
        eventData.init(eventSetup, party.getId());

        // initialize fields
        this.currentStateId = eventData.getInitialStateId();
        this.eventSetup = eventSetup;
        this.eventData = eventData;
        this.eventMessages = new EventMessages();
    }

    // private constructor used for making copies
    private EventContext(String stateId, EventSetup eventSetup, EventMessages eventMessages, EventData eventData) {
        this.currentStateId = stateId;
        this.eventSetup = eventSetup;
        this.eventMessages = eventMessages;
        this.eventData = eventData;
    }

    /**
     * Returns the id of the current state.
     *
     * @return The current state id
     */
    public String getCurrentStateId() {
        return this.currentStateId;
    }

    /**
     * Returns the event setup.
     *
     * @return The event setup
     */
    public EventSetup getEventSetup() {
        return this.eventSetup;
    }

    /**
     * Returns the list of event messages.
     *
     * @return The event messages
     */
    public EventMessages getEventMessages() {
        return this.eventMessages;
    }

    /**
     * Returns the event data.
     *
     * @return The event data
     */
    public EventData getEventData() {
        return this.eventData;
    }

    /**
     * Sets the current state to a new one.
     *
     * @param stateClass The class of the new state
     */
    public void setCurrentState(Class<? extends State<?>> stateClass) {
        this.currentStateId = stateClass.getSimpleName();
    }

    /**
     * Returns a copy of this event context. This method can be used for implementing a simple persistence service.
     *
     * @return A copy of this event context
     */
    public EventContext getCopy() {
        return new EventContext(this.currentStateId, this.eventSetup, this.eventMessages.getCopy(), this.eventData.getCopy());
    }

}
