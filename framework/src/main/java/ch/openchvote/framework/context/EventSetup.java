/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.context;

import ch.openchvote.framework.Participant;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.exceptions.EventSetupException;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ch.openchvote.framework.exceptions.EventSetupException.Type.PARTICIPANT_ALREADY_EXISTS;
import static ch.openchvote.framework.exceptions.EventSetupException.Type.PARTICIPANT_DOES_NOT_EXIST;

/**
 * Instances of this class define the general setup of an election event, which mainly consists of the list of
 * all participating parties. It also defines the protocol used for the election event and the security level. This
 * abstraction is important for allowing parties to be involved simultaneously in multiple election events with
 * different settings.
 */
public class EventSetup {

    private final String eventId;
    private final String protocolId;
    private final int securityLevel;
    private final List<Participant> participants;
    private final Set<String> partyIds;

    /**
     * Constructs a new event setup object and initialized all fields.
     *
     * @param eventId       A unique event id
     * @param protocolId    A unique protocol id
     * @param securityLevel The selected security level
     */
    public EventSetup(String eventId, String protocolId, int securityLevel) {
        this.eventId = eventId;
        this.protocolId = protocolId;
        this.securityLevel = securityLevel;
        this.participants = new ArrayList<>();
        this.partyIds = new HashSet<>();
    }

    /**
     * Returns the event id.
     *
     * @return The event id
     */
    public String getEventId() {
        return this.eventId;
    }

    /**
     * Returns the event's protocol id.
     *
     * @return The protocol id
     */
    public String getProtocolId() {
        return this.protocolId;
    }

    /**
     * Returns the event's security level.
     *
     * @return The security level
     */
    public int getSecurityLevel() {
        return this.securityLevel;
    }

    /**
     * Adds a new party as participant to the event setup.
     *
     * @param party The participating party
     */
    public void addParticipant(Party party) {
        var id = party.getId();
        if (this.partyIds.contains(id)) {
            throw new EventSetupException(PARTICIPANT_ALREADY_EXISTS);
        }
        Participant participant;
        var typeName = party.getType().getName();
        var certificate = party.getCertificate(this.securityLevel);
        participant = certificate.map(value -> new Participant(id, typeName, value)).orElseGet(() -> new Participant(id, typeName));
        this.participants.add(participant);
        this.partyIds.add(id);
    }

    /**
     * Checks if a party is part of this event setup.
     *
     * @param partyId The party id
     * @return {@code true}, if the party is part of this events setup, {@code false} otherwise
     */
    public boolean hasParticipant(String partyId) {
        return this.participants.stream().anyMatch(participant -> participant.getPartyId().equals(partyId));
    }

    /**
     * Returns the participant for a given party id. If no such participant exist, an {link EventSetupException} is
     * thrown.
     *
     * @param partyId The party id
     * @return The participant for the given party id
     * @throws EventSetupException if no such participant exists
     */
    public Participant getParticipant(String partyId) {
        return this.participants.stream()
                .filter(participant -> participant.getPartyId().equals(partyId))
                .findFirst().orElseThrow(() -> new EventSetupException(PARTICIPANT_DOES_NOT_EXIST));
    }

    /**
     * Returns the list of all participants.
     *
     * @return The list of all participants
     */
    public List<Participant> getParticipants() {
        return Collections.unmodifiableList(this.participants);
    }

    /**
     * Returns the list of all participants of a given party type.
     *
     * @param partyType The party type
     * @return The list of all participants of the given party type
     */
    public List<Participant> getParticipants(Party.Type partyType) {
        return this.getAllParticipants(partyType)
                .collect(Collectors.toList());
    }

    /**
     * Returns the list of all participants of a given party type except the one with the given party id.
     *
     * @param partyType The party type
     * @param partyId   The id of the party to exclude
     * @return The list of all participants of the given party type except the one with the given party id
     */
    public List<Participant> getOtherParticipants(Party.Type partyType, String partyId) {
        return this.getAllParticipants(partyType)
                .filter(participant -> !participant.getPartyId().equals(partyId))
                .collect(Collectors.toList());
    }

    /**
     * Returns the list of all participants of a given party type that precede the party with the
     * given party id in the event setup's list of participants.
     *
     * @param partyType The party type
     * @param partyId   The party id
     * @return The list of all participants of the given party type that precede the party with the
     * given party id
     */
    public List<Participant> getPrecedingParticipants(Party.Type partyType, String partyId) {
        return this.getAllParticipants(partyType)
                .takeWhile(participant -> !participant.getPartyId().equals(partyId))
                .collect(Collectors.toList());
    }

    /**
     * Returns the index in the event setup's list of participants of the party with the given id.
     *
     * @param partyType The party type
     * @param partyId   The party id
     * @return The index of the party in the list of participant
     */
    public int getParticipantIndex(Party.Type partyType, String partyId) {
        return (int) this.getAllParticipants(partyType)
                .takeWhile(participant -> !participant.getPartyId().equals(partyId))
                .count() + 1;
    }

    /**
     * Returns the participant of a given party type with the specified index in the event setup's list of participants.
     *
     * @param partyType The party type
     * @param index     The index
     * @return The participant with the specified index in the event setup's list of participants
     */
    public Participant getParticipant(Party.Type partyType, int index) {
        return this.getAllParticipants(partyType)
                .skip(index - 1)
                .findFirst()
                .orElseThrow(() -> new EventSetupException(PARTICIPANT_DOES_NOT_EXIST));
    }

    /**
     * Counts the number of participants for a given party type.
     *
     * @param partyType The party type
     * @return The number of participants of that type
     */
    public int getMaxParticipantIndex(Party.Type partyType) {
        return (int) this.getAllParticipants(partyType).count();
    }

    // private helper method to obtain a stream of all participant of a given type
    private Stream<Participant> getAllParticipants(Party.Type partyType) {
        return this.participants.stream().filter(participant -> participant.getPartyType().equals(partyType.getName()));
    }

}
