/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework;

import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

/**
 * This is a generic base class for a party's states in a protocol. The generic type parameter {@code <P>}
 * represents the type of the party the deals with a particular state object. State classes inheriting from {@link State}
 * need to be defined for every protocol and every party in that protocol. State transitions are implemented by
 * overriding {@link State#handleMessage(Message, EventContext)} or {@link State#handleInternalMessage(EventContext)}.
 * The default behaviour of the former is to throw an exception, whereas the default behaviour of the latter is ignoring
 * the internal message.
 *
 * @param <P> The type of the state's party
 */
public class State<P extends Party> {

    // this variable defines the name of the package which contains the state classes of a given protocol
    private static final String STATES_PACKAGE_NAME = "states";

    protected final P party;
    protected final Type type;

    /**
     * This method uses Java Reflection for constructing a state object for given protocol and state ids.
     *
     * @param protocolId The protocol id
     * @param stateId The state id
     * @param party The party
     * @return A new state object for the given protocol and state ids
     */
    public static State<?> create(String protocolId, String stateId, Party party) {
        try {
            var clazz = Class.forName(party.getClass().getPackageName() + "." + protocolId.toLowerCase() + "." + STATES_PACKAGE_NAME + "." + stateId);
            return (State<?>) clazz.getDeclaredConstructor(Party.class).newInstance(party);
        } catch (ReflectiveOperationException exception) {
            throw new RuntimeException(exception);
        }
    }

    @SuppressWarnings("unchecked")
    /*
      This is the base constructor of this class, which must be called by corresponding constructors of all
      sub-classes. It links the state with the party and setups the state's type.
     */
    protected State(Party party, Type type) {
        this.party = (P) party;
        this.type = type;
    }

    /**
     * Returns the type of the state.
     *
     * @return The type of the state
     */
    public Type getType() {
        return this.type;
    }

    /**
     * This method is called by the party's method {@link Party#onMessage(Message)} to trigger the processing of an
     * external message received from another party. This base class defines the method's default behaviour, which
     * consists in throwing an exception indicating that the current state is unable to process the message. State
     * classes inheriting from this class must override this method for obtaining the desired behaviour.
     *
     * @param message The message received from another party
     * @param context The context of the corresponding election event
     * @throws MessageException if the current state is unable to process the message
     */
    public void handleMessage(Message message, EventContext context) {
        throw new MessageException(message, UNKNOWN_TYPE);
    }

    /**
     * This method is called by the party's method {@link Party#onInternalMessage(String)} to trigger the processing of
     * an internal message. This base class defines the method's default behaviour, which consists
     * in ignoring the internal message. State classes inheriting from this class must override this method for
     * obtaining the desired behaviour.
     *
     * @param context The context of the corresponding election event
     */
    public void handleInternalMessage(EventContext context) {
        // do nothing
    }

    /**
     * There are four different types of state in every state diagram. This enum type represents them. It is useful for
     * a state to 'know' its type, for example to distinguish an error state from a normal final state.
     */
    public enum Type {
        START, REGULAR, FINAL, ERROR
    }

}
