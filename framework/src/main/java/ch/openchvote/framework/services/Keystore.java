/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.services;

import ch.openchvote.framework.security.Certificate;
import ch.openchvote.framework.security.KeyGenerator;

import java.util.Optional;

/**
 * This interface defines the basic functionality of a keystore, which consists of (privateKey,certificate)-pairs. Such
 * pairs are called {@link Keystore.Entry}.
 */
public interface Keystore {

    /**
     * Add a new entry to the keystore.
     *
     * @param entry The new entry
     */
    void addEntry(Entry entry);

    /**
     * Returns the certificate for a given security level.
     *
     * @param securityLevel The given security level.
     * @return The certificate for the given security level.
     */
    Optional<Certificate> getCertificate(int securityLevel);

    /**
     * Returns the private key for a given certificate.
     *
     * @param certificate The given certificate
     * @return The corresponding private key
     */
    Optional<String> getPrivateKey(Certificate certificate);

    /**
     * Uses the given {@link KeyGenerator} for generating new keys and certificates for all security levels known to the
     * key generator. The new keys are added to the keystore.
     *
     * @param subject The subject owning the keys
     * @param keyGenerator The given key generator
     */
    void generateKeys(String subject, KeyGenerator keyGenerator);

    interface Entry {
        String getPrivateKey();
        Certificate getCertificate();
    }

}
