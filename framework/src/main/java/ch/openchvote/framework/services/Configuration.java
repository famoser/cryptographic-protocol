/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.services;

import ch.openchvote.framework.Party;

import java.io.IOException;
import java.util.Properties;

/**
 * Instances of this class can be used to load a party's configuration from a configuration file. Such a configuration
 * may consists of a {@link MessagingService}, a {@link PersistenceService}, a {@link Logger}, and a {@link Keystore}.
 * The class provides corresponding methods for creating corresponding objects.
 */
public class Configuration {

    // the name of the configuration file
    private static final String CONFIG_FILE = "config.properties";

    // the properties loaded from the configuration file
    private final Properties properties;

    /**
     * Constructs a new configuration object for a given party. It tries to load the configuration file into
     * a {@link Properties} object, which is stored in an instance variable. Each entry in the property list defines
     * one of the supported services. They can then be accessed through corresponding getter methods.
     */
    public Configuration(Party party) {
        this.properties = new Properties();
        try (var inputStream = party.getClass().getResourceAsStream(CONFIG_FILE)) {
            this.properties.load(inputStream);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    /**
     * Returns the messaging service of this configuration.
     *
     * @return The messaging service
     */
    public MessagingService getMessagingService() {
        return this.getInstance("messagingService", MessagingService.class);
    }

    /**
     * Returns the persistence service of this configuration.
     *
     * @return The persistence service
     */
    public PersistenceService getPersistenceService() {
        return this.getInstance("persistenceService", PersistenceService.class);
    }

    /**
     * Returns the logger of this configuration.
     *
     * @return The logger
     */
    public Logger getLogger() {
        return this.getInstance("loggerClass", Logger.class);
    }

    /**
     * Returns the keystore of this configuration.
     *
     * @return The keystore
     */
    public Keystore getKeystore() {
        return this.getInstance("keystoreClass", Keystore.class);
    }

    @SuppressWarnings("unchecked")
    // private helper method to construct the service object from a property name
    private <T> T getInstance(String propertyName, Class<T> type) {
        try {
            var clazz = Class.forName(this.properties.getProperty(propertyName));
            return (T) clazz.getDeclaredConstructor().newInstance();
        } catch (ReflectiveOperationException exception) {
            throw new RuntimeException(exception);
        }
    }

}
