/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.services;

/**
 * This interface defines the core functionality of a logger, which is used by the parties to document incidents
 * that happen during a protocol run. The logger supports various log levels and modes of operations, which are defined
 * in respective enum classes {@link Level} and {@link Mode}. When a log entry is added by calling
 * {@link Logger#log(Level, String)}, the behaviour of the logger depends on both the log's level and the logger's
 * mode of operation.
 */
public interface Logger {

    /**
     * The logger's default modes of operation.
     */
    enum Mode {NONE, OUTPUT, ERROR, WARNING, INFO, DEBUG}

    /**
     * The supported log levels.
     */
    enum Level {OUTPUT, ERROR, WARNING, INFO, DEBUG}

    /**
     * Adds a new entry to the log history. The log consists of a string message and is atrributed to one of the
     * available log levels.
     *
     * @param level The given log level
     * @param message The given log message
     */
    void log(Level level, String message);

    /**
     * Sets the logger's mode of operation to a new value.
     *
     * @param mode The new mode of operation
     */
    void setMode(Mode mode);

}
