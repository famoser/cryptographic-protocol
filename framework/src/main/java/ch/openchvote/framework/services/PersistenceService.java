/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.services;

import ch.openchvote.framework.context.EventContext;

/**
 * A party participating in a protocol requires a persistence service to persist the current {@link EventContext} of a
 * given election event. Usually, for making the transitions from one state to another atomic, changes to an event
 * context should be synchronized. Loading and saving an event context should therefore be embraced by locking and
 * unlocking the event. Classes implementing this interface provide methods for exactly this purpose.
 */
public interface PersistenceService {

    /**
     * Locks an election event for allowing changes to its context.
     *
     * @param eventId The event id
     */
    void lockEvent(String eventId);

    /**
     * Loads the persisted context of an election event.
     *
     * @param eventId The election id
     * @return The pesisted event context
     */
    EventContext loadContext(String eventId);

    /**
     * Persists the events context of an election event.
     */
    void saveContext(EventContext context);

    /**
     * Unlocks an election event.
     *
     * @param eventId The event id
     */
    void unlockEvent(String eventId);

}
