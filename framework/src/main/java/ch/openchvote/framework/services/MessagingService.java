/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework.services;

import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;

/**
 * An message service is needed to connect a party participating in a protocol with the other protocol participants. The
 * interface provides methods for registering the party to the messaging service, for sending a {@link Message} to
 * another party, and for sending internal messages to the party itself.
 */
public interface MessagingService {

    /**
     * Registers the given party to the messaging service.
     *
     * @param party The given party
     */
    void registerParty(Party party);

    /**
     * Registers the given partyID to the messaging service.
     *
     * @param partyId The given partyId
     */
    void registerParticipant(String partyId);

    /**
     * Sends the given message to the intended recipient.
     *
     * @param message The given message
     */
    void sendMessage(Message message);

    /**
     * Sends an internal message for the given event to the party itself.
     *
     * @param senderId The sender's party id
     * @param eventId The given event id
     */
    void sendInternalMessage(String senderId, String eventId);

}
