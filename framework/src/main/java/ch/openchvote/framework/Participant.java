/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.framework;

import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.framework.security.Certificate;

/**
 * Instances of this class represents parties participating in a particular election event. For some participating parties,
 * a {@link Certificate} is assigned to allow the party to sign messages or receive encrypted messages. The goal of this
 * class is to allow parties to use different certificates in different election events. All fields other than
 * {@link Participant#certificate} are of type {@link String}, but certificates are also composed of strings. This facilitates
 * the serialization of the list of participants in an {@link EventSetup}.
 */
public class Participant {

    private final String partyId;
    private final String partyType;
    private final Certificate certificate;

    // short version of constructor for participants without certificate

    /**
     * This constructor is a short version of {@link Participant#Participant(String, String, Certificate)} for
     * participants without certificate.
     *
     * @param partyId The participating party's id
     * @param partyType The type of the participating party
     */
    public Participant(String partyId, String partyType) {
        this(partyId, partyType, null);
    }

    /**
     * Creates a new participant for an election event.
     *
     * @param partyId The participating party's id
     * @param partyType The type of the participating party
     * @param certificate The participant's certificate
     */
    public Participant(String partyId, String partyType, Certificate certificate) {
        this.partyId = partyId;
        this.partyType = partyType;
        this.certificate = certificate;
    }

    /**
     * Returns the id of the participating party.
     *
     * @return The participating party's id
     */
    public String getPartyId() {
        return this.partyId;
    }

    /**
     * Returns the type of the participating party.
     *
     * @return The participating party's tpe
     */
    public String getPartyType() {
        return this.partyType;
    }

    /**
     * Returns the certificate of the participating party, or {@code null} if the participant has no certificate.
     *
     * @return The participating party's certificate (or {@code null})
     */
    public Certificate getCertificate() {
        return this.certificate;
    }

    /**
     * Checks if the participant has a certificate.
     *
     * @return {@code true}, if the participant has a certificate, {@code false} otherwise
     */
    public boolean hasCertificate() {
        return this.certificate != null;
    }

}
