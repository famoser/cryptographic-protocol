# Election Authority

This module provides an implementation of the CHVote party *"Election Authority"*. In the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the election authority is described as follows:

> A group of election authorities guarantees the integrity and privacy of the votes submitted during the election period. They are numbered with indices *j=1,...,  s*. During the pre-election phase, they establish jointly a public ElGamal encryption key *pk*. They also generate the credentials and codes to be printed on the voting cards. During vote casting, they respond to the submitted ballots and confirmations. At the end of the election period, they perform a cryptographic shuffle of the encrypted votes. Finally, they use their private key shares *sk_j* to decrypt the votes in a distributed manner. During the protocol execution, the keep track of all their incoming and outgoing messages, which they provide as input for the universal verification process.

The intention of this Maven module is to provide production-quality Java code that implements the election authority's role in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by simply changing the module's configuration file <code>config.properties</code>.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagrams

#### Pre-Election

![PreElection](img/ElectionAuthority-PreElection.svg)

#### Election

![Election](img/ElectionAuthority-Election.svg)

#### Post-Election

![Post-Election](img/ElectionAuthority-PostElection.svg)

## Messages

#### Incoming
| Protocol | Message | Sender             | Signed | Encrypted | Content |    |
:---------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
|  7.1     | MAE1    | Administrator      | yes    | no        | Public key share          | ![MAE1](img/MAE1.png) |
|  7.1     | MEE1    | Election authority | yes    | no        | Public key share          | ![MEE1](img/MEE1_k.png) |
|  7.2     | MEE2    | Election authority | yes    | no        | Public voting credentials | ![MEE2](img/MEE2_k.png) |
|  7.5     | MCE1    | Voting client      | no     | no        | Voter index               | ![MCE1](img/MCE1.png) |
|  7.5     | MCE2    | Voting client      | no     | no        | Ballot                    | ![MCE2](img/MCE2.png) |
|  7.6     | MCE3    | Voting client      | no     | no        | Confirmation              | ![MCE3](img/MCE3.png) |
|  7.7     | MEE3    | Election authority | yes    | no        | Shuffled encryptions      | ![MEE3](img/MEE3_k.png) |
|  7.8     | MEE4    | Election authority | yes    | no        | Partial decryptions       | ![MEE4](img/MEE4_k.png) |
|  7.10    | MCE4    | Voting client      | no     | no        | Voter index               | ![MCE4](img/MCE4.png) |
| –        | MXE1    | Coordinator        | yes    | no        | Start vote casting        | – |
| –        | MXE2    | Coordinator        | yes    | no        | Start post-election       | – |
| –        | MXE3    | Coordinator        | yes    | no        | Election event finished   | – |

#### Outgoing
| Protocol | Message | Receiver           | Signed | Encrypted | Content |    |
:---------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
|  7.1     | MEE1    | Election authority | yes    | no        | Public key share             | ![MEE1](img/MEE1.png) |
|  7.2     | MEE2    | Election authority | yes    | no        | Public voting credentials    | ![MEE2](img/MEE2.png) |
|  7.3     | MEP1    | Printing authority | yes    | yes       | Voting card data             | ![MEP1](img/MEP1.png) |
|  7.5     | MEC1    | Voting client      | yes    | no        | Public key share             | ![MEC1](img/MEC1.png) |
|  7.5     | MEC2    | Voting client      | yes    | no        | Response                     | ![MEC2](img/MEC2.png) |
|  7.6     | MEC3    | Voting client      | yes    | no        | Finalization                 | ![MEC3](img/MEC3.png) |
|  7.7     | MEE3    | Election authority | yes    | no        | Shuffled encryptions         | ![MEE3](img/MEE3.png) |
|  7.8     | MEE4    | Election authority | yes    | no        | Partial decryptions          | ![MEE4](img/MEE4.png) |
|  7.9     | MEA1    | Administrator      | yes    | no        | Combined partial decryptions | ![MEA1](img/MEA1.png) |
|  7.10    | MEC4    | Voting client      | yes    | no        | Inspection                   | ![MEC4](img/MEC4.png) |
| –        | MEX1    | Coordinator        | yes    | no        | Pre-election done            | – |
| –        | MEX2    | Coordinator        | yes    | no        | Post-election done           | – |

## Tasks

| Protocol | Task | States | Description  |
|:--------:|:----:|:------:|:-------------|
| 7.1      | T1   | S1, S2 | ![T1](img/T1.png) |
| 7.1      | T2   | S1     | ![T2](img/T2.png) |
| 7.1      | T3   | S2     | ![T3](img/T3.png) |
| 7.2      | T4   | S2     | ![T4](img/T4.png) |
| 7.2      | T5   | S2, S3 | ![T5](img/T5.png) |
| 7.2      | T6   | S3     | ![T6](img/T6.png) |
| 7.5      | T7   | S5     | ![T7](img/T7.png) |
| 7.5      | T8   | S5     | ![T8](img/T8.png) |
| 7.6      | T9   | S5     | ![T9](img/T9.png) |
| 7.7      | T10  | S5     | ![T10](img/T10.png) |
| 7.7      | T11  | S6, S7 | ![T11](img/T11.png) |
| 7.7      | T12  | S6     | ![T12](img/T12.png) |
| 7.8      | T13  | S7     | ![T13](img/T13.png) |
| 7.8      | T14  | S7, S8 | ![T14](img/T14.png) |
| 7.8      | T15  | S8     | ![T15](img/T15.png) |
| 7.10     | T16  | S9     | ![T16](img/T16.png) |
