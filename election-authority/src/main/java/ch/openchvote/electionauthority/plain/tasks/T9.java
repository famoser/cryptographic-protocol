/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.common.CheckConfirmation;
import ch.openchvote.algorithms.plain.GetVotingParameters;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.plain.VotingParameters;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.util.tuples.Pair;

public class T9 {

    public static Pair<VotingParameters, Finalization> run(int v, Confirmation gamma, EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();
        var bold_y_hat = eventData.bold_y_hat.get();
        var bold_z_hat = eventData.bold_z_hat.get();
        var B_j = eventData.B_j.get();
        var C_j = eventData.C_j.get();
        var F_j = eventData.F_j.get();

        // perform task
        if (!CheckConfirmation.run(v, gamma, bold_y_hat, bold_z_hat, B_j, C_j, params)) {
            throw new TaskException(T9.class, TaskException.Type.INVALID_CONFIRMATION);
        }
        C_j.append(v, gamma);
        var delta_j = F_j.search(v);
        var VP_v = GetVotingParameters.run(v, EP);

        // return data
        return new Pair<>(VP_v, delta_j);
    }

}
