/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.plain.tasks.T14;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.*;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.Message;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.electionauthority.plain.tasks.T11;
import ch.openchvote.electionauthority.plain.tasks.T13;
import ch.openchvote.util.math.VMGJFassade;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S7 extends State<ElectionAuthority> {

    public S7(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEE3:
                this.handleMEE3(message, context);
                break;
            case MEE4:
                this.handleMEE4(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEE3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE3.class, message, EP, eventSetup);
        var bold_e_tilde_k = messageContent.get_bold_e_tilde();
        var pi_tilde_k = messageContent.get_pi_tilde();

        // get sender's participant index
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_E_tilde.set(k, bold_e_tilde_k);
        eventData.bold_pi_tilde.set(k, pi_tilde_k);

        try {
            // perform task
            if (eventData.bold_E_tilde.isPresent(k - 1) && eventData.bold_pi_tilde.isPresent(k - 1)) {
                T11.run(k, eventData, params);
            }
            if (eventData.bold_E_tilde.isPresent(k + 1) && eventData.bold_pi_tilde.isPresent(k + 1)) {
                T11.run(k + 1, eventData, params);
            }

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E9.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E10.class);
        }
    }

    private void handleMEE4(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE4.class, message, EP, eventSetup);
        var bold_c_k = messageContent.get_bold_c();
        var pi_prime_k = messageContent.get_pi_prime();

        // get sender's participant index
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_C.set(k, bold_c_k);
        eventData.bold_pi_prime.set(k, pi_prime_k);

        try {
            // perform task
            T14.run(k, eventData, params);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E13.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E14.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        if (eventMessages.hasAllOtherMessages(eventSetup, MessageType.MEE3, this.party.getId())) {

            // select event data
            int j = eventData.j.get();
            var EP = eventData.EP.get();
            var pk = eventData.pk.get();

            // release precomputation table
            VMGJFassade.releasePrecomputationTable(pk);

            try {
                // perform task
                T13.run(eventData, params);

                // select event data
                var bold_c_j = eventData.bold_C.get(j);
                var pi_prime_j = eventData.bold_pi_prime.get(j);

                // send MEE4 message to election authorities
                this.party.sendMessage(new MEE4(bold_c_j, pi_prime_j), EP, eventSetup);

                // update state
                context.setCurrentState(S8.class);

                // send internal message
                this.party.sendInternalMessage(eventSetup);

            } catch (AlgorithmException exception) {
                // move on to error state
                context.setCurrentState(E12.class);
            }
        }
    }
}
