/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.common.GetInspection;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.util.ByteArray;

public class T16 {

    public static ByteArray run(int v, EventData eventData, Parameters params) {

        // select event data
        var bold_a_j = eventData.bold_a_j.get();
        var bold_P_j = eventData.bold_P_j.get();
        var C_j = eventData.C_j.get();

        // perform task
        var I_j = GetInspection.run(v, bold_P_j, bold_a_j, C_j, params);

        // return data
        return I_j;
    }

}
