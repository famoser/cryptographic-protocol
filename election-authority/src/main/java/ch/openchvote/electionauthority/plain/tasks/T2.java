/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.plain.GenKeyPair;
import ch.openchvote.algorithms.plain.GenKeyPairProof;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.parameters.Parameters;

public class T2 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        int j = eventData.j.get();

        // perform task
        var pair = GenKeyPair.run(params);
        var sk_j = pair.getFirst();
        var pk_j = pair.getSecond();
        var pi_j = GenKeyPairProof.run(sk_j, pk_j, params);

        // update event data
        eventData.sk_j.set(sk_j);
        eventData.bold_pk.set(j, pk_j);
        eventData.bold_pi.set(j, pi_j);
    }

}
