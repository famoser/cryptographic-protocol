/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.common.GenCredentialProof;
import ch.openchvote.algorithms.common.GenElectorateData;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.parameters.Parameters;

public class T4 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        int j = eventData.j.get();
        int s = eventData.s.get();
        var EP = eventData.EP.get();

        // perform task
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();
        var electorateData = GenElectorateData.run(bold_n, bold_k, bold_E, s, params);
        var bold_a_j = electorateData.get_bold_a();
        var bold_d_j = electorateData.get_bold_d();
        var bold_x_j = electorateData.get_bold_x();
        var bold_y_j = electorateData.get_bold_y();
        var bold_z_j = electorateData.get_bold_z();
        var bold_x_hat_j = electorateData.get_bold_x_hat();
        var bold_y_hat_j = electorateData.get_bold_y_hat();
        var bold_z_hat_j = electorateData.get_bold_z_hat();
        var bold_P_j = electorateData.get_bold_P();
        var pi_hat_j = GenCredentialProof.run(bold_x_j, bold_y_j, bold_z_j, bold_x_hat_j, bold_y_hat_j, bold_z_hat_j, params);

        // update event data
        eventData.bold_a_j.set(bold_a_j);
        eventData.bold_d_j.set(bold_d_j);
        eventData.bold_X_hat.set(j, bold_x_hat_j);
        eventData.bold_Y_hat.set(j, bold_y_hat_j);
        eventData.bold_Z_hat.set(j, bold_z_hat_j);
        eventData.bold_pi_hat.set(j, pi_hat_j);
        eventData.bold_P_j.set(bold_P_j);
    }

}
