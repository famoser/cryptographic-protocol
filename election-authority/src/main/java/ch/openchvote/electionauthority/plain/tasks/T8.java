/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.common.GenResponse;
import ch.openchvote.algorithms.plain.CheckBallot;
import ch.openchvote.algorithms.plain.GetVotingParameters;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.model.common.Response;
import ch.openchvote.model.plain.Ballot;
import ch.openchvote.model.plain.VotingParameters;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.util.tuples.Pair;

public class T8 {

    public static Pair<VotingParameters, Response> run(int v, Ballot alpha, EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();
        var pk = eventData.pk.get();
        var bold_x_hat = eventData.bold_x_hat.get();
        var bold_P_j = eventData.bold_P_j.get();
        var B_j = eventData.B_j.get();
        var F_j = eventData.F_j.get();

        // perform task
        var bold_n = EP.get_bold_n();
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();
        if (!CheckBallot.run(v, alpha, pk, bold_k, bold_E, bold_x_hat, B_j, params)) {
            throw new TaskException(T8.class, TaskException.Type.INVALID_BALLOT);
        }
        B_j.append(v, alpha);
        var bold_a = alpha.get_bold_a();
        var pair = GenResponse.run(v, bold_a, pk, bold_n, bold_k, bold_E, bold_P_j, params);
        var beta_j = pair.getFirst();
        var delta_j = pair.getSecond();
        F_j.append(v, delta_j);
        var VP_v = GetVotingParameters.run(v, EP);

        // return data
        return new Pair<>(VP_v, beta_j);
    }

}
