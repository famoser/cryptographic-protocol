/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.common.CheckCredentialProof;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;

public class T5 {

    public static void run(int k, EventData eventData, Parameters params) {

        // select event data
        var bold_x_hat_k = eventData.bold_X_hat.get(k);
        var bold_y_hat_k = eventData.bold_Y_hat.get(k);
        var bold_z_hat_k = eventData.bold_Z_hat.get(k);
        var pi_hat_k = eventData.bold_pi_hat.get(k);

        // perform task
        if (!CheckCredentialProof.run(pi_hat_k, bold_x_hat_k, bold_y_hat_k, bold_z_hat_k, params)) {
            throw new TaskException(T5.class, TaskException.Type.INVALID_ZKP_PROOF);
        }
    }

}
