/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.plain.GenDecryptionProof;
import ch.openchvote.algorithms.plain.GetDecryptions;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.parameters.Parameters;

public class T13 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        int j = eventData.j.get();
        int s = eventData.s.get();
        var sk_j = eventData.sk_j.get();
        var pk_j = eventData.bold_pk.get(j);
        var bold_e_tilde_s = eventData.bold_E_tilde.get(s);

        // perform task
        var bold_c_j = GetDecryptions.run(bold_e_tilde_s, sk_j, params);
        var pi_prime_j = GenDecryptionProof.run(sk_j, pk_j, bold_e_tilde_s, bold_c_j, params);

        // update event data
        eventData.bold_C.set(j, bold_c_j);
        eventData.bold_pi_prime.set(j, pi_prime_j);
    }

}
