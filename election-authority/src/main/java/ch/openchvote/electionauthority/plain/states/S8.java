/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.electionauthority.plain.tasks.T14;
import ch.openchvote.electionauthority.plain.tasks.T15;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MEA1;
import ch.openchvote.protocol.message.plain.MEE4;
import ch.openchvote.protocol.message.plain.MEX2;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S8 extends State<ElectionAuthority> {

    public S8(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEE4:
                this.handleMEE4(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEE4(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE4.class, message, EP, eventSetup);
        var bold_c_k = messageContent.get_bold_c();
        var pi_prime_k = messageContent.get_pi_prime();

        // get sender's participant index
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_C.set(k, bold_c_k);
        eventData.bold_pi_prime.set(k, pi_prime_k);

        try {
            // perform task
            T14.run(k, eventData, params);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E13.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E14.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        if (eventMessages.hasAllOtherMessages(eventSetup, MessageType.MEE4, this.party.getId())) {
            try {
                // perform task
                T15.run(eventData, params);

                // select event data
                int s = eventData.s.get();
                var bold_c = eventData.bold_c.get();
                var bold_e_tilde_s = eventData.bold_E_tilde.get(s);
                var EP = eventData.EP.get();
                var U = EP.get_U();

                // send MEA1 message to administrator
                this.party.sendMessage(new MEA1(bold_c, bold_e_tilde_s), EP, eventSetup);

                // send MEX2 message to coordinator
                this.party.sendMessage(new MEX2(), U, eventSetup);

                // update state
                context.setCurrentState(S9.class);

            } catch (AlgorithmException exception) {
                // move on to error state
                context.setCurrentState(E15.class);
            }
        }
    }

}
