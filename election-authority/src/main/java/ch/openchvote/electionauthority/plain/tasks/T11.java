/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.plain.tasks;

import ch.openchvote.algorithms.plain.CheckShuffleProof;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.electionauthority.plain.EventData;
import ch.openchvote.parameters.Parameters;

public class T11 {

    public static void run(int k, EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();
        var pi_tilde_k = eventData.bold_pi_tilde.get(k);
        var bold_e_tilde_k_minus_1 = eventData.bold_E_tilde.get(k - 1);
        var bold_e_tilde_k = eventData.bold_E_tilde.get(k);
        var pk = eventData.pk.get();

        // perform task
        var U = EP.get_U();
        if (!CheckShuffleProof.run(U, pi_tilde_k, bold_e_tilde_k_minus_1, bold_e_tilde_k, pk, params)) {
            throw new TaskException(T11.class, TaskException.Type.INVALID_ZKP_PROOF);
        }
    }

}
