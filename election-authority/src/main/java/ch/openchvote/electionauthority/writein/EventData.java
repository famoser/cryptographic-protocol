/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein;

import ch.openchvote.electionauthority.writein.states.S1;
import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.model.common.Confirmation;
import ch.openchvote.model.common.CredentialProof;
import ch.openchvote.model.common.Finalization;
import ch.openchvote.model.common.VotingCardData;
import ch.openchvote.model.writein.*;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.util.ByteArray;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.SearchableList;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;
import ch.openchvote.util.tuples.Pair;

import java.math.BigInteger;

public class EventData extends ch.openchvote.framework.context.EventData {

    // election parameters
    public final Value<Integer> j = new Value<>();
    public final Value<Integer> s = new Value<>();
    public final Value<ElectionParameters> EP = new Value<>();

    // polynomials
    public final Value<Vector<ByteArray>> bold_a_j = new Value<>();
    public final Value<Vector<VotingCardData>> bold_d_j = new Value<>();
    public final Value<Matrix<Pair<BigInteger, BigInteger>>> bold_P_j = new Value<>();

    // public credentials
    public final ValueMap<Vector<BigInteger>, Matrix<BigInteger>> bold_X_hat = new ValueMap<>(Matrix::fromSafeColMap);
    public final ValueMap<Vector<BigInteger>, Matrix<BigInteger>> bold_Y_hat = new ValueMap<>(Matrix::fromSafeColMap);
    public final ValueMap<Vector<BigInteger>, Matrix<BigInteger>> bold_Z_hat = new ValueMap<>(Matrix::fromSafeColMap);
    public final ValueMap<CredentialProof, Vector<CredentialProof>> bold_pi_hat = new ValueMap<>(Vector::fromSafeMap);
    public final Value<Vector<BigInteger>> bold_x_hat = new Value<>();
    public final Value<Vector<BigInteger>> bold_y_hat = new Value<>();
    public final Value<Vector<BigInteger>> bold_z_hat = new Value<>();

    // private and public keys
    public final Value<BigInteger> sk_j = new Value<>();
    public final Value<Vector<BigInteger>> bold_sk_prime_j = new Value<>();
    public final ValueMap<QuadraticResidue, Vector<QuadraticResidue>> bold_pk = new ValueMap<>(Vector::fromSafeMap);
    public final ValueMap<Vector<QuadraticResidue>, Matrix<QuadraticResidue>> bold_PK_prime = new ValueMap<>(Matrix::fromSafeColMap);
    public final Value<QuadraticResidue> pk = new Value<>();
    public final Value<Vector<QuadraticResidue>> bold_pk_prime = new Value<>();
    public final ValueMap<KeyPairProof, Vector<KeyPairProof>> bold_pi = new ValueMap<>(Vector::fromSafeMap);

    // ballots, confirmations, finalizations
    public final Value<SearchableList<Ballot>> B_j = new Value<>();
    public final Value<SearchableList<Confirmation>> C_j = new Value<>();
    public final Value<SearchableList<Finalization>> F_j = new Value<>();

    // shuffle
    public final ValueMap<Vector<AugmentedEncryption>, Matrix<AugmentedEncryption>> bold_E_tilde = new ValueMap<>(Matrix::fromSafeColMap);
    public final ValueMap<ShuffleProof, Vector<ShuffleProof>> bold_pi_tilde = new ValueMap<>(Vector::fromSafeMap);

    // decryption
    public final ValueMap<Vector<QuadraticResidue>, Matrix<QuadraticResidue>> bold_C = new ValueMap<>(Matrix::fromSafeColMap);
    public final ValueMap<Matrix<QuadraticResidue>, Vector<Matrix<QuadraticResidue>>> bold_d_arrow = new ValueMap<>(Vector::fromSafeMap);
    public final ValueMap<DecryptionProof, Vector<DecryptionProof>> bold_pi_prime = new ValueMap<>(Vector::fromSafeMap);
    public final Value<Vector<QuadraticResidue>> bold_c = new Value<>();
    public final Value<Matrix<QuadraticResidue>> bold_D = new Value<>();

    @Override
    public Class<S1> getInitialState() {
        return S1.class;
    }

    @Override
    public void init(EventSetup eventSetup, String partyId) {
        int j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, partyId);
        int s = eventSetup.getMaxParticipantIndex(PartyType.ELECTION_AUTHORITY);
        this.j.set(j);
        this.s.set(s);
        this.B_j.set(new SearchableList<>());
        this.C_j.set(new SearchableList<>());
        this.F_j.set(new SearchableList<>());
    }

}
