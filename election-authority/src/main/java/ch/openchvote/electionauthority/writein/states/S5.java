/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.electionauthority.writein.tasks.T10;
import ch.openchvote.electionauthority.writein.tasks.T7;
import ch.openchvote.electionauthority.writein.tasks.T8;
import ch.openchvote.electionauthority.writein.tasks.T9;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.*;

import static ch.openchvote.framework.exceptions.MessageException.Type.INVALID_CONTENT;
import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S5 extends State<ElectionAuthority> {

    public S5(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCE1:
                this.handleMCE1(message, context);
                break;
            case MCE2:
                this.handleMCE2(message, context);
                break;
            case MCE3:
                this.handleMCE3(message, context);
                break;
            case MXE2:
                this.handleMXE2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCE1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // get participant index of active party
        int j = eventData.j.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCE1.class, message, eventSetup);
        var v = messageContent.get_v();

        try {

            // perform task
            var VP_v = T7.run(v, eventData);

            // select event data
            var pk_j = eventData.bold_pk.get(j);
            var bold_pk_prime_j = eventData.bold_PK_prime.get(j);
            var pi_j = eventData.bold_pi.get(j);

            // send MEC1 message to voting client
            this.party.sendMessage(message.getSenderId(), new MEC1(pk_j, bold_pk_prime_j, pi_j), VP_v, eventSetup);

        } catch (AlgorithmException exception) {

            // discard message
            throw new MessageException(message, INVALID_CONTENT);
        }
    }

    private void handleMCE2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCE2.class, message, eventSetup);
        var v = messageContent.get_v();
        var alpha = messageContent.get_alpha();

        try {
            // perform task
            var pair = T8.run(v, alpha, eventData, params);
            var VP_v = pair.getFirst();
            var beta_j = pair.getSecond();

            // send MEC2 message to voting client
            this.party.sendMessage(message.getSenderId(), new MEC2(beta_j), VP_v, eventSetup);

        } catch (TaskException | AlgorithmException exception) {
            // discard message
            throw new MessageException(message, INVALID_CONTENT);
        }
    }

    private void handleMCE3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCE3.class, message, eventSetup);
        var v = messageContent.get_v();
        var gamma = messageContent.get_gamma();

        try {
            // perform task
            var pair = T9.run(v, gamma, eventData, params);
            var VP_v = pair.getFirst();
            var delta_j = pair.getSecond();

            // send MEC3 message to voting client
            this.party.sendMessage(message.getSenderId(), new MEC3(delta_j), VP_v, eventSetup);

        } catch (TaskException | AlgorithmException exception) {
            // discard message
            throw new MessageException(message, INVALID_CONTENT);
        }
    }

    private void handleMXE2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();
        var U = EP.get_U();

        // check and get message content
        this.party.checkAndGetContent(MXE2.class, message, U, eventSetup);

        try {
            // perform task
            T10.run(eventData, params);

            // update state
            context.setCurrentState(S6.class);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move on to error state
            context.setCurrentState(E8.class);
        }
    }

}
