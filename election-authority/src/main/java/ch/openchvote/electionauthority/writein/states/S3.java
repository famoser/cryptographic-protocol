/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.electionauthority.writein.states.E5;
import ch.openchvote.electionauthority.writein.states.E6;
import ch.openchvote.electionauthority.writein.states.E7;
import ch.openchvote.electionauthority.writein.tasks.T5;
import ch.openchvote.electionauthority.writein.tasks.T6;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MEE2;
import ch.openchvote.protocol.message.writein.MEX1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S3 extends State<ElectionAuthority> {

    public S3(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEE2:
                this.handleMEE2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEE2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // get participant index of sender
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE2.class, message, EP, eventSetup);
        var bold_x_hat_k = messageContent.get_bold_x_hat();
        var bold_y_hat_k = messageContent.get_bold_y_hat();
        var bold_z_hat_k = messageContent.get_bold_z_hat();
        var pi_hat_k = messageContent.get_pi_hat();

        // update event data
        eventData.bold_X_hat.set(k, bold_x_hat_k);
        eventData.bold_Y_hat.set(k, bold_y_hat_k);
        eventData.bold_Z_hat.set(k, bold_z_hat_k);
        eventData.bold_pi_hat.set(k, pi_hat_k);

        try {
            // perform task
            T5.run(k, eventData, params);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E5.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E6.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        if (eventMessages.hasAllOtherMessages(eventSetup, MessageType.MEE2, this.party.getId())) {
            try {
                // perform task
                T6.run(eventData, params);

                // update state
                context.setCurrentState(S4.class);

                // select event data
                var EP = eventData.EP.get();
                var U = EP.get_U();

                // send MEX1 message to coordinator
                this.party.sendMessage(new MEX1(), U, eventSetup);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E7.class);
            }
        }
    }

}
