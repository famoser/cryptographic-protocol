/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.tasks;

import ch.openchvote.algorithms.writein.GenShuffle;
import ch.openchvote.algorithms.writein.GenShuffleProof;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.parameters.Parameters;

public class T12 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        int j = eventData.j.get();
        var EP = eventData.EP.get();
        var pk = eventData.pk.get();
        var bold_pk_prime = eventData.bold_pk_prime.get();
        var bold_e_tilde_j_minus_1 = eventData.bold_E_tilde.get(j - 1);

        // perform task
        var U = EP.get_U();
        var quadruple = GenShuffle.run(bold_e_tilde_j_minus_1, pk, bold_pk_prime, params);
        var bold_e_tilde_j = quadruple.getFirst();
        var bold_r_tilde_j = quadruple.getSecond();
        var bold_r_tilde_prime_j = quadruple.getThird();
        var psi_j = quadruple.getFourth();
        var pi_tilde_j = GenShuffleProof.run(U, bold_e_tilde_j_minus_1, bold_e_tilde_j, bold_r_tilde_j, bold_r_tilde_prime_j, psi_j, pk, bold_pk_prime, params);

        // update event data
        eventData.bold_E_tilde.set(j, bold_e_tilde_j);
        eventData.bold_pi_tilde.set(j, pi_tilde_j);
    }

}
