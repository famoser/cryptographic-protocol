/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.algorithms.general.GetPrimes;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.electionauthority.writein.tasks.T1;
import ch.openchvote.electionauthority.writein.tasks.T2;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MAE1;
import ch.openchvote.protocol.message.writein.MEE1;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.math.Math;
import ch.openchvote.util.math.VMGJFassade;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S1 extends State<ElectionAuthority> {

    public S1(Party party) {
        super(party, Type.START);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MAE1:
                this.handleMAE1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMAE1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MAE1.class, message, eventSetup);
        var EP = messageContent.get_EP();
        var pk_0 = messageContent.get_pk();
        var bold_pk_prime_0 = messageContent.get_bold_pk_prime();
        var pi_0 = messageContent.get_pi();

        // update event data
        eventData.EP.set(EP);
        eventData.bold_pk.set(0, pk_0);
        eventData.bold_PK_prime.set(0, bold_pk_prime_0);
        eventData.bold_pi.set(0, pi_0);

        // create precomputation tables (numbers taken from Table 12.5)
        int s = eventData.s.get();
        int N_E = EP.get_bold_d().getLength();
        VMGJFassade.precomputeTable(params.g, (s + 5) * N_E);
        VMGJFassade.precomputeTable(params.h, 2 * N_E);
        Parallel.forEachLoop(GetPrimes.run(Math.intSum(EP.get_bold_n()), params), p_i -> VMGJFassade.precomputeTable(p_i, N_E));
        VMGJFassade.precomputeTable(params.g_hat, params.p_hat, params.q_hat, (3 * s + 12) * N_E);

        try {
            // perform task
            T1.run(0, eventData, params);

            try {
                // perform task
                T2.run(eventData, params);

                // select event data
                int j = eventData.j.get();
                var pk_j = eventData.bold_pk.get(j);
                var bold_pk_prime_j = eventData.bold_PK_prime.get(j);
                var pi_j = eventData.bold_pi.get(j);

                // send MEE1 to election authorities
                this.party.sendMessage(new MEE1(pk_j, bold_pk_prime_j, pi_j), EP, eventSetup);

                // update state
                context.setCurrentState(S2.class);

                // send internal message
                this.party.sendInternalMessage(eventSetup);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E3.class);
            }
        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E1.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        }
    }

}
