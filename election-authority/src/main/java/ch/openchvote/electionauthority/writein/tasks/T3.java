/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.tasks;

import ch.openchvote.algorithms.writein.GetPublicKeys;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.parameters.Parameters;

public class T3 {

    public static void run(EventData eventData, Parameters params) {

        // perform task
        var bold_pk = eventData.bold_pk.get();
        var bold_PK_prime = eventData.bold_PK_prime.get();
        var pair = GetPublicKeys.run(bold_pk, bold_PK_prime, params);
        var pk = pair.getFirst();
        var bold_pk_prime = pair.getSecond();

        // update event data
        eventData.pk.set(pk);
        eventData.bold_pk_prime.set(bold_pk_prime);
    }

}
