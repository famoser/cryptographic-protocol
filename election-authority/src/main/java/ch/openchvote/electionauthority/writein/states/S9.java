/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.electionauthority.writein.tasks.T16;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MCE4;
import ch.openchvote.protocol.message.writein.MEC4;
import ch.openchvote.protocol.message.writein.MXE3;

import static ch.openchvote.framework.exceptions.MessageException.Type.INVALID_CONTENT;
import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S9 extends State<ElectionAuthority> {

    public S9(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCE4:
                this.handleMCE4(message, context);
                break;
            case MXE3:
                this.handleMXE3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCE4(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCE4.class, message, eventSetup);
        var v = messageContent.get_v();

        try {
            // perform task
            var I_j = T16.run(v, eventData, params);

            // send MEC4 message to voting client
            this.party.sendMessage(message.getSenderId(), new MEC4(I_j), eventSetup);

        } catch (AlgorithmException exception) {
            // discard message
            throw new MessageException(message, INVALID_CONTENT);
        }
    }

    private void handleMXE3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var EP = eventData.EP.get();
        var U = EP.get_U();

        // check and get message content
        this.party.checkAndGetContent(MXE3.class, message, U, eventSetup);

        // update state
        context.setCurrentState(S10.class);
    }

}
