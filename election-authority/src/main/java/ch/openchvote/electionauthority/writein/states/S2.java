/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.electionauthority.writein.states.E1;
import ch.openchvote.electionauthority.writein.states.E2;
import ch.openchvote.electionauthority.writein.states.E3;
import ch.openchvote.electionauthority.writein.states.E4;
import ch.openchvote.electionauthority.writein.states.E5;
import ch.openchvote.electionauthority.writein.states.E6;
import ch.openchvote.electionauthority.writein.tasks.T1;
import ch.openchvote.electionauthority.writein.tasks.T3;
import ch.openchvote.electionauthority.writein.tasks.T4;
import ch.openchvote.electionauthority.writein.tasks.T5;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MEE1;
import ch.openchvote.protocol.message.writein.MEE2;
import ch.openchvote.protocol.message.writein.MEP1;
import ch.openchvote.util.Parallel;
import ch.openchvote.util.math.VMGJFassade;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S2 extends State<ElectionAuthority> {

    public S2(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEE1:
                this.handleMEE1(message, context);
                break;
            case MEE2:
                this.handleMEE2(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMEE1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // get participant index of sender
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE1.class, message, EP, eventSetup);
        var pk_k = messageContent.get_pk();
        var bold_pk_prime_k = messageContent.get_bold_pk_prime();
        var pi_k = messageContent.get_pi();

        // update event data
        eventData.bold_pk.set(k, pk_k);
        eventData.bold_PK_prime.set(k, bold_pk_prime_k);
        eventData.bold_pi.set(k, pi_k);

        try {
            // perform task
            T1.run(k, eventData, params);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E1.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        }
    }

    private void handleMEE2(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // select event data
        var EP = eventData.EP.get();

        // get participant index of sender
        int k = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEE2.class, message, EP, eventSetup);
        var bold_x_hat_k = messageContent.get_bold_x_hat();
        var bold_y_hat_k = messageContent.get_bold_y_hat();
        var bold_z_hat_k = messageContent.get_bold_z_hat();
        var pi_hat_k = messageContent.get_pi_hat();

        // update event data
        eventData.bold_X_hat.set(k, bold_x_hat_k);
        eventData.bold_Y_hat.set(k, bold_y_hat_k);
        eventData.bold_Z_hat.set(k, bold_z_hat_k);
        eventData.bold_pi_hat.set(k, pi_hat_k);

        try {
            // perform task
            T5.run(k, eventData, params);

        } catch (AlgorithmException exception) {
            // move to error state
            context.setCurrentState(E5.class);
        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E6.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        if (eventMessages.hasAllOtherMessages(eventSetup, MessageType.MEE1, this.party.getId())) {
            try {
                // perform task
                T3.run(eventData, params);

                // select event data
                var pk = eventData.pk.get();
                var bold_pk_prime = eventData.bold_pk_prime.get();
                var EP = eventData.EP.get();
                int N_E = EP.get_bold_d().getLength();

                // create precomputation tables (numbers taken from Table 12.5)
                VMGJFassade.precomputeTable(pk, 3 * N_E);
                Parallel.forEachLoop(bold_pk_prime, pk_prime_i -> VMGJFassade.precomputeTable(pk_prime_i, 2 * N_E));

                try {
                    // perform task
                    T4.run(eventData, params);

                    // select event data
                    int j = eventData.j.get();
                    var bold_d_j = eventData.bold_d_j.get();
                    var bold_x_hat_j = eventData.bold_X_hat.get(j);
                    var bold_y_hat_j = eventData.bold_Y_hat.get(j);
                    var bold_z_hat_j = eventData.bold_Z_hat.get(j);
                    var pi_hat_j = eventData.bold_pi_hat.get(j);

                    // send MEP1 to  printing authority
                    this.party.sendMessage(new MEP1(bold_d_j), EP, eventSetup);

                    // send MEE2 to election authorities
                    this.party.sendMessage(new MEE2(bold_x_hat_j, bold_y_hat_j, bold_z_hat_j, pi_hat_j), EP, eventSetup);

                    // update state
                    context.setCurrentState(S3.class);

                    // send internal message
                    this.party.sendInternalMessage(eventSetup);

                } catch (AlgorithmException exception) {
                    // move to error state
                    context.setCurrentState(E4.class);
                }
            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E3.class);
            }
        }
    }

}
