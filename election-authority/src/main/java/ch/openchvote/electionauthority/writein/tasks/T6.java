/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.electionauthority.writein.tasks;

import ch.openchvote.algorithms.common.GetPublicCredentials;
import ch.openchvote.electionauthority.writein.EventData;
import ch.openchvote.parameters.Parameters;

public class T6 {

    public static void run(EventData eventData, Parameters params) {

        // perform task
        var bold_X_hat = eventData.bold_X_hat.get();
        var bold_Y_hat = eventData.bold_Y_hat.get();
        var bold_Z_hat = eventData.bold_Z_hat.get();
        var triple = GetPublicCredentials.run(bold_X_hat, bold_Y_hat, bold_Z_hat, params);
        var bold_x_hat = triple.get_bold_x_hat();
        var bold_y_hat = triple.get_bold_y_hat();
        var bold_z_hat = triple.get_bold_z_hat();

        // update event data
        eventData.bold_x_hat.set(bold_x_hat);
        eventData.bold_y_hat.set(bold_y_hat);
        eventData.bold_z_hat.set(bold_z_hat);
    }

}
