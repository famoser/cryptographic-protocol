# Services

This module provides simple implementations of several infrastructure  services to be used by the protocol parties:

- <code>SimpleMessagingService</code>: Instances of this class have a common register for the parties using this service. When the service receives a message from a sender, they spawn a new Java thread for delivering the message to the intented receiver. This all happens in the same JVM, i.e. messages are delivered without using a real communication channel.

- <code>SimplePersistenceService</code>: Ths class implements a trivial persistence service for <code>EventContext</code> objects. To avoid synchronization problems between the concurrent threads, the process of modifying an event context is protected with locks. By copying <code>EventContext</code> objects in memory, it is possible to reject unsaved operations in case of failures during the processing of a message. 
  
- <code>SimpleLogger</code>: This class implement a simple logger, which sends messages simply to the standard output. The logs are therefore not persisted and no tools for inspecting the logging history are available. In a real system implementation, the use of more sophisticated logging utilities such as <code>java.util.logging</code> or <code>log4j</code> is recommended.
  
- <code>SimpleKeystore</code>: This class provides a simple keystore for storing and retrieving private keys and corresponding certificates. No particular measures are taken for protecting the private keys. Instances of this class should therefore not be used in a real system implementation.
  
The main intention behind these simple service implementations is to provide exemplary code for such components, which can be used for simulations and for testing purposes in a single JVM. These components are all fully functional and sufficiently efficient, but they do not provide the robusteness and security properties required in a real system implementation. Here is where a real system implementation based on OpenCHVote must depart.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)


## Installation of Maven Artefact
As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````
