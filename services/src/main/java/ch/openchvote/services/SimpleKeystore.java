/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.services;

import ch.openchvote.framework.security.Certificate;
import ch.openchvote.framework.security.KeyGenerator;
import ch.openchvote.framework.services.Keystore;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This is a simple implementation of the {@link Keystore} interface. The private keys and corresponding certificates
 * are stored in corresponding hashmaps. No particular measures are taken for protecting the private keys. Instances of
 * this class should therefore not be used in real election systems.
 */
public class SimpleKeystore implements Keystore {

    // the hashmap for storing certificates
    private final Map<Integer, Certificate> certificateMap = new HashMap<>();

    // the hashmap for storing the private keys
    private final Map<String, String> privateKeyMap = new HashMap<>();

    @Override
    public void addEntry(Keystore.Entry entry) {
        var privateKey = entry.getPrivateKey();
        var certificate = entry.getCertificate();
        this.certificateMap.put(certificate.getSecurityLevel(), certificate);
        this.privateKeyMap.put(certificate.getPublicKey(), privateKey);
    }

    @Override
    public Optional<Certificate> getCertificate(int securityLevel) {
        var certificate = this.certificateMap.get(securityLevel);
        return Optional.ofNullable(certificate);
    }

    @Override
    public Optional<String> getPrivateKey(Certificate certificate) {
        var privateKey = this.privateKeyMap.get(certificate.getPublicKey());
        return Optional.ofNullable(privateKey);
    }

    @Override
    public void generateKeys(String subject, KeyGenerator keyGenerator) {
        for (int securityLevel : keyGenerator.getSecurityLevels()) {
            var entry = keyGenerator.generateKeystoreEntry(subject, securityLevel);
            this.addEntry(entry);
        }
    }

}
