/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.services;

import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.services.PersistenceService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * This is a simple implementation of the {@link PersistenceService} interface. The purpose of this class is to
 * allow parties in tests and simulations to behave like 'real' parties, but without persisting the election data to
 * non-volatile memory. For that, the event contexts are kept in a hashmap with the event ids as keys. Manipulations
 * are conducted on a shallow copy of the event context, which allows the rejection of unsaved operations. Saving the
 * event context then means to override the entry in the hashmap by the modified copy.
 */
public class SimplePersistenceService implements PersistenceService {

    // the hashmap for keeping the contexts of the election event
    private final Map<String, EventContext> contextMap = new HashMap<>();

    // each election event has its own lock
    private final Set<String> locks = new HashSet<>();

    @Override
    public synchronized void lockEvent(String eventId) {
        while (this.locks.contains(eventId)) {
            try {
                this.wait();
            } catch (InterruptedException exception) {
                throw new RuntimeException(exception);
            }
        }
        this.locks.add(eventId);
    }

    @Override
    public EventContext loadContext(String eventId) {
        var context = this.contextMap.get(eventId);
        if (context == null)
            throw new RuntimeException("Context " + eventId + " not found");
        return context.getCopy();
    }

    @Override
    public void saveContext(EventContext contextCopy) {
        var eventId = contextCopy.getEventSetup().getEventId();
        this.contextMap.put(eventId, contextCopy);
    }

    @Override
    public synchronized void unlockEvent(String eventId) {
        if (this.locks.remove(eventId)) {
            this.notifyAll();
        }
    }

}
