/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.services;

import ch.openchvote.framework.services.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * This is a simple implementation of the {@link Logger} interface. In this class, logs are only written to the
 * standard output, i.e. they are not written to a log file or a log data base. The logger's mode of operation
 * determines which logs are actually written to the standard output and which are ignored. Ignored logs are lost.
 */
public class SimpleLogger implements Logger {

    private static final Mode DEFAULT_MODE = Mode.INFO;

    // entries of this hashmap determine which log levels are shown and which are ignored
    private static final Map<Mode, Set<Level>> LEVEL_MAP = new HashMap<>();

    static {
        LEVEL_MAP.put(Mode.NONE, Set.of());
        LEVEL_MAP.put(Mode.OUTPUT, Set.of(Level.OUTPUT));
        LEVEL_MAP.put(Mode.ERROR, Set.of(Level.ERROR, Level.OUTPUT));
        LEVEL_MAP.put(Mode.WARNING, Set.of(Level.WARNING, Level.ERROR, Level.OUTPUT));
        LEVEL_MAP.put(Mode.INFO, Set.of(Level.INFO, Level.WARNING, Level.ERROR, Level.OUTPUT));
        LEVEL_MAP.put(Mode.DEBUG, Set.of(Level.DEBUG, Level.INFO, Level.WARNING, Level.ERROR, Level.OUTPUT));
    }

    // the loggers mode of operation
    private Mode mode;

    /**
     * Constructs a new logger object and initializes its mode of operation to the default value.
     */
    public SimpleLogger() {
        this(DEFAULT_MODE);
    }

    /**
     * Constructs a new logger object and initializes its mode of operation.
     *
     * @param mode The logger's initial mode of operation.
     */
    public SimpleLogger(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void setMode(Mode mode) {
        this.mode = mode;
    }

    @Override
    public void log(Level level, String message) {
        var levels = LEVEL_MAP.get(this.mode);
        var prefix = String.format("%-7s ", "[" + level + "]");
        if (levels.contains(level)) {
            switch (level) {
                case INFO:
                case DEBUG:
                    System.out.println(prefix + message);
                    break;
                case WARNING:
                case ERROR:
                    System.err.println(prefix + message);
                    break;
                default:
            }
        }
    }

}
