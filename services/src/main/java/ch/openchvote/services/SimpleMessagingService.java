/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.services;

import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.services.Configuration;
import ch.openchvote.framework.services.MessagingService;

import java.util.HashMap;
import java.util.Map;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_RECEIVER;

/**
 *  This is a simple implementation of the {@link MessagingService} interface, which allows party created in the same
 *  JVM to exchange messages over common memory. Otherwise, instances of this class behave like a 'real' messaging
 *  service, which transmits the messages over a communication channel. Multiple instances of this class share a
 *  common hashmap of all registered parties. In this way, every party can create its own instance as part of the
 *  {@link Configuration}.
 */
public class SimpleMessagingService implements MessagingService {

    // a static hashmap for storing the parties registered to the messaging service
    private static final Map<String, Party> REGISTERED_PARTIES = new HashMap<>();

    @Override
    public synchronized void registerParty(Party party) {
        REGISTERED_PARTIES.put(party.getId(), party);
    }

    @Override
    public void registerParticipant(String partyId) {
        //not needed for this implementation of the MessagingService interface
    }

    @Override
    public void sendMessage(Message message) {
        var party = REGISTERED_PARTIES.get(message.getReceiverId());
        if (party != null) {
            var thread = new Thread(() -> party.onMessage(message));
            thread.setName(message.getSenderId());
            thread.start();
        } else {
            throw new MessageException(message, UNKNOWN_RECEIVER);
        }
    }

    @Override
    public void sendInternalMessage(String senderId, String eventId) {
        var party = REGISTERED_PARTIES.get(senderId);
        if (party != null) {
            var thread = new Thread(() -> party.onInternalMessage(eventId));
            thread.setName(senderId);
            thread.start();
        } else {
            throw new MessageException(UNKNOWN_RECEIVER);
        }
    }

}