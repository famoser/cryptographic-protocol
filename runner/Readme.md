# Runner 

This module provides a class <code>ElectionRunner</code>, which can be used for simulating election events based on various parameters. By running such a simulation using an election runner, every relevant aspect of the CHVote Protocol is included exactly as defined in [CHVote Protocol Specification](https://eprint.iacr.org/2017/325). To start such a simulation, the election runner creates all necessary protocol parties. In the current release of OpenCHVot, all parties are equipped with simple infrastructure services from the <code>services</code> module. This includes a simple <code>MessagingService</code>, over which the parties can communicate.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## Executing the Election Runner

By following the above installation instructions, all artefact JAR files are created and installed into the local Maven repository. To execute an exemplary election simulation, type:

````shell script
$ mvn exec:exec
-- a lot of output
...
Election-03     Candidate-17        1    0    1    1    0      3  
3-out-of-8      Candidate-18        0    0    0    0    1      1  
                Candidate-19        0    1    1    1    1      4  
                Candidate-20        0    0    0    2    0      2  
                Candidate-21        1    1    0    1    0      3  
                Brooke_Aichele      0    0    0    1    0      1  
                Delma_Haydel        1    0    0    0    0      1  
                Lester_Schillaci    0    0    1    0    0      1  
                Oma_Naval           0    0    0    0    1      1  
                Suzanne_Haydel      0    1    0    0    0      1  
----------------------------------------------------------------
Total           Ballots             2    1    3    2    1      9  
----------------------------------------------------------------

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  ... s
[INFO] Finished at: ...
[INFO] ------------------------------------------------------------------------

````

> **Note 1:** The displayed election results differ from run to run due to random choices made by simulated voters.

> **Note 2:** You can safely ignore the warning:
> 
> `INFO: LibVMGJ is not available, computations will be slower.`
