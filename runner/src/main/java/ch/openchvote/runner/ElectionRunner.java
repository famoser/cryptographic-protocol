/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.runner;

import ch.openchvote.administrator.Administrator;
import ch.openchvote.administrator.ElectionParametersFactory;
import ch.openchvote.coordinator.Coordinator;
import ch.openchvote.electionauthority.ElectionAuthority;
import ch.openchvote.framework.State;
import ch.openchvote.framework.services.Logger;
import ch.openchvote.model.common.ElectionParameters;
import ch.openchvote.model.common.ElectionResult;
import ch.openchvote.model.writein.WriteIn;
import ch.openchvote.printingauthority.PrintingAuthority;
import ch.openchvote.protocol.Protocol;
import ch.openchvote.thepublic.ThePublic;
import ch.openchvote.util.IntVector;
import ch.openchvote.util.RandomMode;
import ch.openchvote.util.Vector;
import ch.openchvote.util.tuples.Triple;
import ch.openchvote.voter.Voter;
import ch.openchvote.voter.VotingStrategy;
import ch.openchvote.votingclient.VotingClient;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Instances of this class represent a set of CHVote parties connected over a common messaging service. This set of
 * parties can be used for simulating election events using different protocols. Various simulation parameters exist
 * for defining the type, size, and behaviour of the simulation.
 */
public class ElectionRunner {

    private static final int DEFAULT_s = 4; // number of election authorities
    private static final int DEFAULT_N_E = 10; // number of voters
    private static final int DEFAULT_t = 3; // number of elections in an election event
    private static final int DEFAULT_n_j = 8; // number of candidates in every election
    private static final int DEFAULT_k_j = 3; // number of selections in every election
    private static final int DEFAULT_w = 5; // number of counting circles

    private static final double DEFAULT_P_CIRCLE = 0.75; // probability of a counting circle to allow votes for an election
    private static final double DEFAULT_P_ELIGIBILITY = 0.95; // probability af a voter being eligible in an election
    private static final double DEFAULT_P_WRITEINS = 0.7; // probability that write-ins are allowed in an election
    private static final double DEFAULT_P_PARTICIPATION = 0.8; // probability of a voter to participate in an election

    private static final RandomMode DEFAULT_RANDOM_MODE = RandomMode.RANDOM; // RANDOM, DETERMINISTIC, MAX, MIN
    private static final Logger.Mode DEFAULT_LOGGER_MODE = Logger.Mode.INFO; // NONE, ERROR, WARNING, INFO, DEBUG

    private static final int DEFAULT_SECURITY_LEVEL = 1; // 0, 1, 2, 3 (see Parameters.SECURITY_LEVELS)

    // the parties involved in the simulation
    private final Coordinator coordinator;
    private final Administrator administrator;
    private final PrintingAuthority printingAuthority;
    private final ArrayList<ElectionAuthority> electionAuthorities;
    private final ArrayList<VotingClient> votingClients;
    private final ArrayList<Voter> voters;
    private final ThePublic thePublic;

    // for keeping track of previous and current election events
    private int numPlainEvents;
    private int numWriteInEvents;
    private final List<String> eventIds;

    /**
     * A simple constructor for creating an election runner with default parameters.
     */
    public ElectionRunner() {
        this(DEFAULT_s, DEFAULT_N_E);
    }

    /**
     * A simple constructor for creating an election runner of a certain size, but otherwise with default parameters.
     *
     * @param s Number of authorities
     * @param N_E Number of voters
     */
    public ElectionRunner(int s, int N_E) {
        this(s, N_E, DEFAULT_t, DEFAULT_n_j, DEFAULT_k_j, DEFAULT_w, DEFAULT_P_CIRCLE, DEFAULT_P_ELIGIBILITY, DEFAULT_P_WRITEINS, DEFAULT_P_PARTICIPATION, DEFAULT_RANDOM_MODE, DEFAULT_LOGGER_MODE);
    }

    /**
     * This constructor offers full flexibility for creating election runners with arbitrary parameters.
     *  @param s Number of authorities
     * @param N_E Number of voters
     * @param t Number of election events
     * @param n_j Number of candidates in an election
     * @param k_j Number of selection in an eleciton
     * @param w Number of counting circles
     * @param p_circle Probability of a counting circle to allow votes for an election
     * @param p_eligibility Probability af a voter being eligible in an election
     * @param p_writeIn Probability that write-ins are allowed in an election
     * @param p_participation Probability of a voter to participate in an election
     * @param randomMode The mode of operation of the random generator
     * @param loggerMode The mode of operation of the logger
     */
    public ElectionRunner(int s, int N_E, int t, int n_j, int k_j, int w, double p_circle, double p_eligibility, double p_writeIn, double p_participation, RandomMode randomMode, Logger.Mode loggerMode) {

        // create coordinator
        this.coordinator = new Coordinator("Coordinator", loggerMode);

        // create administrator
        var electionParameterFactory = new ElectionParametersFactory(N_E, t, n_j, k_j, w, p_circle, p_eligibility, p_writeIn, randomMode);
        this.administrator = new Administrator("Administrator", electionParameterFactory, loggerMode);

        // create printing authority
        this.printingAuthority = new PrintingAuthority("PrintingAuthority", loggerMode);

        // create election authorities
        this.electionAuthorities = new ArrayList<>();
        IntStream.rangeClosed(1, s)
                .mapToObj(i -> "Authority-" + i)
                .map(id -> new ElectionAuthority(id, loggerMode))
                .forEach(this.electionAuthorities::add);

        // create votingClients
        this.votingClients = new ArrayList<>();
        IntStream.rangeClosed(1, N_E)
                .mapToObj(i -> "VotingClient-" + i)
                .map(id -> new VotingClient(id, loggerMode))
                .forEach(this.votingClients::add);

        // create voters
        this.voters = new ArrayList<>();
        IntStream.rangeClosed(1, N_E)
                .mapToObj(i -> "Voter-" + i)
                .map(id -> new Voter(id, new VotingStrategy(p_participation, randomMode), loggerMode))
                .forEach(this.voters::add);

        // create the public
        this.thePublic = new ThePublic("ThePublic", loggerMode);

        // initialize eventId list
        this.eventIds = new LinkedList<>();
        this.numPlainEvents = 0;
        this.numWriteInEvents = 0;
    }

    /**
     * Runs a single election event for the {@link Protocol#PLAIN} protocol and the default security level.
     */
    public void runPlainEvent() {
        this.runPlainEvent(1);
    }

    /**
     * Runs {@code n} election events for the {@link Protocol#PLAIN} protocol and the default security level.
     *
     * @param n The number of election events
     */
    public void runPlainEvent(int n) {
        this.runPlainEvent(n, DEFAULT_SECURITY_LEVEL);
    }

    /**
     * Runs {@code n} election events for the {@link Protocol#PLAIN} protocol and the given security level.
     *
     * @param n The number of election events
     * @param securityLevel The given security level
     */
    public void runPlainEvent(int n, int securityLevel) {

        // setup events
        IntStream.rangeClosed(1, n).forEach(i -> {
            this.numPlainEvents = this.numPlainEvents + 1;
            String eventId = "PlainEvent-" + this.numPlainEvents;
            this.coordinator.initEvent(eventId, Protocol.PLAIN.getId(), securityLevel, this.administrator, this.printingAuthority, this.electionAuthorities, this.votingClients, this.voters, this.thePublic);
            this.eventIds.add(eventId);
        });

        // start events
        eventIds.forEach(coordinator::startEvent);
    }

    /**
     * Runs a single election event for the {@link Protocol#WRITEIN} protocol and the default security level.
     */
    public void runWriteInEvent() {
        this.runWriteInEvent(1);
    }

    /**
     * Runs {@code n} election events for the {@link Protocol#WRITEIN} protocol and the default security level.
     *
     * @param n The number of election events
     */
    public void runWriteInEvent(int n) {
        this.runWriteInEvent(n, DEFAULT_SECURITY_LEVEL);
    }

    /**
     * Runs {@code n} election events for the {@link Protocol#WRITEIN} protocol and the given security level.
     *
     * @param n The number of election events
     * @param securityLevel The given security level
     */
    public void runWriteInEvent(int n, int securityLevel) {

        // setup events
        IntStream.rangeClosed(1, n).forEach(i -> {
            this.numWriteInEvents = this.numWriteInEvents + 1;
            String eventId = "WriteInEvent-" + this.numWriteInEvents;
            coordinator.initEvent(eventId, Protocol.WRITEIN.getId(), securityLevel, this.administrator, this.printingAuthority, this.electionAuthorities, this.votingClients, this.voters, this.thePublic);
            this.eventIds.add(eventId);
        });

        // start events
        eventIds.forEach(coordinator::startEvent);
    }

    /**
     * Return the list of ids of all terminated and running election events.
     *
     * @return The ids of all terminated and running election events
     */
    public List<String> getEventIds(){
        return this.eventIds;
    }

    /**
     * Return whether or not the given event is finished
     * @param eventId The given event id
     * @return The status of the event
     */
    public boolean isFinished(String eventId){

        var stateTypes = this.getCurrentStateTypes(eventId);
        for (State.Type type : stateTypes) {
            if (type != State.Type.FINAL){
                return false;
            }
        }
        return true;

        //return (this.thePublic.getCurrentStateType(eventId) == State.Type.FINAL);
    }

    /**
     * Returns the {@link ElectionParameters} of an election event.
     *
     * @param eventId The given event id
     * @return The election parameters of the election event
     */
    public ElectionParameters getElectionParameters(String eventId){
        return this.administrator.getElectionParameters(eventId);
    }

    /**
     * Returns the {@link ElectionResult} of an election event.
     *
     * @param eventId The given event id
     * @return The result of the election event
     */
    public ElectionResult getElectionResult(String eventId){
        return this.thePublic.getElectionResult(eventId);
    }

    /**
     * Returns the list all votes submitted by the voters.
     *
     * @param eventId The given event id
     * @return The list of all votes
     */
    public List<Triple<Integer, IntVector, Vector<WriteIn>>> getVotes(String eventId){
        return this.voters.stream().filter(voter -> voter.getParticipationBit(eventId) == 1).map(voter -> voter.getVote(eventId)).collect(Collectors.toList());
    }

    /**
     * Return a list of the types of the current states of all participating parties.
     * @param eventId The given event id
     * @return The list of state types
     */
    public List<State.Type> getCurrentStateTypes(String eventId){
        var stateTypes = new ArrayList<State.Type>();
        stateTypes.add(this.coordinator.getCurrentStateType(eventId));
        stateTypes.add(this.administrator.getCurrentStateType(eventId));
        stateTypes.add(this.printingAuthority.getCurrentStateType(eventId));
        this.electionAuthorities.forEach(electionAuthority -> stateTypes.add(electionAuthority.getCurrentStateType(eventId)));
        this.votingClients.forEach(votingClient -> stateTypes.add(votingClient.getCurrentStateType(eventId)));
        this.voters.forEach(voter -> stateTypes.add(voter.getCurrentStateType(eventId)));
        stateTypes.add(this.thePublic.getCurrentStateType(eventId));
        return stateTypes;
    }

}
