/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.runner


import ch.openchvote.framework.services.Logger
import ch.openchvote.model.common.ElectionParameters
import ch.openchvote.model.writein.WriteIn
import ch.openchvote.util.*
import ch.openchvote.util.math.Math
import ch.openchvote.util.tuples.Triple
import spock.lang.Specification
import spock.lang.Unroll

class ElectionRunnerSpec extends Specification {

    @Unroll
    def "runPlainEvent(numEvents, securityLevel)"() {
        given:

        def electionRunner = new ElectionRunner(s, N_E, t, n_j, k_j, w, p_circle, p_eligibility, p_writeIn, p_participation, randomMode, loggerMode)
        electionRunner.runPlainEvent(numEvents, securityLevel)

        for (String eventId : electionRunner.getEventIds()) {

            // wait for event to terminate
            while (!electionRunner.isFinished(eventId)) {
                Thread.sleep(1000)
            }
            // get actual election result (from protocol)
            def ER_actual = electionRunner.getElectionResult(eventId)

            // compute expected election result (from voters)
            def votes = electionRunner.getVotes(eventId)
            def EP = electionRunner.getElectionParameters(eventId)
            def bold_u = get_bold_u(votes)
            def bold_V = get_bold_V(votes, EP)
            def bold_W = get_bold_W(votes, EP)
            def ER_expected = new ch.openchvote.model.plain.ElectionResult(bold_u, bold_V, bold_W)

            // check election results
            assert ER_actual == ER_expected

        }

        where:
        numEvents | securityLevel | s | N_E | t | n_j | k_j | w | p_circle | p_eligibility | p_participation | p_writeIn | randomMode               | loggerMode
        1         | 1             | 1 | 0   | 1 | 2   | 1   | 0 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 0   | 1 | 2   | 1   | 0 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 1 | 1   | 1 | 2   | 1   | 1 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 1   | 1 | 2   | 1   | 1 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 1 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 1 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 2             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 2             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        2         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        2         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
    }

    @Unroll
    def "runWriteInEvent(numEvents, securityLevel)"() {
        given:

        def electionRunner = new ElectionRunner(s, N_E, t, n_j, k_j, w, p_circle, p_eligibility, p_writeIn, p_participation, randomMode, loggerMode)
        electionRunner.runWriteInEvent(numEvents, securityLevel)

        for (String eventId : electionRunner.getEventIds()) {

            // wait for event to terminate
            while (!electionRunner.isFinished(eventId)) {
                Thread.sleep(1000)
            }
            // get actual election result (from protocol)
            def ER_actual = electionRunner.getElectionResult(eventId)

            // compute expected election result (from voters)
            def votes = electionRunner.getVotes(eventId)
            def EP = electionRunner.getElectionParameters(eventId)
            def bold_u = get_bold_u(votes)
            def bold_V = get_bold_V(votes, EP)
            def bold_W = get_bold_W(votes, EP)
            def bold_S = get_bold_S(votes, EP)
            def bold_T = get_bold_T(votes, EP)
            def ER_expected = new ch.openchvote.model.writein.ElectionResult(bold_u, bold_V, bold_W, bold_S, bold_T)

            // check election results
            assert ER_actual == ER_expected

        }

        where:
        numEvents | securityLevel | s | N_E | t | n_j | k_j | w | p_circle | p_eligibility | p_participation | p_writeIn | randomMode               | loggerMode
        1         | 1             | 1 | 0   | 1 | 2   | 1   | 0 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 0   | 1 | 2   | 1   | 0 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 1 | 1   | 1 | 2   | 1   | 1 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 1   | 1 | 2   | 1   | 1 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 1 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 1 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 2   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 1   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        1         | 2             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        1         | 2             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        2         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.RANDOM        | Logger.Mode.NONE
        2         | 1             | 2 | 10  | 2 | 5   | 4   | 2 | 0.75     | 0.8           | 0.8             | 0.5       | RandomMode.DETERMINISTIC | Logger.Mode.NONE
        5         | 1             | 2 | 3   | 5 | 5   | 2   | 1 | 1        | 0.75          | 1               | 0.75      | RandomMode.DETERMINISTIC | Logger.Mode.NONE
    }

    private static def get_bold_u(List<Triple<Integer, IntVector, Vector<WriteIn>>> votes) {
        int N = votes.size()
        def bold_u = new IntVector.Builder(N).fill(1).build()
        return bold_u
    }

    private static def get_bold_V(List<Triple<Integer, IntVector, Vector<WriteIn>>> votes, ElectionParameters EP) {
        def bold_n = EP.get_bold_n()
        int n = Math.intSum(bold_n)
        int N = votes.size()
        def builder_bold_V = new IntMatrix.RowBuilder(N, n)
        for (def vote : votes) {
            def bold_s = vote.getSecond()
            def builder_bold_v_i = new IntVector.Builder(n)
            builder_bold_v_i.fill(0)
            for (int s : bold_s) {
                builder_bold_v_i.setValue(s, 1)
            }
            def bold_v_i = builder_bold_v_i.build()
            builder_bold_V.addRow(bold_v_i)
        }
        def bold_V = builder_bold_V.build()
        return bold_V
    }

    private static def get_bold_W(List<Triple<Integer, IntVector, Vector<WriteIn>>> votes, ElectionParameters EP) {
        def bold_w = EP.get_bold_w()
        int w = Math.intMax(bold_w)
        int N = votes.size()
        def builder_bold_W = new IntMatrix.RowBuilder(N, w)
        for (def vote : votes) {
            int v = vote.getFirst()
            def builder_bold_w_i = new IntVector.Builder(w)
            builder_bold_w_i.fill(0)
            builder_bold_w_i.setValue(bold_w.getValue(v), 1)
            def bold_w_i = builder_bold_w_i.build()
            builder_bold_W.addRow(bold_w_i)
        }
        def bold_W = builder_bold_W.build()
        return bold_W
    }

    private static def get_bold_S(List<Triple<Integer, IntVector, Vector<WriteIn>>> votes, ElectionParameters EP) {
        def bold_k = EP.get_bold_k()
        def bold_E = EP.get_bold_E()
        def bold_z = EP.get_bold_z()
        int z = Math.intMax(bold_E.getRows().mapToInt({ bold_e_i -> Math.intSumProd(bold_e_i, bold_k, bold_z) }))
        int N = votes.size()
        def builder_bold_S = new Matrix.RowBuilder<WriteIn>(N, z)
        for (def vote : votes) {
            def bold_s_prime = vote.getThird()
            def builder_bold_s_i = new Vector.Builder<WriteIn>(z)
            builder_bold_s_i.fill(null)
            int i = 1
            for (WriteIn S_i : bold_s_prime) {
                builder_bold_s_i.setValue(i, S_i)
                i++
            }
            def bold_s_i = builder_bold_s_i.build()
            builder_bold_S.addRow(bold_s_i)
        }
        def bold_S = builder_bold_S.build()
        return bold_S
    }

    private static def get_bold_T(List<Triple<Integer, IntVector, Vector<WriteIn>>> votes, ElectionParameters EP) {
        def bold_k = EP.get_bold_k()
        def bold_E = EP.get_bold_E()
        def bold_z = EP.get_bold_z()
        int z = Math.intMax(bold_E.getRows().mapToInt({ bold_e_i -> Math.intSumProd(bold_e_i, bold_k, bold_z) }))
        int N = votes.size()
        int t = bold_k.getLength()
        def builder_bold_T = new IntMatrix.RowBuilder(N, z)
        for (def vote : votes) {
            int v = vote.getFirst()
            def builder_bold_t_i = new IntVector.Builder(z)
            builder_bold_t_i.fill(0)
            int i = 1
            int k_prime = 0
            for (int j = 1; j <= t; j++) {
                int k_j = bold_k.getValue(j)
                if (bold_z.getValue(j) == 1 && bold_E.getValue(v, j)) {
                    for (int k = k_prime + 1; k <= k_prime + k_j; k++) {
                        builder_bold_t_i.setValue(i, j)
                        i++
                    }
                }
                k_prime = k_prime + k_j
            }
            def bold_t_i = builder_bold_t_i.build()
            builder_bold_T.addRow(bold_t_i)
        }
        def bold_T = builder_bold_T.build()
        return bold_T
    }

}
