# Administrator

This module provides an implementation of the CHVote party *"Administrator"*. In the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the administrator is described as follows:

> The election administrator is responsible for setting up an election event. This includes tasks such as defining the electoral roll, the number of elections, the set of candidates in each election, and the eligibility of each voter in each election [...]. At the end of the election process, the election administrator determines and publishes the final election result.

The intention of this Maven module is to provide production-quality Java code that implements the administrator's role in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by simply changing the module's configuration file <code>config.properties</code>.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/Administrator.svg)

## Messages

#### Incoming
| Protocol | Message | Sender             | Signed | Encrypted | Content |    |
|:--------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| 7.4      | MCA1    | Voting client      | no     | no        | Voter index                  | ![MCA1](img/MCA1.png)|
| 7.9      | MEA1    | Election authority | yes    | no        | Combined partial decryptions | ![MEA1](img/MEA1.png)|
| –        | MXA1    | Coordinator        | yes    | no        | Start pre-election           | – |
| –        | MXA2    | Coordinator        | yes    | no        | Start vote casting           | – |
| –        | MXA3    | Coordinator        | yes    | no        | Start post-election          | – |

#### Outgoing
| Protocol | Message |  Receiver          | Signed | Encrypted | Content | |
|:--------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| 7.1      | MAE1    | Election authority | yes    | no        | Election parameters | ![MAE1](img/MAE1.png)|
| 7.3      | MAP1    | Printing authority | yes    | no        | Election parameters | ![MAP1](img/MAP1.png)|
| 7.4      | MAC1    | Voting client      | yes    | no        | Voting parameters, public key share | ![MAC1](img/MAC1.png)|
| 7.9      | MAT1    | The public         | yes    | no        | Election result     | ![MAT1](img/MAT1.png)|
| –        | MAX1    | Coordinator        | yes    | no        | Pre-election done   | – |

## Tasks

| Protocol | Task | State | Description |
|:--------:|:----:|:-----:|:------------|
| 7.1      | T1   | S1    | ![T1](img/T1.png) |
| 7.4      | T2   | S3    | ![T2](img/T2.png) |
| 7.9      | T3   | S4    | ![T3](img/T3.png) |
| 7.9      | T4   | S4    | ![T4](img/T4.png) |
