/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator;

import ch.openchvote.framework.services.Logger;
import ch.openchvote.model.common.ElectionParameters;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.Protocol;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;
import ch.openchvote.framework.Party;

/**
 * This class implements the 'Administrator' party of the CHVote protocol. It is a direct sub-class of {@link Party} with
 * with a single added field {@link Administrator#electionParametersFactory}. This extension allows the administrator
 * to create new election parameters for simulation election events. The specific role of the administrator in the
 * protocol is implemented in the classes {@link ch.openchvote.administrator.plain.EventData} (plain protocol) and
 * {@link ch.openchvote.administrator.writein.EventData} (write-in protocol) and in corresponding state and task classes.
 */
public class Administrator extends Party {

    // the administrator's election parameters factor
    private final ElectionParametersFactory electionParametersFactory;

    /**
     * Constructs a new instance of this class.
     *
     * @param id   The administrator's party id
     * @param mode The logger's mode of operation
     */
    public Administrator(String id, ElectionParametersFactory electionParametersFactory, Logger.Mode mode) {
        super(id, PartyType.ADMINISTRATOR, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
        this.electionParametersFactory = electionParametersFactory;
    }

    /**
     * Returns the administrator's election parameters factory.
     *
     * @return The administrator's election parameters factory
     */
    public ElectionParametersFactory getElectionParametersFactory() {
        return this.electionParametersFactory;
    }

    /**
     * Returns the election parameters generated for a specific election event. This method is mainly used for testing
     * purposes.
     *
     * @param eventId The election event id
     * @return The election parameters generated for a specific election event
     */
    public ElectionParameters getElectionParameters(String eventId) {
        this.persistenceService.lockEvent(eventId);
        var context = this.persistenceService.loadContext(eventId);
        var eventSetup = context.getEventSetup();
        var protocolId = eventSetup.getProtocolId();
        var protocol = Protocol.valueOf(protocolId);
        switch (protocol) {
            case PLAIN: {
                var eventData = (ch.openchvote.administrator.plain.EventData) context.getEventData();
                this.persistenceService.unlockEvent(eventId);
                return eventData.EP.get();
            }
            case WRITEIN: {
                var eventData = (ch.openchvote.administrator.writein.EventData) context.getEventData();
                this.persistenceService.unlockEvent(eventId);
                return eventData.EP.get();
            }
            default:
                this.persistenceService.unlockEvent(eventId);
                throw new RuntimeException();
        }
    }

}
