/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.plain.tasks;

import ch.openchvote.administrator.plain.EventData;
import ch.openchvote.algorithms.plain.GenKeyPair;
import ch.openchvote.algorithms.plain.GenKeyPairProof;
import ch.openchvote.parameters.Parameters;

public class T1 {

    public static void run(EventData eventData, Parameters params) {

        // perform task
        var pair = GenKeyPair.run(params);
        var sk_0 = pair.getFirst();
        var pk_0 = pair.getSecond();
        var pi_0 = GenKeyPairProof.run(sk_0, pk_0, params);

        // update event data
        eventData.sk_0.set(sk_0);
        eventData.pk_0.set(pk_0);
        eventData.pi_0.set(pi_0);
    }

}
