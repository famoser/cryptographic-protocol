/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.plain.tasks;

import ch.openchvote.administrator.plain.EventData;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.model.common.Encryption;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;

public class T3 {

    public static void run(Vector<QuadraticResidue> bold_c_j, Vector<Encryption> bold_e_tilde_s_j, EventData eventData) {

        // select event data
        var bold_c = eventData.bold_c.get();
        var bold_e_tilde_s = eventData.bold_e_tilde_s.get();

        // perform task
        if (!bold_c_j.equals(bold_c) || !bold_e_tilde_s_j.equals(bold_e_tilde_s)) {
            throw new TaskException(T3.class, TaskException.Type.VALUES_NOT_EQUAL);
        }
    }

}
