/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.plain.states;

import ch.openchvote.administrator.Administrator;
import ch.openchvote.administrator.plain.EventData;
import ch.openchvote.administrator.plain.tasks.T2;
import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MAC1;
import ch.openchvote.protocol.message.plain.MCA1;
import ch.openchvote.protocol.message.plain.MXA3;

import static ch.openchvote.framework.exceptions.MessageException.Type.INVALID_CONTENT;
import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S3 extends State<Administrator> {

    public S3(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MCA1:
                this.handleMCA1(message, context);
                break;
            case MXA3:
                this.handleMXA3(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMCA1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MCA1.class, message, eventSetup);
        var v = messageContent.get_v();

        try {
            // perform task
            var VP_v = T2.run(v, eventData);
            var pk_0 = eventData.pk_0.get();
            var pi_0 = eventData.pi_0.get();

            // send MAC1 messages to voting client
            this.party.sendMessage(message.getSenderId(), new MAC1(VP_v, pk_0, pi_0), eventSetup);

        } catch (AlgorithmException exception) {
            // discard message
            throw new MessageException(message, INVALID_CONTENT);
        }
    }

    private void handleMXA3(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var EP = eventData.EP.get();
        var U = EP.get_U();

        // check and get message content
        this.party.checkAndGetContent(MXA3.class, message, U, eventSetup);

        // update state
        context.setCurrentState(S4.class);
    }

}
