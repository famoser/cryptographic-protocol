/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.plain.tasks;

import ch.openchvote.administrator.plain.EventData;
import ch.openchvote.algorithms.plain.GetElectionResult;
import ch.openchvote.algorithms.plain.GenDecryptionProof;
import ch.openchvote.algorithms.plain.GetVotes;
import ch.openchvote.algorithms.plain.GetDecryptions;
import ch.openchvote.parameters.Parameters;

public class T4 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();
        var sk_0 = eventData.sk_0.get();
        var pk_0 = eventData.pk_0.get();
        var bold_c = eventData.bold_c.get();
        var bold_e_tilde_s = eventData.bold_e_tilde_s.get();

        // perform task
        var bold_c_0 = GetDecryptions.run(bold_e_tilde_s, sk_0, params);
        var pi_prime_0 = GenDecryptionProof.run(sk_0, pk_0, bold_e_tilde_s, bold_c_0, params);
        var bold_m = GetVotes.run(bold_e_tilde_s, bold_c, bold_c_0, params);
        var bold_n = EP.get_bold_n();
        var bold_w = EP.get_bold_w();
        var ER = GetElectionResult.run(bold_m, bold_n, bold_w, params);

        // update event data
        eventData.bold_c_0.set(bold_c_0);
        eventData.pi_prime_0.set(pi_prime_0);
        eventData.ER.set(ER);
    }

}
