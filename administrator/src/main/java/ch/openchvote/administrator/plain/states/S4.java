/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.plain.states;

import ch.openchvote.administrator.Administrator;
import ch.openchvote.administrator.plain.EventData;
import ch.openchvote.administrator.plain.tasks.T3;
import ch.openchvote.administrator.plain.tasks.T4;
import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.framework.exceptions.TaskException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.plain.MAT1;
import ch.openchvote.protocol.message.plain.MEA1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S4 extends State<Administrator> {

    public S4(Party party) {
        super(party, Type.REGULAR);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MEA1:
                this.handleMEA1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }


    private void handleMEA1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var EP = eventData.EP.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEA1.class, message, EP, eventSetup);
        var bold_c_j = messageContent.get_bold_c();
        var bold_e_tilde_s_j = messageContent.get_bold_e_tilde_s();

        // update event data
        eventData.bold_c.setIfAbsent(bold_c_j);
        eventData.bold_e_tilde_s.setIfAbsent(bold_e_tilde_s_j);

        try {
            // perform task
            T3.run(bold_c_j, bold_e_tilde_s_j, eventData);

            // send internal message
            this.party.sendInternalMessage(eventSetup);

        } catch (TaskException exception) {
            // move to error state
            context.setCurrentState(E2.class);
        }
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        if (eventMessages.hasAllMessages(eventSetup, MessageType.MEA1)) {
            try {
                // perform task
                T4.run(eventData, params);

                // select event data
                var ER = eventData.ER.get();

                // send MAT1 to the public
                this.party.sendMessage(new MAT1(ER), eventSetup);

                // update state
                context.setCurrentState(S5.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E3.class);
            }
        }
    }

}
