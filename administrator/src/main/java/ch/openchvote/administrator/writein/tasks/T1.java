/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.writein.tasks;

import ch.openchvote.administrator.writein.EventData;
import ch.openchvote.algorithms.writein.GenKeyPairs;
import ch.openchvote.algorithms.writein.GenKeyPairProof;
import ch.openchvote.parameters.Parameters;

public class T1 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();

        // perform task
        var bold_k = EP.get_bold_k();
        var bold_E = EP.get_bold_E();
        var bold_z = EP.get_bold_z();
        var quadruple = GenKeyPairs.run(bold_k, bold_E, bold_z, params);
        var sk_0 = quadruple.getFirst();
        var pk_0 = quadruple.getSecond();
        var bold_sk_prime_0 = quadruple.getThird();
        var bold_pk_prime_0 = quadruple.getFourth();
        var pi_0 = GenKeyPairProof.run(sk_0, pk_0, bold_sk_prime_0, bold_pk_prime_0, params);

        // update event data
        eventData.sk_0.set(sk_0);
        eventData.pk_0.set(pk_0);
        eventData.bold_sk_prime_0.set(bold_sk_prime_0);
        eventData.bold_pk_prime_0.set(bold_pk_prime_0);
        eventData.pi_0.set(pi_0);
    }

}
