/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.writein.states;

import ch.openchvote.administrator.Administrator;
import ch.openchvote.administrator.writein.EventData;
import ch.openchvote.administrator.writein.tasks.T1;
import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MAE1;
import ch.openchvote.protocol.message.writein.MAP1;
import ch.openchvote.protocol.message.writein.MAX1;
import ch.openchvote.protocol.message.writein.MXA1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S1 extends State<Administrator> {

    public S1(Party party) {
        super(party, Type.START);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MXA1:
                this.handleMXA1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMXA1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MXA1.class, message, eventSetup);
        var U = messageContent.get_U();

        // define election parameters
        var EP = this.party.getElectionParametersFactory().getWriteInInstance(U);
        eventData.EP.set(EP);

        try {
            // perform task
            T1.run(eventData, params);

            // select event data
            var pk_0 = eventData.pk_0.get();
            var bold_pk_prime_0 = eventData.bold_pk_prime_0.get();
            var pi_0 = eventData.pi_0.get();

            // send MAE1 messages to election authorities
            this.party.sendMessage(new MAE1(EP, pk_0, bold_pk_prime_0, pi_0), eventSetup);

            // send MAP1 to printing authority
            this.party.sendMessage(new MAP1(EP), eventSetup);

            // send MAX1 message to coordinator
            this.party.sendMessage(new MAX1(), U, eventSetup);

            // update state
            context.setCurrentState(S2.class);

        } catch (AlgorithmException exception) {
            // discard message
            context.setCurrentState(E1.class);
        }
    }

}
