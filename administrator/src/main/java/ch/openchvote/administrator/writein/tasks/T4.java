/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.writein.tasks;

import ch.openchvote.administrator.writein.EventData;
import ch.openchvote.algorithms.writein.GetElectionResult;
import ch.openchvote.algorithms.writein.GenDecryptionProof;
import ch.openchvote.algorithms.writein.GetVotes;
import ch.openchvote.algorithms.writein.GetDecryptions;
import ch.openchvote.parameters.Parameters;

public class T4 {

    public static void run(EventData eventData, Parameters params) {

        // select event data
        var EP = eventData.EP.get();
        var sk_0 = eventData.sk_0.get();
        var pk_0 = eventData.pk_0.get();
        var bold_sk_prime_0 = eventData.bold_sk_prime_0.get();
        var bold_pk_prime_0 = eventData.bold_pk_prime_0.get();
        var bold_c = eventData.bold_c.get();
        var bold_D = eventData.bold_D.get();
        var bold_e_tilde_s = eventData.bold_e_tilde_s.get();

        // perform task
        var pair = GetDecryptions.run(bold_e_tilde_s, sk_0, bold_sk_prime_0, params);
        var bold_c_0 = pair.getFirst();
        var bold_D_0 = pair.getSecond();
        var pi_prime_0 = GenDecryptionProof.run(sk_0, pk_0, bold_sk_prime_0, bold_pk_prime_0, bold_e_tilde_s, bold_c_0, bold_D, params);
        var pair_prime = GetVotes.run(bold_e_tilde_s, bold_c, bold_c_0, bold_D, bold_D_0, params);
        var bold_m = pair_prime.getFirst();
        var bold_M = pair_prime.getSecond();
        var bold_n = EP.get_bold_n();
        var bold_w = EP.get_bold_w();
        var bold_k = EP.get_bold_k();
        var bold_z = EP.get_bold_z();
        var ER = GetElectionResult.run(bold_m, bold_n, bold_w, bold_M, bold_k, bold_z, params);

        // update event data
        eventData.bold_c_0.set(bold_c_0);
        eventData.bold_D_0.set(bold_D_0);
        eventData.pi_prime_0.set(pi_prime_0);
        eventData.ER.set(ER);
    }

}
