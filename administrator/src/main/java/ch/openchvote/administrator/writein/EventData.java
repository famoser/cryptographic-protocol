/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator.writein;

import ch.openchvote.administrator.writein.states.S1;
import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.model.writein.*;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;
import ch.openchvote.util.math.QuadraticResidue;

import java.math.BigInteger;

public class EventData extends ch.openchvote.framework.context.EventData {

    // election parameters
    public final Value<Integer> s = new Value<>();
    public final Value<ElectionParameters> EP = new Value<>();

    // private and public keys
    public final Value<BigInteger> sk_0 = new Value<>();
    public final Value<QuadraticResidue> pk_0 = new Value<>();
    public final Value<Vector<BigInteger>> bold_sk_prime_0 = new Value<>();
    public final Value<Vector<QuadraticResidue>> bold_pk_prime_0 = new Value<>();
    public final Value<KeyPairProof> pi_0 = new Value<>();

    // encrypted votes and partial decryption
    public final Value<Vector<AugmentedEncryption>> bold_e_tilde_s = new Value<>();
    public final Value<Vector<QuadraticResidue>> bold_c = new Value<>();
    public final Value<Vector<QuadraticResidue>> bold_c_0 = new Value<>();
    public final Value<Matrix<QuadraticResidue>> bold_D = new Value<>();
    public final Value<Matrix<QuadraticResidue>> bold_D_0 = new Value<>();
    public final Value<DecryptionProof> pi_prime_0 = new Value<>();

    // election result
    public final Value<ElectionResult> ER = new Value<>();

    @Override
    public Class<S1> getInitialState() {
        return S1.class;
    }

    @Override
    public void init(EventSetup eventSetup, String partyId) {
        int s = eventSetup.getMaxParticipantIndex(PartyType.ELECTION_AUTHORITY);
        this.s.set(s);
    }

}
