/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.administrator;

import ch.openchvote.util.*;

import java.util.Random;

/**
 * This factory class is used by the {@link Administrator} for generating exemplary election parameters. The type and
 * size of the generated election events is determined by various parameters. There are two main methods
 * {@link ElectionParametersFactory#getPlainInstance(String)} and {@link ElectionParametersFactory#getWriteInInstance(String)}
 * for the two existing protocol versions.
 */
public class ElectionParametersFactory {

    private final int N_E;
    private final int t;
    private final int n_j;
    private final int k_j;
    private final int w;
    private final double p_circle;
    private final double p_eligibility;
    private final double p_writeIn;

    // factory's random generator
    private final Random random;

    /**
     * Constructs a new election parameter factory based on various parameters.
     *
     * @param N_E Number of voters
     * @param t Number of election events
     * @param n_j Number of candidates in every election
     * @param k_j Number of selections in every election
     * @param w Number of counting circles
     * @param p_circle Probability of a counting circle to allow votes for an election
     * @param p_eligibility Probability af a voter being eligible in an election
     * @param p_writeIn Probability that write-ins are allowed in an election
     * @param randomMode The random generator's mode of operation
     */
    public ElectionParametersFactory(int N_E, int t, int n_j, int k_j, int w, double p_circle, double p_eligibility, double p_writeIn, RandomMode randomMode) {
        this.N_E = N_E;
        this.t = t;
        this.n_j = n_j;
        this.k_j = k_j;
        this.w = w;
        this.p_circle = p_circle;
        this.p_eligibility = p_eligibility;
        this.p_writeIn = p_writeIn;
        this.random = RandomFactory.getInstance(randomMode);
    }

    /**
     * Creates a new {@link ch.openchvote.model.plain.ElectionParameters} instance based on the factory parameters.
     *
     * @param U The unique election id
     * @return The new election parameters for plain election events
     */
    public ch.openchvote.model.plain.ElectionParameters getPlainInstance(String U) {
        var bold_c = this.get_bold_c();
        var bold_d = this.get_bold_d();
        var bold_e = this.get_bold_e();
        var bold_n = this.get_bold_n();
        var bold_k = this.get_bold_k();
        var bold_E = this.get_bold_E();
        var bold_w = this.get_bold_w();
        var EP = new ch.openchvote.model.plain.ElectionParameters(U, bold_c, bold_d, bold_e, bold_n, bold_k, bold_E, bold_w);
        return EP;
    }

    /**
     * Creates a new {@link ch.openchvote.model.writein.ElectionParameters} instance based on the factory parameters.
     *
     * @param U The unique election id
     * @return The new election parameters for write-in election events
     */
    public ch.openchvote.model.writein.ElectionParameters getWriteInInstance(String U) {
        var bold_c = this.get_bold_c();
        var bold_d = this.get_bold_d();
        var bold_e = this.get_bold_e();
        var bold_n = this.get_bold_n();
        var bold_k = this.get_bold_k();
        var bold_E = this.get_bold_E();
        var bold_w = this.get_bold_w();
        var bold_z = this.get_bold_z();
        var bold_v = this.get_bold_v(bold_z);
        var EP = new ch.openchvote.model.writein.ElectionParameters(U, bold_c, bold_d, bold_e, bold_n, bold_k, bold_E, bold_w, bold_z, bold_v);
        return EP;
    }

    // private helper method for formatting cnadidate, voter, and election descriptions
    private String getFormatString(int x) {
        var digits = (Math.max(("" + x).length(), 2));
        return "%0" + digits + "d";
    }

    // candidate descriptions
    private Vector<String> get_bold_c() {
        int n = this.t * this.n_j;
        var builder_bold_c = new Vector.Builder<String>(n);
        for (int i = 1; i <= n; i++) {
            var c_i = "Candidate-" + String.format(this.getFormatString(n), i);
            builder_bold_c.setValue(i, c_i);
        }
        return builder_bold_c.build();
    }

    // voter descriptions
    private Vector<String> get_bold_d() {
        var builder_bold_d = new Vector.Builder<String>(this.N_E);
        for (int i = 1; i <= this.N_E; i++) {
            var d_i = "Voter-" + String.format(this.getFormatString(this.N_E), i);
            builder_bold_d.setValue(i, d_i);
        }
        return builder_bold_d.build();
    }

    // election descriptions
    private Vector<String> get_bold_e() {
        var builder_bold_e = new Vector.Builder<String>(this.t);
        for (int j = 1; j <= this.t; j++) {
            var e_j = "Election-" + String.format(this.getFormatString(this.t), j);
            builder_bold_e.setValue(j, e_j);
        }
        return builder_bold_e.build();
    }

    // number of candidates
    private IntVector get_bold_n() {
        var builder_bold_n = new IntVector.Builder(this.t);
        builder_bold_n.fill(this.n_j);
        return builder_bold_n.build();
    }

    // number of selections
    private IntVector get_bold_k() {
        var builder_bold_k = new IntVector.Builder(this.t);
        builder_bold_k.fill(this.k_j);
        return builder_bold_k.build();
    }

    // eligibility matrix
    private IntMatrix get_bold_E() {
        var builder_bold_E = new IntMatrix.Builder(this.N_E, this.t);
        for (int i = 1; i <= this.N_E; i++) {
            for (int j = 1; j <= this.t; j++) {
                int e_i_j = (this.random.nextDouble() <= this.p_circle ? 1 : 0) * (this.random.nextDouble() <= this.p_eligibility ? 1 : 0);
                builder_bold_E.setValue(i, j, e_i_j);
            }
        }
        return builder_bold_E.build();
    }

    // counting circles
    private IntVector get_bold_w() {
        var builder_bold_w = new IntVector.Builder(this.N_E);
        for (int i = 1; i <= this.N_E; i++) {
            int w_i = this.random.nextInt(this.w) + 1;
            builder_bold_w.setValue(i, w_i);
        }
        return builder_bold_w.build();
    }

    // write-in elections
    private IntVector get_bold_z() {
        var builder_bold_z = new IntVector.Builder(this.t);
        for (int j = 1; j <= this.t; j++) {
            int z_j = random.nextDouble() <= p_writeIn ? 1 : 0;
            builder_bold_z.setValue(j, z_j);
        }
        return builder_bold_z.build();
    }

    // write-in candidates
    private IntVector get_bold_v(IntVector bold_z) {
        int n = this.t * this.n_j;
        var builder_bold_v = new IntVector.Builder(n);
        int offset = 0;
        for (int j = 1; j <= t; j++) {
            var z_j = bold_z.getValue(j);
            if (z_j == 1) {
                for (int i = 1; i <= this.n_j - this.k_j; i++) {
                    builder_bold_v.setValue(offset + i, 0);
                }
                for (int i = this.n_j - this.k_j + 1; i <= this.n_j; i++) {
                    builder_bold_v.setValue(offset + i, 1);
                }
            } else {
                for (int i = 1; i <= this.n_j; i++) {
                    builder_bold_v.setValue(offset + i, 0);
                }
            }
            offset = offset + this.n_j;
        }
        return builder_bold_v.build();
    }

}
