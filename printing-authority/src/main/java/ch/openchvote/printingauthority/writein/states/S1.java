/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.printingauthority.writein.states;

import ch.openchvote.algorithms.AlgorithmException;
import ch.openchvote.framework.Message;
import ch.openchvote.framework.Party;
import ch.openchvote.framework.State;
import ch.openchvote.framework.context.EventContext;
import ch.openchvote.framework.exceptions.MessageException;
import ch.openchvote.parameters.Parameters;
import ch.openchvote.printingauthority.PrintingAuthority;
import ch.openchvote.printingauthority.writein.EventData;
import ch.openchvote.printingauthority.writein.tasks.T1;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.message.writein.MAP1;
import ch.openchvote.protocol.message.writein.MEP1;
import ch.openchvote.protocol.message.writein.MPV1;

import static ch.openchvote.framework.exceptions.MessageException.Type.UNKNOWN_TYPE;

public class S1 extends State<PrintingAuthority> {

    public S1(Party party) {
        super(party, Type.START);
    }

    @Override
    public void handleMessage(Message message, EventContext context) {
        var messageType = MessageType.valueOf(message.getTypeName());
        switch (messageType) {
            case MAP1:
                this.handleMAP1(message, context);
                break;
            case MEP1:
                this.handleMEP1(message, context);
                break;
            default:
                throw new MessageException(message, UNKNOWN_TYPE);
        }
    }

    private void handleMAP1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MAP1.class, message, eventSetup);
        var EP = messageContent.get_EP();

        // update event data
        eventData.EP.set(EP);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    private void handleMEP1(Message message, EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventData = (EventData) context.getEventData();

        // select event data
        var EP = eventData.EP.get();

        // check and get message content
        var messageContent = this.party.checkAndGetContent(MEP1.class, message, EP, eventSetup);
        var bold_d_j = messageContent.get_bold_d();

        // get sender's participant index
        var j = eventSetup.getParticipantIndex(PartyType.ELECTION_AUTHORITY, message.getSenderId());

        // update event data
        eventData.bold_D.set(j, bold_d_j);

        // send internal message
        this.party.sendInternalMessage(eventSetup);
    }

    @Override
    public void handleInternalMessage(EventContext context) {
        var eventSetup = context.getEventSetup();
        var eventMessages = context.getEventMessages();
        var eventData = (EventData) context.getEventData();
        var params = new Parameters(eventSetup.getSecurityLevel());

        // check if all messages are available
        if (eventMessages.hasAllMessages(eventSetup, MessageType.MAP1) && eventMessages.hasAllMessages(eventSetup, MessageType.MEP1)) {
            try {
                // perform task
                T1.run(eventData, params);

                // select event data
                var N_E = eventData.EP.get().get_bold_d().getLength();
                var bold_vc = eventData.bold_vc.get();

                // send individual MPV1 messages to all voters
                for (int i = 1; i <= N_E; i++) {
                    var VC_i = bold_vc.getValue(i);
                    this.party.sendMessage(i, new MPV1(VC_i), eventSetup);
                }

                // update state
                context.setCurrentState(S2.class);

            } catch (AlgorithmException exception) {
                // move to error state
                context.setCurrentState(E1.class);
            }
        }
    }

}
