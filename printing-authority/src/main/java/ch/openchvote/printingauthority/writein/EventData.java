/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.printingauthority.writein;

import ch.openchvote.framework.context.EventSetup;
import ch.openchvote.model.common.VotingCard;
import ch.openchvote.model.common.VotingCardData;
import ch.openchvote.model.writein.ElectionParameters;
import ch.openchvote.printingauthority.writein.states.S1;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.util.Matrix;
import ch.openchvote.util.Vector;

public class EventData extends ch.openchvote.framework.context.EventData {

    // election parameters
    public final Value<Integer> s = new Value<>();
    public final Value<ElectionParameters> EP = new Value<>();

    // voting cards
    public final ValueMap<Vector<VotingCardData>, Matrix<VotingCardData>> bold_D = new ValueMap<>(Matrix::fromSafeColMap);
    public final Value<Vector<VotingCard>> bold_vc = new Value<>();

    @Override
    public Class<S1> getInitialState() {
        return S1.class;
    }

    @Override
    public void init(EventSetup eventSetup, String partyId) {
        int s = eventSetup.getMaxParticipantIndex(PartyType.ELECTION_AUTHORITY);
        this.s.set(s);
    }

}
