/*
 * Copyright (C) 2020 Berner Fachhochschule https://e-voting.bfh.ch
 *
 *  - This program is free software: you can redistribute it and/or modify                           -
 *  - it under the terms of the GNU Affero General Public License as published by                    -
 *  - the Free Software Foundation, either version 3 of the License, or                              -
 *  - (at your option) any later version.                                                            -
 *  -                                                                                                -
 *  - This program is distributed in the hope that it will be useful,                                -
 *  - but WITHOUT ANY WARRANTY; without even the implied warranty of                                 -
 *  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                                   -
 *  - GNU General Public License for more details.                                                   -
 *  -                                                                                                -
 *  - You should have received a copy of the GNU Affero General Public License                       -
 *  - along with this program. If not, see <http://www.gnu.org/licenses/>.                           -
 */
package ch.openchvote.printingauthority;

import ch.openchvote.framework.services.Logger;
import ch.openchvote.protocol.message.MessageContent;
import ch.openchvote.protocol.message.MessageType;
import ch.openchvote.protocol.PartyType;
import ch.openchvote.protocol.security.HybridEncryptionScheme;
import ch.openchvote.protocol.security.SchnorrKeyGenerator;
import ch.openchvote.protocol.security.SchnorrSignatureScheme;
import ch.openchvote.framework.Party;

/**
 * This class implements the 'Printing Authority' party of the CHVote protocol. It is a direct sub-class of {@link Party}
 * with no particular extensions. The specific role of the printing authority in the protocol is implemented in the classes
 * {@link ch.openchvote.printingauthority.plain.EventData} (plain protocol) and
 * {@link ch.openchvote.printingauthority.writein.EventData} (write-in protocol) and in corresponding state and task classes.
 */
public class PrintingAuthority extends Party {

    /**
     * Constructs a new instance of this class.
     *
     * @param id   The printing authority's party id
     * @param mode The logger's mode of operation
     */
    public PrintingAuthority(String id, Logger.Mode mode) {
        super(id, PartyType.PRINTING_AUTHORITY, new MessageContent.Factory(), new MessageType.Factory(), new SchnorrKeyGenerator(), new SchnorrSignatureScheme(), new HybridEncryptionScheme(), mode);
    }

}
