# Printing Authority

This module provides an implementation of the CHVote party *"Printing Authority"*. In the [CHVote Protocol Specification](https://eprint.iacr.org/2017/325), the role of the printing authority is described as follows:

> The printing authority is responsible for printing the voting cards and delivering them to the voters. They receive the data necessary for generating the voting cards from the election administrator and the election authorities.

The intention of this Maven module is to provide production-quality Java code that implements the printing authority's role in the CHVote protocol. Robust implementations of corresponding services can be connected to this module by simply changing the module's configuration file <code>config.properties</code>.

## Dependencies

The following UML component diagram illustrates the dependencies to other Maven modules withing this project:

![Maven dependency](img/maven-dependency.svg)

## Installation of Maven Artefact

As this module depends on other modules, these must be available in the local Maven repository. The POM in the root of this module takes care of the dependencies. Thus, the easiest way to compile, package, and install the Maven artefacts is to do it from there.

Provided that the required artefacts are available in the local Maven directory, then this module can be built as follows:

````shell script
$ mvn clean install
-- a lot of output
````

## State Diagram

![StateDiagram](img/PrintingAuthority.svg)

## Messages

#### Incoming
| Protocol | Message | Sender             | Signed | Encrypted | Content |    |
:---------:|:-------:|:-------------------|:------:|:---------:|:--------|:---|
| 7.3      | MAP1    | Administrator      | yes    | no        | Election parameters | ![MAP1](img/MAP1.png) |
| 7.3      | MEP1    | Election authority | yes    | yes       | Voting card data    | ![MEP1](img/MEP1.png) |

#### Outgoing
| Protocol | Message | Receiver          | Signed | Encrypted | Content |    |
:---------:|:-------:|:------------------|:------:|:---------:|:--------|:---|
| 7.3      | MPV1    | Voter             | no     | no        | Voting cards | ![MPV1](img/MPV1.png) |


## Tasks

| Protocol | Task   | State | Description |
|:--------:|:------:|:-----:|:------------|
| 7.3      | T1     | S1    | ![T1](img/T1.png) |
