# Release Notes

## Version 1.1 

#### New Features
- Class`ch.openchvote.framework.context.EventData` extended with two local classes `Value` and `ValueMap`. All `EventData` sub-classes greatly simplified.
- Protocol update from *CHVote Protocol Specification – Version 3.2* implemented: 
    - Administrator participates in generating the shared encryption key and the distributed decryption.
    - Order of pre-election phases modified. 
    - Batch ZKP verification algorithms removed.

#### Changes 
- Simplified naming conventions for states and tasks.
- Algorithms `GetElectionIndices` and `GetElectionIndex` replaced by simplified algorithm `GetEligibility`
- Simplified `ElectionResult` classes without redundant fields from `ElectionParameters`.
- `ElectionRunner` test class simplified based on adjustments in `Voter` class.
- Broader range of `ElectionRunner` test cases.
- Sizes of precomputation tables for fixed-base exponentiations adjusted to specification.
- Extended and updated documentation.

#### Bug Fixes
- Precomputation tables for fixed base of type `QuadraticResidue`  initialized with correct `BigInteger` base (sqrt instead of value).
- Manipulations of precomputation tables synchronized. 
- Missing internal messages added to support elections with empty electorate.

#### Notes

This release is compatible with Version 3.2 of the *CHVote Protocol Specification*. 

## Version 1.0

#### Notes

Initial release of this software. Compatible with Version 3.1 of the *CHVote Protocol Specification*. 
